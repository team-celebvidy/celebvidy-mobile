//NOTE THIS FILE WON'T WORK WITH HOT MODULE REPLACEMENT. YOU WILL HAVE TO MANUALLY RELOAD THE PAGE OR NAVIGATE BACK AND FORTH BETWEEN ROUTES.

import React, {Alert, View, Text, TouchableOpacity, Dimensions, Navigator} from 'react-native';

const {width, height} = Dimensions.get('window');

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import SearchInput from './widgets/SearchInput';
import Select from './widgets/ModalSelect';
import NavButton, {BackButton, NextSubmitButton} from './widgets/NavButton';
import {FloatFromTop, FloatFromRightFade} from './widgets/SceneConfigs';

import ORDER_TYPES from './misc/OrderTypes';
import {isOrderEditable} from './misc/orderStatuses';

import styles from './styles/FormStyles';

import Celebrities from './components/Celebrities';
import CelebrityRequests from './components/CelebrityRequests';
import Request from './components/RecordFlow/Request';

import AppIntroTour from './components/LoginFlow/TourFlow/AppIntroTour';
import LoginSignup from './components/LoginFlow/LoginSignup';
import EmailConfirmationCode from './components/LoginFlow/EmailConfirmationCode';
import Password from './components/LoginFlow/Password';
import CustomerLoginSignup from './components/CustomerAccount/CustomerLoginSignup';

import Record from './components/RecordFlow/Record';

import CreateOrder from './components/CheckoutFlow/CreateOrder';
import PaymentInfo from './components/CheckoutFlow/PaymentInfo';
import OptionalTxt from './components/CheckoutFlow/OptionalTxt';
import Confirm from './components/CheckoutFlow/Confirm';

import Order from './components/CustomerOrderFlow/Order';
import EditOrder from './components/CustomerOrderFlow/EditOrder';

import {GiftedFormManager} from 'react-native-gifted-form';
import * as actions from './actions';
import {getState} from './configureStore';
import {connect} from 'react-redux';

//Celebrities Navigator:
export function CelebritiesRoute(props) {
  let search;

  return {
    renderScene() {
      return <Celebrities getSearch={() => search.getWrappedInstance()} />
    },
    renderTitle() {
      return <SearchInput ref={c => search = c} placeholder='search celebrities' />;
    },
    renderBackButton(navigator) {
      return (
        <BackButton backgroundColor='rgba(4,4,4,1)' navigator={navigator} />
      );
    },
    showStatusBar: true,
    showFullPage: false,
    showTabBar: true,
  };
}

export function CelebrityRoute(props) {
  return {
    renderTitle() {
      return null;
    },
    showStatusBar: false,
    showFullPage: false,
    showTabBar: false,
  };
}

export function CreateOrderRoute(props) {
  let component;

  return {
    getTitle() {
      return 'Celebvidy Details';
    },
    renderScene() {
      return <CreateOrder ref={c => component = c} />;
    },
    renderRightButton(navigator) {
      return <NextSubmitButton onPress={() => component.getWrappedInstance().store()} />;
    },
    configureScene() {
      if(props.viewedAfterCheckout && props.editing) return FloatFromTop();
      else return FloatFromRightFade();
    },
    showFullPage: false,
    showTabBar: false,
  };
}

export function PaymentInfoRoute(props) {
  let component;

  return {
    getTitle() {
      return 'Payment Info';
    },
    renderScene() {
      return <PaymentInfo ref={c => component = c} />;
    },
    renderRightButton(navigator) {
      if(props.editingPaymentInfo) {
        return <NextSubmitButton text='SAVE' onPress={() => component.getWrappedInstance().save()} />
      }
      else {
        return <NextSubmitButton onPress={() => component.getWrappedInstance().store()} />;
      }
    },
    configureScene() {
      return FloatFromRightFade();
    },
    showFullPage: false,
    showTabBar: false,
  };
}

export function OptionalTxtRoute(props) {
  let component;

  return {
    getTitle() {
      return 'Optional TXT';
    },
    renderScene() {
      return <OptionalTxt ref={c => component = c} />;
    },
    renderRightButton(navigator) {
      return <NextSubmitButton onPress={() => component.getWrappedInstance().skip()} text='SKIP' />;
    },
    configureScene() {
      return FloatFromRightFade();
    },
    showFullPage: false,
    showTabBar: false,
  };
}

export function ConfirmRoute(props) {
  let component;

  return {
    configureScene() {
      return FloatFromTop();
    },
    renderScene() {
      return <Confirm ref={c => component = c} />;
    },
    renderRightButton(navigator) {
      return (
        <NavButton
          icon='edit'
          onPress={() => navigator.replacePreviousAndPopWithProps('CreateOrder')}
        />
      );
    },
    showFullPage: false,
    showTabBar: false,
  };
}

export function ThankYouRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    showStatusBar: true,
    showFullPage: true,
    showTabBar: true,
  };
}


//CelebrityRequests Navigator:
export function CelebrityRequestsRoute(props) {
  let component;

  return {
    renderScene() {
      return <CelebrityRequests {...props} ref={(c) => component = c}/>
    },
    getTitle() {
      return 'My Celebvidys';
    },
    renderBackButton(navigator) {
      return <BackButton onPress={() => {
          navigator.pop();
          setTimeout(() => component.getWrappedInstance().refresh(), 750); //imperatively update the tab-based listviews based on changes on Request page
        }} />
    },
    renderRightButton() {
      //Note: getState() is used since kind state isn't changed by any thing other than this Select
      //and all we need is it remembered in redux storage for reloads. Or otherwise, i wouldn't be lazy and create a separate component
      //and connect it to Redux.
      return (
        <Select
          title={'Celebvidy Type'}
          pickers={[ORDER_TYPES]}
          selections={[getState().requestsFetchOptions.kind]}
          onSubmitEditing={([kind]) => props.dispatch({type: 'SELECT_KIND', payload: kind})}
          pickerBarStyles={{backgroundColor: 'black'}}
          pickerContainerStyles={{backgroundColor:  "rgba(0,0,0,0.95)" }}
          pickerItemStyles={{fontFamily: 'ProximaNova-Thin'}}
         >
          <MaterialIcon name='line-weight' size={30} color='rgba(27,58,223,1)' style={{marginRight: 12, marginTop: 0}} />
        </Select>
      );
    },
    showFullPage: false,
    showTabBar: true,
  };
}


const RecordButton = connect( ({currentRequest}) => ({currentRequest}) )(function({currentRequest, onPress}) {
  if(currentRequest && currentRequest.status && currentRequest.status.indexOf('rejected') > -1) {
    return <View />;
  }

  return (
    <TouchableOpacity
      activeOpacity={.7}
      onPress={onPress}
      hitSlop={{top: 30, left: 30, bottom: 30, right: 30}}
    >
      <Icon name='ios-circle-filled' style={{fontSize: 27, color: 'rgba(27,58,223,1)', marginRight: 12, marginTop: 2}} />
    </TouchableOpacity>
  );
});

export function RequestRoute(props) {
  let component;

  return {
    getTitle() {
      let recipient = (props.recipient || '').substring(0, 23);
      return recipient.length >= 23 ? recipient+'...' : recipient;
    },
    renderScene() {
      return <Request ref={(c) => component = c}/>
    },
    renderRightButton(navigator) {
      if(/complete|canceled/.test(props.order.phase)) return <View />;
      return <RecordButton onPress={() => component.getWrappedInstance().pushRecordPage()} />
    },
    onWillFocus() {
      delete GiftedFormManager.stores.editRequest; //this must be done so cached values by GiftedForm don't override data from new props when you click a new row; looking for a better solution...
    },
    showFullPage: false,
    showTabBar: false,
    showStatusBar: true,
  };
}


export function CelebrityConciergeRoute(props) {
  return {
    getTitle() {
      return 'Concierge';
    },
    configureScene() {
      return FloatFromRightFade();
    },
  };
}
export function CustomerConciergeRoute(props) {
  return {
    getTitle() {
      return 'Concierge';
    },
    configureScene() {
      return FloatFromRightFade();
    },
  };
}


export function RecordRoute(props) {
  return {
    renderScene() {
      return <Record {...props} />;
    },
    renderRightButton() {
      return null;
    },
    showFullPage: true,
    showTabBar: false,
    showStatusBar: false,
  };
}

export function VideoPageRoute(props) {
  return {
    showFullPage: true,
    showTabBar: false,
    showStatusBar: false,
  };
}



//CustomerOrders Navigator:
export function CustomerOrdersRoute(props) {
  return {
    getTitle() {
      return 'My Celebvidys';
    },
    renderBackButton(navigator) {
      return <BackButton navigator={navigator} />;
    },
    showFullPage: false,
    showTabBar: true,
  };
}

export function OrderRoute(props) {
  let component;

  return {
    getTitle() {
      return 'My Celebvidy';
    },
    renderScene() {
      return <Order ref={c => component = c}/>;
    },
    renderRightButton(navigator) {
      if(!isOrderEditable(props.order.status)) {
        return null;
      }

      return (
        <NavButton
          icon='edit'
          onPress={() => component.getWrappedInstance().pushEditOrder()}
        />
      );
    },
    configureScene() {
      return FloatFromRightFade();
    },
    onWillFocus() {
      delete GiftedFormManager.stores.editOrder; //this must be done so cached values by GiftedForm don't override data from new props when you click a new row; looking for a better solution...
    },
    showFullPage: false,
    showTabBar: getState().isCelebrity ? true : false, //props.isCelebrity ? true : false, //tab bar shouldn't be moving when pages displayed in drawer for celebs
  };
}

export function EditOrderRoute(props) {
  return {
    configureScene() {
      return FloatFromTop();
    },
    getTitle() {
      return 'Change Order';
    },
    showFullPage: false,
    showTabBar: props.isCelebrity ? true : false, //tab bar shouldn't be moving when pages displayed in drawer for celebs
    showStatusBar: true,
  };
}

export function CelebvidyRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    showFullPage: true,
    showTabBar: false, //modal used instead of navigator for celebrity, since page appears in Drawer and video needs full width
    showStatusBar: false,
  };
}


//AppIntroTour Navigator:
export function AppIntroTourRoute(props) {
  return {
    getTitle() {
      return '';
    },
    renderTitle() {
      return null;
    },
    renderBackButton(navigator) {
      return null;
    },
    showFullPage: true,
  };
}

//should be renamed to CelebrityLoginSignupRoute
export function LoginSignupRoute(props) {
  return {
    renderScene() {
      return <LoginSignup />; //I shouldn't have to return this, but for some weird reason LoginSignup doesn't hot module replace on its own
    },
    renderBackButton(navigator) {
      let {celebrity: {twitter_avatar: isTwitterAuth}} = getState();

      if(!isTwitterAuth) {
        return <BackButton navigator={navigator} />;
      }
      else {
        return (
          <NavButton
            backgroundColor='transparent'
            text='FINISH'
            width={130}
            hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
            onPress={() => finishLater(props.toggleTour)} />
        );
      }
    },
    renderTitle() {
      return null;
    },
    configureScene() {
      return FloatFromTop();
    },
    showFullPage: true,
  };
}

//not in AppIntroNavigator, but it's here because Password and ConfirmationCode is shared by both LoginSignup routes
export function CustomerLoginSignupRoute(props) {
  return {
    renderScene() {
      return <CustomerLoginSignup />; //I shouldn't have to return this, but for some weird reason LoginSignup doesn't hot module replace on its own
    },
    getTitle() {
      return 'Login';
    },
    configureScene() {
      return FloatFromRightFade();
    },
  };
}

export function EmailConfirmationCodeRoute(props) {
  let component;

  return {
    getTitle() {
      return 'Confirmation Code';
    },
    renderScene(navigator) {
      return <EmailConfirmationCode ref={(c) => component = c}  />
    },
    renderRightButton(navigator) {
      return <NavButton navigator={navigator} text='NEXT' onPress={() => component.getWrappedInstance().submitCode()} width={props.celebritySidebar && 78} />;
    },
    onWillFocus() {
      //setTimeout(() => component.refs.code.focus(), 500); //modal and other things seems to block autoFocus
    },
    configureScene() {
      return FloatFromRightFade();
    },
    showFullPage: false,
  };
}

//only pushed as a route when logging in with twitter without having entered a pin yet
//otherwise, fyi, CelebrityPin is included in the AppIntroTour route as a component
export function CelebrityPinRoute(props) {
  return {
    renderBackButton(navigator) {
      if(props.returnTo === 'TwitterSignin') {
        return (
          <NavButton
            backgroundColor='transparent'
            text='FINISH'
            width={130}
            hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
            onPress={() => finishLater(props.toggleTour)} />
        );
      }
      else {
        return null;
      }
    },
    configureScene() {
      return FloatFromRightFade();
    },
  };
}
export function PasswordRoute(props) {
  let component;

  return {
    renderScene(navigator) {
      return <Password ref={(c) => component = c} />
    },
    renderBackButton(navigator) {
      if(props.isCheckout) {
        return <BackButton navigator={navigator} />;
      }

      return (
        <NavButton
          backgroundColor='transparent'
          text='FINISH'
          width={130}
          hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
          onPress={() => finishLater(props.toggleTour)} />
      );
    },
    renderRightButton(navigator) {
      let text = props.signingUp ? 'SIGNUP' : 'LOGIN';
      let width = text === 'NEXT' ? 70 : (props.celebritySidebar && 95 || 80);

      if(text === 'SIGNUP') width = 90;
      else if(text === 'LOGIN') width = 85;

      return <NextSubmitButton text={text} onPress={() => component.getWrappedInstance().submitPassword()} width={width} />;
    },
    onWillFocus() {
      //setTimeout(() => component.getWrappedInstance().refs.password.focus(), 500); //modal and other things seemsto block autoFocus
    },
    configureScene() {
      return FloatFromRightFade();
    },
    showFullPage: false,
  };
}


function finishLater(toggleTour) {
  Alert.alert(
    'FINISH LATER',
    "Are you sure you want to finish before completing your profile? You can finish your Celebvidy profile anytime, but you won't be submitted to the directory until you do.",
    [{text: 'CANCEL', onPress: () => null}, {text: 'FINISH', onPress: () => toggleTour(false, 'finishLater')}]
  );
}


//CustomerAccount Navigator:
export function CustomerAccountRoute(props) {
  return {
    renderTitle() {
      return <Text style={styles.pageTitle}>Account </Text>; //this is just so a Space can be added after the word Account so it centers under the time above in the customer panel
    },
    configureScene() {
      return FloatFromRightFade();
    },
    showTabBar: true,
  };
}

export function CustomerBasicInfoRoute(props) {
  return {
    getTitle() {
      return 'Basic Info';
    },
    configureScene() {
      return FloatFromRightFade();
    },
    showTabBar: false,
  };
}

export function CustomerPaymentInfoRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    showTabBar: false,
  };
}

export function ChangePasswordRoute(props) {
  return {
    getTitle() {
      return 'Change Password';
    },
    configureScene() {
      return FloatFromRightFade();
    },
    showTabBar: false,
  };
}

//CelebrityAccountRoute Navigator:
export function CelebrityAccountRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    getTitle() {
      return 'Account';
    }
  };
}

export function CelebrityBasicInfoRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    getTitle() {
      return 'Basic Info';
    }
  };
}
export function CelebrityInventoryRoute(props) {
  return {
    getTitle() {
      return 'Inventory';
    },
    configureScene() {
      return FloatFromRightFade();
    },
  };
}
export function BioRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
  };
}
export function NotificationsRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
  };
}
export function ProfileMediaRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
  };
}
export function PayoutPreferencesRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
  };
}
export function CelebrityTermsRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
  };
}

export function CelebrityPaymentInfoRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    getTitle() {
      return 'Payment Info';
    }
  };
}

export function CelebrityCustomerOrdersRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    getTitle() {
      return 'My Orders';
    },
    showFullPage: false,
    showTabBar: true,
  };
}

export function CelebrityChangePasswordRoute(props) {
  return {
    configureScene() {
      return FloatFromRightFade();
    },
    getTitle() {
      return 'Change Password';
    }
  };
}




export function FaqRoute(props) {
  return {
    getTitle() {
      return 'FAQ';
    },
    renderBackButton(navigator) {
      return <BackButton navigator={navigator} />;
    },
    configureScene() {
      return FloatFromRightFade();
    },
  };
}

export function FaqAnswerRoute(props) {
  return {
    getTitle() {
      return 'Answer';
    },
    renderBackButton(navigator) {
      return <BackButton navigator={navigator} />;
    },
    configureScene() {
      return FloatFromRightFade();
    },
  };
}

/**


export function ConciergeRoute(props) {
  return {

  };
}
export function CelebvidyRoute(props) {
  return {

  };
}

export function ConciergeRoute(props) {
  return {

  };
}
**/
