import {View, Text, TouchableHighlight, StyleSheet, Dimensions, PixelRatio, TouchableOpacity} from 'react-native';

import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import ExNavigator from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import React, { Component } from 'react';
import formStyles from '../styles/FormStyles';

var {width, height} = Dimensions.get('window');


export default class SubmitButton extends Component {
  submit() {
    this.refs.submit._doSubmit();
  }

  render() {
    if(this.props.hasRenderRightSubmitButton) return null;

    return (
      <GiftedForm.SubmitWidget
        ref='submit'
        title={this.props.text || this.props.children || 'SAVE'}
        activityIndicatorColor={'white'}
        formName={this.props.formName}
        displayInlineErrors={true}
        hideErrors={true}
        widgetStyles={{
          textSubmitButton: {
            fontFamily: 'Proxima Nova',
          },
          submitButton: {
            backgroundColor: 'rgba(0,0,0, .6)',
            borderWidth: 1,
            borderColor: 'gray',
            margin: 0,
            marginTop: 20,
            ...this.props.style,
          },
          disabledSubmitButton: {
            backgroundColor: 'rgb(52, 52, 52)',
            borderWidth: 1,
            borderColor: 'rgb(66, 66, 66)',
            borderRightWidth: 2/PixelRatio.get(),
            borderLeftWidth: 2/PixelRatio.get(),
            borderRightColor: 'rgb(58, 58, 58)',
            borderLeftColor: 'rgb(58, 58, 58)',
          },
        }}
        onSubmit={(isValid, values, validationResults, postSubmit = null, modalNavigator = null) => {
          this.props.onSubmit && this.props.onSubmit(isValid, values, validationResults, postSubmit, modalNavigator);
        }}
      />
    );
  }
}
