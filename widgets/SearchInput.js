import { Text, View, TextInput, PixelRatio, Dimensions, TouchableHighlight, Animated, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Gradient from 'react-native-linear-gradient';
import dismissKeyboard from 'react-native-dismiss-keyboard';

import INDUSTRIES from '../misc/IndustryOptions';
import Select from './ModalSelect';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import React, { Component } from 'react';

const { width, height } = Dimensions.get('window');

import {connect} from 'react-redux';

class SearchInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: '',
      focused: false,
      coverVisible: false,
    };

    this.inputCoverRight = new Animated.Value(0);
    this.cancelOpacity = new Animated.Value(0);
  }
  onFocus() {
    this.setState({coverVisible: true});

    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.stagger(100, [
          Animated.timing(this.inputCoverRight, {
            toValue: 35,
            duration: 200,
          }),
          Animated.timing(this.cancelOpacity, {
            toValue: 1,
            duration: 200,
          })
        ]).start(() => this.setState({focused: true}));
      });
    });
  }
  onBlur() {
    this.setState({focused: false});

    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.stagger(100, [
          Animated.timing(this.cancelOpacity, {
            toValue: 0,
            duration: 200,
          }),
          Animated.timing(this.inputCoverRight, {
            toValue: 0,
            duration: 300,
          }),
        ]).start(() => {
          this.setState({coverVisible: false});
          if(!this.state.value) this.submitSearch();
        });
      });
    });
  }
  interpolateCancelOpacity() {
    return this.cancelOpacity.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0]
    });
  }
  onChangeText(celebName) {
    this.setState({value: celebName});
  }

  submitSearch() {
    this.props.dispatch({type: 'SEARCH_CELEBRITY', payload: this.state.value})
  }

  render() {
    return (
      <View style={{marginLeft: 20, marginTop: 4, flexDirection: 'row', width}}>
        <TextInput
          style={[styles.searchInput, {
            borderColor: this.state.focused ? "rgba(44,73,192,1)" : '#414549',
            backgroundColor: this.state.focused ? "rgba(2,25,141,1)" : null,
            color: this.state.focused ? "rgba(172,180,230,1)" : 'rgba(215,215,215,.7)',
            alignSelf: 'flex-start',
           }]}
          width={this.state.focused ? width - 95 : width - 60}
          {...this.props}
          placeholderTextColor={this.state.focused ? "rgba(79,105,243,1)" :  "rgba(53,53,53,1)"}
          selectTextOnFocus={true}
          keyboardAppearance={'dark'}
          autoCapitalize='words'
          clearButtonMode={'while-editing'}
          autoCorrect={false}
          selectionColor={"rgba(255,255,255,0.44)"}
          returnKeyType={'search'}
          onChangeText={this.onChangeText.bind(this)}
          onFocus={this.onFocus.bind(this)}
          onBlur={this.onBlur.bind(this)}
          value={this.state.value}
          selectionColor={'rgba(44,73,192,1)'}
          onSubmitEditing={() => this.submitSearch()}
        />

        {this.state.coverVisible &&
          <Animated.View style={[styles.inputRightCoverContainer, {right: this.inputCoverRight}]} >
            <View style={styles.inputRightCover} />
          </Animated.View>
        }

        <Icon name='android-search' size={20} color={this.state.focused ? 'black' : "rgba(91,90,90,1)"}  style={{position: 'absolute', top: 5, left: 7}} />


        <Select
          title={'Industries'}
          pickers={[INDUSTRIES]}
          selections={[this.props.celebritiesFetchOptions.industry]}
          onSubmitEditing={([industry]) => {
            this.props.dispatch({type: 'SELECT_INDUSTRY', payload: industry});
            this.setState({value: ''})
          }}
          pickerBarStyles={{backgroundColor: 'black'}}
          pickerContainerStyles={{backgroundColor: "rgba(0,0,0,0.92)"}}
          pickerItemStyles={{fontFamily: 'ProximaNova-Thin'}}
          returnKeyType='SELECT'
         >
          <Animated.View style={{marginLeft: 8, opacity: this.interpolateCancelOpacity()}}>
            <MaterialIcon
              ref='industrySelector'
              name='line-weight' size={30} color='rgba(27,58,223,1)'
              ref='industriesIcon'
            />
          </Animated.View>
        </Select>


        {this.state.coverVisible &&
           <Animated.View style={{opacity: this.cancelOpacity, position: 'absolute',
            right: 12, top: -23, width: 83, height: 50, paddingTop: 25, paddingLeft: 4, alignItems: 'center',
          }}>
            <TouchableOpacity onPress={() => dismissKeyboard()} hitSlop={{top: 20, left: 20, bottom: 20, right: 20}}>
              <Text style={{color: 'rgba(27,58,255,.8)', fontFamily: 'STHeitiSC-Light', fontSize: 17}}>cancel</Text>
            </TouchableOpacity>
          </Animated.View>
        }

      </View>
    );
  }
}

export default connect(
  ({celebritiesFetchOptions}) => ({celebritiesFetchOptions}),
  null, null, {withRef: true})
(SearchInput)

var styles = {
  searchInput: {
    height: 30,
    borderColor: '#414549',
    borderWidth: 1/PixelRatio.get(),
    color: 'white',
    width: width -70,
    paddingLeft: 30,
    paddingTop: 2,
    fontFamily: 'Avenir',
    fontSize: 15
  },

  inputRightCoverContainer: {
    position: 'absolute',
    right: 0,
    top: 0,
    width: 60,
    backgroundColor: 'black',
    borderWidth: 0,
  },
  inputRightCover: {
    borderColor: '#414549',
    borderWidth: 0,
    borderLeftWidth: 1/PixelRatio.get(),
    width: 1,
    height: 30,
  }
};
