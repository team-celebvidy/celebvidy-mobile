import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import formStyles from '../styles/FormStyles';
import React, { Component } from 'react';
var {width, height} = Dimensions.get('window');


module.exports = class AccountItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      deliveryMessage: '',
    };

    this.defaultProps = {
      showDisclosure: true
    };
  }

  render() {
    return (
      <TouchableHighlight underlayColor='rgba(11, 54, 128, 0.43)' onPress={this.props.onPress} style={[{}, this.props.style]}>
        <View style={[{marginTop: 8}, this.props.style]}>
          {this.props.overline && <View style={{position: 'relative', top: -8, height: StyleSheet.hairlineWidth, backgroundColor: 'rgba(193,193,193,0.32)'}}/>}

          {this.props.completed && <Icon name='checkmark' style={{color: '#2038D8', position: 'absolute', left: 1, top: 4}} />}

          <Text style={[{
              fontFamily: 'AvenirNext-Regular',
              fontSize: 16,
              color: 'white',
              marginBottom: 8,
              marginLeft: typeof this.props.marginLeft !== 'undefined' ? this.props.marginLeft : 15,
            }, this.props.textStyle]}>
            {this.props.text}
          </Text>

          {this.props.showDisclosure && <Icon name='chevron-right' style={{color: 'gray', position: 'absolute', right: 5, top: 5}} />}

          {this.props.underline && <View style={{height: StyleSheet.hairlineWidth, backgroundColor: 'rgba(193,193,193,0.32)'}}/>}
        </View>
      </TouchableHighlight>
    );
  }
}
