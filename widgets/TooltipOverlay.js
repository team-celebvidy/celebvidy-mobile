import { View, Text, Animated, Dimensions, StyleSheet, TouchableOpacity, LayoutAnimation } from 'react-native';
const {width, height} = Dimensions.get('window');

import {Timer} from 'react-timer-mixin';
import Icon from 'react-native-vector-icons/Ionicons';
import React, { Component } from 'react';

export default class TooltipOverlay extends Timer(Component) {
  constructor(props, context) {
    super(props, context);
    this.sideDelta = new Animated.Value(0);
    this.direction = 1;
  }

  animate() {
    if(this.stopped) return;

    this.direction *= -1;

    Animated.timing(this.sideDelta, {
      toValue: this.direction * 3,
    }).start(() => this.animate());
  }

  start() {
    this.stopped = false;
    this.animate();
  }
  stop() {
    this.stopped = true;
    this.sideDelta.stopAnimation();
  }

  componentDidMount() {
    this.animate(-1);
  }

  componentWillMount() {
    LayoutAnimation.configureNext({...LayoutAnimation.Presets.spring, duration: 1500 || this.props.duration}, this.props.onAppear);
  }

  componentWillReceiveProps(nextProps) {
    if(!this.props.vanishDirection && nextProps.vanishDirection) {
      this.handleVanish(nextProps.vanishDirection);
    }

    LayoutAnimation.configureNext({...LayoutAnimation.Presets.spring, duration: 1500 || this.props.duration}, nextProps.onAppear);
  }

  componentDidUpdate() {
    this.left = this.top = null; //set so props determine position in next render instead of the vanishing position
  }

  handleVanish(direction) {
    switch(direction) {
      case 'top':
        this.top = -height;
        return;
      case 'right':
        this.left = 2*width;
        return;
      case 'bottom':
        this.top = 2*height;
        return;
      case 'left':
      default:
        this.left = -width;
        return;
    }
  }

  getArrowPosition() {
    switch(this.props.side) {
      case 'top':
        return {
          top: -63,
          left: this.getStartMiddleEnd('top', this.props.placement),
        };
      case 'right':
        return {
          top: this.getStartMiddleEnd('right', this.props.placement),
          right: -25,
        };
      case 'bottom':
        return {
          bottom: -71,
          left: this.getStartMiddleEnd('bottom', this.props.placement),
        };
      case 'left':
      default:
        return {
          top: this.getStartMiddleEnd('left', this.props.placement),
          left: -25,
        };
    }
  }

  getStartMiddleEnd(side, placement) {
    side = side || 'left';
    placement = placement || 'middle';

    if(side === 'left' || side === 'right') {
      if(placement === 'start') return -15;
      else if (placement === 'middle') return 175/2 - 80/2;
      else if (placement === 'end') return 175 - 15 - 80;
    }
    else if(side === 'top' || side === 'bottom') {
      if(placement === 'start') return 25;
      else if (placement === 'middle') return 280/2 - 56/2;
      else if (placement === 'end') return 280 - 25 - 35;
    }
  }

  getIconName() {
    switch(this.props.side) {
      case 'top':
        return 'android-arrow-dropup';
      case 'right':
        return 'android-arrow-dropright';
      case 'bottom':
        return 'android-arrow-dropdown';
      case 'left':
      default:
        return 'android-arrow-dropleft';
    }
  }

  getTop() {
    return this.top || this.props.top || height - 290;
  }
  getLeft() {
    return this.left || this.props.left || width/2 - 280/2;
  }

  render() {
    window.box = this;
    let side = this.props.side || 'bottom';

    let top = this.getTop();
    let left = this.getLeft();

    return (
      <View style={styles.container}>

        <Animated.View style={{flex: 1, [side]: this.sideDelta}}>
          <View style={[styles.box, {top, left }]}>
            {!this.props.hideArrow &&
              <View style={[styles.arrowContainer, this.getArrowPosition()]}>
                <Icon name={this.getIconName()} style={styles.arrow} />
              </View>
            }

            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
              <Text style={styles.caption}>
                {this.props.caption || 'You can revisit the sidebar anytime to change your settings.'}
              </Text>
            </View>

            <TouchableOpacity activeOpacity={.5} onPress={this.props.onPress}>
              <View style={styles.button}>
                <Text  style={styles.buttonText}>{this.props.buttonText || 'GOT IT'}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Animated.View>


      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    width,
    height: height - 50,
    backgroundColor: 'transparent',
  },
  box: {
    position: 'absolute',
    backgroundColor: 'rgba(5,5,16, .95)',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgb(35,35,35)',
    width: 280,
    height: 180,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  arrowContainer: {
    position: 'absolute',
  },
  arrow: {
    color: 'rgba(30,30,30, .987)',
    fontSize: 100,
  },
  caption: {
    fontFamily: 'Proxima Nova',
    fontSize: 15,
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: 8,
  },
  button: {
    height: 50,
    backgroundColor: 'rgba(25,25,25, .987)',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: 'rgba(45,45,45, .987)',
    padding: 15,
    margin: 5,
  },
  buttonText: {
    fontFamily: 'Proxima Nova',
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    textShadowOffset: {width: 2*StyleSheet.hairlineWidth, height: -2*StyleSheet.hairlineWidth},
    textShadowColor: 'black',
    textShadowRadius: 1,
  }
});
