import React, { Component } from 'react';
import { View, Text, SwitchIOS, SliderIOS, SwitchAndroid, Platform, PixelRatio, Dimensions, TextInput, TouchableOpacity } from 'react-native';

import {GiftedForm, WidgetMixin, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';
var {width, height} = Dimensions.get('window');


module.exports = React.createClass({
  mixins: [WidgetMixin],

  getDefaultProps() {
    return {
      type: 'TextAreaWidget',
    };
  },

  render() {
    return (
      <View ref='rowContainer' style={this.getStyle('textAreaRow')}>
        <TextInput
          style={this.getStyle('textArea')}
          multiline={true}

          {...this.props}

          onFocus={() => this.props.onFocus(this.refs.rowContainer)}
          onChangeText={this._onChange}
          value={this.state.value}
        />
      </View>
    );
  },

  defaultStyles: {
    textAreaRow: {
      backgroundColor: '#FFF',
      height: 120,
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: '#c8c7cc',
      alignItems: 'center',
      paddingLeft: 10,
      paddingRight: 10,
    },
    textArea: {
      fontSize: 15,
      flex: 1,
    },
  },

});
