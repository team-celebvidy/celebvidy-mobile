import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import React, { Component } from 'react';


export default class NavButton extends Component {
  render() {
    let props = this.props;
    let text = props.text || props.children;
    let isBack = (!text && !props.icon) || props.isBack; //should be phased out to just rely on props.isBack
    let isNext = text === 'NEXT' || text === 'SKIP' || text === 'LOGIN' || text === 'SIGNUP';

    return (
      <TouchableOpacity
        activeOpacity={.5}
        onPress={() => {
          if(props.onPress) props.onPress();
          else if(isBack) props.navigator.pop();
        }}
        style={[{height: 34, overflow: 'hidden', marginTop: 5}, props.style]}
        hitSlop={props.hitSlop || {top: 30, left: 30, bottom: 30, right: 30}}
      >
        {this.props.icon
          ? this.renderIcon(props, props.icon)
          : this.renderLink(props, isBack, isNext, text)
        }
      </TouchableOpacity>
    );
  }
  renderLink(props, isBack, isNext, text) {
    let marginLeft = isBack ? -4 : (isNext ? 5 : 16);
    let {width, backgroundColor, paddingLeft} = props;
    width = width || 70;
    backgroundColor = backgroundColor || 'transparent';
    
    return (
      <View style={{flexDirection: 'row', justifyContent: 'flex-start', width: width, height: 34, marginTop: -10, paddingTop: 11, paddingLeft, backgroundColor}}>
        {isBack && <Icon name='chevron-left' style={{fontSize: 27, color: 'rgba(27,58,223,1)', backgroundColor: 'transparent'}} />}
        <Text style={{marginLeft, color: 'rgba(27,58,223,1)', fontSize: 16, fontFamily: 'Montserrat-Light'}}>{text || 'BACK'}</Text>
        {isNext && <Icon name='chevron-right' style={{marginLeft: -5, fontSize: 27, color: 'rgba(27,58,223,1)', backgroundColor: 'transparent'}} />}
      </View>
    );
  }
  renderIcon(props, icon) {
    return (
      <View style={{backgroundColor: props.backgroundColor || 'transparent'}}>
        <IonIcon name={icon} style={{marginRight: 11, fontSize: 27, color: 'rgba(27,58,223,1)', backgroundColor: 'transparent'}} />
      </View>
    );
  }
}

export function BackButton(props) {
  return <NavButton {...props} isBack={true} width={80} />;
}



export class NextButton extends Component {
  render() {
    return (
      <NavButton
        {...this.props}
        text={this.props.text || this.props.children || 'NEXT'}
        onPress={this.props.onPress}
      />
    );
  }
}

//should be removed at some point. it originally did some automtic form validation and retrieving of values
export class NextSubmitButton extends Component {
  render() {
    return (
      <NavButton
        {...this.props}
        text={this.props.text || this.props.children || 'NEXT'}
        onPress={this.props.onPress}
      />
    );
  }
}
