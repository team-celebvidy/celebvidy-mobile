import React, { Component } from 'react';
import { View, Text, SwitchIOS, SliderIOS, SwitchAndroid, Platform, PixelRatio, Dimensions, TextInput, TouchableOpacity, StyleSheet} from 'react-native';

import {GiftedForm, WidgetMixin, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';
var {width, height} = Dimensions.get('window');


module.exports = React.createClass({
  mixins: [WidgetMixin],

  getDefaultProps() {
    return {
      type: 'TextAreaWidget',
    };
  },

  getInitialState() {
    return {
      value: this.props.value || ''
    }
  },

  characterCount() {
    return this.state.value ? this.state.value.length : 0;
  },
  charsRemaining() {
    return this.props.maxCharacters - (this.state.value.length || 0);
  },

  render() {
    return (
      <View ref='rowContainer'>
        <View style={this.getStyle('textAreaRow')}>
          <TextInput
            ref='input'
            style={this.getStyle('textArea')}
            multiline={typeof this.props.multiline !== 'undefined' ? this.props.multiline : true}

            {...this.props}

            onFocus={() => this.props.onFocus(this.refs.rowContainer)}
            onChangeText={this._onChange}
            value={this.state.value}
          />
        </View>

        {this.props.maxCharacters &&
          <Text style={[this.getStyle('characterCount'), {color: this.charsRemaining() < 0 ? 'red' : 'white'}]}>{this.charsRemaining()} remaining</Text>
        }
      </View>
    );
  },

  defaultStyles: {
    textAreaRow: {
      backgroundColor: '#FFF',
      height: 120,
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: '#c8c7cc',
      alignItems: 'center',
      paddingLeft: 10,
      paddingRight: 10,
    },
    textArea: {
      fontSize: 15,
      flex: 1,
    },
    characterCount: {
      height: 30,
      color: 'white',
      textAlign: 'right',
      marginTop: 7,
      fontFamily: 'ProximaNova-Thin',
      backgroundColor: 'transparent'
    },

  },

});
