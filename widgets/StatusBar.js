import { StatusBar as SB } from 'react-native';
import { connect } from 'react-redux';
import React, { Component } from 'react';


class StatusBar extends Component {
  render() {
    let {showStatusBar, showTour, showDrawer} = this.props;
    let visible = showStatusBar && !showTour && !showDrawer;

    return <SB hidden={!visible} animated={true} showHideTransition='slide' barStyle='light-content' />;
  }
}

export default connect( ({showStatusBar, showTour, showDrawer}) => ({showStatusBar, showTour, showDrawer}) )(StatusBar)
