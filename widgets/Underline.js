import { View } from 'react-native';
import styles from '../styles/common';
import React, { Component } from 'react';

export default function Underline() {
  return <View style={styles.underline} />;
}
