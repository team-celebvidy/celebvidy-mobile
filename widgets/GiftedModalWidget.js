import { View, Text, TouchableHighlight, Navigator, Image, TouchableOpacity, PixelRatio, Dimensions} from 'react-native';
import React, { Component } from 'react';

import {WidgetMixin, GiftedForm, GiftedFormManager, GiftedFormModal} from 'react-native-gifted-form';

import TimerMixin from 'react-timer-mixin';
import moment from 'moment';

import Icon from 'react-native-vector-icons/EvilIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';

import styles from '../styles/FormStyles';

const {width, height} = Dimensions.get('window');

module.exports = React.createClass({
  mixins: [TimerMixin, WidgetMixin],

  getDefaultProps() {
    return {
      type: 'ModalWidget',
      scrollEnabled: true,
      disclosure: true,
      cancelable: false,
      displayValue: '',
    };
  },

  propTypes: {
    type: PropTypes.string,
    scrollEnabled: PropTypes.bool,
    disclosure: PropTypes.bool,
    cancelable: PropTypes.bool,
    displayValue: PropTypes.string,
  },

  getInitialState() {
    return {
      value: this.props.value,
    };
  },

  renderDisclosure() {
    if (this.props.disclosure === true) {
      let style = {};
      if(this.hasValidationError()) style.color = 'red';

      return (
        <Icon name='chevron-down'
          size={34}
          color='rgba(193,193,193,0.82)'
          style={[style, {position: 'absolute', right: 0, top: 7}]}
        />
      );
    }
    return null;
  },

  onPress() {

    // title={this.props.title} // @todo working  ?

    var _self = this;



    var route = {
      renderScene(navigator) {
        // not passing onFocus/onBlur of the current scene to the new scene
        var {onFocus, onBlur, ...others} = _self.props;

        return (
          <GiftedFormModal
            {...others}
            style={styles.giftedForm}
            navigator={navigator}
            isModal={true}
            children={_self._childrenWithProps}
          />
        );
      },
      getTitle() {
        return _self.props.title || '';
      },
      configureScene() {
        var sceneConfig = Navigator.SceneConfigs.FloatFromBottom;
        // disable pop gesture
        sceneConfig.gestures = {};
        return sceneConfig;
      },
      /**
      renderLeftButton(navigator) {
        if (_self.props.cancelable === true) {
          return (
            <TouchableOpacity
              onPress={() => {
                _self.requestAnimationFrame(() => {
                  _self.onClose(null, navigator);
                });
              }}
            >
              <Image
                style={{
                  width: 21,
                  marginLeft: 10,
                  tintColor: '#097881',
                }}
                resizeMode={Image.resizeMode.contain}
                source={require('../icons/close.png')}
              />
            </TouchableOpacity>
          );
        }
        return null;
      },

      renderRightButton(navigator) {
        // @todo other solution than onBack ? maybe do something automatically when scene get focus
        // @todo move button style to themes
        return (
          <TouchableOpacity
            onPress={() => {
              _self.requestAnimationFrame(() => {
                _self.onClose(null, navigator);
              });
            }}
          >
            <IonIcon name='checkmark' color='rgba(27,58,223,1)' size={24} style={{marginRight: 10, marginTop: 3}}/>
          </TouchableOpacity>
        );
      },
      **/
    };

    // console.log('this.props.openModal from modal widget');
    // console.log(typeof this.props.openModal);

    if (this.props.openModal === null) {
      console.warn('GiftedForm: openModal prop is missing in GiftedForm component');
    } else {
      this.props.openModal(route);

      //feature to save previous form, used by CelebvidyDetails at checkout, in case user exits during script editing
      if(this.props.savePrevious) {
        setTimeout(() => {
          this.props.savePrevious();
        }, 1000);
      }
    }
  },

  _prepChildren(value) {
    this._childrenWithProps = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, {
        formStyles: this.props.formStyles,
        openModal: this.props.openModal,
        formName: this.props.formName,
        navigator: this.props.navigator,
        onFocus: this.props.onFocus,
        onBlur: this.props.onBlur,
        onValidation: this.props.onValidation,
        onValueChange: this.props.onValueChange,
        value: value || this.props.value,
        onClose: this.onClose,
      });
    });
  },

  componentWillMount() {
    this._prepChildren();
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.value !== 'undefined' && nextProps.value !== this.props.value) {
      this._prepChildren(nextProps.value);
    }
  },

  componentDidMount() {
    if(this.props.value) {
      GiftedFormManager.updateValue(this.props.formName, this.props.displayValue, this.props.value);
    }

    this.setState({
      value: this._getDisplayableValue(),
    });
  },

  onClose(value, navigator = null) {
    console.log("VALUE", value)
    if (typeof value === 'string') {
      this.setState({
        value: value,
      });
    } else if (this.props.displayValue !== '') {
      this.setState({
        value: this._getDisplayableValue(),
      });
    }

    if (navigator !== null) {
      navigator.pop();
    }
  },

  refreshDisplayableValue() {
    this.setState({
      value: this._getDisplayableValue(),
    });
  },

  _getDisplayableValue() {
    if (this.props.displayValue !== '') {
      if (typeof GiftedFormManager.stores[this.props.formName] !== 'undefined') {
        if (typeof GiftedFormManager.stores[this.props.formName].values !== 'undefined') {
          if (typeof GiftedFormManager.stores[this.props.formName].values[this.props.displayValue] !== 'undefined') {
            if (typeof this.props.transformValue === 'function') {
              return this.props.transformValue(GiftedFormManager.stores[this.props.formName].values[this.props.displayValue]);
            } else if (GiftedFormManager.stores[this.props.formName].values[this.props.displayValue] instanceof Date) {
              return moment(GiftedFormManager.stores[this.props.formName].values[this.props.displayValue]).calendar(null, {
                sameDay: '[Today]',
                nextDay: '[Tomorrow]',
                nextWeek: 'dddd',
                lastDay: '[Yesterday]',
                lastWeek: '[Last] dddd'
              });
            }
            if (typeof GiftedFormManager.stores[this.props.formName].values[this.props.displayValue] === 'string') {
              return GiftedFormManager.stores[this.props.formName].values[this.props.displayValue].trim();
            }
          } else {
            // @todo merge with when not select menu

            // when values[this.props.displayValue] is not found
            // probably because it's a select menu
            // options of select menus are stored using the syntax name{value}, name{value}
            var values = GiftedFormManager.getValues(this.props.formName);
            if (typeof values === 'object') {
              if (typeof values[this.props.displayValue] !== 'undefined') {
                if (typeof this.props.transformValue === 'function') {
                  return this.props.transformValue(values[this.props.displayValue]);
                } else {
                  if (Array.isArray(values[this.props.displayValue])) {
                    // @todo
                    // should return the title and not the value in case of select menu
                    return values[this.props.displayValue].join(', ');
                  } else if (values[this.props.displayValue] instanceof Date) {
                    return moment(values[this.props.displayValue]).calendar(null, {
                      sameDay: '[Today]',
                      nextDay: '[Tomorrow]',
                      nextWeek: 'dddd',
                      lastDay: '[Yesterday]',
                      lastWeek: '[Last] dddd'
                    });
                  } else {
                    return values[this.props.displayValue];
                  }
                }
              }
            }
          }
        }
      }
    }
    return '';
  },


  isValid() {
    var validators = null;
    if (this.props.displayValue) {
      // in case of modal widget
      validators = GiftedFormManager.getValidators(this.props.formName, this.props.displayValue);
    } else {
      validators = GiftedFormManager.getValidators(this.props.formName, this.props.name);
    }

    let toValidate = false;
    if (Array.isArray(validators.validate)) {
      if (validators.validate.length > 0) {
        toValidate = true;
      }
    }

    return this.state.validationErrorMessage === null  && toValidate === true;
  },
  isFilled() {
    return !(typeof this.state.value === 'undefined' || this.state.value === '');
  },
  renderIcon() {
    if(this.props.editable === false || this.props.noIcon) return null;

    if (this.hasValidationError() && this.props.invalidIcon !== null && this.props.type !== 'OptionWidget') {
      return  <IonIcon name={this.props.invalidIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconInvalid')]} />;
    } else if ((this.isValid() && this.isFilled() && this.props.validIcon !== null && this.props.type !== 'OptionWidget') || this.props.isValid) {
      return <IonIcon name={this.props.validIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconValid')]} />;
    } else if (this.props.icon) {
        return <IonIcon name={this.props.icon} style={this.getStyle('rowIcon')} />;
    }
    return null;
  },

  hasValidationError() {
    return this.state.validationErrorMessage !== null && this.state.validationErrorMessage !== '';
  },

  render() {
    return (
      <TouchableHighlight
        onPress={() => {
          if(this.props.editable === false) return;

          this.requestAnimationFrame(() => {
            this.onPress();
          });
        }}
        underlayColor={this.props.editable === false ? null : this.getStyle('underlayColor').pop()}

        {...this.props} // mainly for underlayColor
      >
        <View style={this.getStyle('rowContainer')}>
          <View style={this.getStyle('row')}>
            {this.renderIcon()}
            <Text numberOfLines={1} style={[this.getStyle('modalTitle'), this.props.longTitle && {flex: 0}]}>{this.props.shortTitle || this.props.title}</Text>
            <View style={this.getStyle('alignRight')}>
              <Text numberOfLines={1} style={this.getStyle('modalValue')}>{this.state.value}</Text>
            </View>
            {this.renderDisclosure()}
          </View>

          {this.renderValidationError()}
          {this._renderUnderline()}
        </View>
      </TouchableHighlight>
    );
  },

  renderValidationError() {
    //if (!(typeof this.state.value === 'undefined' || this.state.value === '') && this.state.validationErrorMessage !== null && this.state.validationErrorMessage !== '') {
    if (this.hasValidationError()) {

      return (
        <GiftedForm.ValidationErrorWidget
          {...this.props}
          message={this.state.validationErrorMessage}
        />
      );
    }
    return null;
  },
  _renderUnderline() {
    if (this.props.underlined === true) {
      if (this.hasValidationError()) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineError'])}
          />
        );
      }
      if (this.state.focused === true || this.props.highlighted === true) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineFocused'])}
          />
        );
      }
      return (
        <View
          style={[this.getStyle('underline'), this.getStyle('underlineIdle')]}
        />
      );
    }
    return null;
  },

  defaultStyles: {
    rowImage: {
      height: 20,
      width: 20,
      marginLeft: 10,
    },
    rowContainer: {
      backgroundColor: '#FFF',
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: '#c8c7cc',
    },
    underlayColor: 'rgba(32, 56, 215, 0.17)',
    row: {
      flexDirection: 'row',
      height: 44,
      alignItems: 'center',
    },
    modalTitle: {
      flex: 1,
      fontSize: 15,
      color: '#000',
      paddingLeft: 10,
    },
    alignRight: {
      flex: 1,
      alignItems: 'flex-end',
    },
    modalValue: {
      fontSize: 15,
      color: '#c7c7cc',
      right: 25,
    },

    underline: {
      flex: 1,
      borderBottomWidth: 1/PixelRatio.get(),
    },
    underlineIdle: {
      borderColor: 'rgba(193,193,193,0.32)',
    },
    underlineFocused: {
      borderColor: '#3498db',
    },
    underlineError: {
      borderColor: 'red',
    },
  },
});
