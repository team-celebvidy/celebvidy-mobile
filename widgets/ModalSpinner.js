import { View, Text,  StyleSheet, Dimensions, PixelRatio, Modal, Animated, Easing } from 'react-native';
import React, { Component } from 'react';
const {width, height} = Dimensions.get('window');

export default class ModalSpinner extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {visible: false};

    this.opacity = new Animated.Value(0);
    this.rotateZ = new Animated.Value(0);
  }

  show() {
    this.setState({visible: true});
    this.rotateZ.setValue(0);

    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.parallel([
           Animated.timing(this.opacity, {
             toValue: 1,
             easing: Easing.in(Easing.quad),
             duration: 300
           }),
           Animated.timing(this.rotateZ, {
             toValue: 100,
             easing: Easing.linear,
             duration: 2 * 60 * 1000 //animate for 2 months; shouldn't ever be used longer than that
           }),
         ]).start();
      });
    });
  }

  hide() {
    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.timing(this.opacity, {
           toValue: 0,
           easing: Easing.out(Easing.quad),
           duration: 400
         }).start(() => {
           this.setState({visible: false});
         });
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.visible &&  nextProps.visible != this.props.visible) this.show();
    if(!nextProps.visible &&  nextProps.visible != this.props.visible) this.hide();
  }

  render() {
    let rotateZ = this.rotateZ.interpolate({
      inputRange: [0, 100],
      outputRange: ['0deg', '40000deg'],
    });

    return (
      <Modal
        animated={false}
        transparent={true}
        style={{position: 'absolute'}}
        visible={this.state.visible}
        //onShow={this.onShow.bind(this)}
        //onDismiss={this.onDismiss.bind(this)}
      >
        <Animated.View style={[styles.container, {opacity: this.opacity}]}>
          <Animated.Image
            source={require('../images/full_spinner.png')}
            resizeMode=  "contain"
            style={{
              position: 'absolute',
              top: height/2 - 130,
              left: width/2 - 110,
              width: 220,
              height: 220,
              transform: [
                {rotateZ: rotateZ}
              ]
            }}
          />

          {this.props.note && <Text style={styles.note}>{this.props.note}</Text>}
        </Animated.View>
      </Modal>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0, .7)',
    width, height,
  },
  progressStyle: {
    position: 'absolute',
    width,
    height: 224,
    left: width/2 - 224/2,
    top: height/2 - 224/2,
  },
  percentContainer: {
    position: 'absolute',
    width: 232,
    top: 75,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
  },
  percent: {
    color: '#7591af',
    fontSize: 45,
    fontWeight: "100",
    textAlign: 'center',
  },
  note: {
    position: 'absolute',
    bottom: 40,
    width: width - 120,
    left: 60,
    color: '#7591af',
    fontSize: 17,
    fontWeight: "100",
    textAlign: 'center',
    fontFamily: 'ProximaNova-Thin'
  }
});
