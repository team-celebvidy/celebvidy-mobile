import {View, Text, StyleSheet, Dimensions, Animated, PanResponder} from 'react-native';

import {Timer} from 'react-timer-mixin';
import _ from 'underscore';

import {AnimatedCircularProgress} from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/Ionicons';

import formStyles from '../styles/FormStyles';
import React, { Component } from 'react';

var {width, height} = Dimensions.get('window');

export default class CircularTouchSlider extends Timer(Component) {
  constructor(props, context) {
    super(props, context);
    this.state = {
      endPercent: 0,
    };
  }

  getValue() {
    return this.state.endPercent;
  }

  componentDidMount() {
    this.setTimeout(() => {
      this.setState({endPercent: typeof this.props.percent !== 'undefined' ? this.props.percent : 50});
    }, 500);
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderTerminationRequest: (evt, gestureState) => true,

      onPanResponderMove: (evt, {dy}) => {
        let sensitivity = this.props.sensitivity || 4;
        let percentChange = Math.round(-dy / sensitivity);

        let tempPercent = this.state.endPercent + percentChange;
        this.percent = tempPercent > 0 ? Math.min(tempPercent, 100) : 0;
      },
      onPanResponderRelease: (evt, gestureState) => {
        this.setState({endPercent: Math.round(this.percent)});
      },
    });
  }


  formatPercent(percent) {
    percent = this.props.formatPercent ? this.props.formatPercent(percent) : Math.round(percent);
    if(_.isNaN(percent)) return 0; //this should almost never happen, but happens occasionally and I can't figure out where the bad math comes from
    else return percent;
  }

  render() {
    return (
      <Animated.View style={[styles.container, this.props.style]} {...this._panResponder.panHandlers}>
        <AnimatedCircularProgress
          size={this.props.size || 224}
          width={this.props.width || 12}
          fill={this.state.endPercent}
          tintColor="rgba(27,58,223,1)"
          backgroundColor="#212121"
          tension={this.props.tension || 80}
          friction={this.props.friction || 20}
        >
          {
            (percent) => (
              <View style={[styles.percentContainer, this.props.percentContainerStyle, {marginTop: this.props.percentLabel ? -155 : -145}]}>
                <Text style={[styles.percent, this.props.percentStyle]}>
                  { this.formatPercent(percent) }
                </Text>

                {this.props.percentLabel &&
                  <Text style={[styles.percentLabel, this.props.percentStyleLabel]}>
                    {this.props.percentLabel}
                  </Text>
                }
              </View>
            )
          }
        </AnimatedCircularProgress>

        <Icon name='arrow-up-b' size={30} color='rgba(50, 50, 50, 0.7)' style={{position: 'absolute', top: height/2 - 125 - 64 + 20, left: width/2 - 10}} />
        <Icon name='arrow-down-b' size={30} color='rgba(50, 50, 50, 0.7)'  style={{position: 'absolute', top: height/2 - 125 - 64 + 200, left: width/2 - 10}}/>
      </Animated.View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: height/2 - 125 - 64,
  },
  percentContainer: {
    alignSelf: 'center',
    marginTop: -145,
    marginLeft: 12,
    alignItems: 'center',
  },
  percent: {
    color: 'rgb(236, 236, 236)',
    fontSize: 45,
    fontWeight: "100",
    marginTop: -10,
  },
  percentLabel: {
    color: 'rgb(236, 236, 236)',
    fontSize: 24,
    fontWeight: "100",
  },
});
