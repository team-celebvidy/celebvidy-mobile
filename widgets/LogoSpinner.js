import { View, Image, Animated, Dimensions, Easing } from 'react-native';
import React, { Component } from 'react';

const {width, height} = Dimensions.get('window');

export default class LogoSpinner extends Component {

  constructor(props, context) {
		super(props, context);
		this.spinner = new Animated.Value(0);
    this.increments = 0;
	}

	componentDidMount() {
		if(this.props.auto) this.spin();
	}
	componentWillUnmount() {
		this.spinner.stopAnimation();
	}

  spin() {
    if(this.increments !== 0) return; //already spinning
    this.logo.setNativeProps({style: {opacity: 1}});
    this.animate();
  }
  stop() {
    this.logo.setNativeProps({style: {opacity: 0}});
    this.increments = 0;
    this.spinner.stopAnimation();
    this.spinner.setValue(0);
  }

	animate() {
    this.increments += 100;

		Animated.timing(this.spinner, {
      toValue: this.increments,
      duration: 8 * 10000,
      easing: Easing.linear,
    }).start();
	}

  render() {
    var rotate = this.spinner.interpolate({
      inputRange: [0, 10000],
      outputRange: ['0deg', (360*10000)+'deg'],
    });

    return (
      <Animated.Image
        ref={(logo) => this.logo = logo}
        style={[{
          alignSelf: 'center',
          width: 60,
          height: 60,
          marginBottom: 60,
          opacity: 0,
          transform: [
            {rotateZ: rotate},
          ]
        }, this.props.style]}
        source={require('../images/full_spinner.png')}
      />
    );
  }
}
