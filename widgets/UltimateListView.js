import { View, Dimensions, Animated, Text, Easing, InteractionManager, TouchableHighlight, TouchableOpacity, StyleSheet } from 'react-native';

import {Timer} from 'react-timer-mixin';

const {width, height} = Dimensions.get('window');

import _ from 'underscore';

import GiftedListView from 'react-native-gifted-listview';
import LogoSpinner from './LogoSpinner';
import ClassyActionButton from './ClassyActionButton';
import React, { Component } from 'react';

const COLORS = [
  'rgb(230, 230, 230)',
  'rgb(54, 66, 235)',
  'rgb(255, 255, 255)',
  'rgb(12, 21, 144)',
  'rgb(121, 128, 177)',
  'rgb(12, 38, 214)'
];

const TOP_AND_BOTTOM_HEIGHT = 64 + 52 + 42; //navigation bar + bottom tab bar + filter tabs

export default class UltimateListView extends Timer(Component) {
  static defaultProps = {
      DY: 115,
  };

  constructor(props, context) {
    super(props, context);
    this.refreshAnimation = new Animated.Value(0);
  }

  _onFetch(page = 1, callback, options) {
    this.props.onFetch(page, (...args) => {
      this.refs.refreshControl.stopSpinning();
      callback(...args);
    }, options);
  }
  shouldComponentUpdate(nextProps) {
    let {rows, fetchOptions} = nextProps;

    if(rows !== this.props.rows) {
      return true;
    }
    else if(nextProps.fetchOptions !== this.props.fetchOptions) {
      this.refs.refreshControl.spin();
      return true;
    }
    else return false;
  }


  refresh() {
    //funnily enough with the new declarative style gifted/ultimate-listview, the old imperative refresh does not work
    //this.refs.refreshControl.spin();
    //this.listview.refresh();
  }


  renderRefreshControl() {
    return (
      <RefreshControl
        ref='refreshControl'
        DY={this.props.DY}
        refreshAnimation={this.refreshAnimation}
        offset={this.props.offset}
        refreshBackgroundInputRange={this.props.refreshBackgroundInputRange}
        refreshBackgroundColorsOutputRange={this.props.refreshBackgroundColorsOutputRange}
        refreshFlashBackgroundColorsOutputRange={this.props.refreshFlashBackgroundColorsOutputRange}
        noFlashing={this.props.noFlashing}
      />
    );
  }

  render() {
    return (
      <View style={{width: this.props.offset ? width - this.props.offset : width, height: this.props.height || height - TOP_AND_BOTTOM_HEIGHT, backgroundColor: 'transparent'}}>
        {this.renderRefreshControl()}

        <GiftedListView
          ref={(listview) => this.listview = listview}
          {...this.props}

          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: this.refreshAnimation}}}],
            {listener: ({nativeEvent: {contentOffset: {y: dy}}}) => {
              if(dy < -this.props.DY) {
                this.refs.refreshControl.spin(); //should be -150, but at -this.DY iOS jumps to 150
              }
              else if(dy > -1 && dy < 1) { //sometimes the last recorded value is -0.666 or -.333 && it must be lower than 2 because when you do a refreshing action and it scrolls the page to the top, it will trigger stop spinning just as it starts to make it spin
                this.refs.refreshControl.stopSpinning()
              }
            }}
          )}
          onFetch={this._onFetch.bind(this)}
          onRefresh={() => this.refs.refreshControl.stopSpinning()}
          enableEmptySections={true}
          showsVerticalScrollIndicator={false}
          firstLoader={true} // display a loader for the first fetching
          pagination={false} // enable infinite scrolling using touch to load more
          autoPaginate={true}
          refreshable={true} // enable pull-to-refresh for iOS and touch-to-refresh for Android
          withSections={false} // enable sections
          refreshableTintColor='transparent'
          removeClippedSubviews={false}
          refreshableTitle=''
          emptyView={this.renderEmptyView.bind(this)}
          style={[{backgroundColor: 'transparent'}, this.props.style]}
          paginationFetchingView={() => <LogoSpinner auto={true} style={{marginTop: 10, marginBottom: 10}}/>}
          initialListSize={this.props.limit || 5}
          dontConcat={true}
          paginationAllLoadedView={this.paginationAllLoadedView.bind(this)}
          limit={this.props.limit || 5}
        />
      </View>
    );
  }

  renderEmptyView(refreshCallback) {
    let backgroundColor = this.refreshAnimation.interpolate({
      inputRange: [-this.props.DY, 0],
      outputRange: ['rgba(38,110,236,.4)', 'rgba(4, 0, 215, .4)'],
      extrapolate: 'clamp'
    });

    let borderTopColor = this.refreshAnimation.interpolate({
      inputRange: [-this.props.DY, 0],
      outputRange: ['rgba(87, 135, 228, 0.7)', 'rgba(87, 135, 228, .25)'],
      extrapolate: 'clamp'
    });

    let messageOpacity = this.refreshAnimation.interpolate({
      inputRange: [-this.props.DY/3, 0],
      outputRange: [0, 1],
      extrapolate: 'clamp'
    });

    return (
      <TouchableOpacity onPress={() => null} activeOpacity={.8}>
        <Animated.View style={{
            flex: 1,
            height: height - 50 - 64,
            backgroundColor,
            padding: 20,
            borderTopWidth: StyleSheet.hairlineWidth,
            borderTopColor,
        }}>
          <Animated.View style={{
              marginTop: 15,
              marginHorizontal: 0,
              height: 50,
              flexDirection: 'column',
              justifyContent: 'center',
              backgroundColor: 'rgba(20,20,20, .7)',
              opacity: messageOpacity,
              borderWidth: StyleSheet.hairlineWidth,
              borderColor: 'rgba(32,32,32, .7)',
            }}>
            <Text style={{color: 'gray', textAlign: 'center'}}>
              {this.props.emptyMessage}
            </Text>
          </Animated.View>

          {this.props.isCustomer &&
            <View style={{position: 'absolute', bottom: 50, width: width - 40, left: 20}}>
              <ClassyActionButton text='BROWSE CELEBRITIES' onPress={() => this.props.goToPage(2)} style={{borderColor: 'rgb(28,28,28)'}}  />
            </View>
          }
        </Animated.View>
      </TouchableOpacity>
    );
  }

  paginationAllLoadedView(refreshCallback) {
    return _.isEmpty(this.props.rows) ? this.renderEmptyView(refreshCallback) : null;
  }
}



class RefreshControl extends Timer(Component) {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

  constructor(props, context) {
    super(props, context);

    this.refreshAnimation = this.props.refreshAnimation;
    this.flashAnimation = new Animated.Value(0);
    this.DY = this.props.DY;
  }

  spin(flash=true) {
    if(!this.isSpinning) {
      this.handle = InteractionManager.createInteractionHandle();
      this.spinnerParts.setNativeProps({style: {opacity: 0}});
      this.spinner.spin();
      this.startFlashing();

      this.isSpinning = true;
    }
  }
  stopSpinning() {
    if(this.isSpinning === false) return;

    this.spinner.stop();
    this.isSpinning = false;

    this.setTimeout(() => {
      if(this.isSpinning) return;

      this.spinnerParts.setNativeProps({style: {opacity: 1}}); //it's better to set these once the refresher is hidden
      if(this.handle) InteractionManager.clearInteractionHandle(this.handle);
    }, 300); //wait until spinner is hidden again to show the parts
  }
  startFlashing() {
    requestAnimationFrame(() => {
      this.flashAnimation.setValue(0);

      Animated.timing(this.flashAnimation, {
        toValue: 1,
        duration: 750,
        easing: Easing.in(Easing.sin)
      }).start(() => {
        if(this.isSpinning) this.startFlashing();
        else this.flashBackground.setNativeProps({style: {backgroundColor: 'transparent'}});
      });
    });
  }


  render() {
    let horizontal = this.refreshAnimation.interpolate({
      inputRange: [-this.DY, 0],
      outputRange: [width/2 - 20, 0],
      extrapolate: 'clamp'
    });

    let top = this.refreshAnimation.interpolate({
      inputRange: [-this.DY, 0],
      outputRange: [12, -25],
      extrapolate: 'clamp'
    });

    let hideBarOpacity = this.refreshAnimation.interpolate({
      inputRange: [-23, 0],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    });

    let backgroundColor = this.refreshAnimation.interpolate({
      inputRange: this.props.refreshBackgroundInputRange || [-250, -50, 0],
      outputRange: this.props.refreshBackgroundColorsOutputRange || ['rgba(0,0,0,0)', 'rgb(3, 28, 152)', 'rgba(0,0,0,0)'],
      extrapolate: 'clamp',
    });


    let flashBgColor = this.flashAnimation.interpolate({
      inputRange: [0, .5, 1],
      outputRange: this.props.refreshFlashBackgroundColorsOutputRange || ['rgba(0,0,0, .8)',  'rgba(0, 1, 173, .5)', 'rgba(0,0,0, .8)'],
      extrapolate: 'clamp',
    });

    return (
      <Animated.View style={{position: 'absolute', left: this.props.offset ? -this.props.offset/2 : 0, top: 0, height: height - 64,
        backgroundColor: !this.props.noFlashing ? backgroundColor : 'transparent',
        opacity: .9, overflow: 'hidden',
      }}>
          <Animated.View ref={(flashBackground) => this.flashBackground = flashBackground} style={{
              width, height: height - 64,
              backgroundColor: !this.props.noFlashing ? flashBgColor : 'transparent',
          }}>
            <Animated.View ref={(spinnerParts) => this.spinnerParts = spinnerParts}>
              <Animated.Image
                style={{position: 'absolute', top: top, left: horizontal, width: 40, height: 40, opacity: hideBarOpacity}}
                source={require('../images/spinner_circle.png')}
              />

              <Animated.Image
                style={{position: 'absolute', top: top, right: horizontal, width: 40, height: 40, opacity: hideBarOpacity}}
                source={require('../images/spinner_star.png')}
               />
            </Animated.View>

           <LogoSpinner
             ref={(spinner) => this.spinner = spinner}
             style={{position: 'absolute', top: 12, left: width/2 - 20, width: 40, height: 40,}}
           />
         </Animated.View>
      </Animated.View>
    );
  }
}
