import { View, Text, Image, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, Modal, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
const {width, height} = Dimensions.get('window');
import TouchableBounce from 'react-native-touchable-bounce';
var dismissKeyboard = require('react-native-dismiss-keyboard');
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import {Timer} from 'react-timer-mixin';

const numberKeys = [
    [
        {mainText: '1', otherText: ''},
        {mainText: '2', otherText: 'ABC'},
        {mainText: '3', otherText: 'DEF'}
    ],
    [
        {mainText: '4', otherText: 'GHI'},
        {mainText: '5', otherText: 'JKL'},
        {mainText: '6', otherText: 'MNO'}
    ],
    [
        {mainText: '7', otherText: 'PQRS'},
        {mainText: '8', otherText: 'TUV'},
        {mainText: '9', otherText: 'WXYZ'}
    ]
];


export default class NumberPad extends Timer(Component) {

    constructor(props) {
        super(props);
        this.state = {
          modalVisible: false,
          text: this.props.value || '',
        }
    }

    _setModalVisible(visible) {
      visible && this.props.onShow && this.props.onShow();
      !visible && this.props.onHide && this.props.onHide();

      if(visible) dismissKeyboard();
      this.setState({modalVisible: visible});
    }
    focus() {
      this.open();
    }
    open() {
      if(this.props.editable === false) return;

      this._setModalVisible(true);
    }
    close(pressedCloseKey) {
      this._setModalVisible(false);

      pressedCloseKey === true && this.props.onReturnKeyKeyPress && this.props.onReturnKeyKeyPress(this.state.text);
      pressedCloseKey === true && this.props.onSubmitEditing && this.props.onSubmitEditing(this.state.text);
    }

    _clearAll() {
      this.setState({text: ''});
      this.props.onClear && this.props.onClear();
    }

    _onPress(num) {
      let text = this.state.text;

      if (num === '') {
          return;
      } else if (num === 'del') {
        text = text.substr(0, text.length-1);

        this.setState({text: text});
        this.props.onDelete && this.props.onDelete();
      } else if (num === this.returnKey()) {
        this.close(true);
      // number key
      } else {
        let maxLength = this.props.maxLength;

        text = text + num;

        if(maxLength && text.length > maxLength) text = text.substr(0, maxLength);

        this.setState({text: text});
        this.props.onKeyPress && this.props.onKeyPress(num);
      }

      this.props.onChangeText && this.props.onChangeText(text);
    }

    _renderKey(key, index) {
        return (
            <TouchableHighlight
                key={index}
                underlayColor={BG_COLOR}
                style={keyStyle.wrapper}
                onPress={this._onPress.bind(this, key.mainText)}
            >
                <View style={keyStyle.bd}>
                    <Text style={keyStyle.mainText}>{key.mainText}</Text>
                    <Text style={keyStyle.otherText}>{key.otherText}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    _renderNumberKeys() {
        return numberKeys.map((group, groupIndex) => {
            return (
                <View key={groupIndex} style={styles.row}>
                    {group.map(this._renderKey.bind(this))}
                </View>
            );
        });
    }

    _isDecimalPad() {
        return this.props.keyboardType === 'decimal-pad';
    }


    returnKey() {
      let key = this.props.returnKeyType || 'DONE';
      return key.toUpperCase();
    }
    render() {
      let props = this.props;
      let activeOpacity = this.props.editable === false ? 1 : .5;

      return (
        <View style={{flex: 0}}>

          <TouchableOpacity activeOpacity={activeOpacity} onPress={this.open.bind(this)} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}} >
            {this.props.children}
          </TouchableOpacity>

          <Modal
            animated={true}
            transparent={true}
            visible={this.state.modalVisible}
          >
            <TouchableWithoutFeedback onPress={this.close.bind(this)}>
              <View style={{flex: 1}}>
                <View style={styles.wrapper}>
                    <View style={styles.main}>

                        {this._renderNumberKeys()}

                        <View style={styles.row}>
                          <TouchableHighlight
                              underlayColor="black"
                              style={[keyStyle.wrapper, keyStyle.bg]}
                              onPress={this._onPress.bind(this, 'del')}
                              onLongPress={this._clearAll.bind(this)}
                          >
                              <View style={keyStyle.bd}>
                                <Icon name='backspace-outline' size={24} color='rgba(27,58,223,1)' />
                              </View>
                          </TouchableHighlight>

                            <TouchableHighlight
                                underlayColor={BG_COLOR}
                                style={keyStyle.wrapper}
                                onPress={this._onPress.bind(this, '0')}
                            >
                                <View style={keyStyle.bd}>
                                    <Text style={keyStyle.mainText}>0</Text>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                underlayColor="black"
                                style={[keyStyle.wrapper, keyStyle.bg]}
                                onPress={this._onPress.bind(this, this.returnKey())}
                            >
                                <View style={keyStyle.bd}>
                                    <Text style={keyStyle.mainText}>{this.returnKey()}</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      );
    }
}


NumberPad.propTypes = {
    keyboardType :   PropTypes.oneOf(['number-pad', 'decimal-pad']),
    onKeyPress   :   PropTypes.func,
    onDelete     :   PropTypes.func,
    onClear      :   PropTypes.func
};


NumberPad.defaultProps = {
    keyboardType :   'number-pad',
    closeKey     :   'close',
    onKeyPress   :   () => {},
    onDelete     :   () => {},
    onClear      :   () => {}
};




const hairlineWidth = StyleSheet.hairlineWidth;

export const BG_COLOR = '#1c1c1c';


const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        backgroundColor: '#1c1c1c',
        position: 'absolute',
        bottom: 0,
        width,
    },
    main: {
        flex: 1,
        alignSelf: 'flex-end'
    },
    row: {
        flexDirection: 'row'
    }
});


const keyStyle = StyleSheet.create({
    wrapper: {
        flex: 1,
        height: 48,
        backgroundColor: '#0e0e0e'
    },
    bd: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: hairlineWidth,
        borderTopWidth: hairlineWidth,
        borderColor: '#373737'
    },
    mainText: {
        fontSize: 20,
        fontFamily: 'ProximaNova-Thin',
        color: 'rgba(27,58,223,1)'
    },
    otherText: {
        fontSize: 10,
        color: '#bfbfbf',
    },
    bg: {
        backgroundColor: BG_COLOR
    },
    dot: {
        height: 30,
        fontSize: 30,
        lineHeight: 25
    }
});
