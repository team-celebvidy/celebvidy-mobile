import { Dimensions, PixelRatio } from 'react-native';
import React, { Component } from 'react';

import {Navigator} from 'react-native-deprecated-custom-components';
const {width, height} = Dimensions.get('window');
const buildStyleInterpolator = require('buildStyleInterpolator');

var BaseConfig = Navigator.SceneConfigs.FloatFromRight;

export function FloatFromTop() {
  var gesture = Object.assign({}, BaseConfig.gestures.pop, {
    //snapVelocity: 8,   // Make it snap back really quickly after canceling pop
    //edgeHitWidth: SCREEN_WIDTH, // Make it so we can drag anywhere on the screen
    direction: 'bottom-to-top',
  });

  return Object.assign({}, BaseConfig, {
    //springTension: 100, // A very tighly wound spring will make this transition fast
    //springFriction: 1,
    gestures: {
      pop: gesture,
    },
    animationInterpolators: {
      into: buildStyleInterpolator(FromTheTop),
      out: buildStyleInterpolator(ToTheDown),
    }
  });
}


export function FloatFromRightFade() {
  return {
    ...BaseConfig,
    animationInterpolators: {
      into: buildStyleInterpolator(FromTheRight),
      out: buildStyleInterpolator(FadeToTheLeft),
    },
  };
}


export function FadeIn() {
  var gesture = Object.assign({}, BaseConfig.gestures.pop, {
    //snapVelocity: 8,   // Make it snap back really quickly after canceling pop
    //edgeHitWidth: SCREEN_WIDTH, // Make it so we can drag anywhere on the screen
    direction: 'bottom-to-top',
  });

  return Object.assign({}, BaseConfig, {
    //springTension: 100, // A very tighly wound spring will make this transition fast
    //springFriction: 1,
    gestures: null,
    springFriction: 1,
    springTension: 1,
    defaultTransitionVelocity: .1,
    animationInterpolators: {
      into: buildStyleInterpolator(FadeIn),
      out: buildStyleInterpolator(FadeOut),
    }
  });
}



var FadeIn = {
  opacity: {
    from: 0,
    to: 1,
    min: .1,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: 100,
  },
};

var FadeOut = {
  opacity: {
    from: 1,
    to: 0,
    min: 0,
    max: 0.9,
    type: 'linear',
    extrapolate: true,
    round: 100,
  },
};



var ToTheDown = {
  transformTranslate: {
    from: {x: 0, y: 0, z: 0},
    to: {x: 0, y: height, z: 0},
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },
  opacity: {
    value: 1.0,
    type: 'constant',
  },
  translateY: {
    from: 0,
    to: height,
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },
};

var FromTheRight = {
  opacity: {
    value: 1.0,
    type: 'constant',
  },

  transformTranslate: {
    from: {x: width, y: 0, z: 0},
    to: {x: 0, y: 0, z: 0},
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },

  translateX: {
    from: width,
    to: 0,
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },

  scaleX: {
    value: 1,
    type: 'constant',
  },
  scaleY: {
    value: 1,
    type: 'constant',
  },
};


var FadeToTheLeft = {
  // Rotate *requires* you to break out each individual component of
  // rotation (x, y, z, w)
  transformTranslate: {
    from: {x: 0, y: 0, z: 0},
    to: {x: -Math.round(Dimensions.get('window').width * 0.3), y: 0, z: 0},
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },
  // Uncomment to try rotation:
  // Quick guide to reasoning about rotations:
  // http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/#Quaternions
  // transformRotateRadians: {
  //   from: {x: 0, y: 0, z: 0, w: 1},
  //   to: {x: 0, y: 0, z: -0.47, w: 0.87},
  //   min: 0,
  //   max: 1,
  //   type: 'linear',
  //   extrapolate: true
  // },
  transformScale: {
    from: {x: 1, y: 1, z: 1},
    to: {x: 0.95, y: 0.95, z: 1},
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true
  },
  opacity: {
    from: 1,
    to: 0, //0.3,
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: false,
    round: 100,
  },
  translateX: {
    from: 0,
    to: -Math.round(Dimensions.get('window').width * 0.3),
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },
  scaleX: {
    from: 1,
    to: 0.95,
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true
  },
  scaleY: {
    from: 1,
    to: 0.95,
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true
  },
};



var FromTheTop = {
  ...FromTheRight,
  transformTranslate: {
    from: {y: -height, x: 0, z: 0},
    to: {x: 0, y: 0, z: 0},
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },
  translateY: {
    from: -height,
    to: 0,
    min: 0,
    max: 1,
    type: 'linear',
    extrapolate: true,
    round: PixelRatio.get(),
  },
};
