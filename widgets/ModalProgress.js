import { View, Text, LayoutAnimation, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, Dimensions, PixelRatio, Modal, Animated, Easing, PanResponder, Alert } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

import React, { Component } from 'react';
import formStyles from '../styles/FormStyles';

import LogoAnimationProgress from '../components/LogoAnimationProgress';

var {width, height} = Dimensions.get('window');

export default class ModalProgress extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      visible: false,
      percent: 0,
      animationVisible: false,
    };

    this.opacity = new Animated.Value(0);
    this.percentOpacity = new Animated.Value(1);
    this.progressOpacity = new Animated.Value(1);

    this.fakeProgress = new Animated.Value(0);
    this.fakeProgress.addListener(({value: percent}) => {
      percent = parseInt(percent); //ignore decimal percents

      if(this.state.percent !== percent) { //only setState if next integer reached
        this.setState({percent});
      }

      if(percent === 100) this.complete();
    });
  }

  complete() {
    if(this.isCompleting) return;
    this.isCompleting = true;

    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({noteHidden: true});

    if(this.props.hasAlert) this.alert();
    else {
      if(this.props.onHideMessageBox) this.props.onHideMessageBox();
      this.vanish();
    }
  }

  alert() {
    Alert.alert(
      'Custom Message Incomplete',
      'Hey there, you started filling out the custom message, but did not complete it. Would you like to complete it or just send the recipient your default message?',
      [
        {text: 'NO', style: 'destructive', onPress: () => {
          if(this.props.onHideMessageBox) this.props.onHideMessageBox();
          this.vanish();
        }},
        {text: 'RESUME', style: 'default', onPress: () => null}, //modal will vanish when !nextProps.hasAlert
      ]
    );
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.percent > 0 && this.props.percent === 0) {
      if(!this.state.visible) this.show();
    }
    else if(nextProps.percent === 100 && this.isCompleting !== true) { //originally I compared nextProps.percent === 100 && this.props.percent !== 100, but for some reason (likely because upload percentages are changing so fast) they were both 100 sometimes, so now we imperatively check if i marked this instance as isCompleting
      this.complete();
    }
    else if(!nextProps.hasAlert && this.props.hasAlert && nextProps.percent === 100) { //this occurs after the user finishes the delivery message after having been asked if he wants to complete it
      this.vanish();
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    if(nextState.percent !== this.state.percent && this.refs.progressWidget) {
      this.refs.progressWidget.incrementPercent(nextState.percent);               //increment animated progress imperatively, passing the percent through this component
      return nextState.percent !== 0 && nextState.percent !== 100 ? false : true; //while blocking re-rendering during upload so form isn't frozen
    }
    else if(nextProps.percent !== this.props.percent && this.refs.progressWidget) {
      this.refs.progressWidget.incrementPercent(nextProps.percent);               //increment animated progress imperatively, passing the percent through this component
      return nextProps.percent !== 0 && nextProps.percent !== 100 ? false : true; //while blocking re-rendering during upload so form isn't frozen
    }

    return true;
  }

  show() {
    this.setState({visible: true});

    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.timing(
           this.opacity, {
             toValue: 1,
             easing: Easing.quad,
             duration: 300
           },
         ).start(() => {
           if(this.isFake()) {
             this.startFakeProgress();
           }
         });
      });
    });
  }

  hide() {
    if(this.isFake()) {
      this.fakeProgress.stopAnimation(this.completeFakeProgress.bind(this));
    }
    else this.complete();
  }
  vanish() {
    if(this.props.animateLogo) this.animateLogo();
    else this.onVanishComplete();
  }
  animateLogo() {
    this.setState({animationVisible: true});
    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.stagger(500, [
          Animated.timing(this.percentOpacity, {toValue: 0, duration: 1000, easing: Easing.in(Easing.quad)}),
          Animated.timing(this.progressOpacity, {toValue: 0, duration: 2000, easing: Easing.in(Easing.quad)})
        ]).start();
      });
    });
  }
  onVanishComplete() {
    this.fadeContents(() => {
      this.setState({percent: 0, animationVisible: false});
      this.fakeProgress.setValue(0);
      this.percentOpacity.setValue(1);
      this.progressOpacity = new Animated.Value(1);

      this.setState({visible: false}, () => {
        this.isCompleting = false;
        this.props.onComplete && this.props.onComplete();
      });
    });
  }
  fadeContents(callback) {
    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.timing(
          this.opacity, {
            toValue: 0,
            easing: Easing.quad,
            duration: 300,
          },
        ).start(callback);
      });
    });
  }


  startFakeProgress() {
    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.timing(this.fakeProgress, {
          toValue: 50,
          duration: 3000, //8000
          easing: Easing.out(Easing.exp),
        }).start();
      });
    });
  }
  completeFakeProgress() {
    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.timing(this.fakeProgress, {
          toValue: 100,
          duration: 1000, //3000
          easing: Easing.out(Easing.cubic),
        }).start();
      });
    });
  }

  percent() {
    return this.isFake() ? this.state.percent : this.props.percent;
  }
  isFake() {
    return typeof this.props.percent === 'undefined';
  }

  render() {
    return (
      <Modal
        animated={false}
        transparent={true}
        style={{position: 'absolute'}}
        visible={this.state.visible}
        //onShow={this.onShow.bind(this)}
        //onDismiss={this.onDismiss.bind(this)}
      >
        <Animated.View style={[styles.container, {opacity: this.opacity}]}>
          <Animated.View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, opacity: this.progressOpacity}}>
            <ProgressWidget ref='progressWidget' text={this.props.text} opacity={this.props.percentOpacity} />
          </Animated.View>

          {this.props.note && !this.state.noteHidden && <Text style={styles.note}>{this.props.note}</Text>}

          <LogoAnimationProgress
            visible={this.state.animationVisible}
            onComplete={ () => this.onVanishComplete() }
            hold={true}
            //hideCircle={true}
            caption={this.props.caption || 'UPLOAD COMPLETE!'}
          />

          {this.props.children}
        </Animated.View>
      </Modal>
    );
  }
}


//this is done somewhat imperatively so that the Parent component doesn't re-render with each percent increment.
//The reason is because when it's constantly animating, the delivery message text area is also re-rendering
//and it makes it so the celebrity can't enter any text in it.
class ProgressWidget extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      percent: 0,
    };
  }

  incrementPercent(percent) {
    this.setState({percent});
  }

  render(props) {
    return (
      <AnimatedCircularProgress
        size={224}
        width={12}
        fill={this.state.percent}
        tintColor="rgba(27,58,223,1)"
        backgroundColor="#212121"
        style={styles.progressStyle}
      >
        {
          (fill) => (
            <Animated.View style={[styles.percentContainer, {opacity: this.props.opacity}]}>
              <Text style={styles.percent}>{Math.round(fill)}%</Text>
              <Text style={[styles.percent, {fontSize: 20}]}>{this.props.text}</Text>
            </Animated.View>
          )
        }
      </AnimatedCircularProgress>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'black',
    width, height,
  },
  progressStyle: {
    position: 'absolute',
    width,
    height: 224,
    left: width/2 - 224/2,
    top: height/2 - 224/2,
  },
  percentContainer: {
    position: 'absolute',
    width: 232,
    top: 70,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
  },
  percent: {
    color: '#7591af',
    fontSize: 45,
    fontWeight: "100",
    textAlign: 'center',
  },
  note: {
    position: 'absolute',
    bottom: 40,
    width: width - 120,
    left: 60,
    color: '#7591af',
    fontSize: 17,
    fontWeight: "100",
    textAlign: 'center',
    fontFamily: 'ProximaNova-Thin'
  }
});
