'use strict';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Animated, Dimensions, Easing, PixelRatio} from 'react-native';

import {connect} from 'react-redux';

import DeveloperMenu from './DeveloperMenu';

var {width, height} = Dimensions.get('window');

import {setImmediateAnimationFrame} from '../misc/helpers';

import Icon from 'react-native-vector-icons/Ionicons';
const AnimatedIcon = Animated.createAnimatedComponent(Icon);


const TabBar = React.createClass({
  selectedTabIcons: [],
  unselectedTabIcons: [],

  propTypes: {
    visible: PropTypes.bool,
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    underlineColor : PropTypes.string,
    backgroundColor : PropTypes.string,
    activeTextColor : PropTypes.string,
    inactiveTextColor : PropTypes.string,
  },

  getInitialState() {
    return {
      translateY: new Animated.Value(this.props.visible ? 0 : 51),
    };
  },
  componentWillReceiveProps(nextProps) {
    if(nextProps.visible && !this.props.visible) this.slideTabBarUp(nextProps.callback);
    else if(!nextProps.visible && this.props.visible) this.slideTabBarDown(nextProps.callback);
  },

  slideTabBarDown() {
    setImmediateAnimationFrame(() => {
      Animated.timing(this.state.translateY, {
         duration: 300,
         easing: Easing.sin,
         toValue: 51,
       }).start();
    });
  },
  slideTabBarUp() {
    setImmediateAnimationFrame(() => {
      Animated.timing(this.state.translateY, {
         duration: 300,
         easing: Easing.sin,
         toValue: 0,
       }).start();
    });
  },

  renderTabOption(name, page) {
    var isTabActive = this.props.activeTab === page;

    let inputRange = [page -1, page, page+1];

    var iconScale = this.props.scrollValue.interpolate({
      inputRange: inputRange,
      outputRange: [.95, 1.15, .95],
      extrapolate: 'clamp'
    });

    return (
      <TouchableOpacity key={name} onPress={() => this.props.goToPage(page)} style={styles.tab}>
        <AnimatedIcon name={name} size={30} color={this.props.activeTextColor} style={[styles.icon, {transform:[{scale: iconScale}]}]}
          ref={(icon) => { this.selectedTabIcons[page] = icon }}/>

        <AnimatedIcon name={name} size={30} color={this.props.inactiveTextColor} style={[styles.icon, {transform:[{scale: iconScale}]}]}
          ref={(icon) => { this.unselectedTabIcons[page] = icon }}/>
      </TouchableOpacity>
    );
  },


  componentDidMount() {
    this.setAnimationValue({value: this.props.activeTab});
    this._listener = this.props.scrollValue.addListener(this.setAnimationValue);
  },

  setAnimationValue({value}) {
    var currentPage = this.props.activeTab;

    this.unselectedTabIcons.forEach((icon, i) => {
      var iconRef = icon;

      if (!icon.setNativeProps && icon !== null) {
        iconRef = icon.refs.icon_image
      }

      if(!iconRef || !iconRef.setNativeProps) return;

      if (value - i >= 0 && value - i <= 1) {
        iconRef.setNativeProps({ style: {opacity: value - i} });
      }
      if (i - value >= 0 &&  i - value <= 1) {
        iconRef.setNativeProps({ style: {opacity: i - value} });
      }
    });
  },


  render() {
    var containerWidth = this.props.containerWidth;
    var numberOfTabs = this.props.tabs.length;
    var tabUnderlineStyle = {
      position: 'absolute',
      width: containerWidth / numberOfTabs,
      height: 4,
      backgroundColor: this.props.underlineColor,
      borderWidth: 1/PixelRatio.get(),
      borderColor: 'rgb(32, 27, 250)',
      bottom: 0,
    };

    var translateX = this.props.scrollValue.interpolate({
      inputRange: [0, 1], outputRange: [0, containerWidth / numberOfTabs]
    });

    return (
      <Animated.View style={{transform: [{translateY: this.state.translateY}]}}>
        <View style={[styles.tabs, this.props.style, {backgroundColor: this.props.backgroundColor}]}>
          {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
        </View>
        <Animated.View style={[tabUnderlineStyle, {transform:[{translateX: translateX}]}]} />
        <View style={{position: 'absolute', top: -1, width, height: 1, backgroundColor: 'rgba(19, 19, 19, 1)'}}/>
      </Animated.View>
    );
  },
});


export default connect(
  ({showTabBar: visible}) => ({visible})
)(TabBar);



var styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  tabs: {
    height: 50,
    flexDirection: 'row',
    paddingTop: 5,
    borderWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomColor: 'rgba(0,0,0,0.05)',
    borderTopWidth: 1/PixelRatio.get(),
    borderTopColor: 'rgba(27, 26, 26, 1)',
  },
  icon: {
    position: 'absolute',
    top: 4,
    left: 30,
  },
});
