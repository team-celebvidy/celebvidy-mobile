import { View, Text, TouchableOpacity, StyleSheet, Dimensions, PixelRatio, Modal, Animated, Easing, PanResponder } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

import _ from 'underscore';

import TextArea from './GiftedTextArea';
import TextInput from './GiftedTextInput';

import formStyles from '../styles/FormStyles';
import React, { Component } from 'react';

var {width, height} = Dimensions.get('window');


export default class ModalProgress extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      visible: false,
    };

    this.opacity = new Animated.Value(0);
  }

  show() {
    setImmediate(() => {
      Animated.timing(
         this.opacity, {
           toValue: 1,
           easing: this.props.easing || Easing.sin,
           duration: this.props.duration || 300
         },
       ).start(() => {
         this.setState({visible: true});
         this.props.onShow && this.props.onShow();
       });
    });
  }

  vanish() {
    setImmediate(() => {
      Animated.timing(
        this.opacity, {
          toValue: 0,
          easing: this.props.easing || Easing.sin,
          duration: this.props.duration || 500,
        },
      ).start(() => {
        this.setState({visible: false});
        this.props.onVanish && this.props.onVanish();
      });
    });
  }

  componentDidMount() {
    if(this.props.visible) this.show();
    if(!this.props.visible) this.vanish();
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.visible && !this.props.visible) this.show();
    if(!nextProps.visible && this.props.visible) this.vanish();
  }


  render() {
    if(this.props.textOnly) return (
      <Animated.View style={[styles.container, this.props.style, {opacity: this.opacity}]}>
        <Text style={[styles.textStyle, this.props.textStyle]}>
          {this.props.children}
        </Text>
      </Animated.View>
    );

    return (
      <Animated.View style={[styles.container, this.props.style, {opacity: this.opacity}]}>
        {this.props.children}
      </Animated.View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  textStyle: {
    backgroundColor: 'transparent',
    fontFamily: 'ProximaNova-Thin',
    fontSize: 19,
    marginBottom: 2,
    color: 'white',
  }
});
