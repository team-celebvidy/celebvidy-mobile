import {Dimensions, View, InteractionManager, AsyncStorage} from 'react-native';

import Swipeout from 'react-native-swipeout';
import Overlay from 'react-native-overlay';

import ActionSheet from './ActionSheet';
import NavigatorInstances from '../misc/NavigatorInstances';

import {setImmediateAnimationFrame} from '../misc/helpers';

var {width, height} = Dimensions.get('window');

import testState, {navigatorTestState, allRoutesInNavigator} from '../misc/testState';
import {clearStorage} from '../configureStore';

import * as actions from '../actions';
import { connect } from 'react-redux';
import React, { Component } from 'react';



class DeveloperMenu extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      menuClosed: false,
      showActionSheet: false,
      selectedOption: null,
    };
  }

  render() {
    return (
      <View style={{position: 'absolute', bottom: 0, left: 0, width, height: this.props.height || 50}}>
        <Swipeout
          backgroundColor='transparent'
          left={this.swipeoutButtonsLeft()}
          right={this.swipeoutButtonsRight()}
          close={this.state.menuClosed}
        >
          {this.props.children}
          {this.renderActionSheet()}
        </Swipeout>
      </View>
    );
  }

  closeMenu() {
    this.setState({menuClosed: true});
    setTimeout(() => this.setState({menuClosed: false}), 500); //Swipeout only watches menu close in componetWillReceiveProps
  }

  renderActionSheet() {
    return (
      <ActionSheet
        visible={this.state.showActionSheet}
        onCancel={() => this.hideActionSheet()}
      >
        {this.renderSheetOptions()}
      </ActionSheet>
    );
  }

  swipeoutButtonsLeft() {
    return [
      {text: 'MISC', backgroundColor: 'gray', onPress: this.showMiscOptions.bind(this)},
      {text: 'STATE', backgroundColor: 'rgb(0, 87, 235)', onPress: this.showStateOptions.bind(this)},
      {text: 'RELOAD', backgroundColor: 'red', onPress: this.showRedErrorPage.bind(this)},
    ]
  }
  swipeoutButtonsRight() {
    return [
      {text: 'ROUTES', backgroundColor: 'purple', onPress: this.showRouteOptions.bind(this)},
      {text: 'CUSTOMER', backgroundColor: 'blue', onPress: this.showCustomerOptions.bind(this)},
      {text: 'CELEBRITY', backgroundColor: 'rgb(20,20,20)', onPress: this.showCelebrityOptions.bind(this)},
    ]
  }


  showActionSheet(state) {
    setImmediateAnimationFrame(() => {
      this.setState(Object.assign({showActionSheet: true}, state));
    });
  }
  hideActionSheet() {
    setImmediateAnimationFrame(() => {
      this.setState({showActionSheet: false});
    });
  }

  showMiscOptions() {
    this.showActionSheet({selectedOption: 'misc'});
  }
  showStateOptions() {
    this.showActionSheet({selectedOption: 'state'});
  }
  showRedErrorPage() {
    return Hoorah; //undefined variable will trigger red screen of death where you can press reload ;)
  }
  showRouteOptions() {
    this.showActionSheet({selectedOption: 'routes'});
  }
  showCustomerOptions() {
    let selectedComponent = this.props.selectedComponent;
    let selectedRoute = this.props.selectedRoute;

    if(this.props.selectedComponent === 'CelebrityRequests') { //annoying one-off code to insure routes menu switches options, since navigators replace each other
      selectedComponent = selectedRoute = 'CustomerOrders';
    }
    else if(this.props.selectedComponent === 'CelebrityOwnProfile') {
      selectedComponent = selectedRoute = 'CustomerAccount';
    }

    let state = testState(false, selectedComponent, selectedRoute);
    this.props.setDeveloperState(state);

    this.showActionSheet({selectedOption: 'customer'});
  }
  showCelebrityOptions() {
    let selectedComponent = this.props.selectedComponent;
    let selectedRoute = this.props.selectedRoute;

    if(this.props.selectedComponent === 'CustomerOrders') { //annoying one-off code to insure routes menu switches options, since navigators replace each other
      selectedComponent = selectedRoute = 'CelebrityRequests';
    }
    else if(this.props.selectedComponent === 'CustomerAccount') {
      selectedComponent = selectedRoute = 'CelebrityOwnProfile';
    }

    let state = testState(true, selectedComponent, selectedRoute);
    this.props.setDeveloperState(state);

    this.showActionSheet({selectedOption: 'celebrity'});
  }


  renderSheetOptions() {
    let props = this.props;

    // if(this.state.selectedOption === 'misc') {
    //   return this.renderActionOptions();
    // }
    // else 
    if(this.state.selectedOption === 'state') {
      return this.renderActionOptions([
        ['Login', () => props.login()],
        ['Logout', () => props.logout()],
        ['toggleIntroAnimation()', () => props.toggleIntroAnimation()],
        ['toggleDrawer()', () => props.toggleDrawer()],
        ['toggleTabBar()', () => props.toggleTabBar()],
        ['toggleStatusBar()', () => props.toggleStatusBar()],
        ['toggleTourAgain()', () => props.toggleTourAgain()],
        ['toggleTourReady()', () => props.toggleTourReady()],
        ['toggleFinishLater()', () => props.toggleFinishLater()],
        ['clipTour()', () => props.clipTour()],
        ['unclipTour()', () => props.unclipTour()],
        ['clearStorage()', () => clearStorage()],
      ]);
    }
    else if(this.state.selectedOption === 'routes') {
      return this.renderRouteOptionsForNavigator(this.props.selectedComponent);
    }
    else if(this.state.selectedOption === 'customer') {
      return this.renderSceneOptions([
        'AppIntroTour',
        'CustomerAccount',
        'CustomerOrders',
        'Industries',
        'Celebrities',
        'Share',
      ]);
    }
    else if(this.state.selectedOption === 'celebrity') {
      return this.renderSceneOptions([
        'AppIntroTour',
        'CelebrityAccount',
        'CelebrityOwnProfile',
        'CelebrityRequests',
        'Industries',
        'Celebrities',
        'Share',
      ]);
    }
  }

  renderActionOptions(options) {
    return options.map((option, i) => {
      let name = option[0];
      let onPress = option[1];

      return (
        <ActionSheet.Button key={i} onPress={() => {
            onPress();
            this.hideActionSheet();
          }}
        >
          {name}
        </ActionSheet.Button>
      );
    });
  }
  renderSceneOptions(components) {
    return components.map((component, i) => {
      return (
        <ActionSheet.Button key={i} onPress={() => this.selectComponent(component)} >
          {component}
        </ActionSheet.Button>
      );
    });
  }
  selectComponent(component) {
    let state = testState(this.props.isCelebrity, component);
    this.props.setDeveloperState(state);
    this.hideActionSheet();
  }


  renderRouteOptionsForNavigator(topNavigatorComponent) {
    let components = allRoutesInNavigator(topNavigatorComponent, this.props.isCelebrity);

    return components.map((routeComponent, i) => {
      return (
        <ActionSheet.Button key={i} onPress={() => this.selectRoute(routeComponent)}>
          {routeComponent}
        </ActionSheet.Button>
      );
    });
  }
  selectRoute(routeComponent) {
    let {routeStack, stateProps} = navigatorTestState(this.props.selectedComponent, routeComponent);
    let state = testState(this.props.isCelebrity, this.props.selectedComponent, routeComponent);
    this.props.setDeveloperState(state);

    //if navigators switch, wait until they are reMounted
    setImmediateAnimationFrame(() => {
      let navigator = this.navigatorForComponent(this.props.selectedComponent);
      if(!navigator) {
        alert('NO NAVIGATOR FOR ROUTE. You probably switched users and the CustomerOrders/CelebrityRequests navigator was removed');
        return this.hideActionSheet(); //some components don't have navigators
      }

      let stack = navigator.immediatelyResetRouteStack(routeStack, stateProps);
      let lastRoute = stack[stack.length-1];
      lastRoute.onWillFocus(); //onWillFocus needs to be manually called since `immediatelyResetRouteStack` doesn't call it like usual

      this.hideActionSheet();
      this.closeMenu();
    });
  }

  navigatorForComponent(component) {
    return NavigatorInstances[component];
  }
}

export default connect(
  ({isCelebrity, selectedComponent, selectedRoute}) => ({isCelebrity, selectedComponent, selectedRoute}),
  actions
)(DeveloperMenu)
