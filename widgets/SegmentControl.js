import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, TouchableHighlight, Dimensions, Animated, Easing, InteractionManager } from 'react-native';

var screen = Dimensions.get('window');
var tweenState = require('react-tween-state');

export default React.createClass({

    mixins: [tweenState.Mixin],

    propTypes: {
        duration: PropTypes.number,
        onTransitionStart: PropTypes.func,
        onTransitionEnd: PropTypes.func,
        onPress: PropTypes.func,
        renderTitle: PropTypes.func,
        titles: PropTypes.array,
        index: PropTypes.number,
        barColor: PropTypes.string,
        barPosition: PropTypes.string,
        underlayColor: PropTypes.string,
        stretch: PropTypes.bool,
        selectedTitleStyle: PropTypes.object,
        titleStyle: PropTypes.object,

    },

    getDefaultProps() {
        return {
            duration: 200,
            onTransitionStart: ()=>{},
            onTransitionEnd: ()=>{},
            onPress: ()=>{},
            renderTitle: null,
            index: 0,
            barColor: '#44B7E1',
            barPosition:'top',
            underlayColor: '#CCCCCC',
            stretch: false,
            selectedTextStyle: null,
            textStyle: null,
        };
    },


    getInitialState() {
        return {
          barLeft: new Animated.Value(0),
          barWidth: new Animated.Value(0)
        };
    },

    shouldComponentUpdate(nextProps) {
      if(nextProps.index !== this.props.index) {
        this.moveTo(nextProps.index);
        return true;
      }

      return false;
    },

    measureContainer() {
      this.container.measure((ox, oy, width) => {
        this.state.barWidth.setValue(width/this.props.titles.length);
        setImmediate(() => this.moveTo(this.props.index));
      });
    },

    measureHandler(ox, oy, width) {
      let handle = InteractionManager.createInteractionHandle();

      setImmediate(this.props.onTransitionStart, 0);

      setImmediate(() => {
        requestAnimationFrame(() => {
          Animated.timing(this.state.barLeft, {
              easing: Easing.inOut(Easing.quad),
              duration: 200,
              toValue: ox
          }).start(() => {
            InteractionManager.clearInteractionHandle(handle);
            this.propsonTransitionEnd && this.props.onTransitionEnd();
          });
        });
      });
    },

    moveTo(index) {
        this.refs[index].measure(this.measureHandler);
    },

    _renderTitle(title, i) {
        return (
            <View style={styles.title}>
                <Text style={[this.props.titleStyle, i === this.props.index && this.props.selectedTitleStyle]}>{title}</Text>
            </View>
        );
    },

    renderTitle(title, i) {
        return (
            <View key={i} ref={i} style={{ flex: this.props.stretch ? 1 : 0 }}>
                <TouchableHighlight underlayColor={this.props.underlayColor} onPress={() => this.props.onPress(i)}>
                    {this.props.renderTitle ? this.props.renderTitle(title, i) : this._renderTitle(title, i)}
                </TouchableHighlight>
            </View>
        );
    },

    render() {
        var items = [];
        var titles = this.props.titles;

        if (!this.props.stretch) {
            items.push(<View key={`s`} style={styles.spacer} />);
        }

        for (var i = 0; i < titles.length; i++) {
            items.push(this.renderTitle(titles[i], i));
            if (!this.props.stretch) {
                items.push(<View key={`s${i}`} style={styles.spacer} />);
            }
        }
        var barContainer = (
          <View style={styles.barContainer} onLayout={this.measureContainer} ref={(container) => this.container = container}>
              <Animated.View ref="bar" style={[styles.bar, {
                  left: this.state.barLeft,
                  width: this.state.barWidth,
                  backgroundColor: this.props.barColor
              }, this.props.barStyle]} />
          </View>
        );
        return (
            <View {...this.props} style={[styles.container, this.props.style]}>
                {this.props.barPosition == 'top' && barContainer}
                <View style={styles.titleContainer}>
                    {items}
                </View>
                {this.props.barPosition == 'bottom' && barContainer}
            </View>
        );
    }
});




const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    titleContainer: {
        flexDirection: 'row',
    },
    title: {
        //flex: 0,
        //backgroundColor: '#f3f3f3',
        alignItems: 'center',
        paddingHorizontal: 2,
        paddingVertical: 10,
    },
    spacer: {
        flex: 1,
    },
    barContainer: {
        height: 1,
        position: 'relative',
        backgroundColor: 'rgb(19,19,19)'
    },
    bar: {
        backgroundColor: 'blue',
        position: 'absolute',
        height: 4,
    }
});
