/**
USAGE:

This is a thin wrapper over Ex-Navigator. It does several things, but the main thing it does is let you omit a route config
or many of the keys on the config, which are automatically set based on various logic.

- The `initialComponent` prop for that Navigator should be the name of the component, eg: <Navigator initialComponent={MyComponent} /> ...yay, no indirection with route functions!
- The component can OPTIONALLY define a route function by the same name as the class in routes.js (it's not required though, and the component will automatically be returned from renderScene if not, and the scene title will be that of the Component un-camelcased)
- When pushing a new route, call `navigator.pushWithProps(Component, props)` instead, and the scene will automatically be configured. ...yay!
- Global props will be automatically passed along! ALSO: Various connections to ./DeveloperMenu.js are in place to sync routeStacks
**/

import { Text, View, Image, Dimensions, Animated, ScrollView, InteractionManager, StatusBar, TouchableOpacity, Navigator as ReactNavigator } from 'react-native';
import React, { Component } from 'react';

import _ from 'underscore';

const {width, height} = Dimensions.get('window');

import ExNavigator from 'react-navigation';
import NavBar from './NavBar';
import NavButton from './NavButton';
import {BackButton} from './NavButton';

import styles from '../styles/FormStyles';

import testState, {navigatorTestState, allRoutesInNavigator} from '../misc/testState';
import {setImmediateAnimationFrame} from '../misc/helpers';

import ALL_COMPONENTS from '../misc/ALL_COMPONENTS';
import * as ALL_ROUTES from '../routes';
import NavigatorInstances from '../misc/NavigatorInstances';

import * as actions from '../actions';
import { connect } from 'react-redux';

class Navigator extends Component {
  routeForComponent(name, props) {
    return ALL_ROUTES[name+'Route'] ? ALL_ROUTES[name+'Route'](props) : {}; //pass the Component, as well, so routes.js doesn't need to re-import it
  }


  constructor(props, context) {
    super(props, context);

    let name = this.props.initialComponent;
    NavigatorInstances[name] = this;
    this.name = name;
  }
  componentWillUnmount() {
    delete NavigatorInstances[this.name]; //delete to insure DeveloperMenu doesn't try to use old navigators, such as when switching from CustomerOrdres to CelebrityRequests that replace each other
  }
  shouldComponentUpdate() {
    return false; //it should never re-render; it uses no state, and it only uses its props on ininitial mounting. see dev tools at bottom
  }

  componentWillMount() {
    this._initialRouteStack = this.getInitialRouteStack();
  }
  componentDidMount() {
    if(this.props.selectedComponent === this.props.initialComponent) { //do the following only for the component navigator on the screen
      this._initialRouteStack[this._initialRouteStack.length - 1].onWillFocus(); //onWillFocus not called for initial route, so we gotta do it manually
    }
  }

  render() {
    let {navigationBarStyle, sceneStyle, navBarTransparent, fullPage} = this.props;

    let navigatorProps = {...this.props};
    delete navigatorProps.selectedComponent;
    delete navigatorProps.selectedRoute;

    return (
      <ExNavigator
        ref={(nav) => this.exNavigator = nav}
        //initialRoute={this.initialRoute()}
        initialRouteStack={this._initialRouteStack}
        renderNavigationBar={(props) => <NavBar ref={(navBar) => this.navBar = navBar} {...props} fullPage={fullPage} />}
        renderBackButton={(navigator) => <BackButton navigator={navigator} backgroundColor={'transparent'} />}

        {...navigatorProps}
        sceneStyle={[{
          flex: 1,
          flexDirection: 'row',
          backgroundColor: 'transparent',
        }, sceneStyle]}
        navigationBarStyle={navigationBarStyle}
        titleStyle={styles.pageTitle}
        barButtonTextStyle={{color: 'rgba(27,58,223,1)'}}
        barButtonIconStyle={{opacity: .6}}
      />
    );
  }


  initialRoute() {
    return this.generateRoute(this.props.initialComponent); //usage: <Navigator initialComponent={MySexyComponent} />
  }
  pushWithProps(Component, sceneProps) {
    let route = this.generateRoute(Component, sceneProps);
    this.exNavigator.push(route);
  }
  replacePreviousAndPopWithProps(Component, sceneProps) {
    let route = this.generateRoute(Component, sceneProps);
    this.exNavigator.replacePrevious(route);
    this.exNavigator.pop();
  }

  generateRoute(name, sceneProps) {
    let ComponentOrRouteFunc = ALL_COMPONENTS[name]; //so component's don't need to import modules just for pushing, which would mean more patching during hot-reloading

    let route = {};

    let isComponent = !!(ComponentOrRouteFunc.prototype && ComponentOrRouteFunc.prototype.render);
    //let isComponent = name[0] === name[0].toUpperCase(); //the above commented out line stopped working when built for release, so route funcs starting without capitals as a convention is the best I could come with for now
    let Component = isComponent ? ComponentOrRouteFunc : null;
    let props = this.getPropsWithExtras(this.props, sceneProps);

    if(isComponent) { //MAIN USAGE: allow for pushing just the Component rather than a route function
      route = this.routeForComponent(name, props);
    }
    else if(_.isFunction(ComponentOrRouteFunc)) { //occasionally, we still do wanna use route funcs like ex-navigator's original API
      route = ComponentOrRouteFunc(props);
    }

    return this.createRouteWithRenderScene(Component, route, props, name);
  }
  createRouteWithRenderScene(Component, route, props, name) {
    let oldRenderScene = route.renderScene;

    route.renderScene = (navigator) => {
      //All navigators will now have a `pushWithProps` method which should be used instead of `push` to get access to default props such as `toggleFullPage`.
      navigator.pushWithProps = this.pushWithProps.bind(this);
      navigator.replacePreviousAndPopWithProps = this.replacePreviousAndPopWithProps.bind(this);
      navigator.resetRouteStack = this.immediatelyResetRouteStack.bind(this);
      let scene;

      if(oldRenderScene) { //renderScene() method exists in config within routes.js
        scene = React.cloneElement(oldRenderScene(navigator), {...props, navigator}); //preserve provided renderScene and add extra props
      }
      else scene = React.createElement(Component, {...props, navigator});

      return <View style={{flex: 1, marginTop: 64, height}}>{scene}</View>; //wrap in a view to control marginTop. Somewhat of a hack, since navigator positioning can be finnicky.
    }

    //a little automation:
    route.name = name;
    route.onWillFocus = this.assignOnWillFocus(route, props); //super important. it allows for declarative route definitions for features like showTabBar, etc
    route = this.assignRenderTitle(name, route);

    return route;
  }

  popToTop() {
    this.exNavigator.popToTop();
  }

  //VERY IMPORTANT METHOD THAT ENFORCES A DECLARATIVE STYLE THROUGHOUT THE APP
  //BASED ON KEYS SET IN ROUTE OBJECTS WITHIN routes.js
  assignOnWillFocus(route, props) {
    let {name, showFullPage, showTabBar, showStatusBar, onWillFocus} = route;

    return () => {
      if(typeof showTabBar !== 'undefined') {
        this.props.dispatch(actions.toggleTabBar(showTabBar));
      }

      if(typeof showStatusBar !== 'undefined') {
        this.props.dispatch(actions.toggleStatusBar(showStatusBar));
      }

      if(typeof showFullPage !== 'undefined') {
        this.navBar && this.navBar.toggleFullPage(showFullPage)
        //navBar handles its own state outside of redux since there are muliple navigators/tabBars
        //and each is also intialized with a default prop for fullPage (e.g. AppIntroTour is full page to begin with)
      }

      this.props.dispatch(actions.selectRoute(name));
      onWillFocus && onWillFocus();
    }
  }

  assignRenderTitle(name, route) {
    let oldRenderTitle = route.renderTitle;

    route.renderTitle = (navigator) =>  {
      if(oldRenderTitle) return oldRenderTitle(navigator);
      else if(route.getTitle && route.getTitle()) return <Text style={styles.pageTitle}>{route.getTitle()}</Text>;
      else {
        name = name.replace(/([A-Z])/g, ' $1').slice(1).replace(/^./, function(str){ return str.toUpperCase(); }); //convert camel case to words (with first letter capitalized)

        return <Text style={styles.pageTitle}>{name}</Text>;
      }
    }

    return route;
  }

  //add "extra" methods we want available to all scene components who utilize the state of this navigator.
  getPropsWithExtras(globalNavigatorProps, sceneProps) {
    let props = {...globalNavigatorProps, ...sceneProps, dispatch: this.props.dispatch, navigator: this.exNavigator};
    delete props.selectedComponent;
    delete props.selectedRoute;
    return props;
  }

  //DEVELOPMENT TOOLS. SEE /misc/testData.js for how to pre-configure routeStacks you want to test
  immediatelyResetRouteStack(routes, props) {
    let stack = routes.map((route) => this.generateRoute(route, props));
    if(this.exNavigator) this.exNavigator.immediatelyResetRouteStack(stack); //sometimes exNavigator isn't there (when switching between CustomerOrders and CelebrityRequests where different navigators replace each other), but it should be after mounting. I rather just block the red screen of death.
    return stack;
  }

  //navigate to route on reload if navigator's initialComponent === selectedComponent
  //NOTE: we are using this for users too!
  //The app always remembers where they were; this makes dev mode not differ too much from user mode, which means we won't bugs that doesnt exist in dev mode
  getInitialRouteStack() {
    if(true || !__DEV__ || this.props.selectedComponent !== this.props.initialComponent ) {
      return [this.initialRoute()];
    }
    else { //remember the route stack when developing -- we should actually integrate this for prdocution, but would need to move all `pushWithProps` calls to basically use no props and use redux everywhere instead, or the props from the previous component are lost. There is probably some better solution that will come to us, such as using the declarative Navigator-Expirmental once a community component makes the equivalent of Ex Navigator to wrap it
      let {routeStack, stateProps} = navigatorTestState(this.props.selectedComponent, this.props.selectedRoute);
      return routeStack.map((route) => this.generateRoute(route, stateProps));
    }
  }
}

export default connect(
  ({selectedComponent, selectedRoute}) => ({selectedComponent, selectedRoute}),
  null, null, {withRef: true}
)(Navigator)
