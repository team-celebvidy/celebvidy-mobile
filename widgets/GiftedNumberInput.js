var React = require('react-native');
import { View, Text, SwitchIOS, SliderIOS, SwitchAndroid, Platform, PixelRatio, Dimensions, TextInput, TouchableOpacity } from 'react-native';

import {GiftedForm, WidgetMixin, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';
import * as hyphenators from '../misc/hyphenator';

import NumberPad from './NumberPad';
import React, { Component } from 'react';
var {width, height} = Dimensions.get('window');


module.exports = React.createClass({
  mixins: [WidgetMixin],

  getDefaultProps() {
    return {
      inline: true,
      // @todo type avec suffix Widget pour all
      type: 'TextInputWidget',
      underlined: false,
      onTextInputFocus: (value) => value,
    }
  },

  getInitialState() {
    return {
      focused: false,
      highlighted: false,
      value: this.props.value,
    }
  },

  hasValidationError() {
    return this.state.validationErrorMessage !== null && this.state.validationErrorMessage !== '';
  },
  renderValidationError() {
    //if (!(typeof this.state.value === 'undefined' || this.state.value === '') && this.state.validationErrorMessage !== null && this.state.validationErrorMessage !== '') {
    if (this.hasValidationError()) {
      return (
        <GiftedForm.ValidationErrorWidget
          {...this.props}
          message={this.state.validationErrorMessage}
        />
      );
    }
    return null;
  },
  isValid() {
    var validators = null;
    if (this.props.displayValue) {
      // in case of modal widget
      validators = GiftedFormManager.getValidators(this.props.formName, this.props.displayValue);
    } else {
      validators = GiftedFormManager.getValidators(this.props.formName, this.props.name);
    }

    let toValidate = false;
    if (Array.isArray(validators.validate)) {
      if (validators.validate.length > 0) {
        toValidate = true;
      }
    }

    return this.state.validationErrorMessage === null  && toValidate === true;
  },
  isFilled() {
    return !(typeof this.state.value === 'undefined' || this.state.value === '');
  },
  renderIcon() {
    if(this.props.editable === false) return null;

    if (this.hasValidationError() && this.props.invalidIcon !== null && this.props.type !== 'OptionWidget' && this.props.validationImage === true) {
      return  <Icon name={this.props.invalidIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconInvalid')]} />;
    } else if (!this.hasValidationError() && this.isFilled() && this.props.validIcon !== null && this.props.type !== 'OptionWidget' && this.props.validationImage === true) {
      return <Icon name={this.props.validIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconValid')]} />;
    } else if (this.props.icon) {
        return <Icon name={this.props.icon} style={this.getStyle('rowIcon')} />;
    }
    return null;
  },
  _renderTitle() {
    if (this.props.title !== '') {
      return (
        <Text
          numberOfLines={1}
          style={this.getStyle(['textInputTitleInline'])}
        >
          {this.props.title}
        </Text>
      );
    }
    return (
      <View style={this.getStyle(['spacer'])}/>
    );
  },

  onChangeText(value) {
    this.props.onTextInputChangeText && this.props.onTextInputChangeText(value);
    this._onChange(value);
  },

  focusInput() {
    this.refs.input.focus();
  },

  _renderRow() {
    let value = this.state.value || this.props.placeholder;
    let {format} = this.props;

    return (
      <View ref='rowContainer' style={this.getStyle(['rowContainer'])}>
        <View style={this.getStyle(['row'])}>
          {this.renderIcon()}
          <TouchableOpacity onPress={() => this.refs.input.focus()}>
            {this._renderTitle()}
          </TouchableOpacity>

          <NumberPad
            ref='input'
            maxLength={this.props.maxLength}
            editable={this.props.editable}
            returnKeyType={this.props.returnKeyType || 'next'}

            onReturnKeyKeyPress={this.props.onReturnKeyKeyPress}
            onClear={this.props.onClear}
            onDelete={this.props.onDelete}
            onKeyPress={this.props.onKeyPress}
            onSubmitEditing={this.props.onSubmitEditing}

            onShow={() => {
              this.onFocus();
              this.props.onShow && this.props.onShow();
            }}
            onHide={(value) => {
              this.setState({highlighted: false});
              this.onBlur();
              this.props.onHide && this.onHide.onShow();
            }}

            onChangeText={(value) => {
              this.onChangeText(value.replace(/-|,/g, ''));
            }}
            value={this.state.value}
          >
            <View style={{width: 250}}>
              <Text style={{fontFamily: 'Avenir', fontSize: 16, marginLeft: 5, color: this.state.value ? 'white' : "rgba(203,203,203,.6)"}}>
                {format && hyphenators[format] ? hyphenators[format](value) : value}
              </Text>
            </View>
          </NumberPad>
        </View>
        {this.renderValidationError()}
        {this._renderUnderline()}
      </View>
    );
  },


  onFocus() {
    this.setState({
      focused: true,
      highlighted: true,
    });

    this.props.onFocus(this.refs.rowContainer);

    let oldText = this.state.value;
    let newText = this.props.onTextInputFocus(this.state.value);
    if (newText !== oldText) {
      this._onChange(newText);
    }
  },

  onBlur() {
    this.setState({
      focused: false,
    });

    this.props.onBlur();
    this.props.onTextInputBlur && this.props.onTextInputBlur(this.state.value);
  },



  _renderUnderline() {
    if (this.props.underlined === true) {
      if (this.hasValidationError() && this.state.focused === true) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineFocusedError'])}
          />
        );
      }
      if (this.hasValidationError()) {
        return (
          <View
            style={[this.getStyle(['underline', 'underlineError']), {width: this.props.underlineWidth}]}
          />
        );
      }
      if (this.state.focused === true || this.props.highlighted === true || this.state.highlighted === true) {
        return (
          <View
            style={[this.getStyle(['underline', 'underlineFocused']), {width: this.props.underlineWidth}]}
          />
        );
      }
      return (
        <View
          style={[this.getStyle(['underline', 'underlineIdle']), {width: this.props.underlineWidth}]}
        />
      );
    }
    return null;
  },

  render() {
    return this._renderRow();
  },

  defaultStyles: {
    rowImage: {
      height: 20,
      width: 20,
      marginLeft: 10,
    },
    underline: {
      borderBottomWidth: 1/PixelRatio.get(),
    },
    underlineIdle: {
      borderColor: 'gray',
    },
    underlineFocused: {
      borderColor: '#3498db',
    },
    underlineFocusedError: {
      borderColor: 'rgb(255, 35, 0)',
    },
    underlineError: {
      borderColor: 'red',
    },
    spacer: {
      width: 10,
    },
    rowContainer: {
      backgroundColor: '#FFF',
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: '#c8c7cc',
    },
    row: {
      flexDirection: 'row',
      height: 44,
      alignItems: 'center',
    },
    titleContainer: {
      paddingTop: 10,
      flexDirection: 'row',
      alignItems: 'center',
      // selfAlign: 'center',
      // backgroundColor: '#ff0000',
    },
    textInputInline: {
      fontSize: 15,
      flex: 1,
      height: 40,// @todo should be changed if underlined
      marginTop: 2,
    },
    textInputTitleInline: {
      width: 110,
      fontSize: 15,
      color: '#000',
      paddingLeft: 10,
    },
    textInputTitle: {
      fontSize: 13,
      color: '#333',
      paddingLeft: 10,
      flex: 1
    },
    textInput: {
      fontSize: 15,
      flex: 1,
      height: 40,
      marginLeft: 40,
    },
  },
});
