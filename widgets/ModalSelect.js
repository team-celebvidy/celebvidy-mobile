'use strict';
import { StyleSheet, Modal, Text, Dimensions, TouchableOpacity, PickerIOS, TouchableWithoutFeedback, Platform, Picker, View, PixelRatio } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
var dismissKeyboard = require('react-native-dismiss-keyboard');

const {width, height} = Dimensions.get('window');

export default class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      selections: props.selections,
    };

    this.defaultProps = {
      editable: true
    };
  }

  _setModalVisible(visible) {
    visible && this.props.onShow && this.props.onShow();
    !visible && this.props.onHide && this.props.onHide();

    if(visible) dismissKeyboard();
    this.setState({modalVisible: visible});
  }
  focus() {
    this.open();
  }
  focusInput() {
    this.focus();
  }
  open() {
    if(this.props.editable === false) return;

    setImmediate(() => {
      requestAnimationFrame(() => {
        this._setModalVisible(true);
      });
    });
  }
  close(pressedCloseKey) {
    this._setModalVisible(false);
    pressedCloseKey === true && this.props.onSubmitEditing && this.props.onSubmitEditing(this.state.selections);
  }

  _onChange(index, key) {
    let selections = [
      ...this.props.selections.slice(0, index),
      key,
      ...this.props.selections.slice(index + 1)
    ]

    this.setState({selections: selections});
    if(this.props.onChange) this.props.onChange(index, key, selections);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.selections !== this.props.selections && !_.isEqual(nextProps.selections, this.state.selections)) {
      this.setState({selections: nextProps.selections});
    }
  }

  returnKey() {
    let key = this.props.returnKeyType || 'DONE';
    return key.toUpperCase();
  }

  _renderIOS() {
    const { pickers, selectedKey, onChange, style, ...other} = this.props;
    var modalBackgroundStyle = {
      backgroundColor: 'transparent',
    };
    var innerContainerTransparentStyle = null;

    let activeOpacity = this.props.editable === false ? 1 : .5;

    return (
      <View {...other} style={[styles.selectContainer, style]}>

        <TouchableOpacity style={{width: 1000}} activeOpacity={activeOpacity} onPress={this.open.bind(this, true)} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
          {this.props.children}
        </TouchableOpacity>

        <Modal
          animated={true}
          transparent={true}
          style={{position: 'absolute'}}
          visible={this.state.modalVisible}
        >

          <TouchableWithoutFeedback onPress={this._setModalVisible.bind(this, false)}>
            <View style={[styles.container, modalBackgroundStyle]}>

              <View style={[styles.pickerBar, this.props.pickerBarStyles]}>
                <Text style={[styles.title, this.props.titleStyle]}>{this.props.longTitle || this.props.title || ''}</Text>
                <TouchableOpacity onPress={this.close.bind(this, true)} hitSlop={{top: 20, left: 20, bottom: 20, right: 20}}>
                  <Text style={[styles.closeText, this.props.closeTextStyle]}>{this.returnKey()}</Text>
                </TouchableOpacity>
              </View>

              <View
                style={[styles.pickerContainer, this.props.pickerContainerStyles]}
                onStartShouldSetResponder={ (evt) => true }
                onResponderReject={ (evt) => {} }>

                {pickers.map((picker, index) => (
                  <PickerIOS key={index}
                             selectedValue={this.state.selections[index]}
                             onValueChange={this._onChange.bind(this, index)}
                             style={[styles.picker, this.props.pickerStyles]}
                             itemStyle={[styles.pickerItem, this.props.pickerItemStyles]}
                  >
                    {picker.map((model) => <PickerIOS.Item key={model.name} value={model.name} label={model.name} />)}
                  </PickerIOS>
                ))}
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  }

  render() {
    return Platform.OS === 'ios' ? this._renderIOS() : this._renderAndroid();
  }

}

Select.propTypes = {
  title: PropTypes.string,
  pickers: PropTypes.array,
  selections: PropTypes.array,
  style: PropTypes.object,
  pickerBarStyles: PropTypes.object,
  titleStyle: PropTypes.object,
  closeTextStyle: PropTypes.object,
  pickerContainerStyles: PropTypes.object,
  pickerStyles: PropTypes.object,
  pickerItemStyles: PropTypes.object,
  onChange: PropTypes.func
};

var styles = StyleSheet.create({
  selectContainer: {
    flex: 0,
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },

  pickerBar: {
    width: width,
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 7,
    backgroundColor: '#ececec',

    borderTopWidth: 1/PixelRatio.get(),
    borderTopColor: 'rgba(10, 11, 107, .5)'
  },
  title: {
    color: 'white',
    paddingVertical: 5,
    color: 'white',
    fontSize: 22,
    fontFamily: 'ProximaNova-Thin',
    marginLeft: 15
  },
  closeText: {
    color: 'white',
    paddingVertical: 5,
    fontSize: 20,
    color: 'rgba(27,58,223,1)',
    marginRight: 10
  },
  pickerContainer: {
    width: width,
    height: 230,
    alignItems: 'center',

    paddingTop: -10,
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  picker: {
    flex: 1,
    marginTop: -10,
    width: width,
    backgroundColor: 'transparent',
  },
  pickerItem: {
    color: 'white',
  },
});
