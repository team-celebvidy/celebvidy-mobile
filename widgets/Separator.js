import { StyleSheet, View, PixelRatio } from 'react-native';
import React, { Component } from 'react';

export default class Separator extends Component {
render() {
  return (
    <View style={[styles.separator, this.props.style]}></View>
  );
}
}

const styles = StyleSheet.create({
separator: {
  position: 'relative',
  borderBottomWidth: 1/PixelRatio.get(),
  borderColor:  "rgba(193,193,193,0.32)" ,
  marginTop: 20,
},
buttonText: {}
});
