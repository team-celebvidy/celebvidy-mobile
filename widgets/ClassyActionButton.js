import { StyleSheet, Text, View, TouchableOpacity, PixelRatio} from 'react-native'
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

export default class ClassyActionButton extends Component {
  render() {
    return (
      <TouchableOpacity
        {...this.props}
        underlayColor={this.props.underlayColor}
        activeOpacity={typeof this.props.activeOpacity !== 'undefined' ? this.props.activeOpacity : .5}
        onPress={() => this.props.onPress && this.props.onPress() }
        style={[{
          height: 45,
        }, this.props.touchableStyle]}>

          <View ref='button' style={[{
              backgroundColor: 'rgba(0,0,0, .6)',
              borderWidth: 1,
              borderColor: 'rgb(60, 60, 60)',

              alignItems: 'center',
              height: 45,
              justifyContent: 'center'}, this.props.style]}>

            <View style={{flexDirection: 'row'}}>
              {this.props.icon && <Icon name={this.props.icon} style={[{color: 'white'}, this.props.iconStyle]}/>}
              <Text style={[{fontSize: 18, fontFamily: 'Proxima Nova', color: 'rgb(185, 183, 183)'}, this.props.textStyle]}>{this.props.text || this.props.children || 'SAVE'}</Text>
            </View>
          </View>
      </TouchableOpacity>
    );
  }
}
