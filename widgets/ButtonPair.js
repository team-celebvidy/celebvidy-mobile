import {View, Text, PixelRatio, StyleSheet, Dimensions} from 'react-native';
import ActionButton from './ActionButton';
import ClassyActionButton from './ClassyActionButton';
import React, { Component } from 'react';


export default class ButtonPair extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let {
      button1checked,
      button2checked,
      button1text,
      button2text,
      button1icon,
      button2icon,
      icon1style,
      icon2style,
      button1style,
      button2style,
      button1onPress,
      button2onPress,
      width,
      notification1,
      notification2,
    } = this.props;

    return (
      <View style={[styles.container, this.props.style]}>
        <ActionButton
          text={button1text}
          onPress={button1onPress}
          icon={button1checked ? 'checkmark' : button1icon}
          notification={this.props.notification1}
          touchableStyle={{
            flex: 1,
            marginRight: this.props.spaceBetween || 5
          }}
          style={[{
            backgroundColor: 'rgba(25, 54, 210, 1)',
            borderWidth: 2/PixelRatio.get(),
            borderColor: 'rgba(52, 79, 226, 1)',
          }, button1style]}
          iconStyle={[{
            color: 'white',
            fontSize: button1checked ? 16 : 20,
            marginTop: button1checked ? 0 : -5,
          }, icon1style]}
          textStyle={{
            marginLeft: 5,
            marginTop: 1,
            fontFamily: 'ProximaNova-Thin'
          }} />

        <ActionButton
          text={button2text}
          onPress={button2onPress}
          icon={button2checked ? 'checkmark' : button2icon}
          notification={this.props.notification2}
          touchableStyle={{
            flex: 1,
          }}
          style={[{
            borderWidth: 2/PixelRatio.get(),
            borderColor: 'rgba(71, 134, 245, 1)',
          }, button2style]}
          iconStyle={[{
            color: 'white',
            fontSize: button2checked ? 16 : 20,
            marginTop: button2checked ? 0 : -5,
          }, icon2style]}
          textStyle={{
            marginLeft: 5,
            marginTop: 1,
            fontFamily: 'ProximaNova-Thin',
          }} />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 25,
    marginBottom: 25
  }
});
