import { View, Dimensions, Animated, Navigator, StyleSheet } from 'react-native';
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

export default class NavBar extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      fullPage: !!props.fullPage, //initialize navBar with fullPage prop, but don't respond to it after mounting
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  toggleFullPage(fullPage) {
    this.setState({fullPage});
  }


  //this is an undocumented customization as prescribed by React Native core contributor brentvatne:
  //https://github.com/facebook/react-native/issues/2560#issuecomment-139919812
  immediatelyRefresh(...args) {
    this._nav.immediatelyRefresh(...args);
  }
  handleWillFocus(...args) {
    this._nav.updateProgress(...args);
  }
  updateProgress(...args) {
    this._nav.updateProgress(...args);
  }
  onAnimationStart(...args) {
    this._nav.updateProgress(...args);
  }
  onAnimationEnd(...args) {
    this._nav.updateProgress(...args);
  }

  render() {
    let {style} = this.props;
    style = style.slice(0).splice(-1, 0, styles.navBarStyle); //put base style second to the end so that navigationBarStyle prop to navigator overrides it, while also keeping the system-provided style at index 0

    if(this.state.fullPage) style.push({top: -64, overflow: 'hidden'});

    return <Navigator.NavigationBar ref={(nav) => this._nav = nav} {...this.props} style={style} />;
  }
}


const styles = StyleSheet.create({
  navBarStyle: {
    backgroundColor: 'rgba(14,13,13,1)',
    borderBottomWidth: 0,
    position: 'absolute',
    top: 0,
    left: 0
  }
});

