import { Animated } from 'react-native';
import React, { Component } from 'react';
import { Timer } from 'react-timer-mixin';

const methods = {
  flashMessage(message, callback) {
    this.showMessageThenVanish(null, null, message, callback);
  },
  flashMessageStick(message, callback) {
    this.showMessage(null, 0, message, callback);
  },

  showMessageThenVanish(duration, hideDelay, message, callback) {
    this.showMessage(duration, 0, message, null, hideDelay || 3000, callback);
  },

  showMessage(duration, delay, message, callback, hideDelay, hideCallback) {
    if(this.state.messageShowing) {
      return this.setTimeout(() => {
        this.showMessage(...arguments);
      }, 1000); //quick fix cue solution so previous message can gracefully hide before the next message shows
    }

    if(message) this.setState({message, messageShowing: true});
    else this.setState({messageShowing: true});

    Animated.timing(this.messageOpacity, {
      toValue: 1,
      duration: duration || 400,
      delay: delay || 0,
    }).start(callback);

    if(typeof hideDelay !== 'undefined') {
      setTimeout(() => this.hideMessage(duration, hideCallback), hideDelay);
    }
  },
  hideMessage(duration, callback) {
    Animated.timing(this.messageOpacity, {
      toValue: 0,
      duration: duration ? duration - 100 : 300,
    }).start(() => {
      this.setState({messageShowing: false}, callback);
    });
  }
};

export default function AnimateMessageMixin(Component) {
  class NewComponent extends Timer(Component) {
    constructor(props, context) {
      super(props, context);
      this.state = {
        message: '',
      };

      this.messageOpacity = new Animated.Value(0);
    }
  }

  Object.assign(NewComponent.prototype, methods);

  return NewComponent;
};
