import { StyleSheet, Text, View, TouchableOpacity, PixelRatio,} from 'react-native'
import React, { Component } from 'react';

import Icon from 'react-native-vector-icons/Ionicons';

export default class ActionButton extends Component {
  render() {
    return (
      <View style={[{height: 45, backgroundColor: 'black'}, this.props.touchableStyle]}>
        <TouchableOpacity
          {...this.props}
          underlayColor={this.props.underlayColor}
          activeOpacity={typeof this.props.activeOpacity !== 'undefined' ? this.props.activeOpacity : .5}
          onPress={() => this.props.onPress && this.props.onPress() }
          hitSlop={this.props.hitSlop || {top: 15, left: 15, bottom: 15, right: 15}}
          style={{flex: 1}}>

            <View ref='button' style={[{
                backgroundColor: this.props.backgroundColor || "rgba(38,110,236,1)",
                borderColor: !this.props.backgroundColor ?  "rgba(61, 132, 255, 1)" : this.props.borderColor,
                borderWidth: StyleSheet.hairlineWidth,
                alignItems: 'center',
                height: 45,
                justifyContent: 'center'}, this.props.style]}>

              <View style={{flexDirection: 'row'}}>
                {this.props.icon && <Icon name={this.props.icon} style={[{color: 'white'}, this.props.iconStyle]}/>}
                <Text style={[{fontFamily: 'Montserrat', color: 'white'}, this.props.textStyle]}>{this.props.text || this.props.children || 'SAVE'}</Text>
                {!!this.props.notification &&
                  <View style={{
                      marginLeft: 8,
                      marginTop: -1,
                      width: 30,
                      backgroundColor: 'red',
                      borderRadius: 10,
                      borderWidth: StyleSheet.hairlineWidth,
                      borderColor: 'rgb(255, 87, 87)',
                      overflow: 'hidden',
                  }}>
                    <Text style={{color: 'white', fontSize: 14, fontWeight: 'bold', textAlign: 'center'}}>{this.props.notification}</Text>
                  </View>
                }
              </View>
            </View>
        </TouchableOpacity>
      </View>
    );
  }
}
