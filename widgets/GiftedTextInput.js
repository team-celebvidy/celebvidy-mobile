import { View, Text, SwitchIOS, SliderIOS, SwitchAndroid, Platform, PixelRatio, Dimensions, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import React, { Component } from 'react';

import {GiftedForm, WidgetMixin, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';

import * as hyphenators from '../misc/hyphenator';
var {width, height} = Dimensions.get('window');

export default class GiftedTextInput extends Component {
  //mixins: [WidgetMixin],


  getDefaultProps() {
    return {
      inline: true,
      // @todo type avec suffix Widget pour all
      type: 'TextInputWidget',
      underlined: false,
      onTextInputFocus: (value) => value,
    }
  }

  getInitialState() {
    return {
      focused: false,
    }
  }

  hasValidationError() {
    return this.state.validationErrorMessage !== null && this.state.validationErrorMessage !== '';
  }

  renderValidationError() {
    //if (!(typeof this.state.value === 'undefined' || this.state.value === '') && this.state.validationErrorMessage !== null && this.state.validationErrorMessage !== '') {
    if (this.hasValidationError()) {

      return (
        <GiftedForm.ValidationErrorWidget
          {...this.props}
          message={this.state.validationErrorMessage}
        />
      );
    }
    return null;
  }
  isValid() {
    var validators = null;

    if (this.props.displayValue) {
      // in case of modal widget
      validators = GiftedFormManager.getValidators(this.props.formName, this.props.displayValue);
    } else {
      validators = GiftedFormManager.getValidators(this.props.formName, this.props.name);
    }

    let toValidate = false;
    if (Array.isArray(validators.validate)) {
      if (validators.validate.length > 0) {
        toValidate = true;
      }
    }

    //return this.state.validationErrorMessage === null  && toValidate === true;
    return this.hasValidationError()  && toValidate === true;
  }

  isFilled() {
    //allows client code to disable onTextChange validation attempts
    if(this.props.valid) return false;

    return !(typeof this.state.value === 'undefined' || this.state.value === '');
  }

  renderIcon() {
    if(this.props.editable === false) return null;

    if (this.props.valid === false || this.hasValidationError() && this.props.invalidIcon !== null && this.props.type !== 'OptionWidget') {
      return  <Icon name={this.props.invalidIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconInvalid')]} />;
    } else if (!this.hasValidationError() && this.isFilled() && this.props.validIcon !== null && this.props.type !== 'OptionWidget') {
      return <Icon name={this.props.validIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconValid')]} />;
    } else if (this.props.icon) {
        return <Icon name={this.props.icon} style={this.getStyle('rowIcon')} />;
    }
    return null;
  }

  _renderTitle() {
    if (this.props.title !== '') {
      return (
        <Text
          numberOfLines={1}
          style={this.getStyle(['textInputTitleInline'])}
        >
          {this.props.title}
        </Text>
      );
    }
    return (
      <View style={this.getStyle(['spacer'])}/>
    );
  }

  onChangeText(value) {

    this._onChange(value);
  }

  getValue() {
    return this.state.value;
  }

  _renderRow() {
    return (
      <View ref='rowContainer' style={this.getStyle(['rowContainer'])}>
        <View style={this.getStyle(['row'])}>
          {this.renderIcon()}
          <TouchableOpacity onPress={() => this.props.editable !== false && this.focus()}>
            {this._renderTitle()}
          </TouchableOpacity>

          <TextInput
            ref='input'
            style={this.getStyle(['textInputInline'])}
            placeholderTextColor= "rgba(203,203,203,.6)"
            keyboardAppearance='dark'
            autoCorrect={typeof this.props.autoCorrect !== 'undefined' ? this.props.autoCorrect : true}
            returnKeyType={this.props.returnKeyType || 'next'}
            clearButtonMode='while-editing'

            {...this.props}

            onFocus={this.onFocus}
            onBlur={this.onBlur}

            onChangeText={this.onChangeText}
            value={this.props.format && hyphenators[this.props.format] ? hyphenators[this.props.format](this.state.value) : this.state.value}
            editable={this.props.editable}
          />
        </View>
        {this.renderValidationError()}
        {this._renderUnderline()}
      </View>
    );
  }


  onFocus() {
    this.setState({
      focused: true,
    });

    this.props.onFocus(this.refs.rowContainer);

    let oldText = this.state.value;
    let newText = this.props.onTextInputFocus(this.state.value);
    if (newText !== oldText) {
      this._onChange(newText);
    }
  }

  onBlur() {
    this.setState({
      focused: false,
    });

    this.props.onBlur();
    this.props.onTextInputBlur && this.props.onTextInputBlur(this.state.value);
  }



  _renderUnderline() {
    if (this.props.underlined === true) {
      if (this.props.valid === false || this.hasValidationError() && this.state.focused === true) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineFocusedError'])}
          />
        );
      }
      if (this.props.valid === false || this.hasValidationError()) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineError'])}
          />
        );
      }
      if (this.state.focused === true || this.props.highlighted === true) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineFocused'])}
          />
        );
      }
      return (
        <View
          style={[this.getStyle('underline'), this.getStyle('underlineIdle')]}
        />
      );
    }
    return null;
  }

  render() {
    return this._renderRow();
  }
}
  const styles = StyleSheet.create({
    rowImage: {
      height: 20,
      width: 20,
      marginLeft: 10,
    },
    underline: {
      borderBottomWidth: 1/PixelRatio.get(),
    },
    underlineIdle: {
      borderColor: 'gray',
    },
    underlineFocused: {
      borderColor: '#3498db',
    },
    underlineError: {
      borderColor: 'red',
    },
    underlineFocusedError: {
      borderColor: 'rgb(255, 35, 0)',
    },
    spacer: {
      width: 10,
    },
    rowContainer: {
      backgroundColor: '#FFF',
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: '#c8c7cc',
    },
    row: {
      flexDirection: 'row',
      height: 44,
      alignItems: 'center',
    },
    titleContainer: {
      paddingTop: 10,
      flexDirection: 'row',
      alignItems: 'center',
      // selfAlign: 'center',
      // backgroundColor: '#ff0000',
    },
    textInputInline: {
      fontSize: 15,
      flex: 1,
      height: 40,// @todo should be changed if underlined
      marginTop: 2,
    },
    textInputTitleInline: {
      width: 110,
      fontSize: 15,
      color: '#000',
      paddingLeft: 10,
    },
    textInputTitle: {
      fontSize: 13,
      color: '#333',
      paddingLeft: 10,
      flex: 1
    },
    textInput: {
      fontSize: 15,
      flex: 1,
      height: 40,
      marginLeft: 40,
    },
  });
