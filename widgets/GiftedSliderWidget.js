import { View, Text, Slider, SwitchAndroid, Platform, PixelRatio, Dimensions } from 'react-native';
import React, { Component } from 'react';
import { WidgetMixin } from 'react-native-gifted-form';

var {width, height} = Dimensions.get('window');


module.exports = React.createClass({
  mixins: [WidgetMixin],

  getDefaultProps() {
    return {
      type: 'SliderWidget',
    };
  },

  formattedValue() {
    let value = this.state.value;

    return this.props.formatValue ? this.props.formatValue(value) : value;
  },

  _onValueChange(value) {
      if(this.props.onSliderValueChange) this.props.onSliderValueChange(value);
      this._onChange(value);
  },

  render() {
    return (
      <View style={this.getStyle('rowContainer')}>
        <View style={this.getStyle('row')}>
          {this._renderImage()}

          <Text numberOfLines={1} style={this.getStyle('switchTitle')}>{this.props.title} ({this.formattedValue()})</Text>
          <View style={this.getStyle('switchAlignRight')}>
            <Slider
              ref={(slider) => this.slider = slider}
              style={this.getStyle('slider')}
              {...this.props}

              onValueChange={this._onValueChange}
            />
          </View>
        </View>
        {this._renderValidationError()}
      </View>
    );
  },



  defaultStyles: {
    rowImage: {
      height: 20,
      width: 20,
      marginLeft: 10,
    },
    rowContainer: {
      backgroundColor: '#FFF',
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: '#c8c7cc',
    },
    row: {
      flexDirection: 'row',
      height: 44,
      alignItems: 'center',
    },
    switchTitle: {
      fontSize: 15,
      color: '#000',
      flex: 1,
      paddingLeft: 10,
    },
    switchAlignRight: {
      alignItems: 'flex-end',
      justifyContent: 'center',
      marginRight: 10,
    },
    slider: {
      width: width/2.6,
      height: 10,
      margin: 10,
    },
  },
});
