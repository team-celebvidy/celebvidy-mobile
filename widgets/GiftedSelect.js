
import { View, Text, SwitchIOS, SliderIOS, SwitchAndroid, Platform, PixelRatio, Dimensions, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

import React, { Component } from 'react';

var {width, height} = Dimensions.get('window');

import {GiftedForm, WidgetMixin, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';

import ModalSelect from './ModalSelect';

export default class GiftedSelect extends Component {
  //mixins: [WidgetMixin],

  getDefaultProps() {
    return {
      inline: true,
      // @todo type avec suffix Widget pour all
      type: 'TextInputWidget',
      underlined: false,
      onTextInputFocus: (value) => value,
    }
  }

  getInitialState() {
    let value = this.props.value || this.props.placeholder; //use placeholder as default value
    GiftedFormManager.updateValue(this.props.formName, this.props.name, value); //make it so GiftedFormManager.getValues() will return placeholder values so selects don't require interaction to trigger default value

    return {
      focused: false,
      highlighted: false,
      neverFocused: true,
      value,
    }
  }

  hasValidationError() {
    return this.state.validationErrorMessage !== null && this.state.validationErrorMessage !== '';
  }
  renderValidationError() {
    if (this.hasValidationError()) {

      return (
        <GiftedForm.ValidationErrorWidget
          {...this.props}
          message={this.state.validationErrorMessage}
        />
      );
    }
    return null;
  }
  isValid() {
    return true;
  }
  isFilled() {
    return !(typeof this.state.value === 'undefined' || this.state.value === '');
  }
  renderIcon() {
    if(this.state.neverFocused && !this.props.value) return null;
    if(this.props.editable === false) return null;

    if (this.hasValidationError() && this.props.invalidIcon !== null && this.props.type !== 'OptionWidget') {
      return  <Icon name={this.props.invalidIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconInvalid')]} />;
    } else if (!this.hasValidationError() && this.props.validIcon !== null && this.props.type !== 'OptionWidget') {
      return <Icon name={this.props.validIcon} style={[this.getStyle('rowIcon'), this.getStyle('rowIconValid')]} />;
    } else if (this.props.icon) {
        return <Icon name={this.props.icon} style={this.getStyle('rowIcon')} />;
    }
    return null;
  }
  _renderTitle() {
    if (this.props.title !== '') {
      return (
        <Text
          numberOfLines={1}
          style={this.getStyle(['textInputTitleInline'])}
        >
          {this.props.title}
        </Text>
      );
    }
    return (
      <View style={this.getStyle(['spacer'])}/>
    );
  }

  onChangeText(value) {
    this.props.onTextInputChangeText && this.props.onTextInputChangeText(value);
    this._onChange(value);
  }


  focusInput() {
    this.refs.input.focus();
  }

  _renderRow() {
    return (
      <View ref='rowContainer' style={this.getStyle(['rowContainer'])}>
        <View style={this.getStyle(['row'])}>
          {this.renderIcon()}
          <TouchableOpacity onPress={() => this.refs.input.focus()}>
            {this._renderTitle()}
          </TouchableOpacity>

          <ModalSelect
            ref='input'
            title={this.props.title}
            longTitle={this.props.longTitle}
            editable={this.props.editable}
            returnKeyType={this.props.returnKeyType || 'next'}

            pickers={this.props.pickers}
            selections={this.state.selections || this.props.selections || []}

            pickerBarStyles={this.props.pickerBarStyles || {backgroundColor: 'black'}}

            pickerContainerStyles={this.props.pickerContainerStyles || {backgroundColor: "rgba(0,0,0,0.92)", flexDirection: 'row'}}
            pickerStyles={this.props.pickerStyles || {width: 250, flex: 1}}
            pickerItemStyles={this.props.pickerItemStyles || {fontFamily: 'ProximaNova-Thin'}}

            onReturnKeyKeyPress={this.props.onReturnKeyKeyPress}
            onSubmitEditing={(selections) => {
              let separator = this.props.selectionSeparator || '/';
              let value = selections.join(separator);

              this.onChangeText(value);
              this.setState({selections: selections.slice(0)});

              this.props.onSubmitEditing && this.props.onSubmitEditing(value);
            }}

            onShow={() => {
              this.setState({highlighted: true, neverFocused: false});
              this.onFocus();
              this.props.onShow && this.props.onShow();
            }}
            onHide={(value) => {
              this.setState({highlighted: false});
              this.onBlur();
              this.onBlur();
              this.props.onHide && this.onHide.onShow();
            }}

            onChangeText={(value) => {
              this.onChangeText(value);
            }}
            onChange={(index, key, selections) => {
              let separator = this.props.selectionSeparator || '/';
              let value = selections.join(separator);

              this.onChangeText(value);
              this.setState({selections: selections.slice(0)});
            }}
          >
            <View>
              <Text style={{fontFamily: 'Avenir', fontSize: 16,marginLeft: 5, color: this.state.neverFocused && !this.props.value ? "rgba(203,203,203,.6)" : 'white'}}>
                {this.state.value || this.props.placeholder}
              </Text>
            </View>
          </ModalSelect>
        </View>
        {this.renderValidationError()}
        {this._renderUnderline()}
      </View>
    );
  }


  onFocus() {
    this.setState({
      focused: true,
    });

    this.props.onFocus(this.refs.rowContainer);

    let oldText = this.state.value;
    let newText = this.props.onTextInputFocus(this.state.value);
    if (newText !== oldText) {
      this._onChange(newText);
    }
  }

  onBlur() {
    this.setState({
      focused: false,
    });

    this.props.onBlur();
    this.props.onTextInputBlur && this.props.onTextInputBlur(this.state.value);
  }



  _renderUnderline() {
    if (this.props.underlined === true) {
      if (this.hasValidationError() && this.state.focused === true) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineFocusedError'])}
          />
        );
      }
      if (this.hasValidationError()) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineError'])}
          />
        );
      }
      if (this.state.focused === true || this.props.highlighted === true || this.state.highlighted === true) {
        return (
          <View
            style={this.getStyle(['underline', 'underlineFocused'])}
          />
        );
      }
      return (
        <View
          style={[this.getStyle('underline'), this.getStyle('underlineIdle')]}
        />
      );
    }
    return null;
  }

  render() {
    return this._renderRow();
  }
}
  const styles = StyleSheet.create({
    rowImage: {
      height: 20,
      width: 20,
      marginLeft: 10,
    },
    underline: {
      borderBottomWidth: 1/PixelRatio.get(),
    },
    underlineIdle: {
      borderColor: 'gray',
    },
    underlineFocused: {
      borderColor: '#3498db',
    },
    underlineFocusedError: {
      borderColor: 'rgb(255, 35, 0)',
    },
    underlineError: {
      borderColor: 'red',
    },
    spacer: {
      width: 10,
    },
    rowContainer: {
      backgroundColor: '#FFF',
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: '#c8c7cc',
    },
    row: {
      flexDirection: 'row',
      height: 44,
      alignItems: 'center',
    },
    titleContainer: {
      paddingTop: 10,
      flexDirection: 'row',
      alignItems: 'center',
      // selfAlign: 'center',
      // backgroundColor: '#ff0000',
    },
    textInputInline: {
      fontSize: 15,
      flex: 1,
      height: 40,// @todo should be changed if underlined
      marginTop: 2,
    },
    textInputTitleInline: {
      width: 110,
      fontSize: 15,
      color: '#000',
      paddingLeft: 10,
    },
    textInputTitle: {
      fontSize: 13,
      color: '#333',
      paddingLeft: 10,
      flex: 1
    },
    textInput: {
      fontSize: 15,
      flex: 1,
      height: 40,
      marginLeft: 40,
    },
  },
);
