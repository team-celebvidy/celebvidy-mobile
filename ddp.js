import DDPClient from 'ddp-client';
import hash from 'hash.js';
import _ from 'lodash';
import EJSON from 'ejson';
import { AsyncStorage } from 'react-native';
import Mixpanel from 'react-native-mixpanel';

import config from './config';
import {dispatch} from './configureStore';

const trimString = (str) => str.replace(/^\s+|\s+$/gm,'');


const parentCall = DDPClient.prototype.call;

class DDP extends DDPClient {
  callPromise(methodName, params) {
    params = params || undefined;
    if (params && !_.isArray(params)) {
      console.warn('Params must be passed as an array to ddp.call');
    }

    return new Promise((resolve, reject) => {
      this.call(methodName, params, (error, result) => {
          error ? reject(error) : resolve(result);
        }, () => { } // callback which fires when server has finished -- maybe we need this in the future
      );
    });
  }

  call(...args) {
    let connected = this.session && this.socket && this.socket.readyState === this.socket.OPEN;
    let method = args[0];

    if(!connected) {
      this.connectPromise(method)
        .then(() => parentCall.call(this, ...args));
    }
    else {
      parentCall.call(this, ...args);
    }
  }

  connectPromise(method) {
    return new Promise((resolve, reject) => {
      this.connect((error, wasReconnect) => {
        if(error) {
          console.log('CONNECT ERROR')
          dispatch({type: 'CONNECT', payload: false});
          if(!wasReconnect) reject(error);
        }
        else {
          dispatch({type: 'CONNECT', payload: true});

          if(/login|logout|createUser/.test(method)) { //calling code will execute these methods
            if(wasReconnect) {
              console.log('RECONNECT (using login callback)!');

              this.loginWithTokenPromise() //need to login again after a reconnect here as well below since returning users connect callback is this one (the below one may in fact never be called)
                .catch((error) => console.log('loginWithToken FAILED -- '+(error && error.message))) //likely the user is a guest and its all good (the only public ddp endpoints we have are like celebrityPin)
                .then(() => resolve(wasReconnect));
            }
            else {
              console.log('CONNECT AND LOGIN');
              resolve(wasReconnect);
            }
          }
          else { //this is a reconnection after a disconnect, and we want to insure that if the user has a token they are re-logged-in again
            console.log('RECONNECT (using other method callback)!');

            this.loginWithTokenPromise()
              .catch((error) => console.log('loginWithToken FAILED -- '+(error && error.message))) //likely the user is a guest and its all good (the only public ddp endpoints we have are like celebrityPin)
              .then(() => resolve(wasReconnect));
          }
        }
      });
    });
  }


  loginWithTokenPromise(token=null) {
    if(token) {
      return this._loginWithToken(token);
    }
    else {
      return AsyncStorage.getItem('loginToken')
        .then((token) => {
          if(token) return this._loginWithToken(token);
          else throw new Error('No Login Token Found');
        });
    }
  }

  _loginWithToken(token) {
    return this.callPromise("login", [{resume: token}])
      .then((res) => this._onAuthResponse(null, res))
      .catch((error) => this._onAuthResponse(error));
  }

  signUpWithEmailPromise(email, password, params={}) {
    params = {
      email: trimString(email),
      password: this._sha256(password),
      ...params,
    };


    return this.callPromise('createUser', [params])
      .then((res) => this._onAuthResponse(null, res))
      .catch((error) => this._onAuthResponse(error));
  }

  loginWithTwitterPromise(authData, params) {
    params = {twitter: authData, ...params};

    return this.callPromise('login', [params])
      .then((res) => this._onAuthResponse(null, res))
      .catch((error) => this._onAuthResponse(error));
  }

  loginWithEmailPromise(email, password) {
    let params = {
      user: {
        email: trimString(email)
      },
      password: this._sha256(password)
    };

    return this.callPromise('login', [params])
      .then((res) => this._onAuthResponse(null, res))
      .catch((error) => this._onAuthResponse(error));
  }

  changePasswordPromise(oldPassword, newPassword) {
    return this.callPromise('changePassword', [oldPassword, newPassword])
      .then((res) => {
        if(!res.passwordChanged) throw new Error('Could not change your password');
        return true;
      })
      .catch((error) => {
        throw new Error('Could not change your password');
      });
  }


  subscribePromise(pubName, params=[]) {
    return new Promise((resolve, reject) => {
      let id = this.subscribe(pubName, params, () => {
        resolve(id);
      });
    });
  };

  signUpWithEmail(email, password, params={}, cb) {
    params = {
      email: trimString(email),
      password: this._sha256(password),
      ...params,
    };

    return this.call('createUser', [params], (err, res) => {
      this._onAuthResponse(err, res);
      cb && cb(err, res)
    });
  }

  signUpWithUsername(username, password, cb) {
    let params = {
      username: trimString(username),
      password: this._sha256(password)
    };

    return this.call('createUser', [params], (err, res) => {
      this._onAuthResponse(err, res);
      cb && cb(err, res)
    });
  }

  loginWithEmail(email, password, cb) {
    let params = {
      user: {
        email: trimString(email)
      },
      password: this._sha256(password)
    };

    return this.call("login", [params], (err, res) => {
      this._onAuthResponse(err, res);
      cb && cb(err, res)
    });
  }

  loginWithUsername(username, password, cb) {
    let params = {
      user: {
        username: trimString(username)
      },
      password: this._sha256(password)
    };

    return this.call("login", [params], (err, res) => {
      this._onAuthResponse(err, res);
      cb && cb(err, res)
    });
  }




  loginWithToken(token=null, cb) {
    if(typeof token === 'function') {
      cb = token;
      token = null;
    }

    if (token) {
      return this.call("login", [{resume: token}], cb);
    }

    AsyncStorage.getItem('loginToken')
      .then((token) => {
        if (token) {
          this.call("login", [{resume: token}], cb);
        }
      });
  }

  logoutPromise(cb) {
    return AsyncStorage.multiRemove(['userId', 'loginToken', 'loginTokenExpires']).
      then(() => this.callPromise("logout", []));
  }

  user() {
    return AsyncStorage.getItem('userId')
      .then((userId) => {
        if (userId) {
          this.collections = this.collections || {};
          this.collections.users = this.collections.users || {};
          return this.collections.users[userId];
        } else {
          return null;
        }
      });
  }


  /** INTERNALLY USED METHODS **/
  _onAuthResponse(err, res) {
    if (res) {
      let { id, token, tokenExpires } = res;

      AsyncStorage.setItem('userId', id.toString());
      AsyncStorage.setItem('loginToken', token.toString());
      AsyncStorage.setItem('loginTokenExpires', tokenExpires.toString());

      return res; //res response for use by promises
    } else {
      AsyncStorage.multiRemove(['userId', 'loginToken', 'loginTokenExpires']);
      throw err;
    }
  }

  _sha256(password) {
    return {
      digest: hash.sha256().update(password).digest('hex'),
      algorithm: "sha-256"
    };
  }
}



let ddpClient;

export default function() {
  return ddpClient = ddpClient || new DDP(config.ddpConfig);
};
