import {Dimensions} from 'react-native';
import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as actions from '../actions';

import TooltipOverlay from '../widgets/TooltipOverlay';

const {width, height} = Dimensions.get('window');

import {Timer} from 'react-timer-mixin';

class TourTooltip extends Timer(Component) {    //check later
  render() {
    let {dispatch, tooltipStep} = this.props;
    let onPress;
    let onAppear;
    let caption;

    if(!tooltipStep && tooltipStep !== 0) return null;


    switch(this.props.tooltipStep) {
      case 0:
        return <TooltipOverlay side='right' placement='start' top={height + 100} left={width/2 - 280/2 - 5}   />
      case 1:
        onPress = () => {
          dispatch({type: 'TOOLTIP_STEP', payload: 2});
          this.setTimeout(() => dispatch(actions.toggleDrawer(false)), 500);
          this.setTimeout(() => dispatch({type: 'TOOLTIP_STEP', payload: 3}), 1600);
        };

        if(this.props.finishLater) {
          caption = 'Your account is almost setup. It will need to be completed to appear in the directory. For now, you can Tap this to switch to the main Celebvidy panel.';
        }
        else {
          caption = 'Nice work! Your account is setup. You can Tap this to switch to the main Celebvidy panel.';
        }

        return <TooltipOverlay side='right' placement='start' top={5} left={width/2 - 280/2 - 5}  onPress={onPress} caption={caption} />
      case 2:
        return <TooltipOverlay vanishDirection='bottom' top={10} left={width/2 - 280/2 + 20} />
      case 3:
        onPress = () => {
          dispatch({type: 'TOOLTIP_STEP', payload: 4});
          this.setTimeout(() => dispatch(actions.goToPage(1)), 100);
          this.setTimeout(() => dispatch({type: 'TOOLTIP_STEP', payload: 5}), 600);
        };
        caption = 'This is your profile! Tap this or swipe right to view celebvidys ordered by your customers.'

        return <TooltipOverlay side='bottom' placement='middle' top={height - 290} left={width/2 - 280/2} onPress={onPress} caption={caption} />
      case 4:
        return <TooltipOverlay vanishDirection='top' top={height - 290} left={width/2 - 280/2} />
      case 5:
        onPress = () => {
          dispatch({type: 'TOOLTIP_STEP', payload: 6});
          this.props.showLogoAnimation();
          this.setTimeout(() => dispatch(actions.goToPage(2)), 300);
          this.setTimeout(() => dispatch({type: 'TOOLTIP_STEP', payload: 7}), 2250);
        };
        caption = "Your celebvidy requests will appear here. We suggest in the meantime you checkout what other celebrities are doing. Swipe right to do so."

        return <TooltipOverlay hideArrow={true} top={height - 467} left={width/2 - 280/2} onPress={onPress} caption={caption} buttonText='COOL' />
      case 6:
        return <TooltipOverlay vanishDirection='left' top={height - 490} left={width/2 - 280/2} />
      case 7:
        onPress = () => {
          dispatch({type: 'TOOLTIP_STEP', payload: 8});
        };
        caption = "Congratulations, you are all setup! Don't forget to Tweet and share Celebvidy with your fans."

        return <TooltipOverlay side='bottom' placement='end' top={height - 250} left={width - 280 - 5} onPress={onPress} caption={caption} buttonText='OK' />
      case 8:
        onAppear = () => dispatch({type: 'TOOLTIP_STEPS_COMPLETE'});
        return <TooltipOverlay vanishDirection='top' top={-height} left={width/2 - 280/2} onAppear={onAppear} duration={750} />
      default:
        return null;
    }
  }
}


export default connect(
  ({tooltipStep, finishLater}) => ({tooltipStep, finishLater})
)(TourTooltip);
