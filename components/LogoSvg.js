import {View, Text, Image, Animated, Dimensions} from 'react-native';
import React, { Component } from 'react';
import Svg, {Circle, Path} from 'react-native-svg';
const {width, height} = Dimensions.get('window');
const AnimatedPath = Animated.createAnimatedComponent(Path);

export default class LogoSvg extends Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    let {size, color, strokeWidth, fill, style, hideCircle, twinkleAnimation, opacity} = this.props;

    let starOpacity;
    let scale

    /**
    if(twinkleAnimation) {
      starOpacity = this.props.twinkleAnimation.interpolate({
        inputRange:  [0, 2.25,  3.25,  4,  4.75,  5.5, 11],
        outputRange: [1, 1,  .4, 1, .4,  1, .1],
        extrapolate: 'clamp',
      });
    }
    else if(this.props.animation) {
      starOpacity = this.props.animation.interpolate({
        inputRange:  [0, 18.0,  19.0,  20.0,   21.0,  22],
        outputRange: [1, 1,     .5,     1,     .5,    1],
        extrapolate: 'clamp',
      });
    }
    **/

    if(this.props.animation) {
      scale = this.props.animation.interpolate({
        inputRange: [0,      11, 13.25,      17.2],
        outputRange: [1.6,   1.75, 1.875,      1.6],
        extrapolate: 'clamp',
      });
    }


    return (
      <Animated.View style={style}>
        {!hideCircle
          ? <Svg height={size} width={size} style={{backgroundColor: 'transparent'}}>
              <Circle
                cx={size/2}
                cy={size/2}
                r={size/2 - 5}
                stroke={color}
                strokeWidth={strokeWidth}
                fillOpacity={0}
              />
            </Svg>
          : <View style={{width: size, height: size}} />
        }

        <View style={{position: 'absolute', width: size, top: size/5 + 5, left: size/5 + 15}}>
          <Svg height={size} width={size} style={{backgroundColor: 'transparent'}}>
            <Path fill={color} scale={1.6} d="M94.5276,57.3357l-69.9148,45.053a3.5,3.5,0,0,1-4.8685-.9186q-0.041-.06-0.0794-0.1218a3.7778,3.7778,0,0,1-.5245-1.3376v-36.75L40.1494,78.2719a3.0754,3.0754,0,0,0,1.91.7233,2.6537,2.6537,0,0,0,2.6595-2.648q0-.1029-0.0074-0.2056a7.5079,7.5079,0,0,0-.4057-1.4962l-9.51-22.1946,28.342-24.0573,31.39,20.2129a5.0964,5.0964,0,0,1,1.8505,6.807,4.9514,4.9514,0,0,1-1.8505,1.9222h0Z" />
            <Path fill={color} scale={1.6} d="M19.2294,17.6134V6.0306c0-4.2507,4.661-2.9725,5.4824-2.378l31.8748,20.51-31.2712,9.611S20.3971,20.467,19.2294,17.6134Z"  />
            <AnimatedPath fill={color} fillOpacity={starOpacity} scale={scale || 1.6}  d="M38.56,34.77a0.8841,0.8841,0,0,1,.541.8366,0.7038,0.7038,0,0,1-.2974.5752L23.3362,49.201l4.3443,10.0074a1.3978,1.3978,0,0,1,.1484.2972,1.3,1.3,0,0,1,0,.3765,1.2664,1.2664,0,0,1-2.0781.9908L15.6273,53.7093l-7.58,4.35a1.2554,1.2554,0,0,1-.4849,0,1.2674,1.2674,0,0,1-1.2766-1.2583q0-.01,0-0.02a1.4285,1.4285,0,0,1,0-.4459C7.3444,53.3625,8.7,49.4785,8.7,49.3992L3.5344,46.6546A0.743,0.743,0,0,1,3,45.9313a0.8222,0.8222,0,0,1,.6234-0.7728l8.1642-2.9725,3.0183-8.5013a0.8216,0.8216,0,0,1,.7224-0.5747,0.7522,0.7522,0,0,1,.7521.5549l2.563,6.9358s18.9073-5.7641,19.05-5.8063A1.15,1.15,0,0,1,38.56,34.77Z"/>
          </Svg>
        </View>
      </Animated.View>
    );
  }
}


export class LogoStarSvg extends React.Component {
  render() {
    let {size, color, style, stroke, strokeWidth} = this.props;

    return (
      <Svg height={size} width={size} style={{backgroundColor: 'transparent'}}>
        <Path fill={color} stroke={stroke} strokeWidth={strokeWidth} d="M94.5276,57.3357l-69.9148,45.053a3.5,3.5,0,0,1-4.8685-.9186q-0.041-.06-0.0794-0.1218a3.7778,3.7778,0,0,1-.5245-1.3376v-36.75L40.1494,78.2719a3.0754,3.0754,0,0,0,1.91.7233,2.6537,2.6537,0,0,0,2.6595-2.648q0-.1029-0.0074-0.2056a7.5079,7.5079,0,0,0-.4057-1.4962l-9.51-22.1946,28.342-24.0573,31.39,20.2129a5.0964,5.0964,0,0,1,1.8505,6.807,4.9514,4.9514,0,0,1-1.8505,1.9222h0Z" />
        <Path fill={color} stroke={stroke} strokeWidth={strokeWidth} d="M19.2294,17.6134V6.0306c0-4.2507,4.661-2.9725,5.4824-2.378l31.8748,20.51-31.2712,9.611S20.3971,20.467,19.2294,17.6134Z" transform="translate(-2 -2)" />
        <Path fill={color} stroke={stroke} strokeWidth={strokeWidth} d="M38.56,34.77a0.8841,0.8841,0,0,1,.541.8366,0.7038,0.7038,0,0,1-.2974.5752L23.3362,49.201l4.3443,10.0074a1.3978,1.3978,0,0,1,.1484.2972,1.3,1.3,0,0,1,0,.3765,1.2664,1.2664,0,0,1-2.0781.9908L15.6273,53.7093l-7.58,4.35a1.2554,1.2554,0,0,1-.4849,0,1.2674,1.2674,0,0,1-1.2766-1.2583q0-.01,0-0.02a1.4285,1.4285,0,0,1,0-.4459C7.3444,53.3625,8.7,49.4785,8.7,49.3992L3.5344,46.6546A0.743,0.743,0,0,1,3,45.9313a0.8222,0.8222,0,0,1,.6234-0.7728l8.1642-2.9725,3.0183-8.5013a0.8216,0.8216,0,0,1,.7224-0.5747,0.7522,0.7522,0,0,1,.7521.5549l2.563,6.9358s18.9073-5.7641,19.05-5.8063A1.15,1.15,0,0,1,38.56,34.77Z"/>
      </Svg>
    );
  }
}



export class LogoText extends React.Component {
  render() {
    let {fontSize, color, tmTop, vidySize, vidyTop, style} = this.props;

    return (
      <Animated.View style={[{flexDirection: 'row', justifyContent: 'center'}, style]}>
        <Text style={{color, fontSize, fontFamily: 'Acrom'}}>celeb</Text>
        <Text style={{color, fontSize: vidySize || fontSize+1, top: vidyTop || 0, fontFamily: 'Acrom-Bold'}}>vidy</Text>
        <Svg height={15} width={20} style={{top: tmTop || 13, left: -2}}>
          <Path fill={color}  d="M15.9664,2.0721h-2.12L12.2294,6.3615,10.6136,2.0721H8.4781v6.52H9.9855V3.972h0.0314l1.6644,4.6206h1.0819L14.4276,3.972H14.459V8.5926h1.5074v-6.52h0Zm-8.32,0H2.3716V3.3383H4.2088V8.5926H5.81V3.3383H7.646V2.0721h0Z" />
        </Svg>
      </Animated.View>
    );
  }
}
