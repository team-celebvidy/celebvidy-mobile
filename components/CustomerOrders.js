import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions} from 'react-native';
import React, { Component } from 'react';
import {Timer} from 'react-timer-mixin';
import GiftedListView from 'react-native-gifted-listview';
import Button from 'react-native-button';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Gradient from 'react-native-linear-gradient';
import _ from 'underscore';
import Mixpanel from 'react-native-mixpanel';

import UltimateListView from '../widgets/UltimateListView';
import CustomerOrderRow from './CustomerOrderRow';


const {height, width} = Dimensions.get('window');

import {getCustomerOrders} from '../actions';
import {getState} from '../configureStore';
import {connect} from 'react-redux';


class CustomerOrders extends Timer(Component) {  //check later
  componentDidMount() {
    this.notificationPushOrder(this.props.pushedOrderId);
  }
  componentWillReceiveProps({pushedOrderId}) {
    if(pushedOrderId !== this.props.pushedOrderId) {
      this.notificationPushOrder(pushedOrderId);
      return false;
    }

    return true;
  }
  
  notificationPushOrder(pushedOrderId) {
    let {dispatch, navigator} = this.props;

    if(pushedOrderId) {
      if(!getState().isCelebrity) {
        navigator.popToTop(); //CelebrityAccount navigator handles this for celebs
      }

      setTimeout(() => {
        dispatch({type: 'OPENED_PUSH_NOTIFICATION_ORDER'});
      }, 1000);

      setTimeout(() => {
        dispatch({type: 'OPENED_PUSH_NOTIFICATION_ORDER'});

        let order = _.find(this.props.customerOrders, (order) => order._id === pushedOrderId);

        if(!order) {//just to be safe we dont push an order we dont actually have (such as when during testing we are swithing between accounts that may not have the order)
          alert('No request was found. Perhaps you are not logged into the account for whom the request was for.');
          return;
        }

        this.props.dispatch({type: 'CLEAR_CURRENT_ORDER'});
        setTimeout(() => navigator.pushWithProps('Order', {order}), 150);
        setTimeout(() => Mixpanel.trackWithProperties('Customer View Order', order), 2000)
      }, 2000);
    }
  }

  _onFetch(page = 1, callback, options) {
    this.props.dispatch((dispatch, getState) => {
      let state = getState();

      if(!state.connected || (!state.user || !state.user.loggedIn)) { //GiftedListView expects some rows to stop the spinner
        setTimeout(() => {
          dispatch({
            type: 'CUSTOMER_ORDERS',
            payload: {page: 1, rows: []},
          });
        }, 2000);
        return Promise.resolve();
      }
      else return this.props.dispatch(getCustomerOrders({page, ...options}))
    })
      .catch((error) => {
        alert('We had a problem loading your account. Please login again.');

        setTimeout(() => {
          this.props.dispatch({type: "LOGOUT"});
        }, 3000);
      });
  }

  refresh() {
    this.listview.refresh();
  }

  render() {
    let navigator = this.props.navigator;

    return (
      <View style={styles.container}>
        <UltimateListView
          ref={(listview) => this.listview = listview}
          rowView={(data, sectionId, rowIndex) => this._renderRowView(data, rowIndex)}
          onFetch={this._onFetch.bind(this)}
          height={this.props.offset ? height - 64 : height - 64 - 52}
          offset={this.props.offset}
          //DY={115}
          rows={this.props.customerOrders}
          fetchOptions={this.props.customerOrdersFetchOptions}
          isCustomer={!this.props.offset}
          goToPage={this.props.goToPage}
          emptyMessage='You have no celebvidys yet'
          noFlashing={true}
        />

        <Gradient
          locations={[0, .15, .3]}
          colors={['rgba(14,13,13,1)', 'rgba(2, 5, 10, 0.3)', 'rgba(14, 32, 65, 0.0)']}
          style={[styles.linearGradient, {top: 0}]}
        />
      </View>
    );
  }

  _renderRowView(order, index) {
    let rowStyle = this.props.offset ? {width: width - this.props.offset} : {width};

    return <CustomerOrderRow {...this.props} order={order} index={index} rowStyle={rowStyle} />
  }
}

export default connect(({receivedCelebvidys, customerOrders, customerOrdersFetchOptions, pushedOrderId}) => {
  //there's a bug that this component only squeaks by without causing, which is the fact that UltimateListView and GiftedListView
  //compare customerOrders REFERENCES in shouldComponentUpdate, which will always be different unfortunately thanks to the below line.
  //The only reason this is ok is because we always change the fetchOptions to refresh it first and
  //because there aren't many other props that will trigger updates of this component

  //customerOrders = receivedCelebvidys.concat(customerOrders); //prepend any shared celebvidies, which are stored only locally on this device (that means if the user logs into their account on another phone they wont be there, which is good enough since they are just shared celebvidies)
  return {customerOrders, customerOrdersFetchOptions, pushedOrderId};
})(CustomerOrders)


const styles = StyleSheet.create({
  linearGradient: {
    width,
    height: 60,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  completedOverlay: {
    width,
    height: 170,
    position: 'absolute',
    bottom: 0,
    top: 0,
    backgroundColor: "rgba(0,0,0,0.54)",
  },
  container: {
    flex: 1,
  },
  navBar: {
    height: 64,
  },
  pageTitle: {
    fontSize: 18,
    fontFamily: 'ProximaNova-Thin',
    lineHeight: 19,
    color: 'white',
    marginTop: 14,
    marginLeft: 10,
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -2*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor:  "rgba(13,50,246,0.6)" ,
  },
  segmentControl: {
    backgroundColor: 'black',
    flex: 0,
    height: 35,
    //borderBottomWidth: 1*StyleSheet.hairlineWidth,
    //borderBottomColor:  'rgba(27,58,233,.2)' ,
  },
});
