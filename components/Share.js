import {View, ScrollView, Text, TouchableHighlight, TouchableWithoutFeedback,  StyleSheet, Dimensions, PixelRatio, Clipboard, TextInput} from 'react-native';
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

import Icon from 'react-native-vector-icons/Ionicons';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import Mixpanel from 'react-native-mixpanel';

import ActionButton from '../widgets/ActionButton';
import Separator from '../widgets/Separator';
import _ from 'underscore';

import tweet from '../misc/tweet';
import share from '../misc/share';

const {width, height} = Dimensions.get('window');

let copyUrl = 'http://www.celebvidy.com';

import {connect} from 'react-redux';


class Share extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      value: props.isCelebrity ? 'Want a personalized video from me? Check me out: '+this.getUrl() : 'Checkout Celebvidy for ON-DEMAND SHOUT OUTS: '+this.getUrl(),
    };
  }
  componentWillReceiveProps({isCelebrity, slug}) {
    if(isCelebrity !== this.props.isCelebrity || this.props.slug !== slug) {
      let value = isCelebrity ? 'Want a personalized video from me? Check me out: '+this.getUrl() : 'Checkout Celebvidy for ON-DEMAND SHOUT OUTS: '+this.getUrl();
      this.setState({value});
    }
  }
  charsRemaining() {
    return 106 - (this.state.value.length || 0);
  }
  copyToClipboard() {
    let link = GiftedFormManager.getValue('shareForm', 'text');
    Clipboard.setString(link);
  }
  copyPasteUrl() {
    Clipboard.setString(this.getUrl());
  }
  getUrl() {
    return this.props.isCelebrity ? copyUrl+(this.props.slug ? '/c/'+this.props.slug : '') : copyUrl;
  }


  render() {
    return (
      <TouchableWithoutFeedback onPress={() => dismissKeyboard()}>
        <View style={{padding: 10, height}}>
          <View style={{backgroundColor: 'transparent', height: 64, paddingTop: 20}}>
            <Text style={[{color: "white", fontSize: 22, fontFamily: 'ProximaNova-Thin', textAlign: 'center'}]}>Share Celebvidy</Text>
          </View>

          <View style={{height: 20}} />

          <View style={styles.copyContainer}>

            <TextInput
              returnKeyType={'done'}
              selectionColor={"rgba(255,255,255,0.44)"}
              autoCorrect={false}
              keyboardAppearance={'dark'}
              selectTextOnFocus={true}
              style={{
                color: 'white', fontSize: 18, fontFamily: 'ProximaNova-Thin', width, height: 20, overflow: 'hidden',
                textShadowColor: 'rgba(19, 21, 249, .9)', textShadowRadius: 1, textShadowOffset: {width: 1, height: 1}
              }}
              value={this.getUrl()}
              onBlur={() => dismissKeyboard()}
            />

            <View style={{position: 'absolute', right: 1/PixelRatio.get(), top: 1, width: 90, height: 42 + PixelRatio.get(), backgroundColor: 'black'}}>
              <TouchableHighlight underlayColor={'rgb(13, 18, 140)'} onPress={() => this.copyPasteUrl()} style={{height: 43}}>
                <Text style={styles.copyText}>COPY</Text>
              </TouchableHighlight>
            </View>
          </View>


          <View style={{backgroundColor: 'transparent', height: 64, paddingTop: 50}}>
            <Text style={[{color: 'white', fontSize: 22, fontFamily: 'ProximaNova-Thin', textAlign: 'center'}]}>Tweet/Post/Send Celebvidy</Text>
          </View>

          <View style={{height: 50}} />

          <TextInput
            ref='tweetBox'
            name='text'
            multiline={true}
            returnKeyType={'default'}
            selectionColor={"rgba(255,255,255,0.44)"}
            autoFocus={false}
            keyboardAppearance={'dark'}
            autoCorrect={false}
            placeholder='Share something about Celebvidy'
            placeholderTextColor="rgba(203,203,203,.4)"
            style={styles.textArea}
            value={this.state.value}
            onBlur={() => dismissKeyboard()}
            onChangeText={(value) => {
              this.setState({value});
            }}
          />

        <Text style={[styles.characterCount, {color: this.charsRemaining() < 0 ? 'red' : 'white'}]}>{this.charsRemaining()} remaining</Text>


          <View style={{position: 'absolute', bottom: 70, width: width - 20}} >
            <ActionButton
              onPress={() => {
                let event = this.props.isCelebrity ? 'Celebrity Tweet' : 'Customer Tweet';
                Mixpanel.trackWithProperties(event, {message: this.state.value});

                tweet(this.state.value);
              }}
              text='TWEET' style={{height: 50, backgroundColor: 'rgb(19, 78, 182)'}}
              textStyle={{fontSize: 20}}
            />

            <View style={{height: 20}} />

            <TouchableHighlight underlayColor='rgba(85,84,84,1)' onPress={() => {
                  let event = this.props.isCelebrity ? 'Celebrity Share' : 'Customer Share';
                  Mixpanel.trackWithProperties(event, {message: this.state.value});

                  share(this.state.value);
              }}>
              <View style={{height: 50, borderWidth: 1, borderColor: "rgba(85,84,84,1)", paddingTop: 12}}>
                <Text style={{textAlign: 'center', color: "rgba(151,149,149,1)", fontFamily: 'Proxima Nova', fontSize: 20}}>SHARE</Text>
              </View>
            </TouchableHighlight>
          </View>

        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default connect(
  ({isCelebrity, celebrity}) => ({isCelebrity, slug: celebrity.slug}),
  null, null, {withRef: true}
)(Share)


const styles = StyleSheet.create({
  copyContainer: {
    backgroundColor: '#111010',
    height: 45,
    borderWidth: 1/PixelRatio.get(),
    borderColor: 'rgb(30, 33, 38)',
    padding: 13,
    paddingLeft: 8,
    overflow: 'hidden'
  },
  copyText: {
    color: 'white',
    padding: 12,
    textAlign: 'center',
    textShadowColor: 'rgba(27, 107, 199, .6)',
    textShadowRadius: 1,
    textShadowOffset: {width: 1, height:1}
  },
  textArea: {
    color: "rgba(239,236,236,1)",
    fontFamily: 'ProximaNova-Thin',
    fontSize: 16,
    lineHeight: 29,

    backgroundColor: "rgba(20,20,21,.85)",
    borderColor: "rgba(30,30,30,1)",
    borderWidth: 1/PixelRatio.get(),
    height: 175,
    padding: 10
  },
  characterCount: {
    height: 30,
    color: 'white',
    textAlign: 'right',
    marginTop: 7,
    fontFamily: 'ProximaNova-Thin',
    backgroundColor: 'transparent'
  }
});
