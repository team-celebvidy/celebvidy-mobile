import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, TouchableOpacity, ScrollView, StatusBar} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';

import formStyles from '../styles/FormStyles';
import AccountItem from '../widgets/AccountItem';
import NavButton from '../widgets/NavButton';

var {width, height} = Dimensions.get('window');

import * as actions from '../actions';
import { connect } from 'react-redux';


import {CELEBRITY_FAQS, CUSTOMER_FAQS} from '../misc/faqs';


class Faq extends Component {
  render() {
    return this.props.isCelebrity ? this.renderQuestions(CELEBRITY_FAQS) : this.renderQuestions(CUSTOMER_FAQS);
  }

  renderQuestions(faqs) {
   let {navigator} = this.props;

   return (
     <ScrollView style={[formStyles.giftedForm, {flex: 1, paddingHorizontal: 20, alignSelf: 'stretch', backgroundColor: 'transparent'}]}>
       <View style={{height: 35}} />

       {faqs.map((faq, i) => {
         return (
           <AccountItem key={i} textStyle={{paddingRight: 10}} showDisclosure={true} overline={i===0} underline={true} text={faq.question} onPress={() => navigator.pushWithProps('FaqAnswer', {faq})} />
         );
       })}

       <View style={{height: 30}} />
     </ScrollView>
   );
  }
}

export default connect(({isCelebrity}) => ({isCelebrity}))(Faq)



export class FaqAnswer extends React.Component {
  render() {
   let {faq: {question, answer}} = this.props;

   return (
     <ScrollView style={[formStyles.giftedForm, {flex: 1, paddingHorizontal: 20, alignSelf: 'stretch', backgroundColor: 'transparent'}]}>
       <View style={{height: 30}} />

       <Text style={{fontFamily: 'Code Light', fontSize: 24, letterSpacing: 1.2, color: 'rgb(100,100,100)', textShadowColor: 'rgb(200,200,200)', textShadowRadius: 1, textShadowOffset: {width: -StyleSheet.hairlineWidth, height: StyleSheet.hairlineWidth}}}>
         {question}
       </Text>
       <View style={{flex: 1, height: StyleSheet.hairlineWidth, backgroundColor: 'rgba(193,193,193,0.32)', marginTop: 3, marginBottom: 25}}/>

       <Text style={{fontFamily: 'STHeitiSC-Light', fontSize: 16, lineHeight: 21, color: 'rgb(180,180,180)', textAlign: 'justify'}}>
         {answer}
       </Text>
     </ScrollView>
   );
  }
}
