import {
  View,
  Dimensions,
  InteractionManager,
  Animated,
  Easing,
  Alert,
  AsyncStorage,
  StatusBar as SB
} from 'react-native';
import React, { Component } from 'react';
import SplashScreen from '@remobile/react-native-splashscreen';
import FlipView from 'react-native-flip-view';

const {width, height} = Dimensions.get('window');

import AppIntroTourNavigator from './AppIntroTourNavigator';
import Panel from './Panel';
import TourTooltip from './TourTooltip'
import LogoAnimation from './LogoAnimation/LogoAnimation';
import LogoAnimationProgress from './LogoAnimationProgress';

import {setImmediateAnimationFrame} from '../misc/helpers';
import DeveloperMenu from '../widgets/DeveloperMenu';
import StatusBar from '../widgets/StatusBar';;

import * as actions from '../actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ddpClient from '../ddp';


import Mixpanel from 'react-native-mixpanel';
import codePush from "react-native-code-push";
import {listenToPushNotifications} from '../misc/PushNotifications';
import {listenToIncomingLinks} from '../misc/Linking';

if(__DEV__) {
  window.ALERT = Alert;
  window.ddp = ddpClient();
  window.form = require('react-native-gifted-form').GiftedFormManager;
  window.actions = require('../actions');
  window.getStore = require('../configureStore').getStore;
  window.dispatch = require('../configureStore').dispatch;
  window.getState = require('../configureStore').getState;
}



class MainContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      animationComplete: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.appStateLoaded && !this.props.appStateLoaded) {
      this.startApp();
    }
  }


  componentWillMount() {
    if(this.props.appStateLoaded) {
      this.startApp();
    }
  }
  componentDidMount() {
    if(!__DEV__) {
      this.handleCodePushUpdates();
    }

    Mixpanel.sharedInstanceWithToken('ddf439aed91d39ac3a52be1cda8a241a');
  }
  handleCodePushUpdates() {
    codePush.sync({
      installMode: codePush.InstallMode.ON_NEXT_RESUME,
      mandatoryInstallMode: codePush.InstallMode.ON_NEXT_RESUME, //PUT BACK AT SOME POINT: IMMEDIATE
      minimumBackgroundDuration: 5,
      //deploymentKey, //put in once doing dynamic deployments
    }, (status) => {
      if(status === codePush.SyncStatus.UPDATE_INSTALLED) {
        this.props.dispatch({type: 'UPDATE_INSTALLED'});
        //alert('There is a NEW app update. It will be available on next restart of the app. Remove the app from memory or close it and wait 5 seconds ;)');
      }
      else if(status === codePush.SyncStatus.UP_TO_DATE) {
        this.props.dispatch((dispatch, getState) => {
          let {appUpToDate} = getState();

          if(!appUpToDate) {
            dispatch({type: 'UP_TO_DATE'});

            //setTimeout(() => {
              //alert('UPDATE INSTALLED! note: these messages will not show once live and it will be seamless to the end user :) James ');
            //}, 4000);
          }
        });
      }
    });
  }

  startApp() {
    this.props.loginWithToken() //won't actually connect if there is no token stored in redux storage to limit traffic from millions of window-shoppers
      .then(() => SplashScreen.hide())
      .then(() => listenToIncomingLinks())
      .then(() => listenToPushNotifications())
      .then(() => this.setLastLogin());
  }

  setLastLogin() {
    this.props.dispatch((dispatch, getState) => {
      let lastLogin = getState().lastLogin;
      this.deleteOldCelebvidies(lastLogin); //delete old celebvidies based on lastLogin time before updating time
      dispatch({type: 'LAST_LOGIN', payload: new Date});
    });
  }
  deleteOldCelebvidies(lastLogin) {
    if(lastLogin && lastLogin < (new Date) - 1000 * 60 * 60 * 4) { //last login was longer than 4 hours ago
      RNFS.unlink(RNFS.DocumentDirectoryPath + '/celebvidies'); //wait 4 hours before deleting so videos can still be served from local file storage until Youtube processing is complete
    }
  }

  render() {
    let animationVisible = this.shouldShowIntroAnimation();

    return (
      <View style={{backgroundColor: 'black', position: 'absolute', top: 0, right: 0, left: 0, bottom: 0}}>
        <SB hidden={true} animated={true} barStyle={'default'} />

        {this.renderMainApp()}

        <LogoAnimation
          visible={animationVisible}
          handle={InteractionManager.createInteractionHandle()}
          onStart={() => this.setAnimationComplete()}
          delay={0}
          //onComplete={() => this.setState({animationComplete: true})}
         />
      </View>
    );
  }

  isReady() {
    let {appStateLoaded, appReady} = this.props;
    return appStateLoaded && appReady;
  }
  isGuest() {
    let {appReady} = this.props;
    return !/loggedInUser|logout|loginFailed/.test(appReady);
  }
  shouldShowIntroAnimation() {
    return this.isReady() && this.isGuest() && this.props.showIntroAnimation; //showIntroAnimation is simply a developer toggle to skip the animation
  }

  setAnimationComplete() {
    setImmediateAnimationFrame(() => {
      this.setState({animationComplete: true});
    }, 8*1000 + 500); //mark animation complete before it in fact is so main app renders behind it and is ready to show when the animation fades away
  }

  shouldRenderMainApp() {
    let {appStateLoaded, appReady, showIntroAnimation} = this.props;
    let {animationComplete} = this.state;

    if(!animationComplete) {            //1. intro animation *possibly* showing
      if(this.isReady()) {
        if(!showIntroAnimation) {
          return true;                  //1a. developers wanna skip intro animation (via redux setting)
        }
        else {
          if(!this.isGuest()) {
            return true;                //1b. returning users skip animation
          }
          else return false;            //1c. guests see intro animation, yay! (i.e. main app is hidden until 2.)
        }
      }
    }
    else return true;                   //2. if animation was showing, on animation completion, setState({animationComplete: true}) is called and now we render the main app regardless
  }

  renderMainApp() {
    if(!this.shouldRenderMainApp()) {
      return null;
    }

    return (
      <View style={{width, height, backgroundColor: 'black'}}>
        <StatusBar />

        <FlipView
          ref='flipview'
          style={{flex: 1}}
          front={this.renderFront()}
          back={this.renderBack()}
          isFlipped={!this.props.showTour}
          onFlip={this.onFlip.bind(this)}
          onFlipped={this.onFlipped.bind(this)}
          flipAxis="y"
          flipEasing={Easing.inOut(Easing.exp)}
          flipDuration={3000}
          perspective={500}
        />

        {false && __DEV__ && <DeveloperMenu height={50} />}
        <LogoAnimationProgress handle={InteractionManager.createInteractionHandle()} />

        <TourTooltip showLogoAnimation={(callback) => this.props.dispatch(actions.gratify({callback, darkenBackground: true}))} />
      </View>
    );
  }
  renderFront() {
    return <AppIntroTourNavigator />;
  }
  renderBack() {
    return <Panel />;
  }
  onFlip() {
    if(this.props.showTour) this.props.unclipTour();
    else this.props.unclipPanel();
    this.handle = InteractionManager.createInteractionHandle();
  }
  onFlipped() {
    if(!this.props.showTour) this.props.clipTour();
    else this.props.clipPanel();
    InteractionManager.clearInteractionHandle(this.handle);
  }
}

export default connect(
  ({appStateLoaded, appReady, showTour, showIntroAnimation}) => ({appStateLoaded, appReady, showTour, showIntroAnimation}),
  (dispatch) => {
    return {
      dispatch,
      ...bindActionCreators(actions, dispatch),
    }
  }
)(MainContainer);
