import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import Celebrity from './Celebrity';

import * as types from '../actionTypes';
import { connect } from 'react-redux';

let lastPressed;
let Hamburger = function(props) {
  return (
    <TouchableOpacity
      hitSlop={{top: 80, left: 80, bottom: 80, right: 80}}
      onPress={props.toggleDrawer}
    >
      <View style={styles.hamburgerView}>
        <Icon name='navicon-round' size={30} color="rgb(10, 40, 230)" style={styles.hamburgerIcon} />
      </View>
    </TouchableOpacity>
  );
}

//we of course dont risk even the smallest re-rendering messing up animations by passing the `showDrawer` state to Hamburger,
//so we use a thunk which receives getState. This is the idiomatic way to do such things.
Hamburger = connect(null, (dispatch) => {
  return {
    toggleDrawer: () => {
      if(new Date - lastPressed < 1000) return; //annoying
      lastPressed = new Date;

      dispatch((dispatch, getState) => {
        let type = getState().showDrawer ? types.HIDE_DRAWER : types.SHOW_DRAWER;
        dispatch({type});
      });
    }
  }
})(Hamburger);


class CelebrityOwnProfile extends Component {
  render() {
    return (
      <Celebrity
        celeb={this.props.celebrity}
        isOwnProfile={true}
        goToPage={this.props.goToPage}
        toggleDrawer={this.props.toggleDrawer}
        hamburger={<Hamburger />}
      />
    );
  }
}

export default connect(({celebrity}) => ({celebrity}))(CelebrityOwnProfile)



const styles = StyleSheet.create({
  hamburger: {
    marginRight: 15,
    marginTop: 5,
    width: 30,
    height: 30,
    borderRadius: 3,
    alignItems: 'center',
    backgroundColor:  "rgba(0,0,0,0.34)"
  },
  hamburgerView: {
    backgroundColor: 'rgba(0,0,0, .6)',
    paddingHorizontal: 6,
    borderRadius: 4,
  },
  hamburgerIcon: {
    lineHeight: 32,
    textShadowColor: 'rgb(75, 103, 250)',
    textShadowRadius: 1,
    textShadowOffset: {width: 1*StyleSheet.hairlineWidth, height: 1*StyleSheet.hairlineWidth},
  }
});
