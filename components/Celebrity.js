import {Dimensions, Image, ListView, StyleSheet, Text, View, TouchableHighlight, InteractionManager, Modal} from 'react-native';
import React, { Component } from 'react';
import {Timer} from 'react-timer-mixin';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Drawer from 'react-native-drawer';
import Icon from 'react-native-vector-icons/Ionicons';
import Gradient from 'react-native-linear-gradient';
import Progress from 'react-native-progress';
import {BlurView, VibrancyView} from 'react-native-blur';
import formatMoney from 'format-money';
import Mixpanel from 'react-native-mixpanel';
import RNFS from 'react-native-fs';

import ActionButton from '../widgets/ActionButton';
import Separator from '../widgets/Separator';
import {BackButton} from '../widgets/NavButton';
import PlayButton from './RecordFlow/player_widgets/PlayButton';
import Navigator from '../widgets/Navigator';
import {DEFAULT_AVATAR, DEFAULT_BACKGROUND} from '../misc/constants';

import {setImmediateAnimationFrame} from '../misc/helpers';
import share from '../misc/share';
import INDUSTRIES, {colorForIndustry, singularIndustryName} from '../misc/IndustryOptions';
import {avatarSource, backgroundSource} from '../misc/sourceUri';

const {width, height} = Dimensions.get('window');

import * as actions from '../actions';
import { connect } from 'react-redux';


class Celebrity extends Timer(Component) {  //check later 
  constructor(props, context) {
    super(props, context);
    this.state =  {
      imagesLoaded: 0,
    };
  }

  onLoad(event) {
    this.setState({imagesLoaded: this.state.imagesLoaded+1})
  }

  componentDidMount() {
    //interactionHandle prop does not exist on celebrity's own profile.
    //only when coming from Celebrities list.
    this.setTimeout(() => {
      setImmediateAnimationFrame(() => {
        this.setState({didFocus: true});
      });
    }, 500);
  }

  renderBio() {
    return this.props.celeb.bio.split('\n').map((paragraph, index) => {
      return <Text key={index} style={styles.contentTextParagraph}>{paragraph}</Text>
    });
  }

  render() {
    const {celeb, isOwnProfile} = this.props;
    const {didFocus} = this.state;

    return (
      <View style={[styles.parallaxContainer, {top: isOwnProfile ? 0 : -64, height: !isOwnProfile ? height + 69 : height}]}>
        <ParallaxScrollView
          style={{overflow: 'hidden', height: !isOwnProfile ? height - 26 : height -69, position: 'absolute'}}
          contentBackgroundColor='transparent'
          ref="ParallaxView"
          //onScroll={}
          showsVerticalScrollIndicator={false}
          headerBackgroundColor="#333"
          stickyHeaderHeight={STICKY_HEADER_HEIGHT}
          parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
          backgroundSpeed={10}

          renderBackground={() => (
            <View key="background">
              {didFocus &&
                <Image style={styles.avatarBackground} source={backgroundSource(this.props.celeb)} onLoad={this.onLoad.bind(this)} />
              }

              <View style={{position: 'absolute',
                            backgroundColor: "rgba(0,0,0,0.26)",
                            top: 0,
                            width: width,
                            height: PARALLAX_HEADER_HEIGHT}}/>
            </View>
          )}

          renderForeground={() => (
            <View key="parallax-header" style={styles.parallaxHeader}>

              {didFocus && this.state.imagesLoaded < 2 && <Progress.Pie
                  indeterminate={true}
                  color={'rgba(19, 84, 252, 1)'}
                  borderWidth={0}
                  unfilledColor={'rgba(0,0,0,0)'}
                  size={210}
                  style={{position: 'absolute', left: width/2 - 210/2, top: 55}}
                />
              }

              {didFocus
                ? <Image style={styles.avatar} source={avatarSource(this.props.celeb)} defaultSource={DEFAULT_AVATAR} onLoad={this.onLoad.bind(this)} />
                : <Image style={styles.avatar} source={DEFAULT_AVATAR} />
              }


              <View style={{position: 'absolute', top: 230}}>
                <Text style={styles.celebName}>{celeb.display_name || 'Celebrity To Be'}</Text>
                <Text style={styles.celebTagline}>{celeb.tag_line || 'My Awesome Tagline'}</Text>
                <View style={{width, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                  {celeb.verified && <Icon name='checkmark-circled' size={17} color="rgba(233, 233, 233, 1)" />}
                  {celeb.verified && <Text style={styles.verified}>Verified</Text>}
                </View>
              </View>


            </View>
          )}

          renderFixedHeader={() => {
            if(isOwnProfile) return (
              <View key="fixed-header" style={styles.fixedSection}>
                {this.props.hamburger}
              </View>
            );
          }}

          renderStickyHeader={() => (
            <View key="sticky-header" style={styles.stickySection}>
              <Text style={styles.pageTitle}>{celeb.display_name || 'Celebrity To Be'}</Text>
            </View>
          )}
        >
          <View style={styles.contentHolder} >
            <VibrancyView blurType="dark" style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}} />
            <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(0,0,0, .4)'}} />

            <Gradient
                locations={[0, 1] }
                colors={['rgba(14,13,13,.912)',  'black']}
                style={styles.linearGradient} />

            <View style={styles.content}>
              <Text style={styles.tagBoxText}>{celeb.inventory || 0} Celebvidys Available</Text>
              <View style={{marginTop: 5, flexDirection: 'row', justifyContent: 'flex-start'}}>
                <View style={[styles.tagBox, {backgroundColor: colorForIndustry(celeb.industry) || 'rgb(22, 30, 220)'}]}>
                  <Text style={styles.tagBoxText}>{singularIndustryName(celeb.industry)}</Text>
                </View>
                <View style={styles.price}>
                  <Text style={[styles.priceText, {fontFamily: 'ProximaNova-Thin'}]}>Price: {formatMoney(celeb.price || 100, false)}/celebvidy</Text>
                </View>
              </View>

              <Separator style={{marginBottom: 15}}/>
              <Text style={[styles.tagBoxText, {fontSize: 20, fontFamily: 'Avenir'}]}>About {celeb.display_name || 'Celebrity To Be'}</Text>

              {!celeb.bio
                ? <Text style={styles.contentText}>{"I'm still thinking about it..."}</Text>
                : <View style={{marginTop: 8}}>
                    {this.renderBio()}
                  </View>
              }

            </View>



            <Separator style={{width, marginTop: 0, borderBottomWidth: 1*StyleSheet.hairlineWidth, borderColor:  "rgba(42,89,182,0.62)",}}/>
            <View style={{backgroundColor: 'rgba(17, 59, 184, 0.32)', width, height: 110}} >
              <Text style={[styles.tagBoxText, {marginTop: 10, fontSize: 18, fontFamily: 'Avenir', alignSelf: 'center'}]}>DO's & DONT's:</Text>
              <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center', top: -8,}}>
                <Text style={[styles.tagBoxText, {fontSize: 14, fontFamily: 'ProximaNova-RegularIt', alignSelf: 'center', paddingHorizontal: 5, textAlign: 'center'}]}>"{celeb.instructions || 'I rather not say'}"</Text>
              </View>
            </View>
            <Separator style={{width, marginTop: 0, marginBottom: 10, borderBottomWidth: 1*StyleSheet.hairlineWidth, borderColor:  "rgba(42,89,182,0.62)" ,}}/>



            <View style={styles.content}>
              <View style={{flex: 1, flexDirection: 'row', top: -15}}>
                <Image
                  source={this.props.celeb.youtube_id ? avatarSource(this.props.celeb) : require('../images/default_about_video_image.jpg')}
                  style={{height: 300, flex: 1,  resizeMode: 'cover', marginTop: 20, opacity: .8, borderWidth: 2*StyleSheet.hairlineWidth, borderColor: 'rgba(25,25,100, .4)'}}
                />

                <Text style={{position: 'absolute', top: 28, left: 12, color: this.props.celeb.youtube_id ? 'rgb(225, 225, 225)' : 'rgb(20,20,20)', fontFamily: 'Proxima Nova', fontSize: 16, width: width - 100}}>
                  {this.props.celeb.youtube_id ? `WHO AM I: ${celeb.display_name}` : 'HOW CELEBVIDY WORKS'}
                </Text>
                <Text style={{position: 'absolute', top: 28, right: 12, color: 'rgb(200, 200, 200)', fontFamily: 'Proxima Nova', fontSize: 16}}>
                  {celeb.duration && this.formatDuration(celeb.duration)}
                </Text>

                <PlayButton
                  opacity={1}
                  togglePlayback={() => this.setState({playerVisible: true})}
                  paused={true}
                  videoPlayedAtLeastOnce={true}
                  isYoutube={false}
                  style={{position: 'absolute', top: 300/2 - 25, left: width/2 - 45}}
                />

                <Modal animated={true} visible={!!this.state.playerVisible}>
                  {this.state.playerVisible && <Navigator
                    initialComponent={'Celebvidy'}
                    sourceUri={this.props.isOwnProfile && this.props.celeb.youtube_id && RNFS.DocumentDirectoryPath + '/videos/profile-video.mp4'}
                    youtube_id={this.props.celeb.youtube_id || 'vNeL9RZkVJA'}
                    onPressPlayerBack={() => this.setState({playerVisible: false}) }
                    renderNavigationBar={() => null}
                  />}
                </Modal>
              </View>



              <Text style={[styles.tagBoxText, {fontSize: 20, fontFamily: 'Avenir', marginTop: 20, marginBottom: -5}]}>How does my celebrity make my video?</Text>
              <Text style={styles.contentText}>All celebrity members have access to the exclusive celebrity section of the celebvidy™ iPhone app which notifies them of incoming video requests and helps them to record your custom message. If your video-request is reviewed, approved, and then recorded, your credit card is charged for the initially agreed upon amount.</Text>



              <TouchableHighlight underlayColor='rgba(85,84,84,1)' onPress={() => share()} style={{marginTop: 20, marginBottom: 20}}>
                <View style={{height: 50, borderWidth: 2, borderColor: "rgba(85,84,84,1)", paddingTop: 12}}>
                  <Text style={{textAlign: 'center', color: "rgba(151,149,149,1)", fontFamily: 'Proxima Nova', fontSize: 20}}>SHARE</Text>
                </View>
              </TouchableHighlight>


            </View>

          </View>
        </ParallaxScrollView>



        {isOwnProfile
          ? <ActionButton
              text={'GO TO MY CELEBVIDYS'}
              onPress={() => this.props.goToPage(1)}
              style={{
                borderTopWidth: 2*StyleSheet.hairlineWidth,
                borderTopColor: 'rgb(62, 126, 238)',
              }}
              touchableStyle={{position: 'absolute', bottom: isOwnProfile ? -5 : 14, left: 0, height: 100, width}}
            />
          :
            <ActionButton
              key={0}
              text={`ORDER CELEBVIDY  |  ${formatMoney(celeb.price, false)}`}
              onPress={() => {
                let values = {
                  celebrity_id: celeb._id,
                  celebrity_name: celeb.display_name,
                  charity_percent: celeb.charity_percent,
                  amount: celeb.price,
                  background_photo: celeb.background_photo,
                  avatar_photo: celeb.avatar_photo,
                };

                this.props.dispatch(actions.storeCelebvidy(values))
                  .then(() => {
                    this.props.navigator.pushWithProps('CreateOrder');
                    Mixpanel.trackWithProperties('Start Checkout', values);
                  })
              }}
              style={{
                borderTopWidth: 2*StyleSheet.hairlineWidth,
                borderTopColor: 'rgb(62, 126, 238)',
              }}
              hitSlop={{top: 45, left: 45, bottom: 45, right: 45}}
              touchableStyle={{position: 'absolute', bottom: isOwnProfile ?-5 : 14, left: 0, height: 100, width}}
            />
        }


      </View>
    );
  }


  formatDuration(ms) {
    let seconds = ms/1000;
    seconds = Number(seconds);

    let h = Math.floor(seconds / 3600); //our player likely doesnt have space to show hours--luckily we wont have videos that long
    let m = Math.floor(seconds % 3600 / 60);
    let s = Math.floor(seconds % 3600 % 60);

    return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : (m < 10 ? "0" : "")) + m + ":" + (s < 10 ? "0" : "") + s);
  }
}


export default connect()(Celebrity)



const AVATAR_SIZE = 120;
const PARALLAX_HEADER_HEIGHT = 350;
const STICKY_HEADER_HEIGHT = 60;

const styles = StyleSheet.create({
  parallaxContainer: {
    width,
    height: height + 5,
    backgroundColor: 'black',
    //shadowColor:  "rgba(6,47,124,1)" ,
    //shadowOpacity: 0.4,
    //shadowRadius: 8,
    //shadowOffset: {height: -8}
  },
  pageTitle: {
    fontSize: 18,
    fontFamily: 'ProximaNova-Thin',
    lineHeight: 19,
    color: 'white',
    top: -16,
    marginRight: 8,
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -2*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor:  "rgba(13,50,246,0.6)" ,
  },
  linearGradient: {
    width,
    height: 60,
    position: 'absolute',
    left: 0,
    bottom: 0,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255,255,0,0)'
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: width,
    height: PARALLAX_HEADER_HEIGHT
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: width,
    alignItems: 'flex-end',
    justifyContent: "flex-end",
    backgroundColor: 'rgba(4,4,4,1)',
  },
  fixedSection: {
    position: 'absolute',
    bottom: 10,
    left: 11,
  },
  parallaxHeader: {
    alignItems: 'center',
    alignSelf: 'center',
    width,
    flex: 1,
    flexDirection: 'column',
    paddingTop: 100,
  },
  avatarBackground: {
    width: width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  celebName: {
    fontSize: 26,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
  celebTagline: {
    fontSize: 20,
    fontFamily: 'STHeitiSC-Light',
    textAlign: 'center',
    lineHeight: 26,
    color: 'white',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
    marginBottom: 6,
    //backgroundColor: 'rgba(0,0,0, .25)',
    paddingHorizontal: 5,
    width,
  },
  verified: {
    fontSize: 16,
    color:  "white",
    fontFamily: 'ProximaNova-Thin',
    fontWeight: '500',
    marginLeft: 6,
    color: 'rgba(233, 233, 233, 1)',
  },
  priceText: {
    marginTop: 2,
    fontSize: 14,
    lineHeight: 15.5,
    letterSpacing: 1,
    color:  "white",
    fontFamily: 'ProximaNovaCond-Light',
  },
  contentHolder: {
    //backgroundColor: "rgba(14,13,13,.912)",
    backgroundColor: 'transparent',
    borderTopWidth: 1*StyleSheet.hairlineWidth,
    borderTopColor: 'rgba(18, 28, 54, .8)',
  },
  content: {
    width,
    overflow: 'hidden',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 20,
    paddingBottom: 25,
    marginBottom: 10,
  },
  contentText: {
    marginTop: 8,
    fontSize: 13,
    color:  "white",
    fontFamily: 'STHeitiSC-Light',
  },
  contentTextParagraph: {
    marginTop: -2,
    fontSize: 13,
    color:  "white",
    fontFamily: 'STHeitiSC-Light',
  },
  tagBox: {
    backgroundColor: 'rgba(27, 29, 223, 1)',
    //borderWidth: 1*StyleSheet.hairlineWidth,
    //borderColor: 'rgba(37, 39, 233, 1)',
    padding: 3,
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: 5,
    alignItems: 'center',
  },
  price: {
    padding: 3,
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: 5,
    alignItems: 'center',
  },
  tagBoxText: {
    fontSize: 16,
    color:  "white",
    fontFamily: 'ProximaNova-Thin',
    fontWeight: '500',
  }
});
