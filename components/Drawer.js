import {StyleSheet, Text, View, Image, ScrollView, Dimensions, StatusBar, InteractionManager} from 'react-native';
import React, { Component } from 'react';
import {default as DrawerWidget} from 'react-native-drawer';
import Gradient from 'react-native-linear-gradient';
import {setImmediateAnimationFrame} from '../misc/helpers';

const {height, width} = Dimensions.get('window');

import ScrollableTabView from './ScrollableTabView';
import Navigator from '../widgets/Navigator';
import CelebrityAccount from './CelebrityAccount/CelebrityAccount';
import {BackButton} from '../widgets/NavButton';

import {GiftedFormManager} from 'react-native-gifted-form';
import { connect } from 'react-redux';


class Drawer extends Component {
  onOpenStart() {
    this.sidebar && this.sidebar.setNativeProps({style: {opacity: 1}});
  }
  onClose() {
    this.sidebar && this.sidebar.setNativeProps({style: {opacity: 0}});
  }


  render() {
    let {showDrawer, isCelebrity} = this.props;

    //if(!isCelebrity) return <ScrollableTabView  />;

    return (
      <DrawerWidget
        ref='drawer'
        content={isCelebrity ? this.renderCelebrityAccountDrawer() : null}
        openDrawerOffset={0.13}
        side={'left'}
        tweenHandler={DrawerWidget.tweenPresets.parallax}
        tweenDuration={1000}
        tweenEasing={'easeInBack'}
        onOpenStart={this.onOpenStart.bind(this)}
        onClose={this.onClose.bind(this)}
        captureGestures={false}
        acceptPan={false}
        open={showDrawer}
      >
        <ScrollableTabView drawer={this} />

        <Gradient
          start={[0.0, 0.0]} end={[1.0, 0.0]}
          locations={[0, 1] }
          colors={['transparent',  'rgba(27,58,223,.15)']}
          style={styles.linearGradient}
        />
      </DrawerWidget>
    );
  }
  renderCelebrityAccountDrawer() {
    if(!this.props.isCelebrity) {
      this.sidebar = null;
      return null;
    }

    return (
      <View style={{height, width: width - 49}} ref={(sidebar) => this.sidebar = sidebar}>
        <Navigator
          ref='celebrityAccount'
          celebritySidebar={true}
          navigationBarStyle={{borderBottomColor: 'rgba(27,58,233,.10)'}}
          initialComponent={'CelebrityAccount'}
          isCelebrityAccount={true}
          renderBackButton={(navigator) => <BackButton backgroundColor={'transparent'} onPress={() => {
              GiftedFormManager.stores = {}; //clear whatever was entered in the form, but not saved
              navigator.pop();
            }}
          />}
        />
      </View>
    );
  }
}

export default connect( ({showDrawer, isCelebrity}) => ({showDrawer, isCelebrity}) )(Drawer)

const styles = StyleSheet.create({
  linearGradient: {
    width: 15,
    height,
    position: 'absolute',
    left: -15,
    top: 0,
  },
});
