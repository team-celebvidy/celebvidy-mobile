import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, Animated} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import ExNavigator from 'react-navigation';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';

import styles from '../styles/FormStyles';

import ClassyActionButton from '../widgets/ClassyActionButton';
import TextInput from '../widgets/GiftedTextInput';
import ModalSpinner from '../widgets/ModalSpinner';
import AnimateMessageMixin from '../widgets/AnimateMessageMixin';

var {width, height} = Dimensions.get('window');

import {dispatch, getState} from '../configureStore';
import {loginWithEmail} from '../actions';
import ddpClient from '../ddp';
const ddp = ddpClient();

export default class ChangePassword extends AnimateMessageMixin(Component) {  //check later
  componentDidMount() {
    this.flashMessage("Enter your password twice to reset it");
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      spinnerVisible: false,
      passwordValid: true,
      message: '',
      password: undefined,
      passwordConfirm: undefined
    };
  }

  async submitPassword() {
    this.setState({spinnerVisible: true});
    let success = await this.validatePassword();
    this.setState({spinnerVisible: false});
    console.log('SUCCESS', success)
    if(success) {
      this.props.navigator.pop();
    }
  }
  async validatePassword() {
    let password = this.refs.password.getValue();
    let isCorrectLength = password && password.length >= 6;
    let passwordsMatch = this.refs.password.getValue() === this.refs.passwordConfirm.getValue();

    if(!isCorrectLength) {
      this.setState({passwordValid: false});
      this.flashMessage('Password needs to be at least 6 characters');
      return false;
    }
    else if(!passwordsMatch) {
      this.setState({passwordValid: false});
      this.flashMessage('Your passwords do not match');
      return false;
    }
    else {
      let res = await ddp.callPromise('setPassword', [password]); //we need to hash the password ASAP!

      if(res.success) {
        this.setState({passwordValid: true});

        //we need to re-log the user in again, since on the server we log out all connected clients using this password
        let email = getState().user.emails[0].address;

        setTimeout(() => {
          dispatch(loginWithEmail(email, password));
        }, 1000); //for whatever reason we can't log the user back in after immediately being logged out, but a second later works flawlessly

        return true;
      }
      else {
        this.flashMessage(res.message);
        return false;
      }
    }
  }

  render() {
    let {password, passwordConfirm} = this.state;

    return (
      <View style={ST.container}>
        <ModalSpinner visible={this.state.spinnerVisible} />

        <View style={{marginTop: 40, marginBottom: 20}} >
          <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
            {this.state.message}
          </Animated.Text>
        </View>

        <TextInput
          name='password'
          title='Password'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.login}
          underlined={true}
          secureTextEntry={true}
          ref='password'
          valid={this.state.passwordValid}
          value={typeof password !== 'undefined' ? password : '1234567'}
          onFocus={() => this.setState({password: ''})}
          onChangeText={(password) => this.setState({password})}
          onSubmitEditing={() => this.refs.passwordConfirm.focus()}
        />

        <TextInput
          name='passwordConfirm'
          title='Confirm'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.login}
          underlined={true}
          secureTextEntry={true}
          ref='passwordConfirm'
          valid={this.state.passwordValid}
          value={typeof passwordConfirm !== 'undefined' ? passwordConfirm : '1234567'}
          onFocus={() => this.setState({passwordConfirm: ''})}
          onChangeText={(passwordConfirm) => this.setState({passwordConfirm})}
        />

        <GiftedForm.SeparatorWidget/>
        <GiftedForm.SeparatorWidget/>

        <ClassyActionButton onPress={() => this.submitPassword()} />
      </View>
    );
  }
}


const ST = StyleSheet.create({
  container: {
    flex: 1,
    paddingRight: 20,
    paddingLeft: 20,
  }
});
