import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, TouchableHighlight, Navigator, InteractionManager} from 'react-native';
import React, { Component } from 'react';
import Mixpanel from 'react-native-mixpanel';
import Icon from 'react-native-vector-icons/Ionicons';
import _ from 'underscore';
import Gradient from 'react-native-linear-gradient';
import moment from 'moment';

import Order from './CustomerOrderFlow/Order';
import {avatarSource, backgroundSource} from '../misc/sourceUri';
import {customerOrderStatus} from '../misc/orderStatuses';
import {DEFAULT_AVATAR, DEFAULT_BACKGROUND} from '../misc/constants';
import {getIndustryImageSource} from '../misc/IndustryOptions';

const {height, width} = Dimensions.get('window');


export default class CustomerOrderRow extends Component {
  _onPress(order) {
    if(order.status !== 'shared') {
      Mixpanel.trackWithProperties('Customer View Order', order);
      this.props.dispatch({type: 'CLEAR_CURRENT_ORDER'});
      this.props.navigator.pushWithProps('Order', {order});
    }
    else {
      this.props.navigator.pushWithProps('Celebvidy', {order});
    }
  }

  shouldComponentUpdate(nextProps) {
    //if(nextProps._id === this.props._id) return false;
    if(_.isEqual(nextProps, this.props)) return false;
    return true;
  }


  render() {
    let {order, index, rowStyle, offset} = this.props;
    let statusObj = customerOrderStatus(order.status);

    return (
      <View style={{backgroundColor: 'black', flex: 1,}}>
        <TouchableOpacity
          style={rowStyle}
          onPress={() => this._onPress(order)}
          activeOpacity={.5}
        >
          <Image style={[styles.row, {width: offset ? width - offset : width}]} source={backgroundSource({_id: order.celebrity_id, background_photo: order.background_photo})} key={index} resizeMode='cover'>
            <View style={{height: 1, width, position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0,0,0,1)'}} defaultSource={getIndustryImageSource({name: 'All Celebrities'})} />

            <Gradient
              locations={[0, .75, 1] }
              colors={['rgba(14, 32, 65, 0.0)', 'rgba(2, 5, 10, 0.9)', 'black']}
              style={styles.linearGradient} />

            <Image style={styles.avatar} source={avatarSource({_id: order.celebrity_id, avatar_photo: order.avatar_photo})} defaultSource={DEFAULT_AVATAR}/>
            <Text style={styles.celebName}>{order.celebrity_name}</Text>

            <Text style={styles.recipient}>for: {order.recipient}</Text>
            <Text style={styles.createdAt}>on: {moment(order.created_at).format('MM/DD/YYYY')}</Text>

            <View style={[styles.coloredBox, {backgroundColor: statusObj.color}]}>
              <Icon name={statusObj.icon} size={12} color='white' />
              <Text style={styles.subtext}>{statusObj.status}</Text>
            </View>
          </Image>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    width,
    height: 170,
  },
  linearGradient: {
    width,
    height: 60,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  avatar: {
    marginTop: 15,
    alignSelf: 'center',
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  celebName: {
    fontSize: 24,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    opacity: .9,
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
    backgroundColor: 'transparent',
  },
  recipient: {
    position: 'absolute',
    bottom: 5,
    left: 6,
    marginTop: 2,
    fontSize: 12,
    lineHeight: 15.5,
    letterSpacing: 1,
    color:  "white",
    fontFamily: 'Montserrat-Hairline',
    textShadowOffset: {
      width: -1*StyleSheet.hairlineWidth,
      height: 1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1*StyleSheet.hairlineWidth,
    textShadowColor: 'rgba(34, 139, 214, 0.4)',
    backgroundColor: 'transparent',
  },
   createdAt: {
    position: 'absolute',
    bottom: 5,
    right: 6,
    marginTop: 2,
    fontSize: 12,
    lineHeight: 15.5,
    letterSpacing: 1,
    color:  "white",
    fontFamily: 'Montserrat-Hairline',
    textShadowOffset: {
      width: -1*StyleSheet.hairlineWidth,
      height: 1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1*StyleSheet.hairlineWidth,
    textShadowColor: 'rgba(34, 139, 214, 0.4)',
    backgroundColor: 'transparent',
  },
  coloredBox: {
    backgroundColor: "rgba(27,29,223,0.78)",
    //borderWidth: 1*StyleSheet.hairlineWidth,
    //borderColor: "rgba(37,39,233,0.79)",
    opacity: .9,
    paddingLeft: 2,
    paddingRight: 2,
    height: 18,
    position: 'absolute',
    top: 20,
    right: 20,
    padding: 2,
    paddingLeft: 4,
    alignItems: 'center',
    flexDirection: 'row'
  },
  purpleBox: {
    backgroundColor: 'rgba(184, 27, 223, 1)',
    borderWidth: 1*StyleSheet.hairlineWidth,
    borderColor: 'rgba(210, 56, 238, 1)',
  },
  redBox: {
    backgroundColor: 'rgba(223, 27, 27, 1)',
    borderWidth: 1*StyleSheet.hairlineWidth,
    borderColor: 'rgba(244, 54, 44, 1)',
  },
  subtext: {
    color: 'white',
    fontFamily: 'Proxima Nova',
    fontSize: 12,
    textShadowColor: 'blackrgba(0, 0, 0, 0.3)',
    textShadowRadius: 1*StyleSheet.hairlineWidth,
    textShadowOffset: {width: 1*StyleSheet.hairlineWidth, height: 1*StyleSheet.hairlineWidth},
    marginLeft: 3,
  },
  thumb: {
    height: 75,
    resizeMode: 'cover',
  },
  title: {
    color: 'white',
    fontFamily: 'Proxima Nova',
    marginTop: 5,
    marginLeft: 10,
  }
});
