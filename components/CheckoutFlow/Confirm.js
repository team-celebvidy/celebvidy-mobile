import {View, Text, Image, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, Navigator, ScrollView, InteractionManager, Animated} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import ImageProgress from 'react-native-image-progress';
import Progress from 'react-native-progress';
import formatMoney from 'format-money';
import {GiftedFormManager} from 'react-native-gifted-form';
import Mixpanel from 'react-native-mixpanel';

import styles from '../../styles/FormStyles';

import CelebvidyDetails from '../CelebvidyDetails';
import PaymentInfo from './PaymentInfo';

import ActionButton from '../../widgets/ActionButton';
import {NextSubmitButton} from '../../widgets/NavButton';


import {MONTHS, YEARS} from '../../misc/MonthsYears';
import {avatarSource, backgroundSource} from '../../misc/sourceUri';
import {DEFAULT_AVATAR, DEFAULT_BACKGROUND} from '../../misc/constants';

var {width, height} = Dimensions.get('window');

import ThankYou from './ThankYou';

import {purchaseCelebvidy, storeCelebvidy} from '../../actions';
import {connect} from 'react-redux';
import {getState} from '../../configureStore';
import ModalSpinner from '../../widgets/ModalSpinner';


class Confirm extends Component {
  constructor(props, context) {
    super(props, context);
    this.paymentInfoOpacity = new Animated.Value(0);
  }

  purchaseCelebvidy() {
    let props = this.props; //if api request completes too quickly, props passed from redux will be cleared

    return this.props.dispatch(purchaseCelebvidy())
      .then(() => this.pushThanks(props))
      .catch((error) => {
        if(error.message.indexOf('out of stock') > -1) { //yea, i need to develop an internal error code system, this will break unknowning if we simply change error.message
          this.props.navigator.popToTop();
        }
        else {
          this.props.navigator.replacePreviousAndPopWithProps('PaymentInfo');
        }

        //TO BE DELETED
        //this.props.navigator.pop();
        //setTimeout(() => this.props.navigator.pop(), 750); //replace with proper popTo call at some point (probably just need to add to widgets/Navigator)

        alert(error.message);
      });
  }
  pushThanks(props) {
    let {cardholder_name, amount} = props;
    delete GiftedFormManager.stores.createOrder;

    setTimeout(() => {
      Mixpanel.registerSuperProperties({...getState().customer, customer_name: cardholder_name});
      Mixpanel.set({$name: cardholder_name});
      Mixpanel.trackCharge(amount);
      Mixpanel.track('Thank You');
    }, 7000);

    return this.props.navigator.pushWithProps('ThankYou', props);
  }

  nevadaTaxAmount() {
    return this.finalAmount() * .046; //4.6% is the tax rate in nevada
  }
  nevadaTaxFormatted() {
    return formatMoney(this.nevadaTaxAmount());
  }
  finalAmount() {
    if(this.props.recipient_phone) {
      return parseFloat(this.props.amount+'.99');
    }
    else {
      return this.props.amount;
    }
  }
  totalFormatted() {
    if(this.props.state === 'NV') {
      return formatMoney(this.finalAmount() + this.nevadaTaxAmount());
    }
    else {
      return formatMoney(this.finalAmount());
    }
  }

  componentDidMount() {
    setTimeout(() => Mixpanel.track("Confirm"), 750);

    Animated.timing(this.paymentInfoOpacity, {
      toValue: 1,
      duration: 400,
      delay: 850,
    }).start();
  }

  render() {
    let {payment_method, charity_percent, celebrity_name, avatar_photo, celebrity_id, currentCheckoutOrderFetching} = this.props;

    return (
      <View style={{paddingTop: 0, height: height - 64}}>
        <ModalSpinner visible={currentCheckoutOrderFetching} />

        <ScrollView style={{flex: 1, width}}>

          <View style={{flex: 1, alignSelf: 'center', marginTop: 25, alignItems: 'center'}}>
            <ImageProgress style={ST.avatar}
              source={avatarSource({_id: celebrity_id, avatar_photo})}
              defaultSource={DEFAULT_AVATAR}
              indicator={Progress.Pie}
              indicatorProps={{
                size: 120,
                borderWidth: 0,
                color: 'rgba(150, 150, 150, 1)',
                unfilledColor: 'rgba(200, 200, 200, 0.2)'
              }}
            />
            <Text style={[ST.celebName, {fontSize: 20, marginTop: -5, color: 'rgb(184, 184, 184)'}]}>{celebrity_name}</Text>
            {this.props.state === 'NV' && <Text style={[ST.celebName, {fontSize: 14, fontWeight: 'normal'}]}>Nevada state tax: {this.nevadaTaxFormatted()}</Text>}
            <Text style={[ST.celebName, {fontSize: 22}]}>Total: {this.totalFormatted()}</Text>

            {!charity_percent ? null : <Text style={[ST.note]}>a portion of proceeds donated to charity</Text>}

            <View style={{flexDirection: 'row'}}>
              <Icon name='locked' size={13} color='rgb(61, 61, 61)' style={{marginRight: 5, marginTop: 5, backgroundColor: 'transparent'}} />
              <Text style={[ST.note, {fontSize: 12, color: 'gray'}]}>100% money back guarantee</Text>
            </View>
          </View>


          <CelebvidyDetails {...this.props} editable={false} formName='createOrder' />


          <Animated.View style={{opacity: this.paymentInfoOpacity}}>
            <Text style={[styles.fieldTitle, {
                marginTop: 35, fontSize: 20,
                textAlign: 'center', backgroundColor: 'transparent',
              }]}>
              {payment_method === 'paypal' ? 'PAID VIA PAYPAL' : 'Credit/Debit Card:'}
            </Text>

            {payment_method === 'card' && <PaymentInfo {...this.props} editable={false} />}
          </Animated.View>


          <View style={{height: 65}} />
        </ScrollView>

        <ActionButton
          text={'COMPLETE PURCHASE'}
          onPress={() => this.purchaseCelebvidy()}
          style={{
            borderTopWidth: 2/PixelRatio.get(),
            borderTopColor: 'rgb(19, 78, 182)',
          }}
          touchableStyle={{position: 'absolute', bottom: 0, left: 0, width}}
          />
      </View>
    );
  }
}


export default connect(
  ({currentCheckoutOrder, currentCheckoutOrderFetching}) => ({...currentCheckoutOrder, currentCheckoutOrderFetching}),
  null, null, {withRef: true}
)(Confirm)



const AVATAR_SIZE = 120;

const ST = StyleSheet.create({
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  celebName: {
    fontSize: 26,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -1/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  note: {
    fontSize: 13,
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    lineHeight: 20,
    color: 'white',
    backgroundColor: 'transparent',
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -1/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
})
