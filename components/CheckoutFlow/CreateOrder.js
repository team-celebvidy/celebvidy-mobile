import {View, Text, Dimensions} from 'react-native';
import Mixpanel from 'react-native-mixpanel';
import React, { Component } from 'react';
var {width, height} = Dimensions.get('window');

import CelebvidyDetails from '../CelebvidyDetails';

import {isAdmin, hasAutoApproveRequest, hasAutoApproveVideo} from '../../misc/isAdmin';
import {storeCelebvidy, editOrder} from '../../actions';
import {connect} from 'react-redux';
import {GiftedFormManager} from 'react-native-gifted-form';


class CreateOrder extends Component {
  store(values, shouldPush=true) {
    values = values || this.getValues();

    if(!values) {
      alert('Please fix the issues displayed in red');
      return;
    }

    return this.props.dispatch(storeCelebvidy(values))
      .then(() => {
        Mixpanel.trackWithProperties("Payment Info", values);
        Mixpanel.registerSuperProperties(values);
        return shouldPush && this.props.navigator.pushWithProps('PaymentInfo');
      })
      .catch((error) =>  alert(error.message))
  }

  getValues() {
    let values = GiftedFormManager.getValues('createOrder');
    let isValid = GiftedFormManager.validate('createOrder', true).isValid;

    //minor hack to make sure selects receive a checkmark or x when the whole form is validated.
    //since they use their placeholder as default values and show no checkmark until you touch it, this is needed.
    this.refs.details.refs.form.inputs['kind'].setState({neverFocused: false});
    this.refs.details.refs.form.inputs['deadline'].setState({neverFocused: false});

    if(!isValid) return false;
    return values;
  }

  preSave() {
    let values = GiftedFormManager.getValues('createOrder');
    this.props.dispatch(storeCelebvidy(values));
  }

  render() {
    return (
      <CelebvidyDetails
        ref='details'
        {...this.props}
        formName='createOrder'
        store={this.store.bind(this)}
        preSave={this.preSave.bind(this)}
        height={height}
      />
    );
  }
}


export default connect(
  ({currentCheckoutOrder, customer, celebrity}) => {
    let admin = isAdmin(customer, celebrity);

    return {
      ...currentCheckoutOrder,
      can_automatically_approve_request: admin || hasAutoApproveRequest(customer, celebrity),
      can_automatically_approve_video: admin || hasAutoApproveVideo(customer, celebrity),
    };
  },
  null, null, {withRef: true}
)(CreateOrder)
