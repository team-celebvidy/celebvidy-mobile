import {View, Text, Image, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, Navigator, ActionSheetIOS, StatusBar} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from '../../styles/FormStyles';
import ActionButton from '../../widgets/ActionButton';
import ClassyActionButton from '../../widgets/ActionButton';

import {MONTHS, YEARS} from '../../misc/MonthsYears';
import share from '../../misc/share';
import {setImmediateAnimationFrame} from '../../misc/helpers';

var {width, height} = Dimensions.get('window');

import * as actions from '../../actions';
import { connect } from 'react-redux';
import * as Push from '../../misc/PushNotifications';

class ThankYou extends Component {
  shouldComponentUpdate(nextProps) {
    if(nextProps.page !== this.props.page) { //go back to Celebrities after checkout is complete and the user swipes right or left
      this.props.navigator.popToTop();
    }

    return false;
  }

  componentDidMount() {
    if(this.props.isCelebrity) {
      this.props.dispatch(actions.getCustomer());
    }

    setImmediateAnimationFrame(() => {
      this.props.dispatch(actions.gratify({darkenBackground: true, caption: `BooYah!`}));
    }, 500);

    setImmediateAnimationFrame(() => {
      Push.registerPushNotifications();
    }, 5500);
  }

  render() {
    let {celeb, order, isCelebrity} = this.props;

    return (
      <View style={{flex: 1, top: -128}}>

        <View style={{marginTop: 150, alignItems: 'center'}}>

          <Text style={[styles.fieldTitle, {fontSize: 28,textAlign: 'center', backgroundColor: 'transparent',}]}>
            Thanks for your order
          </Text>
          <Text style={[ST.note]}>Please share Celebvidy if your order is not a surprise or gift</Text>

        </View>


        <View style={{position: 'absolute', bottom: 0, width, padding: 10}}>
          <TouchableHighlight underlayColor='rgba(85,84,84,1)' onPress={() => share(`I ordered a Celebvidy from ${this.props.celebrity_name} via celebvidy.com`) }>
            <View style={{height: 50, borderWidth: 2, borderColor: "rgba(85,84,84,1)", paddingTop: 12}}>
              <Text style={{textAlign: 'center', color: "rgba(151,149,149,1)", fontFamily: 'Proxima Nova', fontSize: 20}}>SHARE</Text>
            </View>
          </TouchableHighlight>


          <ActionButton
            text={!isCelebrity ? 'GO TO MY CELEBVIDYS' : 'GO TO MY ORDERS'}
            touchableStyle={{marginTop: 20}}
            onPress={() => {
              let {dispatch, automatically_approve_request} = this.props;

              if(!isCelebrity) {
                this.props.goToPage(1)
              }
              else {
                this.props.goToPage(0);
                setTimeout(() => dispatch({type: 'SHOW_DRAWER'}), 500);
                setTimeout(() => {
                  dispatch({type: 'SHOW_ORDERS'});
                  setTimeout(() => alert('Nicely done, you ordered a celebvidy too! Your orders as a customer will appear here in your account. Tap "My Orders" any time to review them.'), 600);
                }, 1500)

                if(automatically_approve_request) {
                  //this way for celeb users, the request will be there waiting--makes testing feel as on point as what actual users feel :)
                  setTimeout(() => dispatch({type: 'REFRESH_CELEBRITY_REQUESTS'}), 3000);
                }
              }
            }}
           />
        </View>

      </View>
    );
  }
}

export default connect(({page, isCelebrity}) => ({page, isCelebrity}))(ThankYou)



const AVATAR_SIZE = 120;

const ST = StyleSheet.create({
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  celebName: {
    fontSize: 26,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -1/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  note: {
    fontSize: 13,
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    lineHeight: 20,
    color: 'white',
    backgroundColor: 'transparent',
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -1/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
})
