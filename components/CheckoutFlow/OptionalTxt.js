import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, Navigator, Animated, Alert} from 'react-native';
import React, { Component } from 'react';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import styles from '../../styles/FormStyles';
import TextInput from '../../widgets/GiftedTextInput';
import SubmitButton from '../../widgets/GiftedSubmitButton';
import {NextSubmitButton} from '../../widgets/NavButton';
import Mixpanel from 'react-native-mixpanel';

var {width, height} = Dimensions.get('window');

import AnimateMessageMixin from '../../widgets/AnimateMessageMixin';

import {storeCelebvidy} from '../../actions';
import {connect} from 'react-redux';


class OptionalTxt extends AnimateMessageMixin(Component) {
  constructor(props, context) {
    super(props, context);
    this.state = {
      message: '',
    };
  }
  componentDidMount() {
    this.showMessage(750, 200, `For an additional $0.99, Celebvidy will also send your video by text message. Simply enter a phone number and this option will be added to your order.`);
  }

  store(isValid, values) {
    if(!isValid) {
      alert('The phone number you entered is invalid');
      return Promise.reject(new Error('Invalid phone number'));
    }
    else return this._store(values);
  }

  skip() {
    if(GiftedFormManager.getValues('optionalTxt').recipient_phone) {
      Alert.alert('Are you Sure?', "Are you sure you DON'T want a TXT message sent to the recipient?", [
          {text: 'CANCEL', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'YES, SKIP IT!', onPress: () => {
            this.refs.input.refs.input.clear();
            GiftedFormManager.stores.optionalTxt.values.recipient_phone = '';
            this._store({recipient_phone: ''}); //insure recipient_sent is cleared, i.e. in the case the user reached the Confirm page, clicked edit, but skipped the txt the second time around
          }},
        ]
      );
    }
    else {
      //yes it's a little weird we need to do 3 frigging clearings, but we just do in this particular case
      this.refs.input.refs.input.clear();
      GiftedFormManager.getValues('optionalTxt').recipient_phone = '';
      this._store({recipient_phone: ''});
    }
  }

  _store(values) {
    dismissKeyboard();

    if(!this.props.isSignedIn) {
      Mixpanel.track('Customer LoginSignup')
      this.props.navigator.pushWithProps('CustomerLoginSignup', {isCheckout: true});
    }
    else {
      this.props.navigator.pushWithProps('Confirm', this.props);
    }

    setTimeout(() => {
      this.props.dispatch(storeCelebvidy(values));
    }, 750);
  }

  componentDidMount() {
    Mixpanel.track("Optional TXT");
  }

  render() {
    let {celeb, order, navigator, editable, viewedAfterCheckout} = this.props;
    window.txt = this;
    return (
      <GiftedForm
        formName='optionalTxt'
        scrollEnabled={false}
        style={[styles.giftedForm, {paddingTop: 64}]}
        clearOnClose={false} // delete the values of the form when unmounted
        validators={{
          recipient_phone: {
            title: 'Phone',
            validate: [{
              validator: (num) => {
                if (!num) return true;
                else {
                  num = num.replace(/\D/g,''); 
                  if(num.length !== 10 && num.length !== 11) return false;
                }

                return true;
              },
              message: 'Invalid Phone Number'
            }]
          },
        }}
      >

        <View style={{position: 'absolute', top: 80, width: width - 10, paddingRight: 20, height: 60, flex: 1,}} >
          <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'justify', color: 'white', opacity: this.messageOpacity}}>
            {this.state.message}
          </Animated.Text>
        </View>

        <View style={{height: 150}} />

        <TextInput
          name='recipient_phone'
          title='Phone'
          keyboardType='phone-pad'
          placeholder='xxx-xxx-xxxx'
          returnKeyType='next'
          maxLength={14}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref='input'
          format='phone'
          value={this.props.recipient_phone}
        />

        <SubmitButton
          text={this.props.recipient_phone ? 'KEEP TXT MESSAGE' : 'ADD TXT MESSAGE'}
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            postSubmit();
            this.store(isValid && !!values.recipient_phone, values);
            setTimeout(() => {
              Mixpanel.track("TXT MESSAGE ADDED");
              Mixpanel.registerSuperProperties({optionalTxt: true});
            }, 1500); //this is a heavy call with register super properties that delays the animation
          }}
        />

        {this.props.recipient_phone &&
          <SubmitButton
            style={{top: -18}}
            text='REMOVE TXT MESSAGE OPTION'
            onSubmit={(isValid, values, validationResults, postSubmit) => {
              postSubmit();
              values.recipient_phone = '';
              this.store(isValid, values);
            }}
          />
        }

      </GiftedForm>
    );
  }
}

export default connect(
  ({currentCheckoutOrder, user}) => {
    return {
      ...currentCheckoutOrder,
      isSignedIn: !!user.loggedIn,
    }
  },
  null, null, {withRef: true}
)(OptionalTxt)
