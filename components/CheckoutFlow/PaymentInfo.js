import {View, Text, TouchableHighlight, TouchableOpacity,  StyleSheet, Dimensions, PixelRatio, Navigator} from 'react-native';
import React, { Component } from 'react';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';

import styles from '../../styles/FormStyles';

import ActionButton from '../../widgets/ActionButton';
import TextInput from '../../widgets/GiftedTextInput';
import NumberInput from '../../widgets/GiftedNumberInput';
import SubmitButton from '../../widgets/GiftedSubmitButton'
import Select from '../../widgets/GiftedSelect';
import {NextSubmitButton} from '../../widgets/NavButton';
import Mixpanel from 'react-native-mixpanel';

import {MONTHS, YEARS} from '../../misc/MonthsYears';

var {width, height} = Dimensions.get('window');

import OptionalTxt from './OptionalTxt';

import {storeCelebvidy, editPaymentInfo} from '../../actions';
import {connect} from 'react-redux';

import ModalSpinner from '../../widgets/ModalSpinner';
import Braintree from 'react-native-braintree-xplat';

import ddpClient from '../../ddp';
const ddp = ddpClient();
const BUNDLE_ID = require('../../buildConfig').BUNDLE_ID;


class PaymentInfo extends Component {
  constructor(props, context) {
    super(props, context);
    this.inputs = [];
    this.state = {
      cardNonceFetching: false,
    };
  }

  //used during checkout
  store() {
    let values = this.getValues();
    if(!values) {
      return alert('Please fix the issues displayed in red');
    }

    values.last_4 = (values.card+'').substr(-4);

    this.setState({cardNonceFetching: true});

    return this.getBraintreeNonce(values)
      .then((paymentMethodNonce) => ({...values, paymentMethodNonce, payment_method: 'card'}))
      .then((values) => this.props.dispatch(storeCelebvidy(values)))
      .then(() => this.setState({cardNonceFetching: false}))
      .then(() => setTimeout(() => this.props.navigator.pushWithProps('OptionalTxt'), 300))
      .catch((error) => alert(error.message));
  }

  //used by PaymentInfo page in CustomerAccount drawer
  save() {
    let values = this.getValues();

    return this.props.dispatch(editPaymentInfo(values))
      .then(() => this.props.navigator.pop())
      .catch((error) => alert(error.message))
  }

  getValues() {
    let values = GiftedFormManager.getValues('paymentInfo');
    let isValid = GiftedFormManager.validate('paymentInfo', true).isValid;

    this.refs.form.inputs['expiration'].setState({neverFocused: false}); //minor hack to make sure expiration isn't missing either a check or an x

    if(!isValid) return false;
    return values;
  }


  getBraintreeNonce({card, expiration, security_code}) {
    let [month, year] = expiration.split('/');
    return Braintree.getCardNonce(card, month, year.substr(2), security_code);
  }
  payWithPaypal() {
    return Braintree.showPayPalViewController()
      .then((paymentMethodNonce, paypal_email, firstName, lastName, phone) => {
        if(!paymentMethodNonce) {
          alert('Something went wrong with your paypal authentication. Perhaps try a credit card instead');
        }

        let cardholder_name = firstName ? (firstName + ' ' + lastName) : '';

        return {
          payment_method: 'paypal',
          paymentMethodNonce,
          paypal_email,
          cardholder_name,
          phone,
        };
      })
      .then((values) => this.props.dispatch(storeCelebvidy(values)))
      .then(() => this.props.navigator.pushWithProps('OptionalTxt'))
      .catch((error) => {
        console.log('PAYPAL FAIL', error);
        alert(error.message);
      });
  }
  componentDidMount() {
    if(this.props.editable === false) return; //this component is also shown on the confirm page as non-editable fieldSubtext

    ddp.callPromise('getBraintreeClientToken')
      .then((token) => {
        Braintree.setupWithURLScheme(token, BUNDLE_ID+'.payments');
      })
      .catch((error) => console.log('ERROR', error));
  }


  render() {
    let {cardholder_name, card, security_code, expiration, state, zip, editable} = this.props;

    return (
      <GiftedForm
        formName='paymentInfo'
        ref='form'
        scrollEnabled={false}
        style={[styles.giftedForm, {marginTop: 20}]}
        validators={this.getValidators()}
      >

        <ModalSpinner visible={this.state.cardNonceFetching} />

        <TextInput
          name='cardholder_name'
          editable={this.props.editable}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          title='Name'
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[1].focusInput()}
          value={cardholder_name}
        />


        <NumberInput
          name='card'
          title='Card'
          returnKeyType='next'
          maxLength={16}
          editable={this.props.editable}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[2].focusInput()}
          value={card}
          placeholder={''}
        />

        <NumberInput
          name='security_code'
          title='Code'
          returnKeyType='next'
          placeholder='123'
          maxLength={4}
          editable={this.props.editable}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[3].focusInput()}
          value={security_code}
        />


        <Select
          name='expiration'
          title='Expiration'
          pickers={[MONTHS, YEARS]}
          editable={this.props.editable}
          ref={(input) => this.inputs.push(input)}
          placeholder='6/2020'
          selections={expiration ? expiration.split('/') : ['6', '2020']}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          onSubmitEditing={(value) => this.inputs[4].focus()}
          value={expiration}
         />


         <TextInput
           name='state'
           editable={this.props.editable}
           validIcon={'checkmark'}
           invalidIcon={'close-round'}
           title='State'
           widgetStyles={styles.input}
           underlined={true}
           autoCapitalize='characters'
           maxLength={2}
           ref={(input) => this.inputs.push(input)}
           onSubmitEditing={(value) => this.inputs[5].focusInput()}
           value={state}
         />

        <NumberInput
          name='zip'
          title='Zip'
          returnKeyType='done'
          placeholder='90210'
          maxLength={5}
          editable={this.props.editable}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={() => this.store()}
          value={zip}
        />

        {editable !== false &&
          <View>
            <View style={{marginTop: 30, marginBottom: 20}}>
              <Text style={{textAlign: 'center', color: 'rgb(150,150,150)', fontSize: 15}}>OR</Text>
            </View>

            <TouchableOpacity onPress={() => this.payWithPaypal()}>
              <View style={{backgroundColor: 'rgb(6, 63, 162)', flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 15, paddingVertical: 13, borderWidth: 1, borderColor: 'rgb(29, 77, 159)'}}>
                <Text style={{textAlign: 'center', color: 'white', fontSize: 18}}>PAY WITH PAYPAL</Text>
              </View>
            </TouchableOpacity>
          </View>
        }
      </GiftedForm>
    );
  }

  getValidators() {
    return {
      cardholder_name: {
        title: `Cardholder's Name`,
        validate: [{
          validator: (name) => {
            if (!name) return false;
            else if(!name.split(' ')[1]) return false;

            return true;
          },
          message: `Cardholder's name is invalid`
        }]
      },
      card: {
        title: 'Credit Card',
        validate: [{
          validator: 'isLength',
          arguments: [16],
          message: '{TITLE} must be between {ARGS[0]} digits'
        }]
      },
      security_code: {
        title: 'Security Code',
        validate: [{
          validator: 'isLength',
          arguments: [3, 4],
          message: '{TITLE} must be {ARGS[0]} or {ARGS[1]} digits'
        }]
      },
      expiration: {
        title: 'Expiration',
        validate: [{
          validator: (...args) => {
            let [month, year] = args[0].split('/');
            let selectedDate = new Date(month+'/1/'+year);
            return selectedDate > new Date;
          },
          message: 'The expiration must be in the future'
        }]
      },
      zip: {
        title: 'Zip Code',
        validate: [{
          validator: 'isLength',
          arguments: [5],
          message: '{TITLE} must be between {ARGS[0]} digits'
        }]
      },
      state: {
        title: 'State',
        validate: [{
          validator: 'isLength',
          arguments: [2],
          message: '{TITLE} must be between {ARGS[0]} digits'
        }]
      },
    };
  }
}


export default connect(
  ({currentCheckoutOrder, customer, celebrity}) => {
    return {
      ...customer.payment_info,
      ...currentCheckoutOrder, //only one will be present, but the widget sees em both the same;
      cardholder_name: currentCheckoutOrder.cardholder_name || customer.full_name || celebrity.full_name || '',
    };
  },
  null, null, {withRef: true}
)(PaymentInfo)
