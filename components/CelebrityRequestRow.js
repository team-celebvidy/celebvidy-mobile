import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, TouchableHighlight, Navigator, InteractionManager} from 'react-native';
import React, { Component } from 'react';
import Mixpanel from 'react-native-mixpanel';
import Icon from 'react-native-vector-icons/Ionicons';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome';
import _ from 'underscore';
import moment from 'moment';

const {height, width} = Dimensions.get('window');


export default class CelebrityRequestRow extends Component {
  _onPress(order) {
    Mixpanel.trackWithProperties('Celebrity View Request', order);
    this.props.dispatch({type: 'SET_CURRENT_REQUEST', payload: order});
    this.props.navigator.pushWithProps('Request', {...this.props, order});
  }

  shouldComponentUpdate(nextProps) {
    //if(nextProps._id === this.props._id) return false;
    if(_.isEqual(nextProps, this.props)) return false;
    return true;
  }

  render() {
    let {order, index} = this.props;

    return (
      <TouchableHighlight
        key={index}
        style={{width}}
        underlayColor='rgba(27, 29, 223, .3)'
        onPress={() => this._onPress(order)}
      >
        <View key={index} style={styles.row}>
          <View style={{flex: 1, flexDirection: 'row', height: 20}}>
            <Text style={styles.recipient}>{order.recipient}</Text>
            {!!order.gift && <AwesomeIcon name='gift' size={17} color="rgba(233, 233, 233, 1)" style={{marginLeft: 5, marginTop: 13}} />}
          </View>

          <View style={styles.info}>
            <View style={styles.coloredBox}>
              <Text style={styles.type}>{order.kind}</Text>
            </View>

            <Text style={styles.deadline}>deadline: {moment(new Date(order.deadline)).format('MM/DD/YYYY')}</Text>
          </View>

          <Icon name='chevron-right' size={24} color='rgba(27,58,223,1)' style={styles.chevron} />
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    width: width,
    height: 75,
    flexDirection: 'column',

    borderBottomWidth: 1*StyleSheet.hairlineWidth,
    borderBottomColor: "rgba(57,60,70,0.47)",


    borderRightWidth: 5,
    borderRightColor:  "rgb(19, 19, 19)" ,

    borderLeftWidth: 5,
    borderLeftColor:  "rgb(19, 19, 19)" ,

    overflow: 'hidden',

    justifyContent: 'flex-start',
    backgroundColor: "rgba(0,0,0,0.53)"
  },
  recipient: {
    fontSize: 16,
    fontFamily: 'Proxima Nova',
    lineHeight: 16,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 15,
    marginLeft: 10,
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -2*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: "rgba(4,5,12,0.6)",
  },
  info: {
    flexDirection: 'row',
    height: 32,

    marginLeft: 10
  },
  coloredBox: {
    backgroundColor: 'rgba(27, 29, 223, 1)',
    borderWidth: 1*StyleSheet.hairlineWidth,
    borderColor: 'rgba(37, 39, 233, 1)',
    opacity: .9,
    paddingLeft: 2,
    paddingRight: 2,
    height: 18,
    padding: 2,
    paddingLeft: 4,
    alignItems: 'center'
  },
  type: {
    color: 'white',
    fontFamily: 'Proxima Nova',
    fontSize: 12,
    textShadowColor: 'rgba(0, 0, 0, 0.3)',
    textShadowRadius: 1*StyleSheet.hairlineWidth,
    textShadowOffset: {width: 1*StyleSheet.hairlineWidth, height: 1*StyleSheet.hairlineWidth}
  },
  deadline: {
    marginLeft: 8,
    fontSize: 12,
    lineHeight: 15.5,
    letterSpacing: 1,
    color:  "white",
    fontFamily: 'Montserrat-Hairline',
    textShadowOffset: {
      width: -1*StyleSheet.hairlineWidth,
      height: 1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1*StyleSheet.hairlineWidth,
    textShadowColor: 'rgba(34, 139, 214, 0.4)',
  },
  chevron: {
    position: 'absolute',
    top: 26,
    right: 20
  }
});
