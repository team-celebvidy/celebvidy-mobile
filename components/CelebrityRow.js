import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, TouchableHighlight, Navigator, InteractionManager, StatusBar} from 'react-native';
import React, { Component } from 'react';
import Mixpanel from 'react-native-mixpanel';
import shallowCompare from 'react-addons-shallow-compare';
import GiftedListView from 'react-native-gifted-listview';
import ExNavigator from 'react-navigation';
import Gradient from 'react-native-linear-gradient';
import formatMoney from 'format-money';
import _ from 'underscore';

import Progress from 'react-native-progress';

import {avatarSource, backgroundSource} from '../misc/sourceUri';
import Celebrity from './Celebrity';
import {getIndustryImageSource} from '../misc/IndustryOptions';

const {height, width} = Dimensions.get('window');


export default class CelebrityRow extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      nameWidth: null,
      imagesLoaded: 0,
    };
  }

  _onPress(celeb) {
    Mixpanel.trackWithProperties('View Celebrity', {celebrity: celeb.display_name, price: celeb.price, industry: celeb.industry, inventory: celeb.inventory})
    this.props.navigator.pushWithProps('Celebrity', {celeb});
  }

  onLoad(event) {
    this.setState({imagesLoaded: this.state.imagesLoaded+1})
  }
  measureProgressBar({nativeEvent: {layout: {x, y, width: nameWidth}}}) {
    if(this.state.nameWidth || this.state.imagesLoaded >= 2) return;
    this.setState({nameWidth});
  }
  shouldComponentUpdate(nextProps, nextState) {
    //if(nextProps._id === this.props._id) return false;
    if(!_.isEqual(nextProps, this.props)) {
      return true;
    }
    else {
      return nextState.nameWidth !== this.state.nameWidth || (nextState.imagesLoaded >= 2 && nextState.imagesLoaded !== this.state.imagesLoaded);
    }
  }

  render() {
    let celeb = this.props;

    return (
      <View style={{backgroundColor: 'black'}}>
        <TouchableOpacity activeOpacity={.5} style={styles.row} onPress={() => this._onPress(celeb)}>
          <View style={styles.row}>
            <Image style={styles.rowBackground}
              source={backgroundSource(celeb)}
              onLoad={this.onLoad.bind(this)}
              defaultSource={getIndustryImageSource({name: 'All Celebrities'})}
            />

            <Gradient
              locations={[0, .69, 1] }
              colors={['rgba(0, 0, 0, 0.0)', 'rgba(2, 5, 10, 0.8)', 'rgba(2, 5, 10, 0.99)']}
              style={[styles.linearGradient, {bottom: 0}]} />

            <View ref={(avatar) => this.avatar = avatar} style={{width}}>
              <Image style={styles.avatar} source={avatarSource(celeb)} onLoad={this.onLoad.bind(this)}  />

              <View style={{alignSelf: 'center', flexDirection: 'column'}} onLayout={this.measureProgressBar.bind(this)}>
                <Text style={styles.celebName}>{celeb.display_name || celeb.full_name || celeb._id}</Text>

                {this.state.imagesLoaded < 2 &&
                  <Progress.Bar
                    indeterminate={true}
                    color={'rgba(27,58,223,1)'}
                    width={this.state.nameWidth}
                    style={{marginTop: -2, height: 1, borderColor: 'transparent'}}
                  />
                }
              </View>
            </View>


            <Text style={styles.available}>{celeb.inventory} celebvidys available</Text>

            <View style={[styles.coloredBox, (celeb.price < 1000 ? (celeb.price < 500 ? styles.lightBlueBox : styles.blueBox) : (celeb.price < 5000 ? styles.purpleBox : styles.redBox))]}>
              <Text style={styles.subtext}>{formatMoney(celeb.price, false)}/celebvidy</Text>
            </View>

          </View>
        </TouchableOpacity>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  row: {
    height: 170,
    width,
  },
  rowBackground: {
    position: 'absolute',
    width,
    height: 170,
    resizeMode: 'cover',
  },
  avatar: {
    marginTop: 150/2 - 60,
    marginLeft: width/2 - 50,
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  celebName: {
    fontSize: 24,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    alignSelf: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    opacity: .9,
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    backgroundColor: 'transparent',
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
  available: {
    position: 'absolute',
    bottom: 5,
    left: 6,
    marginTop: 2,
    fontSize: 12,
    lineHeight: 15.5,
    letterSpacing: 1,
    color:  "white",
    fontFamily: 'Montserrat-Hairline',
    textShadowOffset: {
      width: -1*StyleSheet.hairlineWidth,
      height: 1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1*StyleSheet.hairlineWidth,
    textShadowColor: 'rgba(34, 139, 214, 0.4)',
    backgroundColor: 'transparent',
  },
  coloredBox: {
    backgroundColor: 'rgba(27, 29, 223, 1)',
    borderWidth: 1*StyleSheet.hairlineWidth,
    borderColor: 'rgba(37, 39, 233, 1)',
    opacity: .9,
    paddingLeft: 2,
    paddingRight: 2,
    height: 18,
    position: 'absolute',
    top: 20,
    right: 20,
    padding: 2,
    paddingLeft: 4,
    alignItems: 'center'
  },
  lightBlueBox: {
    backgroundColor: 'rgba(32, 106, 232, 1)',
    borderColor: 'rgba(97, 155, 252, 1)',
  },
  blueBox: {
    backgroundColor: 'rgba(27, 29, 223, 1)',
    borderColor: 'rgba(64, 65, 245, 1)',
  },
  purpleBox: {
    backgroundColor: 'rgba(167, 14, 205, 1)',
    borderColor: 'rgba(229, 93, 254, 1)',
  },
  redBox: {
    backgroundColor: 'rgba(223, 27, 27, 1)',
    borderColor: 'rgba(252, 69, 59, 1)',
  },
  subtext: {
    color: 'white',
    fontFamily: 'Proxima Nova',
    fontSize: 12,
    textShadowColor: 'rgba(0, 0, 0, 0.3)',
    textShadowRadius: 1*StyleSheet.hairlineWidth,
    textShadowOffset: {width: 1*StyleSheet.hairlineWidth, height: 1*StyleSheet.hairlineWidth}
  },
  linearGradient: {
    width,
    height: 60,
    position: 'absolute',
    left: 0,
  },
});
