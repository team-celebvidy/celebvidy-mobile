import {View, ScrollView} from 'react-native';
import React, { Component } from 'react';
import CelebvidyDetails from '../CelebvidyDetails';
import ClassyActionButton from '../../widgets/ClassyActionButton';
import ActionButton from '../../widgets/ActionButton'
import {GiftedFormManager} from 'react-native-gifted-form';
import ModalSpinner from '../../widgets/ModalSpinner';

import Mixpanel from 'react-native-mixpanel';

import {editOrder} from '../../actions';
import {connect} from 'react-redux';


class EditOrder extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: false,
    }
  }

  save() {
    this.setState({loading: true});

    let values = this.getValues();

    if(!values) {
      this.setState({loading: false});
      alert('Please fix the issues displayed in red');
      return;
    }

    values._id = this.props.order._id;

    return this.props.dispatch(editOrder(values))
      .then(() => {
        this.setState({loading: false});
        setTimeout(() => {
          this.props.navigator.pop();
        }, 200); //let spinner fade out niacely
      })
      .catch((error) => {
        this.setState({loading: false});
        alert(error.message);
      });
  }
  getValues() {
    let values = GiftedFormManager.getValues('editOrder');
    let isValid = GiftedFormManager.validate('editOrder', true).isValid;

    //minor hack to make sure selects receive a checkmark or x when the whole form is validated.
    //since they use their placeholder as default values and show no checkmark until you touch it, this is needed.
    this.refs.details.refs.form.inputs['kind'].setState({neverFocused: false});
    this.refs.details.refs.form.inputs['deadline'].setState({neverFocused: false});

    if(!isValid) return false;
    return values;
  }

  render() {
    let {order} = this.props;

    return (
      <ScrollView style={{flex: 1}} scrollEnabled={false} keyboardDismissMode='on-drag'>
        <ModalSpinner visible={this.state.loading} />

        <CelebvidyDetails
          ref='details'
          viewedAfterCheckout={true}
          scrollEnabled={false}
          editing={true}
          {...order}
          navigator={this.props.navigator}
          formName='editOrder'
          store={({script}) => this.props.dispatch(editOrder({script, _id: order._id}))}
        />

        <View style={{flex: 1, paddingHorizontal: 10, marginTop: 20}}>
          <ClassyActionButton onPress={() => this.save()} />
        </View>
      </ScrollView>
    );
  }
}


export default connect(({currentOrder}, {order}) => {
  return {
    order: {...order, ...currentOrder}, //overwrite pushed order with any edits in redux
  };
})(EditOrder)
