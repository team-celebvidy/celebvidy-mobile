import {View, InteractionManager} from 'react-native';
import React, { Component } from 'react';
import VideoPage from '../RecordFlow/VideoPage';
import LogoAnimation from '../LogoAnimation/LogoAnimation';

export default class Celebvidy extends Component {
  render() {
    let handle = InteractionManager.createInteractionHandle();

    return (
      <View>
        <VideoPage {...this.props} />

        <LogoAnimation visible={true} handle={handle} delay={0} style={{top: -64}} />
      </View>
    );
  }
}
