import  {View, Text, Image, Modal, TouchableHighlight,  StyleSheet, Dimensions, Navigator, ScrollView, TouchableOpacity, InteractionManager} from 'react-native';
import React, { Component } from 'react';
import Mixpanel from 'react-native-mixpanel';
import Icon from 'react-native-vector-icons/Ionicons';
import ImageProgress from 'react-native-image-progress';
import Progress from 'react-native-progress';
import formatMoney from 'format-money';

import styles from '../../styles/FormStyles';
import commonStyles from '../../styles/common';

import CelebvidyDetails from '../CelebvidyDetails';
import EditOrder from './EditOrder';
import Celebvidy from './Celebvidy';

import ActionButton from '../../widgets/ActionButton';
import ButtonPair from '../../widgets/ButtonPair';

import {customerOrderStatus} from '../../misc/orderStatuses';
import {avatarSource, backgroundSource} from '../../misc/sourceUri';
import {MONTHS, YEARS} from '../../misc/MonthsYears';
import {isAdmin} from '../../misc/isAdmin';
import {DEFAULT_AVATAR, DEFAULT_BACKGROUND} from '../../misc/constants';
const {width, height} = Dimensions.get('window');

import { connect } from 'react-redux';
import {editOrder} from '../../actions';

class Order extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      modalVideoVisible: false,
    };
  }
  componentDidMount() {
    if(this.props.interactionHandle) InteractionManager.clearInteractionHandle(this.props.interactionHandle);

    if(this.props.order.new_message_for_customer ) {
      setTimeout(() => {
        if(this.props.order.status === 'order_rejected') {
          alert('Your order has been rejected. Please visit the concierge to correct it.');
        }
        else alert("There's a new message from the concierge for you.");
      }, 750);
    }
  }

  nevadaTaxAmount() {
    return this.finalAmount() * .046; //4.6% is the tax rate in nevada
  }
  nevadaTaxFormatted() {
    return formatMoney(this.nevadaTaxAmount());
  }
  finalAmount() {
    if(this.props.order.recipient_phone) {
      return parseFloat(this.props.order.amount+'.99');
    }
    else {
      return this.props.order.amount;
    }
  }
  totalFormatted() {
    if(this.props.order.state === 'NV') {
      return formatMoney(this.finalAmount() + this.nevadaTaxAmount());
    }
    else {
      return formatMoney(this.finalAmount());
    }
  }

  chargedTo() {
    let {payment_method, last_4, paypal_email} = this.props.order;

    switch(payment_method) {
      case 'card':
        return `charged to card ending in: ${last_4}`;
      case 'paypal':
        if(paypal_email) return `paid via paypal: ${paypal_email}`; //COLLECTING PAYPAL EMAIL NOT SUPPORTED YET, BUT WILL BE SOON
        else return `paid via paypal`;
    }

  }
  render() {
    let {order, navigator, isCelebrity} = this.props;
    let celeb = order.celeb;
    let statusObj = customerOrderStatus(order.status);

    return (
      <View style={{height: height - 64, backgroundColor: 'transparent'}}>

        {isCelebrity && order.youtube_id &&
          <Modal visible={this.state.modalVideoVisible} animated={true}>
            <View style={{height: 64}} />
            <Celebvidy {...this.props} youtube_id={order.youtube_id[order.youtube_id.length-1]} onPressBack={() => this.setState({modalVideoVisible: false})}/>
          </Modal>
        }

        <ScrollView>

          <View style={[commonStyles.coloredBox, {borderWidth: 0, backgroundColor: statusObj.color}]}>
            <Icon name={statusObj.icon} size={12} color='white' />
            <Text style={commonStyles.subtext}>{statusObj.status}</Text>
          </View>

          <View style={{flex: 1, alignSelf: 'center', marginTop: 25, alignItems: 'center'}}>
            <ImageProgress style={ST.avatar}
              source={avatarSource({_id: order.celebrity_id, avatar_photo: order.avatar_photo})}
              defaultSource={DEFAULT_AVATAR}
              indicator={Progress.Pie}
              indicatorProps={{
                size: 120,
                borderWidth: 0,
                color: 'rgba(15, 54, 255, .7)',
                unfilledColor: 'rgba(200, 200, 200, 0.2)'
              }}
            />

          <Text style={[ST.celebName, {fontSize: 20, marginTop: -5, color: 'rgb(184, 184, 184)'}]}>{order.celebrity_name}</Text>
            <Text style={[ST.celebName, {fontSize: 22}]}>Total: {this.totalFormatted()}</Text>
            <Text style={[ST.note, {}]}>{this.chargedTo()}</Text>

          </View>

          {!!order.youtube_id
            ? <ButtonPair
                style={{paddingLeft: 12, paddingRight: 12, marginBottom: 10}}

                button1text='WATCH CELEBVIDY!'
                button1style={{backgroundColor: 'rgb(4, 35, 170)', borderColor: 'rgb(15, 50, 230)'}}
                //button1icon='edit'
                //icon1style={{fontSize: 12, marginTop: 1}}
                button1onPress={() => {
                  Mixpanel.trackWithProperties('Customer Watch Celebvidy', this.props.order);

                  if(isCelebrity) {
                    this.setState({modalVideoVisible: true});
                  }
                  else navigator.pushWithProps('Celebvidy', {order, youtube_id: order.youtube_id[order.youtube_id.length-1]});
                }}

                button2text='CONCIERGE'
                button2icon='chatboxes'
                notification2={this.props.order.new_message_for_customer}
                icon2style={{fontSize: 14, marginTop: 1}}
                button2onPress={() => navigator.pushWithProps('CustomerConcierge', {
                  ...this.props,
                  customer: this.props.customer,
                  isCelebrity,
                })}
               />
            : <ButtonPair
                style={{paddingLeft: 12, paddingRight: 12, marginBottom: 10}}

                button1text='EDIT'
                button1icon='edit'
                icon1style={{fontSize: 12, marginTop: 1}}
                button1onPress={() => this.pushEditOrder()}

                button2text='CONCIERGE'
                button2icon='chatboxes'
                notification2={this.props.order.new_message_for_customer}
                icon2style={{fontSize: 14, marginTop: 1}}
                button2onPress={() => navigator.pushWithProps('CustomerConcierge', {
                  ...this.props,
                  customer: this.props.customer,
                  isCelebrity,
                })}
             />
          }

          <CelebvidyDetails {...this.props.order} formName='editOrder' editable={false} viewedAfterCheckout={this.props.viewedAfterCheckout}  />

          {this.props.order.status === 'order_submitted' && isAdmin(this.props.customer, this.props.celebrity) &&
            <View style={{flex: 1, paddingHorizontal: 16, marginVertical: 10}}>
              <ActionButton
                text='EXCLUSIVE: APPROVE'
                onPress={() => this.approveOrder()}
                backgroundColor='rgb(25, 25, 25)' borderColor='rgb(40, 40, 40)'
              />
            </View>
          }
        </ScrollView>

      </View>
    );
  }

  approveOrder() {
    this.props.dispatch(editOrder({_id: this.props.order._id, status: 'order_approved'}))
      .then(() => this.props.navigator.pop())
      .catch((error) => alert(error.message));
  }

  pushEditOrder() {
    this.props.navigator.pushWithProps('EditOrder', {...this.props, viewedAfterCheckout: true, editing: true});
    setTimeout(() => {
      Mixpanel.trackWithProperties('Customer Edit Order', this.props.order);
    }, 750);
  }
}

export default connect( ({currentOrder, isCelebrity, customer, celebrity}, {order}) => {
  return {
    isCelebrity,
    customer,
    celebrity, //only needed for exclusive admin feature to approve orders
    order: {...order, ...currentOrder}, //overwrite pushed order with any edits
  };
}, null, null, {withRef: true})(Order)


const AVATAR_SIZE = 120;

const ST = StyleSheet.create({
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  celebName: {
    fontSize: 26,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  note: {
    fontSize: 13,
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    lineHeight: 20,
    color: 'white',
    backgroundColor: 'transparent',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
})
