import React, {Component} from 'react';
import { Provider } from 'react-redux';
import MainContainer from './MainContainer';
import configureStore from '../configureStore';
import {Platform, StyleSheet, Text, View} from 'react-native';

const store = configureStore();


export default class App extends Component {
  componentWillMount() {
    let {DEVICE, DEVELOPER_COMPUTER_IP, ENVIRONMENT} = this.props;
    DEVICE = getDevice(this.props.DEVICE);

    console.log('APP MOUNT', DEVELOPER_COMPUTER_IP, DEVICE, ENVIRONMENT, this.props);

  }
  render() {
    const store = configureStore(this.props.DEVELOPER_COMPUTER_IP);

    return (
      <Provider store={store}>
        <MainContainer />
      </Provider>
      //<View> asd </View>
    );
  }
}




export function getDevice(DEVICE) {
  /**
  @"i386"      on 32-bit Simulator
  @"x86_64"    on 64-bit Simulator

  @"iPhone5,1" on iPhone 5 (model A1428, AT&T/Canada)
  @"iPhone5,2" on iPhone 5 (model A1429, everything else)
  @"iPhone5,3" on iPhone 5c (model A1456, A1532 | GSM)
  @"iPhone5,4" on iPhone 5c (model A1507, A1516, A1526 (China), A1529 | Global)
  @"iPhone6,1" on iPhone 5s (model A1433, A1533 | GSM)
  @"iPhone6,2" on iPhone 5s (model A1457, A1518, A1528 (China), A1530 | Global)

  @"iPhone7,1" on iPhone 6 Plus
  @"iPhone7,2" on iPhone 6
  @"iPhone8,1" on iPhone 6S
  @"iPhone8,2" on iPhone 6S Plus
  @"iPhone8,4" on iPhone SE
  **/

  switch(DEVICE) {
    case 'x86_64':
      return 'simulator';
    case "iPhone5,1":
    case "iPhone5,2":
    case "iPhone5,3":
    case "iPhone5,4":
    case "iPhone6,1":
    case "iPhone6,2":
      return 'iPhone5'
    case "iPhone7,1":
    case "iPhone7,2":
    case "iPhone8,1":
    case "iPhone8,2":
    case "iPhone8,4":
      return 'iPhone6';
  }

}
