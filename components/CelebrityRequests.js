import {StyleSheet, Text, View, Image, TextInput, Dimensions, SegmentedControlIOS, TouchableHighlight} from 'react-native';
import React, { Component } from 'react';
import {Timer} from 'react-timer-mixin';
import ExNavigator from 'react-navigation';
import Button from 'react-native-button';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import Gradient from 'react-native-linear-gradient';
import _ from 'underscore';
import Mixpanel from 'react-native-mixpanel';

import UltimateListView from '../widgets/UltimateListView';
import CelebrityRequestRow from './CelebrityRequestRow';
import SegmentedControl from '../widgets/SegmentControl';


const {height, width} = Dimensions.get('window');

import {getCelebrityRequests, selectRequestsTab} from '../actions';
import {connect} from 'react-redux';



class CelebrityRequests extends Timer(Component) {  //check later
  constructor(props, context) {
    super(props, context);
    this.state = {
      segmentIndex: 0,
    };
  }

  componentDidMount() {
    this.mountTime = new Date;
    this.notificationPushRequest(this.props.pushedRequestId);
  }
  componentWillReceiveProps({pushedRequestId}) {
    if(pushedRequestId !== this.props.pushedRequestId) {
      this.notificationPushRequest(pushedRequestId);
    }
  }
  shouldComponentUpdate() {
    if(new Date - this.mountTime < 50) {
      return false; //on mount, the listview loads itself, so no need to refresh based on a push notification giving us a new pushedRequestId (for which we'll need our listview up to date to discover)
    }

    return true;
  }
  notificationPushRequest(pushedRequestId) {
    let {dispatch, navigator} = this.props;

    if(pushedRequestId) {
      let routes = navigator.getCurrentRoutes();

      navigator.popToTop();

      setTimeout(() => {
        dispatch({type: 'OPENED_PUSH_NOTIFICATION_REQUEST'});
      }, 1000);

      setTimeout(() => {
        let order = _.find(this.props.celebrityRequests, (order) => order._id === pushedRequestId);

        if(!order) {//just to be safe we dont push an order we dont actually have (such as when during testing we are swithing between accounts that may not have the order)
          alert('No request was found. Perhaps you are not logged into the account for whom the request was for.')
          return;
        }

        dispatch({type: 'SET_CURRENT_REQUEST', payload: order});
        setTimeout(() => navigator.pushWithProps('Request', {...this.props, order}), 150);
        setTimeout(() => Mixpanel.trackWithProperties('Celebrity View Request', order), 2000)
      }, 2000);
    }
  }



  _onFetch(page = 1, callback, options) {
    this.props.dispatch((dispatch, getState) => {
      let state = getState();

      if(!state.connected && (!state.user || !state.user.loggedIn)) { //HACK ALERT: GiftedListView expects array of rows to stop the spinner (even if empty)
        setTimeout(() => {
          dispatch({
            type: 'CELEBRITY_REQUESTS',
            payload: {page: 1, rows: []},
          });
        }, 2000);
        return Promise.resolve();
      }
      else return this.props.dispatch(getCelebrityRequests({page, ...options}))
    })
      .catch((error) => {
        alert('We had a problem loading your account. Please login again.');

        setTimeout(() => {
          this.props.dispatch({type: "LOGOUT"});
        }, 3000);
      });
  }

  refresh() {
    return this.props.dispatch({type: 'REFRESH_CELEBRITY_REQUESTS'});
  }

  tabIndex() {
    if(this.props.requestsFetchOptions.status === 'new') return 0;
    else if(this.props.requestsFetchOptions.status === 'completed') return 1;
    else return 2;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{borderLeftWidth: 5, borderRightWidth: 5, borderLeftColor: 'rgb(19, 19, 19)', borderRightColor: 'rgb(19, 19, 19)'}}>
          <SegmentedControl
            titles={["NEW ORDERS", "COMPLETED", "REJECTED"]}
            index={this.tabIndex()}
            onPress={segmentIndex => {
              let status = segmentIndex === 0 ? 'new' : (segmentIndex === 1 ? 'completed' : 'rejected');
              this.props.dispatch({type: 'SELECT_REQUESTS_TAB', payload: status})
            }}

            stretch={true}
            style={styles.segmentControl}
            barColor={"rgba(27,58,223,1)"}
            barStyle={{height: 1}}
            barPosition='top'
            underlayColor={"rgba(27,58,223,1)"}
            width={width - 10}
            renderTitle={(title, i) => {
              let index = this.tabIndex();

              return (
                <View style={[styles.titleView, i === index && styles.selectedTitleView]}>
                  <Text style={[styles.title, i === index && styles.selectedTitle]}>{title}</Text>
                </View>
              );
            }}
          />
        </View>


        <UltimateListView
          ref={(listview) => this.listview = listview}
          rowView={(data, sectionId, rowIndex) => this._renderRowView(data, rowIndex)}
          onFetch={this._onFetch.bind(this)}
          refreshBackgroundInputRange={[-200, -70, -15, 0]}
          refreshBackgroundColorsOutputRange={['rgba(0,0,0,0)', 'rgba(3, 28, 152, .2)', 'rgba(3, 28, 152, .3)', 'rgba(0,0,0,0)']}
          refreshFlashBackgroundColorsOutputRange={['rgba(0,0,0, .3)',  'rgba(18, 18, 180, 0.15)', 'rgba(0,0,0, .3)']}
          rows={this.props.celebrityRequests}
          fetchOptions={this.props.requestsFetchOptions}
          emptyMessage='You have no celebvidys yet'
          limit={10}
        />


        <View style={{position: 'absolute', top: 0, left: 0, width: 5, height, backgroundColor: 'rgb(19,19,19)'}} />
        <View style={{position: 'absolute', top: 0, right: 0, width: 5, height, backgroundColor: 'rgb(19,19,19)'}} />
      </View>
    );
  }
  renderSeparator(sectionId, rowIndex, adjacentRowHighlighted) {
    return (
      <View key={`${sectionId}-${rowIndex}`} style={{height: 3, backgroundColor: "rgba(116,116,116,0.16)"}}></View>
    );
  }
  _renderRowView(order, index) {
    return <CelebrityRequestRow {...this.props} order={order} index={index} />;
  }
}

export default connect(
  ({celebrityRequests, requestsFetchOptions, pushedRequestId}) => ({celebrityRequests, requestsFetchOptions, pushedRequestId}), null, null ,{withRef: true}
)(CelebrityRequests)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0, .2)',
  },
  navBar: {
    height: 64,
  },
  pageTitle: {
    fontSize: 18,
    fontFamily: 'ProximaNova-Thin',
    lineHeight: 19,
    color: 'white',
    marginTop: 14,
    marginLeft: 10,
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -2*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor:  "rgba(13,50,246,0.6)" ,
  },
  segmentControl: {
    backgroundColor: 'black',
    flex: 0,
    height: 43,
    //borderBotomWidth: 1*StyleSheet.hairlineWidth,
    //borderBottomColor: 'rgba(27,58,233,.2)',
  },
  titleView: {
    backgroundColor: "rgba(21,21,24,1)",
    backgroundColor: 'rgba(116,116,116,0.16)',
    borderTopColor: 'red'
  },
  selectedTitleView: {
    backgroundColor: "black",

  },
  title: {
    color: 'gray',
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    paddingHorizontal: 2,
    paddingVertical: 14,
  },
  selectedTitle: {
    color: 'white',
  },
});
