import {StyleSheet, Text, View, Image, Dimensions, InteractionManager} from 'react-native';
import React, { Component } from 'react';
import {default as ScrollableTabViewWidget} from 'react-native-scrollable-tab-view';
import Gradient from 'react-native-linear-gradient';

const {height, width} = Dimensions.get('window');

import TabBar from '../widgets/TabBar';
import Navigator from '../widgets/Navigator';

import CustomerAccount from './CustomerAccount/CustomerAccount';
import CelebrityOwnProfile from './CelebrityOwnProfile';

import CelebrityRequests from './CelebrityRequests';
import CustomerOrders from './CustomerOrders';

import Celebrities from './Celebrities';
import Industries from './Industries';
import Share from './Share.js'

import * as actions from '../actions';
import { connect } from 'react-redux';



class ScrollableTabView extends Component {
  //USED ONLY BY DEVELOPER MENU! (`page` prop is never set externally otherwise)
  //NOTE: Used as `componentWillReceiveProps`, but with the added benefit that we can force re-renderings to never happen :)
  shouldComponentUpdate(nextProps) {
    let {page, scrollEnabled} = nextProps;
    //For performance reasons we do this imperatively internally (rather than passing the `page` prop to ScrollableTabViewWidget),
    //but expose a declarative API to parents. The tab bar underline becomes glitchy if we pass `page` while having it in sync with redux state :(
    if(page !== this.props.page && page !== this.previousPage) {
      this.scrollableTabView.goToPage(page);
    }

    if(scrollEnabled !== this.props.scrollEnabled) {
      this.scrollableTabView.scrollView.setNativeProps({scrollEnabled});
    }

    return false; //never re-render this component, just directly manipulate as needed ;)
  }

  onChangeTab({i: page}) {
    if(page === 3 && this.previousPage === 4) {
      this.refs.Share.getWrappedInstance().refs.tweetBox.blur(); //super annoying, but necessary
    }

    this.previousPage = page;
    this.props.goToPage(page);
  }

  render() {
    let {scrollEnabled, page} = this.props;

    return (
      <View style={styles.container}>
        <ScrollableTabViewWidget
          ref={(scrollableTabView) => { this.scrollableTabView = scrollableTabView; } }
          locked={!scrollEnabled}
          onChangeTab={this.onChangeTab.bind(this)}
          initialPage={page || 0}
          tabBarPosition="overlayBottom"
          tabBarUseIcons={true}
          gap={100}
          tabBarBackgroundColor={'#111010'}
          tabBarUnderlineColor={  "rgb(4, 0, 215)"  }
          tabBarActiveTextColor={"white"}
          tabBarInactiveTextColor={"rgba(136,136,136,1)"}
          contentProps={{
            keyboardShouldPersistTaps: true,
          }}
          renderTabBar={() => <TabBar />}
        >
          <Page1 tabLabel='gear-b' goToPage={this.props.goToPage} />

          <Page2 tabLabel='film-marker' goToPage={this.props.goToPage} />

          <View tabLabel='android-home' style={styles.tabView}>
            <Industries goToPage={this.props.goToPage} />
          </View>

          <View tabLabel='person-stalker' style={styles.tabView}>
            <Gradient
              locations={[0, .3, .5, .7, 1]}
              start={[0, 1]}
              end={[1, 1]}
              colors={['rgba(14,13,13,1)', 'black', 'black', 'black', 'rgba(14,13,13,1)']}
              style={{position: 'absolute', top: 0, left: -150, width: width + 300, height: 64,}}
            />

            <Navigator ref='Celebrities'
              initialComponent={'Celebrities'}
              navigationBarStyle={{backgroundColor: 'transparent'}}
              goToPage={this.props.goToPage}
              drawer={this.props.drawer}
            />
          </View>

          <View tabLabel='social-twitter' style={styles.tabView}>
            <Share ref='Share' />
          </View>

        </ScrollableTabViewWidget>
      </View>
    );
  }
}

export default connect(
  ({page, scrollEnabled, isCelebrity}) => ({page, scrollEnabled, isCelebrity}),
  actions
)(ScrollableTabView)



//render these 2 pages separately so that `isCelebrity` isn't needed as a prop in ScrollableTabView
//NOTE: you may see it there as a prop, but keep in mind `shouldComponentUpdate` keeps it from ever rendering more than once.
//`shouldComponentUpdate` imperatively sets the things it needs to set such as scrollEnabled
const Page1 = connect(
  ({isCelebrity, goToPage}) => ({isCelebrity})
)(({isCelebrity, goToPage}) => {
  return(
    <View style={styles.tabView}>
      {!isCelebrity && <Navigator initialComponent={'CustomerAccount'}  />}
      {isCelebrity && <View style={{position: 'absolute', top: 0, left: -300, width: width + 300, height: 64, backgroundColor: 'black'}} />}
      {isCelebrity && <CelebrityOwnProfile goToPage={goToPage} />}
    </View>
  );
})


const Page2 = connect(
  ({isCelebrity}) => ({isCelebrity})
)(({isCelebrity, goToPage}) => {
  return(
    <View style={styles.tabView}>
      {!isCelebrity && <Navigator initialComponent={'CustomerOrders'} goToPage={goToPage} />}

      {isCelebrity && <Gradient
        locations={[0, .275,  1]}
        start={[0, 1]}
        end={[1, 1]}
        colors={['black', 'rgba(14,13,13,1)', 'rgba(14,13,13,1)']}
        style={{position: 'absolute', top: 0, left: -100, width: width + 200, height: 64,}}
      />}

      {isCelebrity && <Navigator initialComponent={'CelebrityRequests'} goToPage={goToPage}  />}
    </View>
  );
})




var styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    width,
    height,
    backgroundColor: 'transparent',
  },
  tabView: {
    width,
    height,
  },
  text: {
    color: '#99a9ba',
    fontSize: 18,
    fontFamily: 'Kailasa',
    textShadowColor: '#000',
    textShadowOffset: {width: -0.5, height: 0.5},
    textShadowRadius: 2,
  },
  icon: {
    width: 300,
    height: 300,
    alignSelf: 'center',
  }
});
