import {StyleSheet, Text, View, Image, ScrollView, Dimensions, StatusBar, InteractionManager} from 'react-native';
import React, { Component } from 'react';
import Gradient from 'react-native-linear-gradient';
import Drawer from './Drawer';
import LogoSvg from './LogoSvg';

const {height, width} = Dimensions.get('window');

import {connect} from 'react-redux';


export default class Panel extends Component {
  //shouldComponentUpdate(nextProps) {
  //  if(nextProps.panelClipped !== this.props.panelClipped) {
  //    return true;
  //  }
  //  return false;
  //}

  render() {
    //if(this.props.panelClipped) return null;

    return (
      <View style={styles.container}>
        <LogoSvg
          size={300}
          color='rgb(20, 20, 20)'
          strokeWidth={10}
          style={styles.starBg}
        />

        <View style={{position: 'absolute', top: 0, left: 0, height: 64, width, backgroundColor: 'rgba(14,13,13,1)'}} />

        <Drawer />
      </View>
    );
  }
}

//export default connect(({panelClipped}) => ({panelClipped}))(Panel)



var styles = StyleSheet.create({
  starBg: {
    position: 'absolute',
    left: width/2 - 300/2,
    top: height/2 - 300/2,
    width: 300,
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    width,
    height,
    backgroundColor: 'transparent',
  },
});
