import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, Navigator, Animated, Easing} from 'react-native';
import React, { Component } from 'react';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import _ from 'underscore';

import styles from '../styles/FormStyles';
import commonStyles from '../styles/common';

import TextInput from '../widgets/GiftedTextInput';
import TextArea from '../widgets/GiftedTextArea';
import SubmitButton from '../widgets/GiftedSubmitButton';
import ClassyActionButton from '../widgets/ClassyActionButton';
import Select from '../widgets/GiftedSelect';
import NumberInput from '../widgets/GiftedNumberInput';
import GiftedModalWidget from '../widgets/GiftedModalWidget';
import Underline from '../widgets/Underline';
import {NextSubmitButton} from '../widgets/NavButton';

import {ORDER_TYPES} from '../misc/OrderTypes';
import {DAYS, MONTHS, YEARS} from '../misc/MonthsYears';
import orderExtraNote from '../misc/orderExtraNote';

var {width, height} = Dimensions.get('window');

import PaymentInfo from './CheckoutFlow/PaymentInfo';


export default class CelebvidyDetails extends Component {
  constructor(props, context) {
    super(props, context);
    this.inputs = [];
    this.state = {
      kind: 'Happy Birthday',
    };

    this.extraNoteOpacity = new Animated.Value(0);
  }

  componentDidMount() {
    Animated.timing(this.extraNoteOpacity, {
      toValue: 1,
      duration: 400,
      delay: 800,
    }).start();
  }

  isCreatingOrder() {
    return this.props.editable !== false && !this.props.viewedAfterCheckout
  }
  isCreatingOrEditingOrder() {
    return this.props.editable !== false;
  }
  isViewedByCelebrity() {
    return this.props.formName === 'editRequest';
  }
  shouldShowExclusiveFeatures() {
    return this.props.can_automatically_approve_request || this.props.can_automatically_approve_video;
  }

  getDeadlinePlaceholder() {
    let date = new Date;

    let month = ((date.getMonth() + 1) % 12) + 1;
    let day = 1;
    let year = (date.getMonth() === 11) ? date.getUTCFullYear() + 1 : date.getUTCFullYear();

    return `${month}/${day}/${year}`;
  }
  parseDeadline(deadline) {
    if(!deadline) return undefined;
    else if(_.isDate(deadline) || (typeof deadline === 'string' && deadline.indexOf('-') > -1)) {
      return moment(deadline).format('M/D/YYYY'); //deadline from server in datetime format
    }
    else return deadline; //deadline before changed to datetime on server
  }

  render() {
    let {formName, celeb, navigator, editable, viewedAfterCheckout} = this.props;

    let {
      recipient, pronounced, kind, deadline, gift, showcase, script,
      can_automatically_approve_request, can_automatically_approve_video, automatically_approve_request, automatically_approve_video,
      completed_at, phase,
    } = this.props;

    let showExclusive = this.shouldShowExclusiveFeatures();

    let deadlinePlaceholder = this.getDeadlinePlaceholder();
    deadline = this.parseDeadline(deadline);
    let isCompleted = /complete|canceled/.test(phase);
    let completedAt = completed_at ? moment(completed_at).format('M/D/YYYY') : 'n/a';

    return (
      <GiftedForm
        formName={formName}
        ref='form'
        scrollEnabled={typeof this.props.scrollEnabled !== 'undefined' ? this.props.scrollEnabled : editable !== false}
        style={[styles.giftedForm, {paddingTop: 0, flex: 0, height: this.props.height}]}
        openModal={(route) => navigator.push(route)}
        clearOnClose={false}
        validators={this.getValidators()}
      >
        <View style={{height: 10}} />

        <TextInput
          name='recipient'
          title='Recipient'
          editable={this.props.editable}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref='recipient'
          autoCapitalize="words"
          onSubmitEditing={(value) => this.refs.pronounced.focus()}
          value={recipient}
        />

        <TextInput
          name='pronounced'
          title='Pronounced'
          editable={this.props.editable}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref='pronounced'
          autoCapitalize="words"
          onSubmitEditing={(value) => this.refs.kind.focus()}
          value={pronounced}
        />

        <Select
          name='kind'
          title='Kind'
          longTitle='Celebvidy Kind'
          pickers={[ORDER_TYPES]}
          selections={kind ? [kind] : ['Happy Birthday']}
          editable={this.props.editable}
          ref='kind'
          onSubmitEditing={(value) => {
            this.setState({kind: value});
            this.refs.deadline.focus();
          }}
          placeholder='Happy Birthday'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          value={kind}
         />


         {!isCompleted
            ? <Select
              name={'deadline'}
              title={'Deadline'}
              pickers={[MONTHS, DAYS, YEARS]}
              editable={this.props.editable}
              placeholder={deadlinePlaceholder}
              selections={deadline ? deadline.split('/') : deadlinePlaceholder.split('/')}
              validIcon={'checkmark'}
              invalidIcon={'close-round'}
              widgetStyles={styles.input}
              underlined={true}
              ref='deadline'
              onSubmitEditing={(value) => this.refs.script.onPress()}
              value={deadline}
             />
           : <View>
               <Select
                 name={'completed'}
                 title={'Completed'}
                 pickers={[MONTHS, DAYS, YEARS]}
                 editable={this.props.editable}
                 validIcon={'checkmark'}
                 invalidIcon={'close-round'}
                 widgetStyles={styles.input}
                 underlined={true}
                 valid={true}
                 ref='completed'
                 value={completedAt}
                />
             </View>
          }


       {this.isCreatingOrEditingOrder()
         ? [
              <View key={0} style={{height: 20}} />,

              <GiftedForm.SwitchWidget
                  name='gift'
                  title='Is this a Gift?'
                  onTintColor="#2038D8"
                  widgetStyles={styles.widget}
                  key={1}
                  value={gift}
              />,

              <View key={2} style={{height: 10}} />,

              <GiftedForm.SwitchWidget
                name='showcase'
                title='Can Celebvidy showcase the video?'
                onTintColor='#2038D8'
                widgetStyles={styles.widget}
                key={3}
                value={showcase}
              />,

              <View key={5} style={{height: 20}} />,

              <GiftedModalWidget
                title={this.state.kind === 'Ask Me Anything' ? 'Question' : 'Script'}
                displayValue='script'
                ref='script'
                name='script'
                editable={this.props.editable}
                scrollEnabled={false} // true by default
                widgetStyles={styles.modalWidget}
                disclosure={this.props.editable}
                underlined={true}
                validIcon={'checkmark'}
                invalidIcon={'close-round'}
                icon='ios-book'
                key={this.state.kind === 'Ask Me Anything' ? 66 : 6}
                value={script}
                savePrevious={this.props.preSave}
              >
                 <View style={{height: 80}} />

                <TextArea
                  formName='celebvidyDetails'
                  name='script'
                  autoFocus={false}
                  placeholder={this.state.kind === 'Ask Me Anything' ? 'Ask me anything...' : "Write the script you want "+this.props.celebrity_name+" to follow..."}
                  placeholderTextColor="rgba(100,100,100,1)"
                  widgetStyles={styles.largeTextArea}
                  keyboardAppearance='dark'
                  //autoCorrect={false}
                  scrollEnabled={false}
                  maxCharacters={200}
                  value={script}
                />

                <View style={{height: 15}} />
                <ClassyActionButton text='SAVE' onPress={() => {
                    let script = GiftedFormManager.getValue(formName, 'script');
                    if(!script) return alert('The celebvidy script cannot be empty');

                    this.props.navigator.pop(); //no loading indicator, just pop. they can get an alert on the main page in the rare case that something bad happens.

                    this.refs.script.setState({validationErrorMessage: null});
                    this.props.store({script}, false).catch((error) => alert(error.message));
                }}/>

              </GiftedModalWidget>,

              showExclusive && <GiftedModalWidget
                title='Exclusive Features'
                displayValue='exclusive'
                longTitle={true}
                ref='exclusive'
                editable={this.props.editable}
                scrollEnabled={false} // true by default
                widgetStyles={styles.modalWidget}
                disclosure={this.props.editable}
                underlined={true}
                icon='ios-star'
                validIcon={'ios-star'}
                invalidIcon={'close-round'}
                isValid={automatically_approve_request || automatically_approve_video}
                key={7}
              >

                <View style={{height: 80}} />

                {can_automatically_approve_request ? <GiftedForm.SwitchWidget
                  name='automatically_approve_request'
                  title='Automatically approve request?'
                  onTintColor='#2038D8'
                  widgetStyles={styles.widget}
                  formName='createOrder'
                  value={automatically_approve_request}
                /> : <View />}

                <View key={7} style={{height: 10}} />
                {can_automatically_approve_video ? <GiftedForm.SwitchWidget
                  name='automatically_approve_video'
                  title='Automatically approve celebvidy?'
                  onTintColor='#2038D8'
                  widgetStyles={styles.widget}
                  formName='createOrder'
                  value={automatically_approve_video}
                /> : <View />}

                <View style={{height: 15}} />

                <ClassyActionButton text='SAVE' onPress={() => {
                    let {automatically_approve_request, automatically_approve_video} = GiftedFormManager.getValues(formName);

                    this.props.navigator.pop();

                    GiftedFormManager.updateValue(formName, 'automatically_approve_request', automatically_approve_request);
                    GiftedFormManager.updateValue(formName, 'automatically_approve_video', automatically_approve_video);

                    let values = {automatically_approve_request, automatically_approve_video};

                    this.props.store(values, false);
                }}/>

              </GiftedModalWidget>,

              this.isCreatingOrder() && <Text key={8} style={[styles.note, {marginTop: 20}]}>NOTE: you can change your order at any time, pending additional approval</Text>,
            ]
          : this.renderNonEditableDetails()
        }


      </GiftedForm>
    );
  }

  renderNonEditableDetails() {
    let extra = orderExtraNote(this.props);
    let opacity = this.extraNoteOpacity;

    return (
      <View>
        <View style={{marginTop: 30}}>
          <View style={{flexDirection: 'row'}}>
            <Icon name='ios-book' color='rgb(65, 66, 66)' size={18} style={{marginRight: 6, marginTop: -1}} />
            <Text style={[commonStyles.textInputTitle, {marginBottom: 10}]}>Script</Text>
          </View>

          <Underline />
          <Text style={[commonStyles.textInput, {marginTop: 8, textAlign: 'justify', color: 'rgb(194, 192, 192)'}]}>
            {this.props.script}
          </Text>

          <View style={{height: 15}} />
        </View>

        {extra && !this.isViewedByCelebrity() &&
          <Animated.Text style={[styles.fieldSubtext, {opacity, textAlign: 'center', marginTop: 15, marginLeft: 16, fontSize: 14, marginBottom: 0, color: 'gray', backgroundColor: 'transparent'}]}>
            Extra: {extra}
          </Animated.Text>
        }
      </View>
    );
  }

  renderExclusiveFeatures(key) {
    return (
      <View style={{marginTop: 30}} key={key}>
        <View style={{flexDirection: 'row'}}>
          <Icon name='ios-book' color='rgb(65, 66, 66)' size={18} style={{marginRight: 6, marginTop: -1}} />
          <Text style={[commonStyles.textInputTitle, {marginBottom: 10}]}>Exclusive Features</Text>
        </View>

        <Underline />
      </View>
    );
  }

  getValidators() {
    return {
      recipient: {
        title: 'Recipient Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 80],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      pronounced: {
        title: 'Recipient pronunciation',
        validate: [{
          validator: 'isLength',
          arguments: [1, 80],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      script: {
        title: 'Celebvidy Script',
        validate: [{
          validator: 'isLength',
          arguments: [1, 200],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      deadline: {
        title: 'Deadline',
        validate: [{
          validator: (...args) => {
            let selectedDate = new Date(args[0]);
            return selectedDate > new Date;
          },
          message: this.props.editable === false ? 'Deadline missed!' : 'The deadline must be in the future'
        }]
      }
    };
  }
}
