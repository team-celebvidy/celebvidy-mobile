import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../styles/FormStyles';

import TextInput from '../../widgets/GiftedTextInput';
import SubmitButton from '../../widgets/GiftedSubmitButton';
import NumberInput from '../../widgets/GiftedNumberInput';

var {width, height} = Dimensions.get('window');

import * as actions from '../../actions';
import { connect } from 'react-redux';


export default class CustomerBasicInfo extends Component {
  constructor(props, context) {
    super(props, context);
    this.inputs = [];
    this.state = {};
  }

  save(isValid, values, postSubmit=null) {
    if(!isValid) return alert('Please fix the issues displayed in red');

    return this.props.updateCustomer(values)
      .then(() => {
        postSubmit();
        this.props.navigator.pop();
      })
      .catch((error) => {
        postSubmit();
        alert(error.message); //rarely will be called because of gifted-form validation in red, but would be a human-readable message thrown from the server
      })
  }


  render() {
    let {first_name, last_name, email, phone} = this.props;

    return (
      <GiftedForm
        formName='basicInfo'
        style={[styles.giftedForm, {marginTop: 20}]}
        validators={{
          first_name: {
            title: 'First Name',
            validate: [{
              validator: 'isLength',
              arguments: [1, 48],
              message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
            }]
          },
          last_name: {
            title: 'Last Name',
            validate: [{
              validator: 'isLength',
              arguments: [1, 48],
              message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
            }]
          },
          email: {
            title: 'Email address',
            validate: [{
              validator: 'isLength',
              arguments: [6, 255],
              message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
            },{
              validator: 'isEmail',
              message: 'Invalid Email Addresss'
            }]
          },
          phone: {
            title: 'Phone',
            validate: [{
              validator: (num) => {
                if (!num) return true;
                else {
                  num = num.replace(/\D/g,'');
                  if(num.length !== 10 && num.length != 11) return false;
                }

                return true;
              },
              message: 'Invalid Phone Number'
            }]
          },
        }}
      >
        <GiftedForm.SeparatorWidget />

        <TextInput
          name='first_name'
          title='First Name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[1].focus()}
          value={first_name}
        />

        <TextInput
          name='last_name'
          title='Last Name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[2].focus()}
          value={last_name}
        />


        <TextInput
          name='email'
          title='Email'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          keyboardType='email-address'
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='none'
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[3].focus()}
          value={email}
        />

        <NumberInput
          name='phone'
          title='Phone'
          placeholder='xxx-xxx-xxxx'
          returnKeyType='next'
          maxLength={14}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
          format='phone'
          value={phone}
        />

        <GiftedForm.SeparatorWidget />
        <GiftedForm.SeparatorWidget />

        <SubmitButton
          {...this.props}
          formName='basicInfo'
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            this.save(isValid, values, postSubmit);
          }}
        />

      </GiftedForm>
    );
  }
}

// export default connect(
//   ({customer}) => ({...customer}),
//   actions
// )(CustomerBasicInfo)
