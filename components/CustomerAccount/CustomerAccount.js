import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, TouchableOpacity} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import Communications from 'react-native-communications';

import formStyles from '../../styles/FormStyles';
import celebrityAccountValidators from '../../misc/celebrityAccountValidators'
import AccountItem from '../../widgets/AccountItem';
import NavButton from '../../widgets/NavButton';
import ClassyActionButton from '../../widgets/ClassyActionButton';

import CustomerLoginSignup from './CustomerLoginSignup';

var {width, height} = Dimensions.get('window');

import * as actions from '../../actions';
import { connect } from 'react-redux';



class CustomerAccount extends Component {
  emailSupport() {
    Communications.email(['support@celebvidy.com'], null, null, 'new customer support', null);
  }

  render() {
   let {navigator, loggedIn, customer} = this.props;
   if(!loggedIn) return this.renderLogin();

   return (
     <View style={[formStyles.giftedForm, {width: width, paddingLeft: 20, paddingRight: 20, alignSelf: 'stretch'}]}>

       <Text style={{fontFamily: 'Montserrat-Light', fontSize: 24, color: 'white', marginTop: 35, marginBottom: 10}}>
         {customer.first_name ? 'Welcome, '+customer.first_name : 'Welcome'}
       </Text>

       <View style={{height: 35}} />

       <AccountItem showDisclosure={true} marginLeft={10} underline={true} text='Basic Info' overline={true} onPress={() => navigator.pushWithProps('CustomerBasicInfo')} />
       {false && <AccountItem showDisclosure={true} marginLeft={10} underline={true} text='Payment Info' onPress={() => navigator.pushWithProps('PaymentInfo')} />}
       <AccountItem showDisclosure={true} marginLeft={10} underline={true} text='Change Password' onPress={() => navigator.pushWithProps('ChangePassword')} />
       <AccountItem showDisclosure={true} marginLeft={10} underline={true} text='FAQ' onPress={() => navigator.pushWithProps('Faq')} />
       <AccountItem showDisclosure={true} marginLeft={10} underline={true} text='Support' onPress={() => this.emailSupport()} />
       <AccountItem showDisclosure={true} marginLeft={10} underline={true} text='Take Tour Again' onPress={() => this.props.dispatch(actions.toggleTour(true, 'again'))} />
       <AccountItem showDisclosure={true} marginLeft={10} underline={true} text='Signup as Celebrity' onPress={() => this.props.dispatch(actions.logout())} />

       <View style={{height: 35}} />

       <ClassyActionButton text='Logout' onPress={() => this.props.dispatch(actions.logout())} />
     </View>
   );
  }

  renderLogin() {
    return (
      <View style={{height: height - 64 - 50}}>
        <CustomerLoginSignup {...this.props} style={{backgroundColor: 'transparent', height: height - 50, flex: 0, marginTop: 30}} />

        <TouchableOpacity style={{position: 'absolute', bottom: 50, left: 0, right: 0}} onPress={() => this.props.dispatch(actions.toggleTour(true, 'again'))}>
          <Text style={{fontFamily: 'AvenirNext-Regular', fontSize: 16, color: 'rgb(100,100,100)', textAlign: 'center'}}>
            Take Tour Again
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{position: 'absolute', bottom: 20, left: 0, right: 0}} onPress={() => this.props.dispatch(actions.logout())}>
          <Text style={{fontFamily: 'AvenirNext-Regular', fontSize: 16, color: 'rgb(100,100,100)', textAlign: 'center'}}>
            Signup as Celebrity
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(
  ({user, customer}) => ({loggedIn: user.loggedIn, customer})
)(CustomerAccount)
