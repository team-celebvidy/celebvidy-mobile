import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import dismissKeyboard from 'react-native-dismiss-keyboard';

import styles from '../../styles/FormStyles';

import ActionButton from '../../widgets/ActionButton';
import TextInput from '../../widgets/GiftedTextInput';
import SubmitButton from '../../widgets/GiftedSubmitButton'
import Select from '../../widgets/ModalSelect';

import {MONTHS, YEARS} from '../../misc/MonthsYears';

var {width, height} = Dimensions.get('window');


export default class PaymentInfo extends Component {
  constructor(props, context) {
    super(props, context);
    this.inputs = [];
    this.state = {
      expirationFocused: false,
      expirationShownOnce: false,
      month: '',
      year: '',
      name: '',
      card: '',
      code: '',
      zip: '',
    };
  }

  render() {
    return (
      <GiftedForm
        formName='basicInfo'
        style={[styles.giftedForm, {marginTop: 20}]}
        validators={{
          name: {
            title: `Cardholder's Name`,
            validate: [{
              validator: (name) => {
                if (name === undefined) return false;
                else if(!name.split(' ')[1]) return false;

                return true;
              },
              message: `Cardholder's name is invalid`
            }]
          },
          card: {
            title: 'Credit Card',
            validate: [{
              validator: 'isLength',
              arguments: [16],
              message: '{TITLE} must be between {ARGS[0]} digits'
            }]
          },
          code: {
            title: 'Security Code',
            validate: [{
              validator: 'isLength',
              arguments: [3, 4],
              message: '{TITLE} must be {ARGS[0]} or {ARGS[1]} digits'
            }]
          },
          zip: {
            title: 'Zip Code',
            validate: [{
              validator: 'isLength',
              arguments: [5],
              message: '{TITLE} must be between {ARGS[0]} digits'
            }]
          },
        }}
      >
        <GiftedForm.SeparatorWidget />

        <TextInput
          name='name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          title='Name'
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[1].focus()}
        />

        <TextInput
          name='card'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          title='Card'
          maxLength={16}
          keyboardType='phone-pad'
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[2].focus()}
        />

        <TextInput
          name='code'
          title={'Code'}

          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          placeholder='123'
          keyboardType='phone-pad'
          widgetStyles={styles.input}
          underlined={true}
          onChangeText={(code) => this.setState({code})}
          maxLength={4}
          ref={(input) => this.inputs.push(input)}
        />

        <Select
          title={'Expiration'}
          pickers={[MONTHS, YEARS]}
          hitSlop={{}}
          selections={[this.state.month || '1', this.state.year || '2019']}
          onChange={(index, key, selections) => {
            console.log('SElECT', index, key, selections);
            this.setState({
              month: selections[0],
              year: selections[1]
            });
          }}
          pickerBarStyles={{backgroundColor: 'black'}}
          titleStyle={{color: 'white', fontSize: 24, fontFamily: 'ProximaNova-Thin', marginLeft: 15}}
          closeTextStyle={{color: 'rgba(27,58,223,1)', marginTop: 13, marginRight: 10}}
          pickerContainerStyles={{backgroundColor: "rgba(0,0,0,0.92)", marginLeft: width/2 - 60, width: 120}}
          pickerItemStyles={{fontFamily: 'ProximaNova-Thin'}}
          onShow={() => {
            this.setState({
              expirationFocused: true,
              expirationShownOnce: true
            });

            dismissKeyboard();
          }}
          onHide={() => this.setState({expirationFocused: false})}
         >
           <View style={{
               height: 40,
               borderBottomWidth: 1*StyleSheet.hairlineWidth,
               borderBottomColor: this.state.expirationFocused ? 'rgba(27,92,223,1)' : 'rgba(193,193,193,0.32)',
             }}>

             <View style={{flexDirection: 'row'}}>
               <Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 16, color: 'white', marginTop: 12, marginLeft: 0}}>
                 Expiration
               </Text>

               <Text style={{fontFamily: 'Avenir', fontSize: 16, color: this.state.expirationShownOnce ? 'white' : "rgba(203,203,203,.6)", marginTop: 9, marginLeft: 35}}>
                 {this.state.month || '1'}/{this.state.year || '2016'}
               </Text>
             </View>
           </View>
        </Select>


        <TextInput
          name='zip'
          title='Zip'
          maxLength={5}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          placeholder='90210'
          keyboardType='phone-pad'
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.inputs.push(input)}
        />

        <GiftedForm.SeparatorWidget />
        <GiftedForm.SeparatorWidget />
        <SubmitButton />
      </GiftedForm>
    );
  }
}
