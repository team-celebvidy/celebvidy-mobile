import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  TouchableOpacity,
} from 'react-native';
import React, { Component } from 'react';
const validator = require('validator');
import Mixpanel from 'react-native-mixpanel';

const {width, height} = Dimensions.get('window');

import formStyles from '../../styles/FormStyles';

import ActionButton from '../../widgets/ActionButton';
import ClassyActionButton from '../../widgets/ClassyActionButton';
import ModalSpinner from '../../widgets/ModalSpinner';

import TextInput from '../../widgets/GiftedTextInput';
import AnimateMessageMixin from '../../widgets/AnimateMessageMixin';

import * as actions from '../../actions';
import {connect} from 'react-redux';
import {getState} from '../../configureStore';


class CustomerLoginSignup extends AnimateMessageMixin(React.Component) {  //check later
  static defaultProps = {
    formName: 'customerLoginSignup',
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      emailValid: true,
      message: '',
    };
  }

  componentDidMount() {
    Mixpanel.track('Customer LoginSignup');
  }

  login() {
    let email = this.validateEmail();
    if(email) this.pushPasswordPage(email, this.onLoginSubmit, 'login');                 //pass onLoginSubmit  callback to password page.
  }
  onLoginSubmit(email, password) {
    return this.props.loginWithEmail(email, password)                           //dispatch async action + api request that does all the dirty work
      .then(this.onLoginCompletePushNextPage.bind(this))                        //push the next page, duh
      .catch((error) => alert('Celebvidy failed to log you in: '+error.message));
  }

  signup() {
    let email = this.email = this.validateEmail();

    if(email) {
      this.props.sendConfirmationEmailCode(email);                              //email the user the confirmation code (and return it to the client for storage in redux)
      this.pushEmailConfirmationPage(email, this.onSignupSubmitConfirmationCode);
    }
  }
  onSignupSubmitConfirmationCode(email, code) {
    if(code === getState().confirmEmailCode) {                                  //check whether code entered matches the code secretly stored in store (yes, it's secure; it's just so people don't end up with accounts based on the wrong email; they can't hi-jack accounts with this, as when signup completes it will check if a user already exists with that email)
      this.pushPasswordPage(email, this.onSignupSubmit, 'signingUp');
      return Promise.resolve();
    }
    else return Promise.reject(new Error('The confirmation code you entered does not match what was sent to you.')); //insure `.catch()` still works in EmailConfirmationCode.js
  }
  onSignupSubmit(email, password) {
    return this.props.customerSignupWithEmail(email, password)
      .then(this.onLoginCompletePushNextPage.bind(this, 'signingUp'))
      .catch((error) => alert('Celebvidy failed to sign you up: '+error.message));
  }

  forgotPassword() {
    let email = this.validateEmail();

    if(email) {
      this.props.sendConfirmationEmailCodeForgotPassword(email)                //email the user the password confirmation code (similar to signup, but we DO NOT send to client and store in redux)
        .catch((error) => alert(error.message));

      this.pushEmailConfirmationPage(email, this.onForgotPasswordSubmitConfirmationCode, true); //take em to the confirmation page so they can enter the email they received
    }
  }
  onForgotPasswordSubmitConfirmationCode(email, code) {
    return this.props.findForgotPasswordLoginTokenFromCode(email, code)         //instead, we collect the entered code + email and make sure on the server they verify (the code is stored on the user model in the database for comparison)
      .then((password) => this.props.loginWithEmail(email, password) )          //retrieve user and celebrity details in separate requests to server and store in redux
      .then(() => this.pushPasswordPage(email, this.onForgotPasswordSubmit, 'forgotPassword') )  //go to password page to reset the password
      .catch(() => alert('Celebvidy had an issue validating your confirmation code'));
  }
  onForgotPasswordSubmit(email, password) {
    return this.props.changePassword(password)                                   //at this point the user is already logged in, so we just some basic password reset code on the server using the newly entered password
      .then(() => {
        if(this.props.isCheckout) return;                                       //if it's the checkout flow, just take em to the Confirm page, fuck it

        this.props.gratify({darkenBackground: true, caption: "And you're back!"}); //if it's the CustomerAccount navigator, give em some gratification and...
        return new Promise((resolve, reject) => setTimeout(resolve, 3000));
      })
      .then(this.onLoginCompletePushNextPage.bind(this))                        //...take em back to CustomerAccount (well, to Confirm, if in the CheckoutFlow)
      .catch((error) => alert('Celebvidy failed to reset your password: '+error.message));
  }

  onLoginCompletePushNextPage(page='login') {
    if(page === 'signingUp') {
      Mixpanel.createAlias(getState().user._id);
      Mixpanel.identify(getState().user._id);
      Mixpanel.setOnce({customer: true, user_type: 'customer', $email: this.email, $created: new Date().toISOString(), $last_login: new Date().toISOString()});
      Mixpanel.track('Customer Signup Complete');
    }
    else {
      Mixpanel.identify(getState().user._id);
      Mixpanel.set({$last_login: new Date().toISOString()});
      Mixpanel.track('Customer Login Complete');
    }


    if(this.props.isCheckout) {
      this.props.navigator.pushWithProps('Confirm');
    }
    else {
      this.props.navigator.popToTop();
    }
  }

  //convenience methods to make above code more succinct
  pushEmailConfirmationPage(email, onSubmitConfirmationCode, isForgotPassword=false) {
    if(this.props.isCheckout) {
      Mixpanel.track('Customer Email Confirmation');
    }

    this.props.navigator.pushWithProps('EmailConfirmationCode', {
      formName: this.props.formName,
      email,
      isCheckout: this.props.isCheckout,
      isForgotPassword,
      onSubmitConfirmationCode: onSubmitConfirmationCode.bind(this)
    });
  }
  pushPasswordPage(email, onLoginSubmit, page='login') {
    if(this.props.isCheckout) {
      Mixpanel.track('Customer Password');
    }

    this.props.navigator.pushWithProps('Password', {
      formName: this.props.formName,
      email,
      isCheckout: this.props.isCheckout,
      onLoginSubmit: onLoginSubmit.bind(this),
      [page]: true,
    });
  }

  validateEmail() {
    let email = this.refs.email.getValue();
    let isValid = email && validator.isEmail(email);

    if(email && !isValid) {
      this.setState({emailValid: false}, () => {
        this.flashMessage('Invalid email address');
      });
    }
    else if(email && isValid) this.setState({emailValid: true});
    else {
      this.flashMessage('Please enter your email first'); //user pressed Login or Signup thinking it would take em to another page to start Signup/Login flow
      this.refs.email.focus();
    }

    return isValid ? email : null;
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={{flex: 1, top: -60}}>
          <View style={{height: 60, marginBottom: 40}}>
            <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
              {this.state.message}
            </Animated.Text>
          </View>

          <TextInput
            name='email'
            title='Email'
            formName={this.props.formName}
            placeholder='janedoe@email.com'
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            keyboardType='email-address'
            returnKeyType='default'
            autoCapitalize="none"
            autoCorrect={false}
            widgetStyles={formStyles.login}
            underlined={true}
            ref='email'
            value=""
            valid={this.state.emailValid}
          />


          <View style={{height: 40}} />

          <ClassyActionButton
            touchableStyle={{marginBottom: 20}}
            style={{height: 50,}}
            textStyle={{color: 'rgb(185, 183, 183)'}}
            onPress={() => this.login()}
          >
            LOGIN
          </ClassyActionButton>

          <ClassyActionButton
            touchableStyle={{marginBottom: 20}}
            style={{height: 50, }}
            textStyle={{color: 'rgb(185, 183, 183)'}}
            onPress={() => this.signup()}
          >
            SIGNUP
          </ClassyActionButton>

          <View style={{height: 20}} />

          <TouchableOpacity onPress={() => this.forgotPassword()}>
            <Text style={{color: 'rgba(41, 56, 212, 1)',  fontFamily: 'ProximaNova-Thin', fontSize: 20, textAlign: 'center'}}>
              Forgot Password
            </Text>
          </TouchableOpacity>

        </View>

      </View>
    );
  }
}

export default connect(null, actions)(CustomerLoginSignup)



const styles = StyleSheet.create({
  container: {
    marginTop: -64,
    height,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: 'transparent',
  }
});
