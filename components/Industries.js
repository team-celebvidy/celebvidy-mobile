import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  PixelRatio,
  TouchableHighlight,
} from 'react-native';
import React, { Component } from 'react';
import Gradient from 'react-native-linear-gradient';

const {width, height} = Dimensions.get('window');

const Parallax = require('react-native-parallax');

const IMAGE_WIDTH = Dimensions.get('window').width;
const IMAGE_HEIGHT = IMAGE_WIDTH / 2;

import INDUSTRIES, {getIndustryImageSource} from '../misc/IndustryOptions';
import formatMoney from 'format-money';

import * as actions from '../actions'
import {connect} from 'react-redux';
import ddpClient from '../ddp';
import {HOST} from '../config';


class Industries extends Component {
  shouldComponentUpdate() {
    return false;
  }

  componentDidMount() {
    let url = 'http://' + HOST + '/industries';

    fetch(url)
      .then((response) => response.json())
      .then((industries) => {
        this.industries = industries;
        this.forceUpdate();
      });
  }

  formatPriceRange(industry) {
    if(industry.count === 0) return '';
    else return `${formatMoney(industry.min, false)} - ${formatMoney(industry.max, false)}  / celebvidy`;
  }
  celebrityCount({count}) {
    if(count === 0) return 'coming soon';
    else if(count === 1) return '1 celebrity';
    else return count + ' Celebrities';
  }

  getIndustries() {
    return this.industries || INDUSTRIES;
  }
  getSource(industry) {
    return getIndustryImageSource(industry);
  }
  render() {
    return (
      <View style={{height: height - 50}}>
        <IndustriesTitle />

        <Parallax.ScrollView showsVerticalScrollIndicator={false} style={{backgroundColor: 'black'}}>
        {this.getIndustries().map((industry, i) => (

            <Parallax.Image
              key={i}
              style={styles.image}
              overlayStyle={styles.overlay}
              source={this.getSource(industry)}
              parallaxFactor={.3}
              onPress={() => {
                this.props.goToPage(3);
                setTimeout(() => {
                  this.props.dispatch({type: 'SELECT_INDUSTRY', payload: industry.name});
                }, 200)
              }}
              activeOpacity={.5}
            >
              <Text style={styles.title}>{industry.name}</Text>
              <View style={[styles.coloredBox, {backgroundColor: industry.color}]}>
                <Text style={styles.subtext}>{this.celebrityCount(industry)}</Text>
              </View>

              <View style={{position: 'absolute', bottom: 10, right: 15, flexDirection: 'row'}}>
                <Text style={styles.subtext}>{this.formatPriceRange(industry)}</Text>
                <Text style={[styles.subtext, {fontSize:11, fontFamily: 'ProximaNova-RegularIt'}]}></Text>
              </View>
            </Parallax.Image>

        ))}
        </Parallax.ScrollView>

        <Gradient
          locations={[0,  1]}
          colors={['rgba(0,0,0,.6)','rgba(0, 0, 0, 0)']}
          style={{
            width,
            height: 6,
            position: 'absolute',
            left: 0,
            top: 64,
          }}
        />
      </View>
    );
  }
}
export default connect()(Industries);


function IndustriesTitle(props) {
  return (
    <View style={{backgroundColor: 'transparent', height: 64, paddingTop: 30}}>
      <Text style={[{color: "white", fontSize: 22, fontFamily: 'ProximaNova-Thin', textAlign: 'center'}]}>
        {props.isCelebrity ? 'Industries' : 'Select an Industry'}
      </Text>
    </View>
  );
}
IndustriesTitle = connect( ({isCelebrity}) => ({isCelebrity}))(IndustriesTitle)


var styles = StyleSheet.create({
  image: {
    height: IMAGE_HEIGHT,
  },
  overlay: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  title: {
    fontSize: 30,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    textShadowOffset: {
      width: StyleSheet.hairlineWidth,
      height: -StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .7)',
  },
  subtext: {
    color: 'white',
    fontFamily: 'Proxima Nova',
    fontSize: 12,
    textShadowColor: 'black',
    textShadowRadius: 1/PixelRatio.get(),
    textShadowOffset: {width: 1/PixelRatio.get(), height: 1/PixelRatio.get()}
  },
  coloredBox: {
    backgroundColor: 'rgba(11, 87, 236, 1)',
    opacity: .75,
    width: 92,
    height: 18,
    position: 'absolute',
    bottom: 13,
    left: 20,
    padding: 2,
    paddingLeft: 4,
    alignItems: 'center'
  }
});
