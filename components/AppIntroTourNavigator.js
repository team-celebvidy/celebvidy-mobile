import { View } from 'react-native';
import React, { Component } from 'react';
import {connect} from 'react-redux';

import Navigator from '../widgets/Navigator';

export default connect(({tourClipped}) => ({tourClipped}))(function(props) {
  if(props.tourClipped) return <View />; //can't currently return null from a stateless component :(

  return (
    <Navigator
      initialComponent={'AppIntroTour'}
      fullPage={true}
      hasRenderRightSubmitButton={true}
      isTour={true}
      navigationBarStyle={{backgroundColor: 'transparent'}}
    />
  );
})
