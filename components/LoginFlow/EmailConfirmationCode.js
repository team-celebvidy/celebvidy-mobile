import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  ScrollView,
  InteractionManager,
  StatusBar,
  TouchableOpacity,
  Navigator as N,
  PixelRatio,
} from 'react-native';
import React, { Component } from 'react';

import {Timer} from 'react-timer-mixin';

const {SceneConfigs: {FloatFromRight: FloatFromRight}} = N;
const {width, height} = Dimensions.get('window');

import formStyles from '../../styles/FormStyles';

import Navigator from '../../widgets/Navigator';
import NavButton from '../../widgets/NavButton';

import ModalSpinner from '../../widgets/ModalSpinner';

import TextInput from '../../widgets/GiftedTextInput';
import {FloatFromTop} from '../../widgets/SceneConfigs';
import AnimateMessageMixin from '../../widgets/AnimateMessageMixin';

import {connect} from 'react-redux';


class EmailConfirmationCode extends AnimateMessageMixin(Timer(Component)) {  //check later
  componentDidMount() {
    this.setTimeout(() => {
      this.showMessageThenVanish(400, 8000, "Please enter the confirmation code you are about to receive. If you get push notifications for your email, you will see it in the subject line and won't need to leave Celebvidy.");
    }, 650);
  }
  constructor(props, context) {
    super(props, context);
    this.state = {
      codeValid: true,
      message: 'Confirmation code is invalid',
    };
  }

  submitCode() {
    let code = this.clientValidateCode();

    if(code) {
      this.props.onSubmitConfirmationCode(this.props.email, code)
        .catch((error) => {
          this.setState({codeValid: false});
          console.log('ERROR', error)
          this.flashMessageStick(error.message)
        });
    }
  }
  clientValidateCode() {
    let code = this.refs.code.getValue();
    let length = this.props.isForgotPassword ? 8 : 5;
    let isValid = code && code.length === length;

    if(!isValid) {
      this.setState({codeValid: false});
      this.flashMessage(`Confirmation code must be ${length} numbers`);
    }
    else if(isValid) this.setState({codeValid: true});

    return isValid ? code : null;
  }



  render() {
    let {forgotPasswordLoginTokenFromCodeFetching, loginFetching} = this.props;

    return (
      <View style={styles.container}>
        <ModalSpinner visible={forgotPasswordLoginTokenFromCodeFetching || loginFetching} />

        <View style={{flex: 1, top: -110}}>
          <View style={{height: 60, marginBottom: 60}} >
            <Animated.Text style={{fontFamily: 'ProximaNova-Thin',
              fontSize: this.state.message === 'Confirmation code is invalid'  ? 18 : 16,
              textAlign: 'center',
              color: 'white', opacity: this.messageOpacity,
              width: width - 40,
            }}>
              {this.state.message}
            </Animated.Text>
          </View>

          <TextInput
            name='code'
            title='Code'
            formName={this.props.formName}
            placeholder={this.props.isForgotPassword ? '12345678' : '12345'}
            maxLength={8}
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            keyboardType='phone-pad'
            widgetStyles={formStyles.login}
            underlined={true}
            ref='code'
            value={''}
            autoFocus={true}
            valid={this.state.codeValid}
          />
        </View>

      </View>
    );
  }
}

export default connect(
  ({forgotPasswordLoginTokenFromCodeFetching, loginFetching}) => ({forgotPasswordLoginTokenFromCodeFetching, loginFetching}),
  null, null, {withRef: true}
)(EmailConfirmationCode)


const styles = StyleSheet.create({
  container: {
    marginTop: -64,
    height,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: 'black',
  }
});
