import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  ScrollView,
  InteractionManager,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import React, { Component } from 'react';
import Mixpanel from 'react-native-mixpanel';
import Icon from 'react-native-vector-icons/Ionicons';
import dismissKeyboard from 'react-native-dismiss-keyboard';

import formStyles from '../../../styles/FormStyles';
import styles from '../../../styles/intro';

import ClassyActionButton from '../../../widgets/ClassyActionButton';
import TextInput from '../../../widgets/GiftedTextInput';
import AnimateMessageMixin from '../../../widgets/AnimateMessageMixin';
import ModalSpinner from '../../../widgets/ModalSpinner';
import {TourCaption} from './SlideTop';

import {width, height, MARGIN_RIGHT, SLIDE_WIDTH, TOP_SLIDE_HEIGHT, BOTTOM_SLIDE_HEIGHT} from './constants';


import * as actions from '../../../actions';
import {connect} from 'react-redux';

class CelebrityPin extends AnimateMessageMixin(Component) { //chech later 
  constructor(props, context) {
    super(props, context);

    this.unlockedOpacity = new Animated.Value(0);
    this.opacity = new Animated.Value(1);
  }

  submitPin() {
    dismissKeyboard();

    let pin = this.refs.pin.getValue()

    if(!pin || pin.length !== 4) {
      return this.flashMessage('Pin must be exactly 4 numbers');
    }

    pin = parseInt(pin);

    this.props.verifyCelebrityPin({pin})
      .then(() => this.submitPinSuccess(pin))
      .catch(() => this.flashMessage('Pin is invalid'));
  }

  submitPinSuccess(pin) {
    Animated.sequence([
      Animated.stagger(100, [
        Animated.timing(this.opacity, {toValue: 0, duration: 300}),
        Animated.timing(this.unlockedOpacity, {toValue: 1, duration: 400}),
      ]),
      Animated.delay(300),
      Animated.timing(this.unlockedOpacity, {toValue: 0, duration: 500}),
    ]).start(() => {
      Mixpanel.trackWithProperties('Celebrity Submit Pin', {pin});

      if(this.props.returnTo === 'TwitterSignin') {
        this.props.onSubmit();
      }
      else {
        this.props.navigator.pushWithProps('LoginSignup', {scrollToFirstPage: this.props.scrollToFirstPage, toggleTour: this.props.toggleTour});
      }
      this.reset();
    });
  }

  reset() {
    setTimeout(() => {
      this.opacity.setValue(1);
    }, 500);
  }

  componentDidMount() {
    Mixpanel.track('Celebrity Complete Tour');
  }

  render() {
    let messageTop = this.props.returnTo === 'TwitterSignin' ? height/6/2+60 : height/4+30;

    return (
      <Animated.View style={[styles.slide, {marginRight: 0, flexDirection: 'column'}]}>
        <ModalSpinner visible={this.props.celebrityPinFetching}  note='verifying...' />
        <ModalSpinner visible={this.props.loginFetching} note='logging you in...' />

        {this.props.returnTo === 'TwitterSignin' &&
          <TourCaption width={width} marginTop={12} marginBottom={height/6} opacity={1} caption='Provide PIN to Access Celebvidy'/>
        }

        <View style={{position: 'absolute', top: messageTop, height: 60, width, paddingHorizontal: 25}}>
          <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
            {this.state.message}
          </Animated.Text>
        </View>

        <Animated.View style={{paddingTop: 30, width: 300, marginTop: this.props.imageMarginTop, opacity: this.opacity}}>

          <TextInput
            ref='pin'
            name='pin'
            title='PIN'
            placeholder='1234'
            maxLength={4}
            returnKeyType='done'
            keyboardType="phone-pad"
            underlineWidth={170}
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            widgetStyles={formStyles.input}
            underlined={true}
            onSubmitEditing={(value) => null}
            valid={true}
          />

          <ClassyActionButton touchableStyle={{marginTop: 50}} onPress={() => this.submitPin()}>
            ENTER
          </ClassyActionButton>

        </Animated.View>

        <Animated.View style={{opacity: this.unlockedOpacity, position: 'absolute', width, marginTop: -80, justifyContent: 'center', flexDirection: 'row', backgroundColor: 'transparent'}}>
          <Icon name='unlocked' size={17} color='rgb(50, 50, 50)' />
          <Text style={{
            color: 'rgb(79, 79, 79)',
            fontSize: 17,
            fontFamily: 'ProximaNova-Thin',
            marginLeft: 7,
          }}>
            UNLOCKED
          </Text>
        </Animated.View>

      </Animated.View>
    );
  }
}

export default connect(
  ({celebrityPinFetching, loginFetching}) => ({celebrityPinFetching, loginFetching}),
  actions
)(CelebrityPin)
