import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import React, { Component } from 'react';

import * as actions from '../../../actions';
import {connect} from 'react-redux';



class TopRightButton extends Component {
  render() {
    if(!this.props.tourReady) return null;
    if(this.props.tourPage === this.props.slideCount - 1) return null;

    let config = this.getButtonConfig();

    return (
      <TouchableOpacity onPress={config.onPress}
        style={{position: 'absolute', top: 10, right: 12}} hitSlop={{top: 40, right: 40, bottom: 40, left: 40}}
      >
        <Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 16, color: 'rgb(30, 69, 237)', backgroundColor: 'transparent'}}>
          {config.text}
        </Text>
      </TouchableOpacity>
    );
  }
  getButtonConfig() {
    if(this.props.tourAgain) {
      return {text: 'RETURN TO APP', onPress: () => this.props.toggleTour(false)};
    }

    let config = {};

    if(this.props.tourPage === 0) {
      if(this.props.isCelebrity) {
        config = {
          text: 'LOGIN',
          onPress: () => this.props.navigator.pushWithProps('LoginSignup', {
            loginOnly: true,
            scrollToFirstPage: this.props.scrollToFirstPage,
            toggleTour: this.props.toggleTour,
            resetButtons: this.props.resetButtons,
          }),
        };
      }
      else {
        config = {
          text: 'SKIP',
          onPress: () => this.props.toggleTour(false, 'customer'),
        };
      }
    }
    else {
      if(this.props.isCelebrity) {
        config = {
          text: 'SKIP',
          onPress: () => this.props.scrollToLastPage(),
        };
      }
      else {
        config = {
          text: 'SKIP',
          onPress: () => this.props.toggleTour(false, 'customer'),
        };
      }
    }

    return config;
  }
}


export default connect(
  ({tourAgain, isCelebrity, tourReady, tourPage}) => ({tourAgain, isCelebrity, tourReady, tourPage}),
  actions
)(TopRightButton)
