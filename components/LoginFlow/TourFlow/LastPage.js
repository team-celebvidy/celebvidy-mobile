import {
  Text,
  View,
  Dimensions,
  Animated,
} from 'react-native';
import React, { Component } from 'react';
import styles from '../../../styles/intro';

import ClassyActionButton from '../../../widgets/ClassyActionButton';

import CelebrityPin from './CelebrityPin';

import {width, height, MARGIN_RIGHT, SLIDE_WIDTH, TOP_SLIDE_HEIGHT, BOTTOM_SLIDE_HEIGHT} from './constants';


import * as actions from '../../../actions';
import {connect} from 'react-redux';


class LastPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.opacity = new Animated.Value(1);
  }


  render() {
    let {tourAgain, isCelebrity} = this.props;

    if(isCelebrity && !tourAgain) {
      return this.renderCelebrityPin();
    }

    return (
      <Animated.View style={[styles.slide, {marginRight: 0, marginTop: this.props.marginTo}]}>
        <Animated.View style={{paddingTop: 30, width: width - 30, marginTop: this.props.imageMarginTop, opacity: this.opacity}}>

          {!isCelebrity
            ? <ClassyActionButton text='BROWSE CELEBRITIES' touchableStyle={{marginTop: 0}} onPress={() => this.props.toggleTour(false, 'customer')} />

            : <View>
                <ClassyActionButton text='RETURN TO APP' touchableStyle={{marginTop: 0}}
                  onPress={() => this.props.toggleTour(false)}
                />

                <ClassyActionButton text='CONTINUE TO PROFILE WIZARD' touchableStyle={{marginTop: 20}}
                  onPress={() => this.props.navigator.pushWithProps('priceRoute', this.props, false)}
                />
              </View>
            }
        </Animated.View>

      </Animated.View>
    );
  }
  renderCelebrityPin() {
    return (
      <CelebrityPin
        {...this.props}
        marginTop={this.props.marginTop}
        imageMarginTop={this.props.imageMarginTop}
        captionOpacity={this.props.captionOpacity}
        navigator={this.props.navigator}
        toggleFullPage={this.props.toggleFullPage}
        scrollToFirstPage={this.props.scrollToFirstPage}
      />
    );
  }
}

export default connect(
  ({tourAgain, isCelebrity}) => ({tourAgain, isCelebrity}),
  actions
)(LastPage)
