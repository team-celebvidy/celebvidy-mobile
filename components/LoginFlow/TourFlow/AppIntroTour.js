//DIRECTIONS: To add slides, add slides to ./constants.js
import {
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  ScrollView,
  InteractionManager,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import React, { Component } from 'react';

import Gradient from 'react-native-linear-gradient';
import _ from 'underscore';

import styles from '../../../styles/intro';
import {setImmediateAnimationFrame} from '../../../misc/helpers';

import TopRightButton from './TopRightButton';
import SlideBottom from './SlideBottom';
import SlideTop from './SlideTop';
import StartPage from './StartPage';
import LastPage from './LastPage';

import LogoSvg from '../../LogoSvg';

import {CELEBRITY_SLIDES, CUSTOMER_SLIDES, width, height, MARGIN_RIGHT, SLIDE_WIDTH, TOP_SLIDE_HEIGHT, BOTTOM_SLIDE_HEIGHT} from './constants';

import * as actions from '../../../actions';
import {connect} from 'react-redux';


class AppIntroTour extends Component {
  constructor(props, context) {
    super(props, context);

    this.backgroundLogoOpacity = new Animated.Value(0);
    this.overlayLeft = new Animated.Value(0);
    this.direction = new Animated.Value(-1);
    this.swipeLeftOpacityTourAgain = new Animated.Value(1);

    let maxSlides = Math.max(CUSTOMER_SLIDES.length, CELEBRITY_SLIDES.length);

    _.range(0, maxSlides).forEach((slide, index) => {
      this['captionOpacity'+index] = new Animated.Value(0);
      this['slideMarginTop'+index] = new Animated.Value(-10); //previously first and last had 0 values
    });
  }

  //basically this component should never update for absolute optimum performance. The child components like theh TopRightButton
  //receive props such as tourReady, tourPage, isCelebrity, etc. Here, where we can't create a child component, such as with the scrollview,
  //we use imperative direct manipulation instead.
  //Also note that the tour is reConstructed/Mounted every time it is revisited, so it receives initial props like tourAgain, which
  //are only needed on on initial mounting. THE GOAL: flawless first impressions of the app. :)
  shouldComponentUpdate(nextProps) {
    let {tourReady, tourPage, tourAgain} = nextProps;

    if(tourReady && !this.props.tourReady) {
      this.refs.scrollview.setNativeProps({scrollEnabled: true}); //imperatively toggle scroll for performance
      return true; //basically, we only update when the tour is mounted or re-visted
    }
    //else if(tourPage === this.slideCount() - 1) {
    //  this.refs.scrollview.setNativeProps({scrollEnabled: false}); //maybe we will bring this feature back, but for now we want people to be able to return to the start page and change their user types if they ended up as a celebrity out of curiosity
    //  return false; //TopRightButton gets this from Redux directly
    //}
    if(!tourAgain && tourPage !== this.props.tourPage && tourPage === 0) {
      this.resetButtons();
    }
    else if(tourPage !== this.props.tourPage) {
      return false;
    }

    return true; //there's very little that will trigger this since most props are received on mounting besides the above ones which change over time
  }

  componentDidMount() {
    if(this.props.tourAgain) {
      setTimeout(this.props.setTourReady, 3050);
    }
  }


  render() {
    if(this.props.tourClipped) return null;

    return (
      <View ref='mainContainer' style={styles.mainContainer}>
        <LogoSvg
          size={300}
          color='rgb(20, 20, 20)'
          strokeWidth={10}
          style={[styles.starBg, {
            opacity: this.backgroundLogoOpacity,
          }]}
        />

        {this.renderTopReverseScrollingOverlay()}
        {this.renderScrollView()}

        <TopRightButton
          {...this.props}
          slideCount={this.slideCount()}
          scrollToFirstPage={this.scrollToFirstPage.bind(this)}
          scrollToLastPage={this.scrollToLastPage.bind(this)}
          resetButtons={() => this.resetButtons()}
        />

      </View>
    );
  }

  resetButtons(){
    this.refs.startPage.getWrappedInstance().resetButtons()
  }
  scrollTo(x, animated=true) {
    this.refs.scrollview.scrollTo({x, animated});
  }
  scrollToFirstPage() {
    this.scrollTo(0);
    this.refs.scrollview.setNativeProps({scrollEnabled: true});
  }
  scrollToLastPage() {
    let offsetX = this.totalWidth() - width;

    this.scrollTo(offsetX);
    setImmediate(() => {
      requestAnimationFrame(() => {
        this.animateCurrentPage(offsetX);
      });
    });
  }

  renderScrollView() {
    return (
      <ScrollView
        ref='scrollview'
        scrollEnabled={this.props.tourReady}
        style={styles.container}
        horizontal={true}
        removeClippedSubviews={false}
        scrollToTop={false}
        showsHorizontalScrollIndicator={false}
        bounces={true}
        automaticallyAdjustContentInsets={false}
        snapToInterval={SLIDE_WIDTH}
        snapToAlignment="start"
        decelerationRate={0}
        scrollEventThrottle={16}
        contentContainerStyle={styles.content}
        onScroll={Animated.event([{nativeEvent: {contentOffset: {x: this.overlayLeft}}}])}
        onResponderGrant={this.onResponderGrant.bind(this)}
        onResponderMove={this.onResponderMove.bind(this)}
        onMomentumScrollEnd={this.onMomentumScrollEnd.bind(this)}
      >
        <StartPage
          ref='startPage'
          dispatch={this.props.dispatch}
          opacity={this.interpolateOverlayOpacity(0)}
          captionOpacity={this.props.tourAgain ? this.swipeLeftOpacityTourAgain : this['captionOpacity0']}
          logoOpacity={this.interpolateLogoPageOpacity()}
          rotateZ={this.interpolateLogoRotate()}
          onSelectUserType={(userType) => {
            this.props.setUserType(userType, 'tourReady');
          }}
        />

        {this.renderTourSlidesBottom()}

        <LastPage
          {...this.props}
          marginTop={this['slideMarginTop'+(this.slideCount()-1)]}
          imageMarginTop={this.interpolateImageMarginTop(this.slideCount()-1)}
          captionOpacity={this['captionOpacity'+(this.slideCount()-1)]}
          navigator={this.props.navigator}
          toggleFullPage={this.props.toggleFullPage}
          scrollToFirstPage={this.scrollToFirstPage.bind(this)}
        />

      </ScrollView>
    );
  }

  renderTopReverseScrollingOverlay() {
    return (
      <View style={[styles.topOverlay, {width: this.totalWidth()}]}>
        <Animated.View style={[styles.inner, {left: this.overlayLeft}]}>
          {this.renderTourSlidesTop()}
          <View style={[styles.overlaySlide, {marginRight: 0}]} />
        </Animated.View>
      </View>
    );
  }

  slides() {
    return this.props.isCelebrity ? CELEBRITY_SLIDES : CUSTOMER_SLIDES;
  }
  slideCount() {
    return this.slides().length;
  }
  totalWidth() {
    let width = (this.slideCount() * SLIDE_WIDTH) - MARGIN_RIGHT;
    return width;
  }

  renderTourSlidesBottom() {
    return this.slides().slice(1, -1).map((slide, index) => ( //skip first + last page skipped since it is rendered separately
      <SlideBottom
        marginTop={this['slideMarginTop'+(index+1)]}
        imageMarginTop={this.interpolateImageMarginTop(index+1)}
        blurOpacity={this.interpolateImageBlurOpacity(index+1)}
        uri={slide.uri}
        isCelebrity={this.props.isCelebrity}
        key={index}
      />
    ));
  }
  renderTourSlidesTop() {
    return this.slides().slice(1).map((slide, index) => ( //skip just first page
      <SlideTop
        {...this.props}
        tourAgain={this.props.tourAgain}
        title={slide.title}
        caption={slide.caption}
        opacity={this.interpolateOverlayOpacity(index+1)}
        captionOpacity={this['captionOpacity'+(index+1)]}
        marginRight={index === this.slideCount()-1 ? 0 : MARGIN_RIGHT}
        lastSlide={index === this.slideCount()-2}
        isCelebrity={this.props.isCelebrity}
        key={index}
      />
    )).reverse();
  }



  onResponderGrant() {
    this.handle = InteractionManager.createInteractionHandle();
    this.directionConfigured = false;
  }
  onResponderMove({touchHistory: {touchBank: [nothing, {currentPageX, previousPageX, currentPageY, previousPageY}]}}) {
    if(this.directionConfigured) return; //might as well execute less code during movement. not sure if it makes much of a difference.

    let horizontalDirection = previousPageX > currentPageX ? 1 : -1;
    let verticalDirection;

    //user moving forward ? swipe => fly slide upwards : swipe down => fly slide downards
    if(horizontalDirection === 1) verticalDirection = previousPageY > currentPageY ? 1 : -1;
    //user moving backward ? swipe up => fly slide downards : swipe down => fly slide upwards
    else verticalDirection = previousPageY > currentPageY ? -1 : 1;

    this.direction.setValue(verticalDirection);
    this.directionConfigured = true;
  }

  interpolateImageMarginTop(index) {
    const deltaY = 300;
    const speed = .525;

    let marginTop = this.overlayLeft.interpolate({
      inputRange: [(index-speed)*SLIDE_WIDTH, index*SLIDE_WIDTH,  (index+speed)*SLIDE_WIDTH],
      outputRange: [deltaY, 0, -deltaY],
      extrapolate: 'clamp'
    });

    return Animated.multiply(marginTop, this.direction);
  }
  interpolateImageBlurOpacity(index) {
    const speed = .5

    return this.overlayLeft.interpolate({
      inputRange: [(index-speed)*SLIDE_WIDTH, (index-(speed/2))*SLIDE_WIDTH, index*SLIDE_WIDTH, (index+(speed/2))*SLIDE_WIDTH,  (index+speed)*SLIDE_WIDTH],
      outputRange: [1, .98, 0, .98, 1],
      extrapolate: 'clamp'
    });
  }
  interpolateOverlayOpacity(index) {
    const speed = .3

    return this.overlayLeft.interpolate({
      inputRange: [(index-speed)*SLIDE_WIDTH, (index-(speed/4))*SLIDE_WIDTH, index*SLIDE_WIDTH, (index+(speed/4))*SLIDE_WIDTH,  (index+speed)*SLIDE_WIDTH],
      outputRange: [0, .3, 1, .3, 0],
      extrapolate: 'clamp'
    });
  }
  interpolateLogoRotate() {
    const index = 0
    const speed = .525;

    return this.overlayLeft.interpolate({
      inputRange: [(index-speed)*SLIDE_WIDTH, index*SLIDE_WIDTH,  (index+speed)*SLIDE_WIDTH],
      outputRange: ['500deg', '0deg', '-500deg'],
      extrapolate: 'clamp'
    });
  }
  interpolateLogoPageOpacity() {
    const index = 0;
    const speed = .3

    return this.overlayLeft.interpolate({
      inputRange: [(index-speed)*SLIDE_WIDTH, (index-(speed/2))*SLIDE_WIDTH, index*SLIDE_WIDTH, (index+(speed/2))*SLIDE_WIDTH,  (index+speed)*SLIDE_WIDTH],
      outputRange: [.3, .6, 1, .6, .3],
      extrapolate: 'clamp'
    });
  }

  onMomentumScrollEnd({nativeEvent: {contentOffset: {x}}}) {
    this.animateCurrentPage(x)
    if(this.handle) InteractionManager.clearInteractionHandle(this.handle);
  }
  animateCurrentPage(x) {
    let containerWidth = SLIDE_WIDTH;
    let page = parseInt(x/containerWidth, 10);
    let previousPage = this.previousPage || 0;

    if(page === previousPage) return;
    if(page === 1 && previousPage === 0) this.showBackgroundLogo(0);
    else if(page === this.slideCount()-2 && previousPage === this.slideCount()-1) this.showBackgroundLogo(0);
    else if(page === 0 && previousPage === 1) this.hideBackgroundLogo(0);
    else if(page === this.slideCount()-1) this.hideBackgroundLogo(1000);

    setImmediateAnimationFrame(() => {
      Animated.parallel([
        Animated.timing(this['slideMarginTop'+page], {
          toValue: 5,
          duration: 500,
        }),
        Animated.timing(this['captionOpacity'+page], {
          toValue: 1,
          duration: 300,
        }),
      ]).start();
    });

    this['slideMarginTop' + previousPage].setValue(-10);
    this['captionOpacity' + previousPage].setValue(0);

    //cache on the instance instead of using state for optimum performance.
    //use redux for page state so only child components receive the prop.
    this.previousPage = page;
    this.props.setTourPage(page);
  }
  showBackgroundLogo(duration) {
    Animated.timing(this.backgroundLogoOpacity, {
      toValue: 1,
      duration: typeof duration !== 'undefined' ? duration : 300,
    }).start();
  }
  hideBackgroundLogo(duration) {
    Animated.timing(this.backgroundLogoOpacity, {
      toValue: 0,
      duration: typeof duration !== 'undefined' ? duration : 300,
    }).start();
  }
}



export default connect(
  ({tourAgain, tourReady, tourPage, isCelebrity}) => ({tourAgain, tourReady, tourPage, isCelebrity}),
  actions, null, {withRef: true}
)(AppIntroTour)
