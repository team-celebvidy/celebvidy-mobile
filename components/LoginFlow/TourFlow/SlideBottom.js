import {
  Text,
  View,
  Image,
  Animated,
  TouchableOpacity,
} from 'react-native';
import React, { Component } from 'react';
import {VibrancyView} from 'react-native-blur';
import styles from '../../../styles/intro';

import {width, height, MARGIN_RIGHT, SLIDE_WIDTH, TOP_SLIDE_HEIGHT, BOTTOM_SLIDE_HEIGHT} from './constants';

import {connect} from 'react-redux';

export default class SlideBottom extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.isCelebrity !== this.props.isCelebrity;
  }

  render() {
    return (
      <Animated.View style={[styles.slide, {top: -60, marginTop: this.props.marginTop}]}>
        <TouchableOpacity onPress={() => null} activeOpacity={.85} style={styles.image}>
          <Animated.Image
            style={[styles.image, {top: 0, marginTop: this.props.imageMarginTop}]}
            source={this.props.uri}
          >
            <Animated.View style={{width, flex: 1, opacity: this.props.blurOpacity}} >
              <VibrancyView blurType="dark" style={{width, flex: 1}} />
            </Animated.View>
          </Animated.Image>
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

/**
export default connect(
  ({isCelebrity}) => ({isCelebrity})
)(SlideBottom)
**/
