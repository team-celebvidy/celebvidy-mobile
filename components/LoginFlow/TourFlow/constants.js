import {Dimensions} from 'react-native';
import React, { Component } from 'react';
export const width = Dimensions.get('window').width;
export const height = Dimensions.get('window').height;
export const MARGIN_RIGHT = 300;
export const SLIDE_WIDTH = width + MARGIN_RIGHT;

export const TOP_SLIDE_HEIGHT = height/3 - 20;
export const BOTTOM_SLIDE_HEIGHT = height/2 + 20;


export const CELEBRITY_SLIDES = [
  {},
  {
    title: 'SET YOUR PRICE',
    caption: "Set What You Wanna Charge, \nYour Available # of Celebvidys, etc",
    uri: require('../../../images/tour/price.png'),
  },
  {
    title: 'FANS FIND YOU',
    caption: "Yup, You Have a Beautiful Profile \nWhich You are Just About to Setup",
    uri: require('../../../images/tour/celebrities.png'),
  },
  {
    title: 'YOU ARE CHOSEN',
    caption: "Yes, You are the Chosen One \nFans Pay the Ultimate Price",
    uri: require('../../../images/tour/celebrity.png'),
  },
  {
    title: 'YOUR SCRIPT IS PREPARED',
    caption: 'The Tables Have Turned \n Now Fans Tell You What They Want',
    uri: require('../../../images/tour/celebvidy_details.png'),
  },
  {
    title: 'ACTION! RECORD CELEBVIDY',
    caption: 'While Your Script is Displayed in Our Patent Pending Tele-Prompter',
    uri: require('../../../images/tour/record.png'),
  },
  {
    title: 'EVERYONE GOES HOME HAPPY',
    caption: "Your the Star of a Happy Fan's Very Own Personal Shout Out From You!",
    uri: require('../../../images/tour/celebvidy.png'),
  },
  {
    title: 'OH ...AND ONE LAST THING',
    caption: 'You Are Paid Handsomely for Your Services ...or Whatever Price You Set',
    uri: require('../../../images/tour/payout_preferences.png'),
  },
  {
    caption: (tourAgain) => tourAgain ? "What would you like to do next?" : "Provide PIN to Access Celebvidy",
  },
];


export const CUSTOMER_SLIDES = [
  {},
  {
    title: 'SELECT AN INDUSTRY',
    caption: "Choose from Muscians, Comedians, Athletes and More",
    uri: require('../../../images/tour/industries.png'),
  },
  {
    title: 'BROWSE CELEBRITIES',
    caption: "A to Z, Popular, They're All Here \nYou Know the Drill",
    uri: require('../../../images/tour/celebrities.png'),
  },
  {
    title: 'PICK A CELEBRITY',
    caption: "Review their DOs and DON'Ts \nWatch their WHO AM I Video, etc",
    uri: require('../../../images/tour/celebrity.png'),
  },
  {
    title: 'TELL EM WHAT YOU WANT',
    caption: 'Provide Your Celebrity a Script \nSpecify Your Video Request Details',
    uri: require('../../../images/tour/celebvidy_details.png'),
  },
  {
    title: 'YOUR CELEBRITY RECORDS IT',
    caption: 'While Your Script is Displayed in Our Patent Pending Tele-Prompter',
    uri: require('../../../images/tour/record.png'),
  },
  {
    title: 'AWESOME SAUCE!',
    caption: 'You and Your Recipient Receive the Celebvidy, Optionally with a Custom TXT Directly from the Celeb',
    uri: require('../../../images/tour/celebvidy.png'),
  },
  {
    caption: (tourAgain) => tourAgain ? "Let's Get Started." : "Let's Get Started.",
  },
];
