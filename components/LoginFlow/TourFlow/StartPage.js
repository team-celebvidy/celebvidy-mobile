import {
  Text,
  Image,
  View,
  Animated,
  StyleSheet,
  TouchableWithoutFeedback,
  Easing,
  PixelRatio,
} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import Gradient from 'react-native-linear-gradient';

import ST from '../../../styles/common';
import formStyles from '../../../styles/FormStyles';
import styles from '../../../styles/intro';

import ActionButton from '../../../widgets/ActionButton';
import ClassyActionButton from '../../../widgets/ClassyActionButton';
import NavButton from '../../../widgets/NavButton';
import {setImmediateAnimationFrame} from '../../../misc/helpers';

import {width, height, MARGIN_RIGHT, SLIDE_WIDTH, TOP_SLIDE_HEIGHT, BOTTOM_SLIDE_HEIGHT} from './constants';


import * as actions from '../../../actions';
import {connect} from 'react-redux';

import LogoSvg, {LogoText} from '../../LogoSvg';


class StartPage extends Component {
  constructor(props, context) {
    super(props, context);

    this.buttonsOpacity = props.tourAgain ? new Animated.Value(0) : new Animated.Value(1);
    this.thankyouOpacity = new Animated.Value(0);
    this.logoGlowOpacity = new Animated.Value(0.7);
    this.gradientLeft = new Animated.Value(width);
  }


  componentDidMount() {
    setImmediateAnimationFrame(() => {
      this.loopLogoGlow();
    }, 1000);
  }
  loopLogoGlow(toggle) {
    this.logoGlowStopped = false;

    Animated.timing(this.logoGlowOpacity, {
      toValue: toggle ? .3 : .7,
      duration: 1500,
    }).start(() => !this.logoGlowStopped && this.loopLogoGlow(!toggle));
  }
  brightenLogoGlow() {
    this.logoGlowStopped = true;
    this.brightenLogo = true;

    this.logoGlowOpacity.stopAnimation(() => {
      Animated.timing(this.logoGlowOpacity, {
        toValue: 1,
        duration: 200,
      }).start();
    });
  }
  dimLogoGlow() {
    this.logoGlowStopped = true;
    this.brightenLogo = false; //we need to detect if the user presses again while it's dimming, and not trigger loopLogoGLow()

    this.logoGlowOpacity.stopAnimation(() => {
      Animated.timing(this.logoGlowOpacity, {
        toValue: .4,
        duration: 1000,
      }).start(() => !this.brightenLogo && this.loopLogoGlow());
    });
  }
  interpolateLogoOpacity() {
    return this.logoGlowOpacity.interpolate({
      inputRange: [0, .7, 1],
      outputRange: [.8, .8, 1],
      extrapolate: 'clamp',
    });
  }

  render() {
    return (
      <Animated.View style={[styles.slide, {opacity: this.props.logoOpacity}]}>

        <Animated.View style={{position: 'absolute', top: 64, width, opacity: this.props.opacity, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{
            color: 'rgb(180, 180, 180)',
            fontSize: 24,
            fontFamily: 'ProximaNova-Thin',
            marginRight: 8,
            top: 3,
          }}>
          </Text>
          <LogoText fontSize={54} vidySize={57} tmTop={21} vidyTop={-3} color='rgb(23, 46, 167)'/>
        </Animated.View>

        <Animated.View style={{position: 'absolute', top: 150, width, opacity: this.buttonsOpacity}}>
          <Animated.Text style={{
            color: 'rgb(200, 200, 200)',
            fontSize: 16,
            opacity: this.props.opacity,
            fontFamily: 'ProximaNova-Thin',
            textAlign: 'center',
            textShadowColor: 'rgb(0, 2, 48)',
            textShadowRadius: StyleSheet.hairlineWidth*2,
            textShadowOffset: {width: StyleSheet.hairlineWidth*2, height: -StyleSheet.hairlineWidth*2},
            backgroundColor: 'transparent',
          }}>
              Please select an option below
          </Animated.Text>
        </Animated.View>


        <TouchableWithoutFeedback onPressIn={() => this.brightenLogoGlow()} onPressOut={() => this.dimLogoGlow()}>
          <Animated.View style={{
            position: 'absolute',
            //left: width/2 - ((width - 148)/2),
            //top: height/2  - 148,
            top: height/2 - 150,
            left: width/2 - 300/2,
            width: 300,
            height: 300,
            borderRadius: 300,
            //backgroundColor: 'rgb(0, 1, 20)',
            shadowColor: 'blue',
            shadowRadius: 60,
            shadowOpacity: this.logoGlowOpacity,
            shadowOffset: {width: 0, height: 0},
            opacity: this.interpolateLogoOpacity(),
          }}>
            <LogoSvg size={300} color='rgb(23, 46, 167)' strokeWidth={10} style={{transform: [{rotateZ: this.props.rotateZ}]}}/>
          </Animated.View>
        </TouchableWithoutFeedback>


        {!this.props.tourAgain &&
          <Animated.Text style={{
            position: 'absolute',
            bottom: 80,
            opacity: this.thankyouOpacity,
            color: 'rgb(150, 150, 150)',
            fontSize: 19,
            fontFamily: 'ProximaNova-Thin',
            textAlign: 'center',
            width,
          }}>
            THANK YOU
          </Animated.Text>
        }


        <Animated.View style={{position: 'absolute', bottom: 40, opacity: this.props.captionOpacity}}>
          <Animated.View
            style={{
              position: 'absolute', top: -5, left: 0 ,
              width: width,
              height: 25,
              position: 'absolute',
              left: this.gradientLeft,
            }}
          >
            <Gradient
              locations={[0, .2, .5, .8, 1]}
              start={[0, 1]}
              end={[1, 1]}
              colors={['rgba(0, 0, 0, 0)', 'rgba(0, 4, 250, .04)', 'rgba(0, 4, 250, .22)', 'rgba(0, 4, 250, .04)', 'rgba(0,0,0,0)']}
              style={{ width, height: 25}}
            />
          </Animated.View>

          <Animated.Text style={{
            color: 'rgb(79, 79, 79)',
            fontSize: 17,
            opacity: this.props.opacity,
            fontFamily: 'ProximaNova-Thin',
            textAlign: 'center',
            width,
            backgroundColor: 'transparent',
          }}>
            SWIPE LEFT TO BEGIN TOUR
          </Animated.Text>
        </Animated.View>


        {!this.props.tourAgain &&
          <Animated.View style={{position: 'absolute', width, bottom: 25, paddingHorizontal: 20, opacity: this.buttonsOpacity}}>
            <ClassyActionButton
              ref='customerButton'
              touchableStyle={{marginBottom: 15}}
              activeOpacity={.3}
              onPressOut={() => this.refs.customerButton.refs.button.setNativeProps({style: {opacity: 0}})}
              style={{height: 50, borderColor: 'rgb(30,30,30)',
                borderWidth: 1*StyleSheet.hairlineWidth,

                backgroundColor: 'rgb(0, 0, 9)'
              }}
              onPress={() => this.selectCustomer()}
            >
              I'M INTERESTED IN CELEBVIDYS
            </ClassyActionButton>

            <ActionButton
              ref='celebButton'
              touchableStyle={{height: 50}}
              activeOpacity={.3}
              onPressOut={() => this.refs.celebButton.refs.button.setNativeProps({style: {opacity: 0}})}
              style={{height: 50, backgroundColor: 'rgb(6, 7, 113)', borderColor: 'rgb(32, 37, 152)'}}
              textStyle={{color: 'rgb(185, 183, 183)'}}
              onPress={() => this.selectCelebrity()}
            >
              I'M A CELEBRITY
            </ActionButton>
          </Animated.View>
        }

      </Animated.View>
    );
  }

  animateSwipeLeft(opacity) {
    this.swipeAnimation = Animated.parallel([
      Animated.sequence([
        Animated.timing(this.props.captionOpacity, {
          toValue: 0,
          duration: 500,
        }),
        Animated.delay(250),
        Animated.timing(this.props.captionOpacity, {
          toValue: 1,
          duration: 500,
        }),
        Animated.delay(250),
        Animated.timing(this.props.captionOpacity, {
          toValue: 0,
          duration: 500,
        }),
        Animated.delay(250),
        Animated.timing(this.props.captionOpacity, {
          toValue: 1,
          duration: 500,
        }),
        Animated.delay(250),
      ]),
      Animated.sequence([
        Animated.timing(this.gradientLeft, {
          toValue: -width,
          duration: 3000,
          //easing: Easing.linear,
          easing: Easing.inOut(Easing.quad),
        }),
        Animated.timing(this.gradientLeft, {
          toValue: width,
          duration: 0,
        })
      ]),
    ]);

    this.swipeAnimation.start(() => {
      if(this.stopSwiping) return;
      this.animateSwipeLeft(!opacity);
    });
  }

  selectCustomer() {
    this.props.onSelectUserType('customer');
  }
  selectCelebrity() {
    this.props.onSelectUserType('celebrity');
  }

  //will be received after user type is selected
  componentWillReceiveProps(nextProps) {
    if(!nextProps.tourAgain && nextProps.tourReady && !this.props.tourReady) {
      setImmediateAnimationFrame(() => {
        this.hideButtons();
      }, 200);
    }
    else if(nextProps.tourReady && !this.props.tourReady) {
      setImmediateAnimationFrame(() => {
        this.stopSwiping = false;
        this.animateSwipeLeft();
      });
    }
  }
  shouldComponentUpdate() {
    return false;
  }

  resetButtons() {
    this.stopSwiping = true;
    this.buttonsOpacity.setValue(1);
    this.thankyouOpacity.setValue(0);
    this.gradientLeft.setValue(width);
    this.refs.celebButton.refs.button.setNativeProps({style: {opacity: 1}});
    this.refs.customerButton.refs.button.setNativeProps({style: {opacity: 1}});
    this.swipeAnimation.stop();
    setTimeout(() => {
      this.props.captionOpacity.setValue(0); //stupid fucking hack to insure the SWIPE LEFT.. text is set back to 0 when it's stopped; a callback to stop() doesn't work
    }, 150)

    this.props.dispatch({type: 'SET_TOUR_NOT_READY'});
  }

  hideButtons() {
    Animated.sequence([
      Animated.timing(this.buttonsOpacity, {
        toValue: 0,
        duration: 300,
      }),
      Animated.timing(this.thankyouOpacity, {
        toValue: 1,
        duration: 400,
      }),
      Animated.delay(1200),
      Animated.timing(this.thankyouOpacity, {
        toValue: 0,
        duration: 200,
      }),
      Animated.timing(this.props.captionOpacity, {
        toValue: 1,
        duration: 500,
      }),
      Animated.delay(1000),
    ]).start(() => {
      this.stopSwiping = false;
      this.animateSwipeLeft();
    });
  }
}


export default connect(
  ({tourAgain, tourReady}) => ({tourAgain, tourReady}),
  actions, null, {withRef: true}
)(StartPage)
