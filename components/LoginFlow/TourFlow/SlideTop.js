import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
} from 'react-native';
import React, { Component } from 'react';
import Gradient from 'react-native-linear-gradient';
import {VibrancyView} from 'react-native-blur';

import styles from '../../../styles/intro';
import {width, height, MARGIN_RIGHT, SLIDE_WIDTH, TOP_SLIDE_HEIGHT, BOTTOM_SLIDE_HEIGHT} from './constants';

import {connect} from 'react-redux';

export default class SlideTop extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.isCelebrity !== this.props.isCelebrity;
  }

  render() {
    return (
      <Animated.View style={[styles.overlaySlide, {top: -20, opacity: this.props.opacity, marginRight: this.props.marginRight}]}>
        <Text style={styles.pageTitle}>{this.props.title}</Text>
        <TourCaption width={width} marginTop={this.props.lastSlide ? 12 : 0} opacity={this.props.captionOpacity} caption={this.caption()} />
      </Animated.View>
    );
  }

  caption() {
    if(typeof this.props.caption === 'function') {
      return this.props.caption(this.props.tourAgain);
    }
    else {
      return this.props.caption;
    }
  }
}

export function TourCaption({width, marginTop, opacity, caption, marginBottom=0}) {
  return (
    <Animated.View style={{marginTop: 10, width, marginBottom}}>

      <Gradient
        locations={[0, .1, .5, .55, .6, .9, 1]}

        colors={['rgba(0, 0, 0, 0)', 'rgba(0, 4, 250, .05)', 'rgba(0, 4, 250, .25)', 'rgba(0, 4, 250, .28)', 'rgba(0, 4, 250, .25)', 'rgba(0, 4, 250, .05)', 'rgba(0,0,0,0)',]}
        style={{position: 'absolute', top: -40, left: 0, width, height: 140}}
      />

      <Gradient
        start={[0, 1]}
        end={[1, 1]}
        locations={[0, .1, 1]}

        colors={['black', 'rgba(0, 0, 0, .6)', 'transparent',]}
        style={{position: 'absolute', top: -40, left: 0, width: 100, height: 140}}
      />

      <Gradient
        start={[0, 1]}
        end={[1, 1]}
        locations={[0, .9, 1]}

        colors={['transparent', 'rgba(0, 0, 0, .6)', 'black']}
        style={{position: 'absolute', top: -40, right: 0, width: 100, height: 140}}
      />

      <Animated.Text style={[styles.pageCaption, {marginTop,opacity}]}>
        {caption}
      </Animated.Text>
    </Animated.View>
  );
}
