import {Alert, Navigator as N} from 'react-native';
import React, { Component } from 'react';

import Mixpanel from 'react-native-mixpanel';
import {dispatch} from '../../configureStore';
import {toggleTour, updateCelebrityWithAllState} from '../../actions';
import {getState} from '../../configureStore';

import NavButton, {BackButton} from '../../widgets/NavButton';
import {FloatFromTop, FloatFromRightFade} from '../../widgets/SceneConfigs';

import CelebrityBasicInfo from '../../components/CelebrityAccount/CelebrityBasicInfo';
import ProfileMedia from '../../components/CelebrityAccount/ProfileMedia';
import Bio from '../../components/CelebrityAccount/Bio';
import Notifications from '../../components/CelebrityAccount/Notifications';
import PayoutPreferences from '../../components/CelebrityAccount/PayoutPreferences';

import {Price, Inventory, Charity, MonthlyReset} from './CircularTouchSliderPages';

export function priceRoute(props) {
  return createRoute('Price', 'inventoryRoute', props);
}

export function inventoryRoute(props) {
  return createRoute('Inventory', 'charityRoute', props);
}

export function charityRoute(props) {
  return createRoute('Charity', 'monthlyResetRoute', props);
}

export function monthlyResetRoute(props) {
  return createRoute('MonthlyReset', 'celebrityBasicInfoRoute', props, 'Monthly Reset');
}

export function celebrityBasicInfoRoute(props) {
  return createRoute('CelebrityBasicInfo', 'profileMediaRoute', props, 'Basic Info');
}

export function profileMediaRoute(props) {
  return createRoute('ProfileMedia', 'bioRoute', props, 'Profile Media');
}

export function bioRoute(props) {
  return createRoute('Bio', 'notificationsRoute', props);
}

export function notificationsRoute(props) {
  return createRoute('Notifications', 'payoutPreferencesRoute', props);
}

export function payoutPreferencesRoute(props) {
  return createRoute('PayoutPreferences', null, props, 'Payout Preferences');
}


export function createRoute(component, nextRoute, props, title, customConfig) {
  let componentRef; //HMR unfortunately will break access to this enclosed variable, and for example, the signup wizard NEXT button won't be able to call componentRef.getWrappedInstance() on it until you trigger the current page to re-render another way (or in the worst case reload)

  Mixpanel.trackWithProperties(component, getState().celebrity);
  Mixpanel.registerSuperProperties(getState().celebrity);

  return Object.assign({
    renderScene() {
      //originally, we did this dynamically in one line with React.createElement(component), but that
      //killed Hot Module Replacement. HMR obviously doesn't follow createElement very well. It does in some cases though,
      //just not in route functions. In /widgets/Navigator.js in `createRouteWithRenderScene()`, createElement is used, and it works. Weird.
      switch(component) {
        case 'Price':
          return <Price ref={c => componentRef = c} />; //also note: no need to pass props--the navigator handles it
        case 'Inventory':
          return <Inventory ref={c => componentRef = c} />;
        case 'Charity':
          return <Charity ref={c => componentRef = c} />;
        case 'MonthlyReset':
          return <MonthlyReset ref={c => componentRef = c} />;
        case 'CelebrityBasicInfo':
          return <CelebrityBasicInfo ref={c => componentRef = c} nextRoute={{name: nextRoute, props}} />;
        case 'ProfileMedia':
          return <ProfileMedia ref={c => componentRef = c} />;
        case 'Bio':
          return <Bio ref={c => componentRef = c} />;
        case 'Notifications':
          return <Notifications ref={c => componentRef = c} nextRoute={{name: nextRoute, props}} />;
        case 'PayoutPreferences':
          return <PayoutPreferences ref={c => componentRef = c} completeWizard={completeWizard} />;
      }
    },
    renderBackButton(navigator) {
      if(component === 'PayoutPreferences') {
        return <BackButton navigator={navigator} />;
      }

      return (
        <NavButton
          backgroundColor='transparent'
          text='FINISH'
          width={130}
          //style={{marginLeft: -20}}
          hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
          onPress={() => finishLater()}
        />
      );
    },
    renderRightButton(navigator) {
      let onNextPress = function() {
        componentRef.getWrappedInstance().saveWizard()
          .then(() => {
            navigator.pushWithProps(nextRoute, props);
          })
          .catch(() => console.log('invalid form submission'));
      };

      let onDonePress = function() {
        componentRef.getWrappedInstance().saveWizard()
          .then(() => {
            completeWizard();
          })
          .catch(() => console.log('invalid form submission'));
      };

      return nextRoute
        ? <NavButton backgroundColor='transparent' text='NEXT' onPress={onNextPress} width={120} paddingLeft={50} />  //the width paddingLeft value fixes a bug where the previous PasswordRoute's
        : <NavButton backgroundColor='transparent' text='DONE' onPress={onDonePress} width={120} paddingLeft={50} />; //SIGNUP buton still gets triggered even though the route is popped! :(
    },
    getTitle() {
      return title || component;
    },
    configureScene() {
      if(component === Price) return FloatFromTop();
      else return FloatFromRightFade(); //return N.SceneConfigs.FloatFromRight;
    },
    showFullPage: false,
    transparentNavBar: true,
  }, customConfig);
}

function finishLater() {
  Alert.alert(
    'FINISH LATER',
    "Are you sure you want to finish before completing your profile? You can finish your Celebvidy profile anytime, but you won't be submitted to the directory until you do.",
    [{text: 'CANCEL', onPress: () => null}, {text: 'FINISH', onPress: () => {
      completeWizard('finishLater');
    }}]
  );
}

function completeWizard(finishLater) {

  Mixpanel.registerSuperProperties(getState().celebrity);

  if(finishLater) {
    Mixpanel.trackWithProperties('Celebrity Signup Finish Later', getState().celebrity);
  }
  else {
    Mixpanel.trackWithProperties('Celebrity Signup Complete', getState().celebrity);
  }

  dispatch(toggleTour(false, finishLater));

  setTimeout(() => {
    dispatch(updateCelebrityWithAllState())
      .then(() => dispatch({type: 'TOOLTIP_STEP', payload: 0}))
  }, 3050); //send celebrity state to server after flip so flip is smooth

  setTimeout(() => {
    dispatch({type: 'TOOLTIP_STEP', payload: 1});
  }, 3800);
}
