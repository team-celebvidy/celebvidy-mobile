
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  TouchableOpacity,
  NativeModules,
} from 'react-native';
import React, { Component } from 'react';

const { TwitterSignin } = NativeModules;

import Mixpanel from 'react-native-mixpanel';

const validator = require('validator');

const {width, height} = Dimensions.get('window');

import {TWITTER} from '../../config';
import formStyles from '../../styles/FormStyles';


import ActionButton from '../../widgets/ActionButton';
import ClassyActionButton from '../../widgets/ClassyActionButton';
import ModalSpinner from '../../widgets/ModalSpinner';
import {BackButton} from '../../widgets/NavButton';

import TextInput from '../../widgets/GiftedTextInput';
import AnimateMessageMixin from '../../widgets/AnimateMessageMixin';

import * as actions from '../../actions';
import {connect} from 'react-redux';
import {getState} from '../../configureStore';


import ddpClient from '../../ddp';
const ddp = ddpClient();

class LoginSignup extends AnimateMessageMixin(Component) {    //check later
  static defaultProps = {
    formName: 'celebrityLoginSignup',
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      emailValid: true,
      message: '',
    };
  }

  componentDidMount() {
    Mixpanel.track('Celebrity LoginSignup')
  }

  login() {
    let email = this.validateEmail();
    if(email) this.pushPasswordPage(email, this.onLoginSubmit, 'login');        //pass onLoginSubmit  callback to password page.
  }
  onLoginSubmit(email, password) {
    this.props.dispatch({type: 'HIDE_DRAWER'});                                 //Celebrities logging in should land on their profile rather than their account drawer

    return this.props.loginWithEmail(email, password)                           //dispatch async action + api request that does all the dirty work
      .then(() => {
        Mixpanel.identify(getState().user._id);
        Mixpanel.set({$last_login: new Date().toISOString()});
        Mixpanel.track('Celebrity Login Complete');
        return this.props.toggleTour(false);                                    //push the next page, duh (well, flip the tour in this casee)
      })
      .catch((error) => alert('Celebvidy failed to log you in: '+error.message));
  }

  signup() {
    let email = this.validateEmail();

    if(email) {
      this.props.sendConfirmationEmailCode(email);                              //email the user the confirmation code (and return it to the client for storage in redux)
      this.pushEmailConfirmationPage(email, this.onSignupSubmitConfirmationCode);
    }
  }
  onSignupSubmitConfirmationCode(email, code) {
    if(code === getState().confirmEmailCode) {                                  //check whether code entered matches the code secretly stored in store (yes, it's secure; it's just so people don't end up with accounts based on the wrong email; they can't hi-jack accounts with this, as when signup completes it will check if a user already exists with that email)
      this.pushPasswordPage(email, this.onSignupSubmit, 'signingUp');
      return Promise.resolve();
    }
    else return Promise.reject(new Error('The confirmation code you entered does not match what was sent to you.')); //insure `.catch()` still works in EmailConfirmationCode.js
  }
  onSignupSubmit(email, password) {
    return this.props.celebritySignupWithEmail(email, password)
      .then(() => {
        Mixpanel.createAlias(getState().user._id);
        Mixpanel.identify(getState().user._id);
        Mixpanel.setOnce({celebrity: true, user_type: 'celebrity', $email: email, $created: new Date().toISOString(), $last_login: new Date().toISOString()});
        Mixpanel.track('Celebrity Signup Complete');
        return this.props.navigator.pushWithProps('priceRoute');
      })
      .catch((error) => alert('Celebvidy failed to sign you up: '+error.message));
  }

  forgotPassword() {
    let email = this.validateEmail();

    if(email) {
      this.props.sendConfirmationEmailCodeForgotPassword(email)                 //email the user the password confirmation code (similar to signup, but we DO NOT send to client and store in redux)
        .catch((error) => alert(error.message));

      this.pushEmailConfirmationPage(email, this.onForgotPasswordSubmitConfirmationCode, true); //take em to the confirmation page so they can enter the email they received
    }
  }

  //NOTE: loginWithEmail used to be loginWithToken, but now we pass a generated temporary password over the wire instead of a loginToken, since loginTokens won't in fact work when logged out
  onForgotPasswordSubmitConfirmationCode(email, code) {
    return this.props.findForgotPasswordLoginTokenFromCode(email, code)         //instead, we collect the entered code + email and make sure on the server they verify (the code is stored on the user model in the database for comparison)
      .then((password) => this.props.loginWithEmail(email, password) )          //login the user in as usual with their generated password
      .then(() => this.pushPasswordPage(email, this.onForgotPasswordSubmit, 'forgotPassword') )   //go to password page to reset the password
      .catch(() => alert('Celebvidy had an issue validating your confirmation code'));
  }
  onForgotPasswordSubmit(email, password) {
    return this.props.changePassword(password)                                  //at this point the user is already logged in, so we just the standard Meteor clientside `Accounts.changePassword` feature (well, through our react native-specialized DDP client)
      .then(() => {
        Mixpanel.identify(getState().user._id);
        Mixpanel.set({$last_login: new Date().toISOString()});
        Mixpanel.track('Celebrity Login Complete');
        return this.props.toggleTour(false);                                    //take em home
      })
      .catch((error) => alert('Celebvidy failed to reset your password: '+error.message));
  }

  //convenience methods to make above code more succinct
  pushEmailConfirmationPage(email, onSubmitConfirmationCode, isForgotPassword=false) {
    Mixpanel.track('Celebrity Email Confirmation');
    this.props.navigator.pushWithProps('EmailConfirmationCode', {
      formName: this.props.formName,
      email,
      isForgotPassword,
      onSubmitConfirmationCode: onSubmitConfirmationCode.bind(this)
    });
  }
  pushPasswordPage(email, onLoginSubmit, page='login') {
    Mixpanel.track('Celebrity Password');

    this.props.navigator.pushWithProps('Password', {
      formName: this.props.formName,
      email,
      [page]: true,
      onLoginSubmit: onLoginSubmit.bind(this),
    });
  }

  validateEmail() {
    let email = this.refs.email.getValue();
    let isValid = email && validator.isEmail(email);

    if(email && !isValid) {
      this.setState({emailValid: false}, () => {
        this.flashMessage('Invalid email address');
      });
    }
    else if(email && isValid) this.setState({emailValid: true});
    else {
      this.flashMessage('Please enter your email first'); //user pressed Login or Signup thinking it would take em to another page to start Signup/Login flow
      this.refs.email.focus();
    }

    return isValid ? email : null;
  }

  loginWithTwitter() {
    if(!this.props.celebrityPin) {
      return this.props.navigator.pushWithProps('CelebrityPin', {
        returnTo: 'TwitterSignin',
        toggleTour: this.props.toggleTour,
        onSubmit: () => this._loginWithTwitter(),
      });
    }
    else {
      this._loginWithTwitter();
    }
  }
  _loginWithTwitter() {
    TwitterSignin.logIn(TWITTER.consumerKey, TWITTER.secret, (error, loginData) => { //loginData: {authToken, authTokenSecret, userID, userName, email}
      if(!error) {
        console.log('TWITTER LOGIN SUCCESS!!', loginData);

        this.props.loginWithTwitter(loginData)
          .then(() => {
            if(getState().celebrity.price) {
              Mixpanel.identify(getState().user._id);
              Mixpanel.set({$last_login: new Date().toISOString()});
              Mixpanel.track('Celebrity Login Complete');

              return this.props.toggleTour(false);
            }
            else {
              Mixpanel.createAlias(getState().user._id);
              Mixpanel.identify(getState().user._id);
              Mixpanel.setOnce({celebrity: true, user_type: 'celebrity', twitter: getState().celebrity.twitter, $created: new Date().toISOString(), $last_login: new Date().toISOString()});
              Mixpanel.track('Celebrity Signup Complete');

              return this.props.navigator.pushWithProps('priceRoute');
            }
          })
          .catch((error) => alert('Celebvidy failed to log you in: '+error.message));
      } else {
        console.log('TWITTER LOGIN ERROR', error);
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <ModalSpinner visible={this.props.loginFetching} note='logging you in...' />

        <BackButton text='TOUR' style={{position: 'absolute', top: 8, left: 8}} onPress={() => {
          this.props.scrollToFirstPage();

          setTimeout(() => {
            this.props.navigator.pop();
            this.props.resetButtons();
          }, 310);
        }}/>

        <View style={{flex: 1, top: -60}}>
          <View style={{height: 60, marginBottom: 40}} >
            <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
              {this.state.message}
            </Animated.Text>
          </View>


          <TextInput
            name='email'
            title='Email'
            formName={this.props.formName}
            placeholder='janecelebrity@email.com'
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            keyboardType='email-address'
            returnKeyType='default'
            autoCapitalize="none"
            autoCorrect={false}
            widgetStyles={formStyles.login}
            underlined={true}
            ref='email'
            value=""
            valid={this.state.emailValid}
          />


          <View style={{height: 40}} />

          <ClassyActionButton
            touchableStyle={{marginBottom: 20}}
            style={{height: 50,}}
            textStyle={{color: 'rgb(185, 183, 183)'}}
            onPress={() => this.login()}
          >
            LOGIN
          </ClassyActionButton>

          {!this.props.loginOnly &&
            <ClassyActionButton
              touchableStyle={{marginBottom: 20}}
              style={{height: 50, }}
              textStyle={{color: 'rgb(185, 183, 183)'}}
              onPress={() => this.signup()}
            >
              SIGNUP
            </ClassyActionButton>
          }

          <View style={{height: 20}} />

          <TouchableOpacity onPress={() => this.forgotPassword()}>
            <Text style={{color: 'rgba(41, 56, 212, 1)',  fontFamily: 'ProximaNova-Thin', fontSize: 20, textAlign: 'center'}}>
              Forgot Password
            </Text>
          </TouchableOpacity>
        </View>


        {!this.props.loginFetching && <ActionButton
          touchableStyle={{position: 'absolute', left: 10, bottom: 17}}
          style={{width: this.props.isTour ? width - 20 : width - 40, height: 50, backgroundColor: 'rgb(33, 38, 148)', borderColor: 'rgb(47, 53, 172)'}}
          textStyle={{color: 'rgb(185, 183, 183)'}}
          onPress={() => this.loginWithTwitter(this.props.loginOnly)}
        >
          {!this.props.loginOnly ? 'AUTHENTICATE WITH TWITTER' : 'LOGIN WITH TWITTER'}
        </ActionButton>}
      </View>
    );
  }
}

export default connect(({celebrityPin, loginFetching}) => ({celebrityPin, loginFetching}), actions)(LoginSignup)



const styles = StyleSheet.create({
  container: {
    marginTop: -64,
    height,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: 'black',
  }
});
