import {StyleSheet, Text, View, Image, Dimensions, Animated} from 'react-native';
import React, { Component } from 'react';
const {width, height} = Dimensions.get('window');

import AnimateMessageMixin from '../../widgets/AnimateMessageMixin';
import CircularTouchSlider from '../../widgets/CircularTouchSlider';
import valueToPrice, {priceToValue} from '../../misc/valueToPrice';

import {connect} from 'react-redux';
import * as actions from '../../actions';


class Price extends AnimateMessageMixin(Component) {   //check later
  componentDidMount() {
    this.showMessageThenVanish(500, 5000, "Excellent, you've created your Celebvidy account. Let's flush out your profile so you can start taking orders.", () => {
      this.showMessage(300, 100, 'What price would you like to charge for your celebvidys?');
    });
  }
  constructor(props, context) {
    super(props, context);
    this.state = {message: ''};
  }

  saveWizard() {
    let price = this.refs.slider.getValue();
    price = valueToPrice(price, false);
    return this.props.storeCelebrity({price});
  }

  render() {
    return (
      <View style={styles.container}>

        <CircularTouchSlider
          ref='slider'
          percent={priceToValue(this.props.price, 50)}
          formatPercent={valueToPrice}
          size={250}
          tension={800}
          friction={100}
          sensitivity={16}
          percentLabel='per celebvidy'
        />

        <View style={{position: 'absolute', top: 40, height: 60, width, paddingHorizontal: 25}}>
          <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
            {this.state.message}
          </Animated.Text>
        </View>

      </View>
    );
  }
}

Price = connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(Price)



class Inventory extends AnimateMessageMixin(React.Component) {
  componentDidMount() {
    this.showMessage(500, 500, 'How many celebvidys would you like to make available?');
  }

  constructor(props, context) {
    super(props, context);
    this.state = {message: ''};
  }

  saveWizard() {
    let inventory = this.refs.slider.getValue();
    return this.props.storeCelebrity({inventory});
  }

  render() {
    return (
      <View style={styles.container}>

        <CircularTouchSlider
          ref='slider'
          percent={typeof this.props.inventory !== 'undefined' ? this.props.inventory :  50}
          formatPercent={(value) => Math.round(value)}
          size={250}
          tension={800}
          friction={100}
          sensitivity={16}
          percentLabel='celebvidys'
        />

        <View style={{position: 'absolute', top: 40, height: 60, width, paddingHorizontal: 25}}>
          <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
            {this.state.message}
          </Animated.Text>
        </View>

      </View>
    );
  }
}

Inventory = connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(Inventory)



class Charity extends AnimateMessageMixin(React.Component) {
  componentDidMount() {
    this.showMessage(500, 1000, 'What percentage do you want to give to charity (optional)?')
  }
  constructor(props, context) {
    super(props, context);
    this.state = {message: ''};
  }

  saveWizard() {
    let charity_percent = this.refs.slider.getValue();
    return this.props.storeCelebrity({charity_percent});
  }

  render() {
    return (
      <View style={styles.container}>

        <CircularTouchSlider
          ref='slider'
          percent={typeof this.props.charity_percent !== 'undefined' ? this.props.charity_percent : 10}
          formatPercent={(value) => Math.round(value) + '%'}
          size={250}
          tension={800}
          friction={100}
          sensitivity={16}
        />

        <View style={{position: 'absolute', top: 40, height: 60, width, paddingHorizontal: 25}}>
          <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
            {this.state.message}
          </Animated.Text>
        </View>

      </View>
    );
  }
}

Charity = connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(Charity)


class MonthlyReset extends AnimateMessageMixin(React.Component) {
  componentDidMount() {
    this.showMessage(500, 1000, 'On the start of each month, what would you like to have your inventory automatically reset to?')
  }
  constructor(props, context) {
    super(props, context);
    this.state = {message: ''};
  }

  saveWizard() {
    let monthly_reset_inventory = this.refs.slider.getValue();
    return this.props.storeCelebrity({monthly_reset_inventory});
  }

  render() {
    return (
      <View style={styles.container}>

        <CircularTouchSlider
          ref='slider'
          percent={typeof this.props.monthly_reset_inventory !== 'undefined' ? this.props.monthly_reset_inventory : 50}
          formatPercent={(value) => Math.round(value)}
          size={250}
          tension={800}
          friction={100}
          sensitivity={16}
          percentLabel='celebvidys'
        />

        <View style={{position: 'absolute', top: 40, height: 60, width, paddingHorizontal: 25}}>
          <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
            {this.state.message}
          </Animated.Text>
        </View>

      </View>
    );
  }
}

MonthlyReset = connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(MonthlyReset)


export {
  Price,
  Inventory,
  Charity,
  MonthlyReset
}

const styles = StyleSheet.create({
  container: {
    width,
    backgroundColor: 'transparent',
    flex: 1,
  }
});
