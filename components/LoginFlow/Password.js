import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  ScrollView,
  InteractionManager,
  StatusBar,
  TouchableOpacity,
  Navigator as N,
  PixelRatio,
} from 'react-native';
import React, { Component } from 'react';
import dismissKeyboard from 'react-native-dismiss-keyboard';

const {SceneConfigs: {FloatFromRight: FloatFromRight}} = N;
const {width, height} = Dimensions.get('window');

import formStyles from '../../styles/FormStyles';

import Navigator from '../../widgets/Navigator';
import NavButton from '../../widgets/NavButton';
import ModalSpinner from '../../widgets/ModalSpinner';

import TextInput from '../../widgets/GiftedTextInput';
import {FloatFromTop} from '../../widgets/SceneConfigs';
import AnimateMessageMixin from '../../widgets/AnimateMessageMixin';

import {connect} from 'react-redux';


class Password extends AnimateMessageMixin(Component) {   //check later 
  componentDidMount() {
    this.showMessageThenVanish(400, 5000, this.props.forgotPassword ? "Reset your password" : "Enter your password");
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      passwordValid: true,
      message: '',
    };
  }

  submitPassword() {
    let password = this.clientValidatePassword();

    if(password) {
      dismissKeyboard();
      this.props.onLoginSubmit(this.props.email, password)
        .catch((error) => {
          this.setState({passwordValid: false});
          this.flashMessageStick(error.message);
        });
    }
  }

  clientValidatePassword() {
    let password = this.refs.password.getValue();
    let isValid = password && password.length >= 6;

    if(!isValid) {
      this.setState({passwordValid: false});
      this.flashMessage('Password needs to be at least 6 characters');
    }
    else if(isValid) this.setState({passwordValid: true});

    return isValid ? password : null;
  }

  render() {
    let {loginFetching} = this.props;

    return (
      <View style={styles.container}>
        <ModalSpinner visible={loginFetching} />

        <View style={{flex: 1, top: -110}}>
          <View style={{height: 60, marginBottom: 60}} >
            <Animated.Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, textAlign: 'center', color: 'white', opacity: this.messageOpacity}}>
              {this.state.message}
            </Animated.Text>
          </View>

          <TextInput
            name='password'
            title='Password'
            formName={this.props.formName}
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            widgetStyles={formStyles.login}
            underlined={true}
            secureTextEntry={true}
            autoCapitalize="none"
            autoCorrect={false}
            ref='password'
            valid={this.state.passwordValid}
            value={''}
            autoFocus={true}
            onSubmitEditing={(password) => this.submitPassword()}
          />

        </View>

      </View>
    );
  }
}

export default connect(
  ({loginFetching}) => ({loginFetching}),
  null, null, {withRef: true}
)(Password)



const styles = StyleSheet.create({
  container: {
    marginTop: -64,
    height,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: 'black',
  }
});
