'use strict';

import{
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  Animated,
} from 'react-native';
import React, { Component } from 'react';
var ParticleView = require('react-native-particle-system/ParticleView');
var ParticleCell = require('react-native-particle-system/ParticleCell');

const {width, height} = Dimensions.get('window');


let particleProps = {
  name : "untitled",
  enabled : true,
  contentsRect : {x: 0.0, y: 0.0, width: 1.0, height: 1.0},
  magnificationFilter: 'linear', minificationFilter: 'linear',
  scale: 1.0, scaleRange: 1.28, scaleSpeed: -0.29,
  color: '#0021ff',
  //color: 'orange',
  redRange: 0.6,	greenRange: 0.0, blueRange: .0, alphaRange: 0.57,
  redSpeed: 0.0, greenSpeed: 0.0, blueSpeed: -0.66, alphaSpeed: -0.64,

  lifetime: 2230.0,
  lifetimeRange: 33.11,
  birthRate: 350,
  velocity: 90.7, velocityRange: 90.0,
  xAcceleration: 0.0, yAcceleration: -156.0, zAcceleration : 0.0,

  // these values are in radians, in the UI they are in degrees
  spin : .5, spinRange : 50,
  emissionLatitude : 4.0, emissionLongitude : 0.0, emissionRange : 0.0,
};




export default class UnderlineParticles extends Component {
  constructor(props) {
    super(props);
    this.state = {selectedTab: 0};
  }

  captionOpacity() {
    return this.props.particlesOpacity.interpolate({
      inputRange: [0,1],
      outputRange: [1,0],
    });
  }
  render() {
    window.part = this
    var midX = Dimensions.get('window').width/2;
    var midY = Dimensions.get('window').height/2;

    return (
      <Animated.View style={{position: 'absolute', top: -5, left: 0, height, width, backgroundColor: 'transparent', opacity: this.props.opacity}}>
        <Animated.Text style={{
          color: 'rgb(255, 255, 255)',
          //opacity: .85,
          opacity: this.captionOpacity(),
          textShadowOffset: {
            width: 3*StyleSheet.hairlineWidth,
            height: -3*StyleSheet.hairlineWidth,
          },
          textShadowRadius: 3*StyleSheet.hairlineWidth,
          textShadowColor:  "rgb(0, 41, 255)",

          fontSize: 22,
          fontFamily: 'Acrom', position: 'absolute', bottom: 30, left: 0, width, textAlign: 'center'}}>
          ON-DEMAND SHOUT OUTS
        </Animated.Text>

        <Animated.View style={{flex:1, opacity: this.props.particlesOpacity}}>
          <ParticleView name={"emitterLayer"} style={{flex:1, opacity: .6}}
            emitterPosition={{x:midX, y:midY*2 - 50}}
            emitterZPosition={0}
            emitterShape={'line'}
            emitterMode={'surface'}
            emitterSize={{width: 300, height: height - 40}}
            emitterDepth={10}
            preservesDepth={false}
            renderMode={"additive"}
            seed={2197194815}
          >
            {
              <ParticleCell {...particleProps}>
                <Image source={{uri:"spark"}} style={{top:0, left:0, width:64, height:64}}/>
              </ParticleCell>}
          </ParticleView>

        </Animated.View>
      </Animated.View>
    );
  }
}
