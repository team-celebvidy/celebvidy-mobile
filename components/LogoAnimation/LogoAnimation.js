import {AppRegistry, StyleSheet, View, Dimensions, Animated, Image, Text, Easing, InteractionManager, StatusBar} from 'react-native';
import shallowCompare from 'react-addons-shallow-compare';
import React, { Component } from 'react';
import Art from 'ReactNativeART';
import TimerMixin, {Timer} from 'react-timer-mixin';

const {width, height} = Dimensions.get('window');
const {Surface, Group, Shape, Path} = Art;

const AnimatedShape = Animated.createAnimatedComponent(Shape);

import BackgroundParticles from './BackgroundParticles';
import UnderlineParticles from './UnderlineParticles';
import LogoSvg, {LogoText} from '../LogoSvg';

const FILL_COLORS = [
  'rgb(13, 16, 57)',
  'rgb(13, 16, 57)',
  'rgb(14, 61, 201)',
  'rgb(27, 92, 223)',
  'rgb(27, 92, 223)',
  'rgba(0,0,0,0)'
];

const PARTICLE_COLORS = [
  'rgb(13, 16, 57)',
  'rgb(13, 16, 57)',
  'rgb(9, 34, 203)',
  'rgb(13, 16, 57)',
  'rgb(12, 38, 214)',
  'rgb(13, 16, 57)'
];


import * as actions from '../../actions';
import {connect} from 'react-redux';


export default class LogoAnimationWrapper extends Timer(Component) {    //check later
  constructor(props, context) {
    super(props, context);

    this.state = {
      visible: this.props.visible || false,
    };
  }

  //allow for declarative style control
  componentWillReceiveProps(nextProps) {
    if(nextProps.visible != this.props.visible) {
      if(nextProps.visible) this.show();
      if(!nextProps.visible) this.hide();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  componentDidMount() {
    if(this.props.visible) {
      this.props.onStart && this.props.onStart(); //show() below won't be called, since componentWillRecceive props isn't called and the component renders starting with the animation
    }
  }

  //using refs allow for imperative style control
  show() {
    this.props.onStart && this.props.onStart();
    this.setState({visible: true, animationComplete: false});
  }
  hide() {
    this.setState({visible: false, animationComplete: true});
  }

  onComplete() {
    this.hide();
    this.props.onComplete && this.props.onComplete();
    if(this.props.handle) InteractionManager.clearInteractionHandle(this.props.handle);
  }

  render() {
    if(this.state.visible || (this.state.animationComplete && this.props.hold)) {
      return (
        <View style={[{position: 'absolute', top: 0, left: 0, backgroundColor: 'transparent', width, height}, this.props.style]}>
          <StatusBar hidden={true} animated={true} />
          <LogoAnimation onComplete={this.onComplete.bind(this)} delay={this.props.delay} />
        </View>
      );
    }
    else return null;
  }
}



const LogoAnimation = React.createClass({
  mixins: [TimerMixin],
  getInitialState: function() {
    return {
      animation: new Animated.Value(0),
      containerOpacity: new Animated.Value(1),
      logoTextOpacity: new Animated.Value(0),
      particlesOpacity: new Animated.Value(1),
      backgroundOpacity: new Animated.Value(0),
      twinkleAnimation: new Animated.Value(0),
    };
  },

  componentDidMount() {
    let delay = typeof this.props.delay !== 'undefined' ? this.props.delay : 1500;
    this.setTimeout(() => this.explode(), delay);
  },
  explode() {
    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.parallel([
          Animated.timing(this.state.animation, {
            duration: 5000,
            toValue: 24,
            delay: 300,
          }),
          Animated.timing(this.state.backgroundOpacity, {
            toValue: 1,
            duration: 1000,
            delay: 2000,
            easing: Easing.sin,
          }),
          Animated.sequence([
            Animated.timing(this.state.logoTextOpacity, {
              toValue: 1,
              duration: 700,
              delay: 2900,
              easing: Easing.sin,
            }),
            Animated.timing(this.state.backgroundOpacity, {
              toValue: 0,
              duration: 1000,
              delay: 1250,
              easing: Easing.sin,
            }),
            Animated.timing(this.state.particlesOpacity, {
              duration: 1000,
              toValue: 0, //change the last 3 `toValue` values to 0 and commment out the above `hide()` method to work on this without it disappearing
              delay: 500,
              easing: Easing.sin,
            }),
            /**
            Animated.timing(this.state.twinkleAnimation, {
              duration: 2250, //if you bring this back, remove the delay from the following animation
              toValue: 12,
              delay: 0,
              easing: Easing.sin,
            }),
            **/
          ]),
          Animated.timing(this.state.containerOpacity, {
            duration: 500,
            toValue: 0,
            delay: 9000,
            easing: Easing.easeInOut,
          }),

        ]).start(this.props.onComplete);
      });
    });
  },

  render: function() {
    var logo_scale = this.state.animation.interpolate({
      inputRange: [0, .01, 6, 10, 12, 24, 28],
      outputRange: [0.0001, 0.0001, .1, 1, 1.2, .9, .01],
      extrapolate: 'clamp'
    });

    var logo_rotate = this.state.animation.interpolate({
      inputRange: [0, 4, 7, 15, 20, 28],
      outputRange: ['0deg', '450deg', '1500deg', '3240deg', '3240deg', '3240deg'],
    });

    var containerScale = this.state.containerOpacity.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });


    return (
      <Animated.View style={[styles.container, {top: - 50, height: height + 50, backgroundColor: 'black', opacity: this.state.containerOpacity}]}>
        <BackgroundParticles opacity={this.state.backgroundOpacity} />

        <Animated.View style={{transform:[{scale: containerScale}]}}>

          <Surface width={width} height={height} style={{backgroundColor: 'transparent'}}>
            <Group x={width/2 - 75} y={height/2 - 75} style={{backgroundColor: 'transparent'}}>
              {this.getSmallExplosions(75, {x:89, y:75})}
            </Group>
          </Surface>

          <LogoSvg
            animation={this.state.animation}
            twinkleAnimation={this.state.twinkleAnimation}
            size={300}
            color='rgb(23, 46, 167)'
            strokeWidth={10}
            style={{
              position: 'absolute',
              left: width/2 - 150,
              top: height/2 - 150,
              transform: [
                {rotateZ: logo_rotate},
                {scale: logo_scale},
              ]
            }}
          />



          <UnderlineParticles opacity={this.state.logoTextOpacity} particlesOpacity={this.state.particlesOpacity} />

          <LogoText
            color='rgb(200,200,200)'
            fontSize={54}
            vidySize={57}
            tmTop={21}
            vidyTop={-3}
            color='rgb(23, 46, 167)'
            style={{
              position: 'absolute',
              width,
              backgroundColor: 'transparent',
              bottom: 100,
              left: 5,
              opacity: this.state.logoTextOpacity
            }}
          />

        </Animated.View>
      </Animated.View>
    );
  },

  getSmallExplosions: function(radius, offset) {
    return [0,1,2,3,4,5,6].map((v, i, t) => {

      var scaleOut = this.state.animation.interpolate({
        inputRange: [0, 5.99, 6, 13.99, 14, 21],
        outputRange: [0, 0, 1, 1, 1, 0],
        extrapolate: 'clamp'
      });

      var moveUp = this.state.animation.interpolate({
        inputRange: [0, 5.99, 14],
        outputRange: [0, 0, -15],
        extrapolate: 'clamp'
      });

      var moveDown = this.state.animation.interpolate({
        inputRange: [0, 5.99, 14],
        outputRange: [0, 0, 15],
        extrapolate: 'clamp'
      });

      var color_top_particle = this.state.animation.interpolate({
        inputRange: [6, 8, 10, 12, 17, 21],
        outputRange: shuffleArray(PARTICLE_COLORS)
      })

      var color_bottom_particle = this.state.animation.interpolate({
        inputRange: [6, 8, 10, 12, 17, 21],
        outputRange: shuffleArray(PARTICLE_COLORS)
      })

      var position = getXYParticle(7, i, radius)

      return (
        <Group
          key={i}
          x={position.x + offset.x }
          y={position.y + offset.y}
          rotation={getRandomInt(0, 40) * i}
        >
          <AnimatedCircle
            x={moveUp}
            y={moveUp}
            radius={15}
            scale={scaleOut}
            fill={color_top_particle}
          />
          <AnimatedCircle
            x={moveDown}
            y={moveDown}
            radius={8}
            scale={scaleOut}
            fill={color_bottom_particle}
          />
        </Group>
      )
    }, this)
  },
});


const AnimatedCircle = React.createClass({displayName: "Circle",
  render: function() {
    var radius = this.props.radius;
    var path = Path().moveTo(0, -radius)
        .arc(0, radius * 2, radius)
        .arc(0, radius * -2, radius)
        .close();

    return React.createElement(AnimatedShape, React.__spread({},  this.props, {d: path}));
  }
});

function getXYParticle(total, i, radius) {
  var angle = ( (2*Math.PI) / total ) * i;

  var x = Math.round((radius*2) * Math.cos(angle - (Math.PI/2)));
  var y = Math.round((radius*2) * Math.sin(angle - (Math.PI/2)));
  return {x, y};
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    return array;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  }
});
