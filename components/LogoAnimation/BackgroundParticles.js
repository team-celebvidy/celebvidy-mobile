'use strict';

import {
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  Animated,
} from 'react-native';
import React, { Component } from 'react';

var ParticleView = require('react-native-particle-system/ParticleView');
var ParticleCell = require('react-native-particle-system/ParticleCell');

const {width, height} = Dimensions.get('window');


let particleProps = {
  name : "untitled",
  enabled : true,
  contentsRect : {x: 0.0, y: 0.0, width: 1.0, height: 1.0},
  magnificationFilter: 'nearest', minificationFilter: 'nearest',
  scale: 1.1, scaleRange: 1.28, scaleSpeed: -0.29,
  color: '#0021ff',
  //color: 'orange',
  redRange: 0.6,	greenRange: 0.0, blueRange: .0, alphaRange: 0.57,
  redSpeed: 0.2, greenSpeed: 0.0, blueSpeed: -0.66, alphaSpeed: -0.64,

  lifetime: 3230.0, lifetimeRange: .21,
  birthRate: 320,
  velocity: 93.7, velocityRange: 70.0,
  xAcceleration: 0.0, yAcceleration: -266.0, zAcceleration : 0.0,

  // these values are in radians, in the UI they are in degrees
  spin : 3.000, spinRange : 1,
  emissionLatitude : 1.484, emissionLongitude : 0.0, emissionRange : 2.0,
};




export default class BackgroundParticles extends Component {
  render() {
    var midX = Dimensions.get('window').width/2;
    var midY = Dimensions.get('window').height/2;

    return (
      <Animated.View style={{position: 'absolute', top: 0, left: 0, width, height, backgroundColor: 'transparent', opacity: this.props.opacity}}>
        <View style={{flex:1, opacity: .2}}>
          <ParticleView name={"emitterLayer"} style={{flex:1}}
            emitterPosition={{x:midX, y:midY*2 - 320}}
            emitterZPosition={0}
            emitterShape={'cuboid'}
            emitterMode={'surface'}
            emitterSize={{width: width, height: height + 60}}
            emitterDepth={0}
            renderMode={"additive"}
            seed={2197194815}
          >
            {
              <ParticleCell {...particleProps}>
                <Image source={{uri:"spark"}} style={{top:0, left:0, width:64, height:64}}/>
              </ParticleCell>}
          </ParticleView>
        </View>
      </Animated.View>
    );
  }
}
