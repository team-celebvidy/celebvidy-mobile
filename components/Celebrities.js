import {StyleSheet, Text, View, Image, TextInput, Dimensions, Animated, StatusBar} from 'react-native';
import React, { Component } from 'react';
import {Timer} from 'react-timer-mixin';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Gradient from 'react-native-linear-gradient';
import _ from 'underscore';
import ImageLoading from 'react-native-image-progress';
import Progress from 'react-native-progress';
import Mixpanel from 'react-native-mixpanel';

import UltimateListView from '../widgets/UltimateListView';
import CelebrityRow from './CelebrityRow';
import SegmentedControl from '../widgets/SegmentControl';
import {route as celebrityRoute} from './Celebrity';


const {height, width} = Dimensions.get('window');
const FOOTER_HEADER_HEIGHT = 154 + 1*StyleSheet.hairlineWidth;

import {getCelebrities, selectCelebritiesTab} from '../actions'
import {connect} from 'react-redux';


class Celebrities extends Timer(Component) {  //check later 
  constructor(props, context) {
    super(props, context);
    this.state = {
      segmentIndex: 0,
      searchValue: '',
    };
  }

  componentWillReceiveProps({celebrityFromUrl: celeb}) {
    if(celeb !== this.props.celebrityFromUrl) {
      Mixpanel.trackWithProperties('View Celebrity', {celebrity: celeb.display_name, price: celeb.price, industry: celeb.industry, inventory: celeb.inventory})
      this.props.navigator.pushWithProps('Celebrity', {celeb});
    }
  }

  _onFetch(page = 1, callback, options) {
    this.props.dispatch(getCelebrities({page, ...options}))
      .catch((error) => alert('Celebrities could not be retrieved'));
  }

  tabIndex() {
    return this.props.celebritiesFetchOptions.sort === 'popular' ? 0 : 1;
  }

  render () {
    return (
      <View style={styles.container}>

        <SegmentedControl
          titles={["POPULAR", "A-Z"]}
          index={this.tabIndex()}
          onPress={segmentIndex => {
            let sort = segmentIndex === 0 ? 'popular' : 'a-z';
            this.props.dispatch({type: 'SELECT_CELEBRITIES_TAB', payload: sort});
            this.props.getSearch().setState({value: ''});
          }}

          stretch={true}
          style={styles.segmentedControl}
          barColor={"rgba(27,58,223,1)"}
          barStyle={{height: 1}}
          barPosition='top'
          underlayColor={"rgba(27,58,223,1)"}
          renderTitle={(title, i) => {
            let index = this.tabIndex();

            return (
              <View style={[styles.titleView, i === index && styles.selectedTitleView]}>
                <Text style={[styles.title, i === index && styles.selectedTitle]}>{title}</Text>
              </View>
            );
          }}
        />

        <UltimateListView
          ref={(listview) => this.listview = listview}
          rowView={(data, sectionId, rowIndex) => this._renderRowView(this.props.navigator, data, rowIndex)}
          onFetch={this._onFetch.bind(this)}
          rows={this.props.celebrities}
          fetchOptions={this.props.celebritiesFetchOptions}
          emptyMessage='No celebrities found'
        />


        <Gradient
          locations={[0,  1]}
          colors={['rgba(0,0,0,.6)','rgba(0, 0, 0, 0)']}
          style={{
            width,
            height: 6,
            position: 'absolute',
            left: 0,
            top: 44,
          }}
        />
      </View>
    );
  }

  _renderRowView(navigator, celeb, index) {
    return (
      <CelebrityRow
        key={index}
        {...celeb}
        navigator={navigator}
      />
    );
  }
}

export default connect(
  ({celebrities, celebritiesFetchOptions, celebrityFromUrl}) => ({celebrities, celebritiesFetchOptions, celebrityFromUrl}),
)(Celebrities)

var styles = {
  linearGradient: {
    width,
    height: 20,
    position: 'absolute',
    left: 0,
  },
  container: {
    flex: 1,
  },
  navBar: {
    height: 64,
  },
  segmentedControl: {
    backgroundColor: 'black',
    flex: 0,
    height: 44 + 1*StyleSheet.hairlineWidth,
    //borderBottomWidth: 1*StyleSheet.hairlineWidth,
    //borderBottomColor:  'rgba(27,58,233,.2)' ,
  },
  titleView: {
    backgroundColor: "rgba(21,21,24,1)",
  },
  selectedTitleView: {
    backgroundColor:  "black" ,
    borderLeftColor:  "rgba(31, 31, 36, 1)" ,
    borderLeftWidth: StyleSheet.hairlineWidth,
    borderRightColor:  "rgba(31, 31, 36, 1)" ,
    borderRightWidth: StyleSheet.hairlineWidth,
  },
  title: {
    color: 'gray',
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    paddingHorizontal: 2,
    paddingVertical: 14,
  },
  selectedTitle: {
    color: 'white',
  },
};
