import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, PixelRatio, TouchableOpacity, ScrollView, StatusBar} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import Communications from 'react-native-communications';

import formStyles from '../../styles/FormStyles';
import AccountItem from '../../widgets/AccountItem';
import NavButton from '../../widgets/NavButton';

import LoginSignup from '../LoginFlow/LoginSignup';

var {width, height} = Dimensions.get('window');

import * as actions from '../../actions';
import { connect } from 'react-redux';



class CelebrityAccount extends Component {
  emailSupport() {
    Communications.email(['support@celebvidy.com'], null, null, 'celebrity support', null);
  }

  isMediaComplete() {
    let celeb = this.props.celebrity;
    return celeb.youtube_id && celeb.avatar_photo && celeb.background_photo;
  }

  shouldComponentUpdate(nextProps) {
    let {showOrders, selectedRoute, navigator} = nextProps;

    if(showOrders && showOrders !== this.props.showOrders) {
      navigator.resetRouteStack(['CelebrityAccount', 'CelebrityCustomerOrders'], nextProps)
      return false;
    }

    if(selectedRoute !== this.props.selectedRoute) return false;
    return true;
  }

  render() {
    window.NAV = this.props.navigator;
   let {navigator, celebrity, customer} = this.props;

   let isCustomer = customer && customer._id; //for celebrities that are also customers
   let payoutInfoCompleted = !!(celebrity.bankPayoutInfo || celebrity.checkPayoutInfo || celebrity.paypalPayoutInfo);

   return (
     <ScrollView style={[formStyles.giftedForm, {width: width - 49, paddingHorizontal: 20, alignSelf: 'stretch', backgroundColor: 'transparent'}]}>
       <Text style={{fontFamily: 'Montserrat-Light', fontSize: 24, color: 'white', marginTop: 25, marginBottom: 10}}>
         {celebrity.first_name ? 'Welcome, '+celebrity.first_name : 'Welcome'}
       </Text>

       <View style={{height: 25}} />

       <AccountItem showDisclosure={true} completed={!!celebrity.first_name}                         underline={true} text='Basic Info' overline={true}  onPress={() => navigator.pushWithProps('CelebrityBasicInfo')} />
       <AccountItem showDisclosure={true} completed={!!celebrity.price}                              underline={true} text='Inventory' onPress={() => navigator.pushWithProps('CelebrityInventory')} />
       <AccountItem showDisclosure={true} completed={!!this.isMediaComplete()}                       underline={true} text='Profile Media' onPress={() => navigator.pushWithProps('ProfileMedia')} />
       <AccountItem showDisclosure={true} completed={!!celebrity.bio}                                underline={true} text='Bio' onPress={() => navigator.pushWithProps('Bio')} />
       <AccountItem showDisclosure={true} completed={typeof celebrity.notifications !== 'undefined'} underline={true} text='Notifications' onPress={() => navigator.pushWithProps('Notifications')} />
       <AccountItem showDisclosure={true} completed={payoutInfoCompleted}                            underline={true} text='Payout Preferences' onPress={() => navigator.pushWithProps('PayoutPreferences')} />

       <View style={{height: 40}} />


       <AccountItem textStyle={{marginLeft: 4}} underline={true} text='FAQ' onPress={() => navigator.pushWithProps('Faq')} />
       <AccountItem textStyle={{marginLeft: 4}} underline={true} text='Support' onPress={() => this.emailSupport()} />
       {!!isCustomer && <AccountItem underline={true} text='My Orders' textStyle={{marginLeft: 4}} onPress={() => navigator.pushWithProps('CelebrityCustomerOrders', {offset: 49})} />}
       {false && !!isCustomer && <AccountItem text='Payment Info' textStyle={{marginLeft: 4}} onPress={() => navigator.pushWithProps('CelebrityPaymentInfo')} />}

       <View style={{height: 40}} />


       <View style={{flexDirection: 'row', alignSelf: 'center'}}>
         {!celebrity.twitter_avatar  &&
           <TouchableOpacity onPress={() => navigator.pushWithProps('CelebrityChangePassword')}>
             <Text style={{fontFamily: 'ProximaNova-Thin', color: 'white', fontSize: 16}}>Change Password</Text>
           </TouchableOpacity>
         }

         {!celebrity.twitter_avatar && <View style={{backgroundColor: 'rgb(143, 143, 143)', borderRadius: 100, height: 5, width: 5, margin: 6}}></View>}

         <TouchableOpacity onPress={() => this.props.logout()}>
           <Text style={{fontFamily: 'ProximaNova-Thin', color: 'white', fontSize: 16}}>Log Out</Text>
         </TouchableOpacity>
       </View>

       <TouchableOpacity onPress={() => this.props.toggleTour(true, 'again')}>
         <Text style={{marginTop: 10, fontFamily: 'ProximaNova-Thin', color: 'white', fontSize: 16, textAlign: 'center'}}>Take Tour Again</Text>
       </TouchableOpacity>

     </ScrollView>
   );
  }

  renderLogin() {
    return (
      <View style={{flex: 1}}>
        <LoginSignup {...this.props} style={{backgroundColor: 'transparent', marginLeft: -8, height, flex: 0, paddingTop: 30}}/>
      </View>
    );
  }
}

export default connect(({celebrity, customer, showOrders, selectedRoute}) => ({celebrity, customer, showOrders, selectedRoute}), actions)(CelebrityAccount)
