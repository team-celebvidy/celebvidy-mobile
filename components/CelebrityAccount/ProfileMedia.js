import {View, Text, Image, TouchableOpacity,  StyleSheet, Dimensions, ScrollView, Modal, Alert} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons'
import Progress from 'react-native-progress';

import styles from '../../styles/FormStyles';

import ClassyActionButton from '../../widgets/ClassyActionButton';
import ButtonPair from '../../widgets/ButtonPair';
import SubmitButton from '../../widgets/GiftedSubmitButton';
import ModalProgress from '../../widgets/ModalProgress';
import Navigator from '../../widgets/Navigator';
import {DEFAULT_AVATAR, DEFAULT_BACKGROUND} from '../../misc/constants';

var {width, height} = Dimensions.get('window');

import {ImagePickerManager} from 'NativeModules';
import {RNS3} from 'react-native-aws3';

import {connect} from 'react-redux';
import * as actions from '../../actions';

import {avatarSource, backgroundSource} from '../../misc/sourceUri';
import {youtubeUpload} from '../../misc/youtubeProfile';
import PlayButton from '../RecordFlow/player_widgets/PlayButton';

import {dispatch} from '../../configureStore';
import {getDevice} from '../../components/App';




class ProfileMedia extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      uploadedVideo: false,
      imagesLoaded: 0,
      uploadPercent: 0,
      videoUploadPercent: 0,
    }
  }

  onLoad(event) {
    this.setState({imagesLoaded: this.state.imagesLoaded+1})
  }


  saveWizard() {
    return new Promise((resolve, reject) => {
      let {youtube_id, avatar_photo, background_photo, twitter_avatar, twitter_background} = this.props;

      let hasAvatar = avatar_photo || twitter_avatar;
      let hasBackground = background_photo || twitter_background;

      if(youtube_id && hasAvatar && hasBackground) {
        resolve();
      }
      else if(hasAvatar && hasBackground && !youtube_id) {
        let message = `Your profile is looking sexy, but you will need to also upload a Video to appear in the directory. You can do it at any time.`;

        Alert.alert('Heads Up', message, [
            {text: 'COOL', onPress: () => resolve()},
          ]
        );
      }
      else {
        alert('Upload at least your avatar and background photo to contine you. You can always tap Finish and everything you have done thus far will be saved.');
        reject(new Error('Invalid form submission'));
      }
    });
  }

  selectPhoto(type) {
    var options = {
      title: 'Select '+type, // specify null or empty string to remove the title
      cancelButtonTitle: 'Cancel',
      takePhotoButtonTitle: 'Take Photo', // specify null or empty string to remove this button
      chooseFromLibraryButtonTitle: 'Choose from Library', // specify null or empty string to remove this button
      cameraType: 'front', // 'front' or 'back'
      mediaType: 'photo', // 'photo' or 'video'
      quality: 0.5, // 0 to 1, photos only
      allowsEditing: false, // Built in functionality to resize/reposition the image after selection
      storageOptions: true,
      noData: true,
    };

    ImagePickerManager.showImagePicker(options, (res) => {
      if(res.didCancel) return;
      else if (res.error) alert('Something went wrong with your photo selection')
      else {
        this.modalProgress.show();
        this.uploadToS3(res.uri.replace('file://', ''), type.toLowerCase(), this.props);
      }
    });
  }
  uploadToS3(uri, type, {_id, display_name}) {
    let name =  `${new Date().toString().replace(/ /g, '-')}.jpg`;

    let file = {
      uri,
      name,
      type: "image/jpg"
    }

    let slug = display_name ? display_name.replace(/ /g, '-').toLowerCase() + '_' + _id : 'id_'+_id;

    let options = {
      keyPrefix: `celebrities/${slug}/${type}/`,
      bucket: "celebvidy-content",
      region: "us-east-1",
      accessKey: "AKIAIF4BP6KWDXIDXA6A",
      secretKey: "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",
      successActionStatus: 201
    }

    return RNS3.put(file, options)
      .then(response => {
        if (response.status !== 201) {
          throw new Error("Failed to upload image to S3");
        }

        //response.body.postResponse.location:
        //https://celebvidy-content.s3.amazonaws.com/celebrities/${slug}/${type}/${name}.jpg

        this.setState({[type]: uri}); //display local photo so it can be displayed without downloading
        setTimeout(() => {
          this.props.updateCelebrity({[type+'_photo']: `${slug}/${type}/${name}`});
        }, 150); //there seemed to be an issue where the setState immediatelly followed by a dispatch canceled the setState
      })
      .catch(() => alert('Something went wrong with your photo upload'))
      .progress((e) => {
        this.setState({uploadPercent: parseInt(e.loaded/e.total*100)});
      });
  }



  width() {
    return width - (this.props.isTour ? 0 : 54);
  }

  render() {
    let {avatar, background} = this.state;

    return (
      <ScrollView
        style={{flex: 0, width: this.width(), height: height - 64}}
        showsVerticalScrollIndicator={false}
      >
        <ModalProgress
          text='uploading'
          ref={(modal) => this.modalProgress = modal}
          animateLogo={true}
          percent={this.state.uploadPercent}
          onComplete={() => this.setState({uploadPercent: 0})}
        />

        <View style={{flexDirection: 'row', marginTop: -64}}>
          <Image
            source={background ? {uri: background} : backgroundSource(this.props)}
            style={ownStyles.background}
            onLoad={this.onLoad.bind(this)}
            defaultSource={DEFAULT_BACKGROUND}
            onLoad={this.onLoad.bind(this)}
          />

          <View style={ownStyles.avatarContainer}>
            {this.state.imagesLoaded < 2 && <Progress.Pie
                indeterminate={true}
                color={'rgba(19, 84, 252, 1)'}
                borderWidth={0}
                unfilledColor={'rgba(0,0,0,0)'}
                size={210}
                style={{position: 'absolute', left: this.width()/2 - 210/2, top: 55}}
              />
            }


            <Image
              source={avatar ? {uri: avatar} : avatarSource(this.props)}
              style={ownStyles.avatar}
              defaultSource={DEFAULT_AVATAR}
              onLoad={this.onLoad.bind(this)}
            />

          <Text style={ownStyles.celebName}>{this.props.display_name}</Text>
            <Text style={ownStyles.celebTagline}>{this.props.tag_line}</Text>
            <View style={{flexDirection: 'row', alignItems: 'center', flex: 1, alignSelf: 'center'}}>
              {this.props.verified && <Icon name='checkmark-circled' size={17} color="rgba(233, 233, 233, 1)" />}
              {this.props.verified && <Text style={styles.verified}>Verified</Text>}
            </View>
          </View>
        </View>

        <ButtonPair
          style={{paddingHorizontal: 2, marginTop: 5, marginBottom: 5}}

          button1text='AVATAR'
          button1icon='ios-upload'
          button1checked={this.state.avatar || this.props.avatar_photo}
          button1onPress={() => this.selectPhoto('Avatar') }

          button2text='BACKGROUND'
          button2icon='ios-upload'
          button2checked={this.state.background || this.props.background_photo}
          button2onPress={() => this.selectPhoto('Background') }
         />

        <View style={{paddingHorizontal: 2}}>
          <ClassyActionButton text='UPLOAD WHO AM I VIDEO'
            icon={this.props.youtube_id ? 'checkmark' : 'social-youtube'}
            iconStyle={{
              fontSize: 16,
              marginRight: 8,
              marginTop: 2,
              color: 'rgb(182, 182, 182)',
            }}
            onPress={() => this.setState({recorderVisible: true})}
          />


        {!!this.props.youtube_id &&
            <View style={{flex: 1, flexDirection: 'row', top: -15}}>
              <Image
                source={avatar ? {uri: avatar} : avatarSource(this.props)}
                style={{height: height - 457, flex: 1,  resizeMode: 'cover', marginTop: 20, opacity: .8, borderWidth: 2*StyleSheet.hairlineWidth, borderColor: 'rgba(25,25,100, .4)'}}
              />

              <Text style={{position: 'absolute', top: 28, left: 12, color: 'rgb(225, 225, 225)', fontFamily: 'Proxima Nova', fontSize: 16, width: width - 100}}>
                {this.props.youtube_id ? `WHO AM I: ${this.props.display_name}` : 'HOW CELEBVIDY WORKS'}
              </Text>
              <Text style={{position: 'absolute', top: 28, right: 12, color: 'rgb(200, 200, 200)', fontFamily: 'Proxima Nova', fontSize: 16}}>
                {this.props.duration && this.displayDuration(this.props.duration)}
              </Text>

              <PlayButton
                opacity={1}
                togglePlayback={() => this.setState({playerVisible: true})}
                paused={true}
                videoPlayedAtLeastOnce={true}
                isYoutube={false}
                style={{position: 'absolute', top: (height - 457)/2 - 25, left: this.width()/2 - 45}}
              />
            </View>
          }
        </View>


        <Modal animated={true} visible={!!this.state.recorderVisible}>
          {this.state.recorderVisible &&
            <Navigator
              initialComponent={'Record'}
              onSubmitVideo={this.onSubmitVideo.bind(this)}
              uploadText={'Upload'}
              recipient={this.props.display_name}
              isProfileMedia={true}
              kind={'WHO AM I'}
              script={'This is where short scripts written by customers will go. For now record your introduction video to fans. Some ideas of what to say: state your name, what you are known from, what to expect in your celebvidy, provide ideas for what customers can request, inspire them.'}
              onPressBack={() => this.setState({recorderVisible: false}) }
              renderNavigationBar={() => null}
            />
          }

          <ModalProgress
            text='uploading'
            ref={(modal) => this.videoModalProgress = modal}
            animateLogo={true}
            percent={Math.max(0, this.state.videoUploadPercent - 2)}
            onComplete={() => this.setState({videoUploadPercent: 0, recorderVisible: false})}
          />
        </Modal>

        <Modal animated={true} visible={!!this.state.playerVisible}>
          {this.state.playerVisible && <Navigator
            initialComponent={'Celebvidy'}
            sourceUri={this.state.sourceVideoUri}
            youtube_id={this.state.youtube_id || this.props.youtube_id}
            onPressPlayerBack={() => this.setState({playerVisible: false}) }
            renderNavigationBar={() => null}
          />}
        </Modal>
      </ScrollView>
    );
  }

  onSubmitVideo(sourceUri, duration, onComplete) {
    this.videoModalProgress.show();

    if(getDevice() === 'simulator') {
      dispatch(actions.updateCelebrity({_id: this.props._id, youtube_id: 'ofnSojq-vqI', duration: 57 * 1000}));
      this.onUploadProgress(102); //102 percent is interpreted as complete
      this.setState({youtube_id: 'ofnSojq-vqI'});
    }
    else {
      youtubeUpload(sourceUri, duration, this.props, this.onUploadProgress.bind(this), () => {
        onComplete();
      })
        .then((sourceVideoUri) => this.setState({sourceVideoUri}));
    }
  }

  onUploadProgress(videoUploadPercent) {
    videoUploadPercent = parseInt(videoUploadPercent); //ignore decimal percents

    if(this.state.videoUploadPercent !== videoUploadPercent) { //only setState if next integer reached
      this.setState({videoUploadPercent});
    }

    return true; //need to return true for promise chain to continue, see youtube upload code
  }

  displayDuration(ms) {
    let seconds = ms/1000;
    seconds = Number(seconds);

    let h = Math.floor(seconds / 3600); //our player likely doesnt have space to show hours--luckily we wont have videos that long
    let m = Math.floor(seconds % 3600 / 60);
    let s = Math.floor(seconds % 3600 % 60);

    return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : (m < 10 ? "0" : "")) + m + ":" + (s < 10 ? "0" : "") + s);
  }
}


export default connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(ProfileMedia)



const ownStyles = StyleSheet.create({
  background: {
    height: 350,
    flex: 1,
    resizeMode: 'cover',
    opacity: .9
  },
  avatarContainer: {
    position: 'absolute',
    top: 0, left: 0, right: 0, bottom: 0,
    backgroundColor: 'transparent'
  },
  avatar: {
    height: 120,
    width: 120,
    borderRadius: 60,
    alignSelf: 'center',
    marginTop: 110
  },
  celebName: {
    fontSize: 26,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
  celebTagline: {
    fontSize: 20,
    fontFamily: 'STHeitiSC-Light',
    textAlign: 'center',
    lineHeight: 26,
    color: 'white',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
    marginBottom: 6,
  },
  verified: {
    fontSize: 16,
    color:  "white",
    fontFamily: 'ProximaNova-Thin',
    fontWeight: '500',
    marginLeft: 6,
    color: 'rgba(233, 233, 233, 1)',
  },
});
