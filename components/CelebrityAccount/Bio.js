import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, Navigator, Alert} from 'react-native';

import React, { Component } from 'react';

import _ from 'underscore';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';
import Overlay from 'react-native-overlay';

import styles from '../../styles/FormStyles';

import TextInput from '../../widgets/GiftedTextInput';
import TextArea from '../../widgets/GiftedTextArea';
import {BackButton} from '../../widgets/NavButton';
import ClassyActionButton from '../../widgets/ClassyActionButton';
import SubmitButton from '../../widgets/GiftedSubmitButton';
import GiftedModalWidget from '../../widgets/GiftedModalWidget';

var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';


class Bio extends Component {
  constructor(props, text) {
    super(props, text);

    this.state = {
      bio: this.props.bio,
      instructions: this.props.instructions,
    }
  }

  saveWizard() {
    let values = GiftedFormManager.getValues('bio');
    let isValid = GiftedFormManager.validate('bio', true).isValid;

    return this.save(isValid, values);
  }
  save(isValid, values, postSubmit=null) {
    return new Promise((resolve, reject) => {
      if(!isValid) {
        if(!values.bio) this.setState({bio: ''}); //trigger X marks to show if nothing was entered yet (no icon shows when undefined)
        if(!values.instructions) this.setState({instructions: ''});

        if(this.props.isCelebrityAccount) {
          alert('Please make sure both your biography and instructions are filled.');
          return;
        }
        else {
          let message = `You're almost there, but keep in mind you will need to provide your Bio and Instructions to appear in the directory. You can do it at any time.`;

          Alert.alert('Heads Up', message, [
              {text: 'COOL', onPress: () => resolve()},
            ]
          );

          return;
        }
      }

      let updateFunc = this.props.isCelebrityAccount ? 'updateCelebrity' : 'storeCelebrity';

      return this.props[updateFunc](values)
        .then(() => {
          if(this.props.isCelebrityAccount) {
            postSubmit && postSubmit();
            this.props.navigator.pop();
          }
          else {
            resolve();
          }
        })
        .catch((error) => {
          if(this.props.isCelebrityAccount) {
            postSubmit && postSubmit();
          }
          else {
            reject(error.message);
          }

          alert(error.message); //rarely will be called because of gifted-form validation in red, but would be a human-readable message thrown from the server (in the isCelebrityAccount case -- the signup wizard just stores it client side at this step)
        });
    });
  }

  saveInstructions() {
    let instructions = GiftedFormManager.getValue('bio', 'instructions');

    if(!instructions) return alert('Your instructions cannot be empty');
    this.props.navigator.pop(); //no loading indicator, just pop. they can get an alert on the main page in the rare case that something bad happens.

    this.props.storeCelebrity({instructions}).catch((error) => alert(error.message));
  }

  shouldShowBioError() {
    if(typeof this.state.bio === 'undefined') return false;
    else return !this.state.bio;
  }
  shouldShowInstructionsError() {
    if(typeof this.state.instructions === 'undefined') return false;
    else return !this.state.instructions;
  }

  render() {
    return (
      <GiftedForm
        formName='bio'
        style={styles.giftedForm}
        openModal={(route) => {
          this.props.navigator.push(route);
        }}
        validators={{
          bio: {
            title: 'Bio',
            validate: [{
              validator: 'isLength',
              arguments: [1, 2000],
              message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
            }]
          },
          instructions: {
            title: 'Instructions',
            validate: [{
              validator: 'isLength',
              arguments: [1, 180],
              message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
            }]
          },
        }}
      >
        <GiftedForm.SeparatorWidget/>

        <View style={{flexDirection: 'row'}}>
          {this.shouldShowBioError()
            ? <Icon name='close-round' style={[styles.checkmark, {color: 'red'}]} />
            : typeof this.state.bio !== 'undefined' && <Icon name='checkmark' style={styles.checkmark} />
          }
          <Text style={styles.fieldTitle}>Biography</Text>
        </View>

        <GiftedForm.SeparatorWidget/>
        <TextArea
          name='bio'
          ref={(bio) => this.bio = bio}
          autoFocus={false}
          placeholder='Share something interesting about yourself...'
          placeholderTextColor="rgba(100,100,100,1)"
          widgetStyles={styles.largeTextArea}
          onChangeText={(bio) => this.setState({bio})}
          keyboardAppearance='dark'
          //autoCorrect={false}
          scrollEnabled={false}
          value={this.props.bio}
          maxCharacters={2000}
        />

        <GiftedForm.SeparatorWidget/>
        <GiftedForm.SeparatorWidget/>
        <GiftedForm.SeparatorWidget/>

        <View style={{flexDirection: 'row'}}>
          {this.shouldShowInstructionsError()
            ? <Icon name='close-round' style={[styles.checkmark, {color: 'red'}]} />
            : typeof this.state.instructions !== 'undefined' && <Icon name='checkmark' style={styles.checkmark} />
          }
          <Text style={styles.fieldTitle}>Request DO's & DONT's</Text>
        </View>

        <GiftedForm.SeparatorWidget/>
        <GiftedModalWidget
          title={this.props.instructions ? 'instructions:' : 'Instructions'}
          longTitle={true}
          displayValue='instructions'
          name='instructions'
          editable={this.props.editable}
          scrollEnabled={true}
          widgetStyles={styles.modalWidget}
          disclosure={this.props.editable}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          underlined={true}
          noIcon={true}
          value={this.props.instructions}
        >
          <View style={{height: 84}} />

          <Overlay isVisible={this.props.isTour}>
            <View style={{height: 42, width: 80, backgroundColor: 'black', position: 'absolute', top: 0, left: 0, marginTop: 19}}>
              <BackButton {...this.props} />
            </View>
          </Overlay>


          <Text style={[styles.fieldTitle, {marginBottom: 15}]}>Please provide any special instructions or limitations customers should be aware about before ordering a celebvidy. EXAMPLE:</Text>
          <Text style={[styles.fieldTitle, {marginBottom: 30, color: 'rgb(200,200,200)'}]}>"Family-friendly requests only. Please tell me something unique about the recipient. Where does the recipient know me from? Have fun writing your script."</Text>

          <TextArea
            name='instructions'
            formName='bio'
            ref='instructions'
            autoFocus={false}
            placeholder="Your instructions..."
            placeholderTextColor="rgba(100,100,100,1)"
            widgetStyles={styles.smallTextArea}
            keyboardAppearance='dark'
            //autoCorrect={false}
            selectTextOnFocus={true}
            scrollEnabled={false}
            value={this.props.instructions}
            onChangeText={(instructions) => this.setState({instructions})}
            maxCharacters={180}
          />

          <View style={{height: 25}} />
          <ClassyActionButton text='SAVE' onPress={() => this.saveInstructions()}/>

        </GiftedModalWidget>


        <SubmitButton
          {...this.props}
          formName='bio'
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            this.save(isValid, values, postSubmit);
          }}
        />

      </GiftedForm>
    );
  }
}


export default connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(Bio)
