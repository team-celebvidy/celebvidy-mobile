import {View, Text, TouchableHighlight, TouchableOpacity,  StyleSheet, Dimensions, PixelRatio, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import ExNavigator from 'react-navigation';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';
import TouchableBounce from 'react-native-touchable-bounce';

import styles from '../../styles/FormStyles';

import SliderWidget from '../../widgets/GiftedSliderWidget';
import SubmitButton from '../../widgets/GiftedSubmitButton';

import valueToPrice, {priceToValue} from '../../misc/valueToPrice';
import {setImmediateAnimationFrame} from '../../misc/helpers';

var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';


class CelebrityInventory extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      inventoryDelta: 0,
      priceValue: 1000,
    }
  }

  getInventory() {
    let inventory = this.props.inventory || 0;
    inventory += this.state.inventoryDelta;
    return Math.max(inventory, 0); //don't let current orders or pressing the - button too many times take you below 0
  }
  incrementInventory() {
    this.setState( (state) => ({inventoryDelta: state.inventoryDelta + 1}) )
  }
  decrementInventory() {
    if(this.getInventory() <= 0) return; //don't let actual inventory go below 0. even though it won't show negative numbers, it would require more than one press to start going positive again
    this.setState( (state) => ({inventoryDelta: state.inventoryDelta - 1}) )
  }
  componentWillReceiveProps({inventory}) {
    if(inventory !== this.props.inventory) {
      this.setState({inventoryDelta: 0}); //reset inventory delta since props.inventory already reflects complete value :)
    }
  }

  save(values, postSubmit) {
    values.inventory = this.getInventory();
    values.price = valueToPrice(values.price, false);

    return this.props.updateCelebrity(values)
      .then(() => {
        postSubmit();
        this.props.navigator.pop();
      })
      .catch((error) => {
        postSubmit && postSubmit();
        alert(error.message); //rarely will be called because of gifted-form validation in red, but would be a human-readable message thrown from the server
      });
  }

  render() {
    return (
      <GiftedForm
        formName='inventory'
        style={styles.giftedForm}
      >
        <GiftedForm.SeparatorWidget />

        <SliderWidget
          ref={(slider) => this.priceSlider = slider}
          name='price'
          title='Price'
          minimumValue={1}
          maximumValue={100}
          step={1}
          value={priceToValue(this.props.price, 50)}
          onSliderValueChange={(priceValue) => {

          }}
          widgetStyles={styles.widget}
          style={{width: 180, marginRight: -10}}
          maximumTrackTintColor= "rgba(63,84,134,1)"
          minimumTrackTintColor= "#2038D8"
          formatValue={(value) => {
            return valueToPrice(value);
          }}
        />
        <Text style={styles.fieldSubtext}>What would you like to charge per celebvidy?</Text>

        <SliderWidget
          name='charity_percent'
          title='Charity'
          minimumValue={0}
          maximumValue={100}
          step={1}
          value={typeof this.props.charity_percent !== 'undefined' ? this.props.charity_percent :  10}
          widgetStyles={styles.widget}
          style={{width: 180, marginRight: -10}}
          maximumTrackTintColor="rgba(63,84,134,1)"
          minimumTrackTintColor= "#2038D8"
          formatValue={(percent) => '%'+percent}
        />
        <Text style={styles.fieldSubtext}>What percentage would you like to donate to charity? (optional)</Text>

        <GiftedForm.SeparatorWidget/>
        <GiftedForm.SeparatorWidget/>


        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginRight: 2}}>
          <View style={{flexDirection: 'column'}}>
            <Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 18, color: 'white', marginTop: 5}}>Celebvidy Inventory:</Text>
            <Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 13, color: 'white', marginTop: 5, marginLeft: 22}}>current inventory: {this.getInventory()}</Text>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableBounce underlayColor={'navy'} activeOpacity={.25} style={{borderRadius: 2,}} onPress={() => this.decrementInventory() }>
              <View style={{backgroundColor: '#1f66e3', width: 50, height: 50, borderWidth: 1/PixelRatio.get(), borderColor: '#4686f6', paddingTop: 5, paddingLeft: 12,}}>
                <Icon name="android-remove" style={{fontSize: 40, color: 'white'}} />
              </View>
            </TouchableBounce>

            <TouchableBounce  underlayColor={'navy'} activeOpacity={.25} style={{marginLeft: 10, borderRadius: 2,}} onPress={() => this.incrementInventory() }>
              <View style={{backgroundColor: '#1517d5', width: 50, height: 50, borderWidth: 1/PixelRatio.get(), borderColor: '#2d2fdf', paddingTop: 5, paddingLeft: 12}}>
                <Icon name="android-add" style={{fontSize: 40, color: 'white'}} />
              </View>
            </TouchableBounce>
          </View>
        </View>

        <Text style={[styles.fieldSubtext, {marginTop: 15, marginBottom: 5}]}>Anytime you can adjust the inventory you'd like to display as available to influence demand.</Text>

        <SliderWidget
          name='monthly_reset_inventory'
          title='Inventory Reset'
          minimumValue={0}
          maximumValue={500}
          step={1}
          value={typeof this.props.monthly_reset_inventory !== 'undefined' ? this.props.monthly_reset_inventory : 100}
          widgetStyles={styles.widget}
          style={{width: 120, marginRight: -10}}
          maximumTrackTintColor= "rgba(63,84,134,1)"
          minimumTrackTintColor= "#2038D8"
        />

      <Text style={styles.fieldSubtext}>Celebvidy can automatically re-up your inventory at the beginning of each month for you. How many do you want reset each month?</Text>

          <SubmitButton
            {...this.props}
            formName='inventory'
            onSubmit={(isValid, values, validationResults, postSubmit) => {
              this.save(values, postSubmit);
            }}
          />

      </GiftedForm>
    );
  }
}


export default connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(CelebrityInventory)
