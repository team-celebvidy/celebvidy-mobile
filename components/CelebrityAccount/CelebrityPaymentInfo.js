import {View} from 'react-native';
import PaymentInfo from '../CustomerAccount/PaymentInfo';

export default class CelebrityPaymentInfo extends Component {
  render() {
    return (
      <PaymentInfo {...this.props} />
    );
  }
}
