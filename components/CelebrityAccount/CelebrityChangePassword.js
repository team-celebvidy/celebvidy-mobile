import {View} from 'react-native';
import React, { Component } from 'react';
import ChangePassword from '../ChangePassword';


export default class CelebrityChangePassword extends Component {
  render() {
    return (
      <ChangePassword {...this.props} />
    );
  }
}
