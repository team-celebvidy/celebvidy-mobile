import {ScrollView, View, Text} from 'react-native';
import React, { Component } from 'react';


export default class CelebrityTerms extends Component {
  render() {
    return (
      <ScrollView style={{flex: 1, padding: 15, }}>
        <Text style={{color: 'rgb(230,230,230)', fontSize: 12, fontFamily: 'STHeitiSC-Light'}}>
          This Celebrity User Agreement, and the Terms and Conditions of Use
        Agreement (the Terms and Conditions) and Privacy Policy (each of
        which is incorporated herein by reference), governs your use of the
        Application or Services as a Celebrity User. You accept this Agreement
        by creating a Celebrity User account on or through the Application.
        CELEBVIDY hereby grants you a limited, terminable,
          non-exclusive right to access and use the Application to: (i) offer
          Videos for sale to Subscribers of the Application at the prices you
          establish within CELEBVIDY’s guidelines and parameters, (ii) TO
          RECORD VIDEOS CONTAINING EXACTLY THE PERSONAL MESSAGE CONTENT
          REQUESTED BY THE SUBSCRIBERS AND ONLY CONTAINING YOU UNLESS THE
          ORDER SPECIFIES ADDITIONAL PERSONS TO BE INCLUDED IN THE VIDEO, THE
          CELEBRITY USER and (iii) to access, view and change your Celebrity
          User account information and view other transaction information
          regarding your account as provided from time to time by
          CELEBVIDY. YOU ACKNOWLEDGE AND AGREE THAT CELEBVIDY WILL
          AUTOMATICALLY ADD 25 VIDEOS TO YOUR VIDEO INVENTORY FOR SALE ON THE
          APPLICATION EACH MONTH DURING THE TERM; PROVIDED, HOWEVER, THAT YOU
          CAN INCREASE THAT VIDEO INVENTORY THROUGH YOUR USER ACCOUNT. This
          authorizes you to view, download and upload material on the
          Application solely for your use directly related to offering Videos
          for sale or recording and posting purchased Videos on the
          Application. DUE TO THE FACT THAT THE SUBSCRIBERS HAVE PURCHASED
          PERSONALIZED VIDEOS CONTAINING THEIR REQUESTED PERSONAL MESSAGE
          CONTENT, YOU MAY ONLY USE THE VIDEOS RECORDED AT THE REQUEST OF
          SUBSCRIBERS THROUGH THE APPLICATION AND MAY NOT OTHERWISE USE,
          POST, DISTRIBUTE, DISPLAY, SELL OR LICENSE SUCH VIDEOS. You may not
          sell, transfer or assign any of the Services or your rights under
          this Celebrity User Agreement to any third party without our
          express written authorization. You agree that you are solely
          responsible for the content (other than the Personal Message
          Content) of any Video you post to the Application and any damages
          or other consequences arising from such posting.

          This Celebrity User Agreement, and the Terms and Conditions of Use
        Agreement (the Terms and Conditions) and Privacy Policy (each of
        which is incorporated herein by reference), governs your use of the
        Application or Services as a Celebrity User. You accept this Agreement
        by creating a Celebrity User account on or through the Application.
        CELEBVIDY hereby grants you a limited, terminable,
          non-exclusive right to access and use the Application to: (i) offer
          Videos for sale to Subscribers of the Application at the prices you
          establish within CELEBVIDY’s guidelines and parameters, (ii) TO
          RECORD VIDEOS CONTAINING EXACTLY THE PERSONAL MESSAGE CONTENT
          REQUESTED BY THE SUBSCRIBERS AND ONLY CONTAINING YOU UNLESS THE
          ORDER SPECIFIES ADDITIONAL PERSONS TO BE INCLUDED IN THE VIDEO, THE
          CELEBRITY USER and (iii) to access, view and change your Celebrity
          User account information and view other transaction information
          regarding your account as provided from time to time by
          CELEBVIDY. YOU ACKNOWLEDGE AND AGREE THAT CELEBVIDY WILL
          AUTOMATICALLY ADD 25 VIDEOS TO YOUR VIDEO INVENTORY FOR SALE ON THE
          APPLICATION EACH MONTH DURING THE TERM; PROVIDED, HOWEVER, THAT YOU
          CAN INCREASE THAT VIDEO INVENTORY THROUGH YOUR USER ACCOUNT. This
          authorizes you to view, download and upload material on the
          Application solely for your use directly related to offering Videos
          for sale or recording and posting purchased Videos on the
          Application. DUE TO THE FACT THAT THE SUBSCRIBERS HAVE PURCHASED
          PERSONALIZED VIDEOS CONTAINING THEIR REQUESTED PERSONAL MESSAGE
          CONTENT, YOU MAY ONLY USE THE VIDEOS RECORDED AT THE REQUEST OF
          SUBSCRIBERS THROUGH THE APPLICATION AND MAY NOT OTHERWISE USE,
          POST, DISTRIBUTE, DISPLAY, SELL OR LICENSE SUCH VIDEOS. You may not
          sell, transfer or assign any of the Services or your rights under
          this Celebrity User Agreement to any third party without our
          express written authorization. You agree that you are solely
          responsible for the content (other than the Personal Message
          Content) of any Video you post to the Application and any damages
          or other consequences arising from such posting.
        </Text>
      </ScrollView>
    );
  }
}
