import {View, Text, TouchableHighlight, TouchableOpacity,  StyleSheet, Dimensions, PixelRatio, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import ExNavigator from 'react-navigation';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import dismissKeyboard from 'react-native-dismiss-keyboard';

import styles from '../../styles/FormStyles';

import SubmitButton from '../../widgets/GiftedSubmitButton';
import TextInput from '../../widgets/GiftedTextInput';
import NumberInput from '../../widgets/GiftedNumberInput';
import Select from '../../widgets/GiftedSelect';


var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';


class CheckPayoutPreferences extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {

    };
  }

  render() {
    let {isCompany} = this.props;

    return (
      <GiftedForm
        formName='checkPayoutPreferences'
        style={[styles.giftedForm, {height: height - 192, flex: 0}]}
        scrollEnabled={true}
        scrollOnTap={true}
        validators={this.getValidators(isCompany)}
      >

        <TextInput
          name={isCompany ? 'company' : 'fullName'}
          title={isCompany ? 'Company' : 'Full Name'}
          value={isCompany ? this.props.company : this.props.fullName}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.name = input}
          onSubmitEditing={(value) => this.tin.focus()}
        />

        <NumberInput
          name='tin'
          title={isCompany ? 'TIN' : 'SSN'}
          value={this.props.tin}
          returnKeyType='next'
          placeholder={isCompany ? '##-#######' : '###-##-####'}
          maxLength={9}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          format={isCompany ? 'ein' : 'ssn'}
          ref={(input) => this.tin = input}
          onSubmitEditing={(value) => this.address_line1.focus()}
        />

        <TextInput
          name='address_line1'
          title='Address'
          value={this.props.address_line1}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.address_line1 = input}
          onSubmitEditing={(value) => this.address_line2.focus()}
        />

        <TextInput
          name='address_line2'
          title='Address 2'
          value={this.props.address_line2}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.address_line2 = input}
          onSubmitEditing={(value) => this.city.focus()}
        />

        <TextInput
          name='address_city'
          title='City'
          value={this.props.address_city}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.city = input}
          onSubmitEditing={(value) => this.state.focus()}
        />

        <TextInput
          name='address_state'
          title='State'
          value={this.props.address_state}
          maxLength={2}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          placeholder='CA'
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='characters'
          ref={(input) => this.state = input}
          onSubmitEditing={(value) => this.zip.focus(true)}
        />

        <NumberInput
          name='address_zip'
          title='Zip'
          value={this.props.address_zip}
          returnKeyType='next'
          placeholder='90210'
          maxLength={5}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.zip = input}
          onSubmitEditing={(value) => this.email.focus()}
        />


        <TextInput
          name='email'
          value={this.props.email}
          title='Email'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='none'
          ref={(input) => this.email = input}
          returnKeyType={this.props.isCelebrityAccount && 'done'}
          onSubmitEditing={() => this.props.isCelebrityAccount && this.refs.submit.submit()}
        />

        <GiftedForm.SeparatorWidget />
        <GiftedForm.SeparatorWidget />

        {this.props.terms}

        <SubmitButton
          {...this.props}
          ref='submit'
          formName='checkPayoutPreferences'
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            this.props.save(isValid, values, postSubmit);
          }}
        />

      </GiftedForm>
    );
  }

  getValidators(isCompany) {
    return {
      [isCompany ? 'company' : 'fullName']: {
        title: isCompany ? 'Company' : 'Full Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 120],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }, {
          validator: (name) => {
            if(isCompany && name && name.length > 1) return true;
            return name && name.split(' ').length >= 2 && name.split(' ')[1] !== '';
          },
          message: `Individual's names must be at least 2 words`
        }]
      },
      tin: {
        title: isCompany ? 'TIN' : 'Social Security Number',
        validate: [{
          validator: 'isLength',
          arguments: [9, 9],
          message: '{TITLE} must be 9 numbers'
        }]
      },
      address_line1: {
        title: 'Address',
        validate: [{
          validator: 'isLength',
          arguments: [1, 200],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      address_line2: {
        title: 'Address 2',
        validate: [{
          validator: 'isLength',
          arguments: [1, 200],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      address_city: {
        title: 'City',
        validate: [{
          validator: 'isLength',
          arguments: [1, 200],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      address_state: {
        title: 'State',
        validate: [{
          validator: 'isLength',
          arguments: [2, 2],
          message: '{TITLE} must be 2 letters'
        }]
      },
      address_zip: {
        title: 'Zip Code',
        validate: [{
          validator: 'isLength',
          arguments: [5, 5],
          message: '{TITLE} must be 5 numbers'
        }]
      },
      email: {
        title: 'Email',
        validate: [{
          validator: 'isLength',
          arguments: [6, 255],
          message: 'Invalid Email Address',
        }, {
          validator: 'isEmail',
          message: 'Invalid Email Address',
        }]
      }
    };
  }
}

export default connect(
  ({celebrity}) => {
    let props = {
      ...celebrity.bankPayoutInfo,
      ...celebrity.paypalPayoutInfo,
      ...celebrity.checkPayoutInfo,
    };

    if(!props.email) {
      props.email = celebrity.email;
    }

    if(!props.fullName && celebrity.bankPayoutInfo) {
      props.fullName = celebrity.bankPayoutInfo.firstName && (celebrity.bankPayoutInfo.firstName + ' ' + celebrity.bankPayoutInfo.lastName);
    }

    if(!props.fullName) {
      props.fullName = celebrity.full_name;
    }

    return props;
  },
  actions
)(CheckPayoutPreferences)
