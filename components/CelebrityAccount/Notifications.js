import {View, Text, TouchableHighlight,  StyleSheet, Dimensions, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../styles/FormStyles';

import TextInput from '../../widgets/GiftedTextInput';
import SubmitButton from '../../widgets/GiftedSubmitButton';
import AccountItem from '../../widgets/AccountItem';
import TextArea from '../../widgets/GiftedTextArea';

var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';
import * as Push from '../../misc/PushNotifications';


class Notifications extends Component {
  constructor(props, context) {
    super(props, context);
  }

  saveWizard() {
    let values = GiftedFormManager.getValues('notifications');
    return this.save(values);
  }
  save(values, postSubmit=null) {
    if(values.deliveryMessage && values.deliveryMessage.length > 140) {
      alert('Your delivery message cannot be longer than 140 characters.');
      return Promise.reject(new Error('Invalid form submission'));
    }

    let updateFunc = this.props.isCelebrityAccount ? 'updateCelebrity' : 'storeCelebrity';

    return this.props[updateFunc](values)
      .then(() => {
        postSubmit && postSubmit();
        this.props.isCelebrityAccount && this.props.navigator.pop();
      })
      .catch((error) => {
        postSubmit && postSubmit();
        alert(error.message); //rarely will be called because of gifted-form validation in red, but would be a human-readable message thrown from the server
      });
  }

  componentDidMount() {
    if(!this.props.notifications) {
      Push.registerPushNotifications(() => { //registration will persist device token (and AWS SNS arn endpoint) server side
        this.props.storeCelebrity({notifications: true});
      });
    }
  }

  toggleNotifications(value) {
    if(value) {
      //register device token with SNS every time just to be sure we always have the latest token.
      //Apple likely wont show the popup and it may not matter, but here's what the code should look like lol.
      //The server (which is called by this) won't do anything if the device token hasn't changed, so it's all good to call this multiple times.
      Push.registerPushNotifications();
    }
  }

  render() {
    return (
      <GiftedForm
        formName='notifications'
        style={styles.giftedForm}
        validators={{}}
      >
        <GiftedForm.SeparatorWidget />

        <GiftedForm.SwitchWidget
            name='notifications'
            title='Push Notifications'
            onTintColor="#2038D8"
            widgetStyles={styles.widget}
            onChangeText={(value) => this.toggleNotifications(value)}
            value={!!this.props.notifications}
        />
        <GiftedForm.SwitchWidget
            name='email_notifications'
            title='Email Notifications'
            onTintColor="#2038D8"
            widgetStyles={styles.widget}
            onChangeText={(value) => this.toggleNotifications(value)}
            value={!!this.props.email_notifications}
        />

        {this.props.completed_account && <GiftedForm.SwitchWidget
            name='away'
            title='Away Mode'
            value={true}
            onTintColor='#2038D8'
            widgetStyles={styles.widget}
            value={!!this.props.away}
        />}


      <AccountItem text='' underline={true} overline={false} showDisclosure={false} style={{marginTop: 0}}  />

        <GiftedForm.SeparatorWidget/>
        <GiftedForm.SeparatorWidget/>

        <View style={{flexDirection: 'row'}}>
          {!!this.props.delivery_message && <Icon name='checkmark' style={styles.checkmark} />}
          <Text style={styles.fieldTitle}>Delivery Message (optional):</Text>
        </View>

        <Text style={[styles.fieldSubtext, {marginTop: 5, color: 'gray'}]}>Customize the message that accompanies delivered celebvidys.</Text>


        <TextArea
          name='delivery_message'
          formName='notifications'
          autoFocus={false}
          placeholder={`Hey it's ${this.props.display_name}, check out my celebvidy for you`}
          placeholderTextColor="rgba(100,100,100,1)"
          widgetStyles={styles.smallTextArea}
          onChangeText={(deliveryMessage) => this.setState({deliveryMessage})}
          keyboardAppearance='dark'
          //autoCorrect={false}
          selectTextOnFocus={true}
          scrollEnabled={false}
          value={this.props.delivery_message}
          maxCharacters={80}
          value={this.props.delivery_message || `Hey it's ${this.props.display_name}, check out my celebvidy for you`}
          onSubmitEditing={(value) => {
            if(this.props.isCelebrityAccount) {
              this.saveWizard(); //save wizard will do a normal sve, but needs the values usually passed via SubmitButton
            }
            else if(this.props.nextRoute) { //`nextRoute` should be passed from CelebrityAccountSetup wizard routes via the previous top right NEXT button that pushed this page
              this.saveWizard().then(() => {
                let {name, props} = this.props.nextRoute;
                this.props.navigator.pushWithProps(name, props);
              });
            }
          }}
        />


        <SubmitButton
          {...this.props}
          formName='basicInfo'
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            this.save(values, postSubmit);
          }}
        />

      </GiftedForm>
    );
  }
}


export default connect(
  ({celebrity}) => ({...celebrity}),
  actions, null, {withRef: true}
)(Notifications)
