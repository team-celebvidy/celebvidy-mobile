import {View, Text, TouchableHighlight, TouchableOpacity,  StyleSheet, Dimensions, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import CheckBox from 'react-native-icon-checkbox';

import SegmentedControlTabs from '../../widgets/SegmentControl';
import {SegmentedControls} from 'react-native-radio-buttons';

import styles from '../../styles/FormStyles';

import SubmitButton from '../../widgets/GiftedSubmitButton';
import TextInput from '../../widgets/GiftedTextInput';

import Bank from './BankPayoutPreferences';
import Check from './CheckPayoutPreferences';
import Paypal from './PaypalPayoutPreferences';

var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';


class PayoutPreferences extends Component {
  constructor(props, context) {
    super(props, context);
    this.inputs = [];
    this.tabs = ['bank', 'check', 'paypal'];

    this.state = {
      payoutMethod: this.props.payoutMethod || 'bank',
      payeeType: this.props.payeeType || 'individual',
      modalVisible: false,
      termsAgreed: false,
    };
  }

  saveWizard() {
    let formName = `${this.state.payoutMethod}PayoutPreferences`;
    let values = GiftedFormManager.getValues(formName);
    let isValid = GiftedFormManager.validate(formName, true).isValid;

    return this.save(isValid, values);
  }
  save(isValid, values, postSubmit=null) {
    if(!isValid) {
      postSubmit && postSubmit();
      alert('Please correct the issues in red');
      if(!this.props.isCelebrityAccount) {
        return Promise.reject(new Error('Invalid Payout Info')); //so signup wizard properly gets a promise
      }
      else return;
    }

    if(!this.props.celebrityTermsAgreed && !this.state.termsAgreed) { //once they have agreed and it's stored in the db, we don't care because we dont show the checkbox anymore
      postSubmit && postSubmit();
      alert(`You must agree to the Celebvidy Celebrity Terms and Conditions`);
      if(!this.props.isCelebrityAccount) {
        return Promise.reject(new Error('Celebrity terms not agreed to'));
      }
      else return;
    }


    let updateFunc = this.props.isCelebrityAccount ? 'updateCelebrity' : 'storeCelebrity';
    let payoutInfoKey = `${this.state.payoutMethod}PayoutInfo`;

    values.lastUpdated = new Date;
    
    return this.props[updateFunc]({
      payoutMethod: this.state.payoutMethod,
      [payoutInfoKey]: values,
      payeeType: this.state.payeeType,
      celebrityTermsAgreed: true,
    })
      .then(() => {
        if(this.props.isCelebrityAccount) {
          postSubmit && postSubmit();
          this.props.navigator.pop();
        }
      })
      .catch((error) => {
        postSubmit && postSubmit();
        alert(error.message); //human-readable message thrown from the server
      });
  }

  tabIndex() {
    return this.tabs.indexOf(this.state.payoutMethod);
  }
  payoutMethodFromIndex(index) {
    return this.tabs[index];
  }
  handleChecked() {
    this.setState(({termsAgreed}) => ({termsAgreed: !termsAgreed}));
  }

  render() {
    let Terms = (
      <TermsCheckbox
        isChecked={this.state.termsAgreed}
        handleChecked={this.handleChecked.bind(this)}
        {...this.props}
        pushTerms={() => this.props.navigator.pushWithProps('CelebrityTerms')}
      />
    );

    return (
      <View style={{flex: 1}}>
        <View style={{}}>
          <SegmentedControlTabs
            titles={["BANK", "CHECK", "PAYPAL"]}
            index={this.tabIndex()}
            stretch={true}
            onPress={(segmentIndex) => {
              let payoutMethod = this.payoutMethodFromIndex(segmentIndex);
              this.setState({payoutMethod})
            }}
            style={ownStyles.segmentedControl}
            barColor={"rgba(27,58,223,1)"}
            barStyle={{height: 1}}
            barPosition='top'
            underlayColor={"rgba(27,58,223,1)"}
            renderTitle={(title, i) => {
              return (
                <View style={[ownStyles.titleView, i === this.tabIndex() && ownStyles.selectedTitleView]}>
                  <Text style={[ownStyles.title, i === this.tabIndex() && ownStyles.selectedTitle]}>{title}</Text>
                </View>
              );
            }}
          />
        </View>

        <View style={{marginTop: 10, marginLeft: -10, padding: 20}}>
          <SegmentedControls
            options={['individual', 'company']}
            selectedOption={this.state.payeeType}
            onSelection={(payeeType) => this.setState({payeeType})}

            tint= {'rgba(8, 27, 148, 1)'}
            selectedTint= {'rgb(184, 184, 184)'}
            backTint= {'#171a1d'}
            optionStyle= {{
              fontSize: 14,
              fontFamily: 'Proxima Nova',
              padding: 5,
              paddingTop: 2,
              paddingBottom: 2,
            }}
            containerStyle= {{
              width: 160,
              alignSelf: 'center',
            }}
          />
        </View>


        {this.state.payoutMethod === 'bank' &&
          <Bank
            ref='bank'
            {...this.props}
            save={this.save.bind(this)}
            saveWizard={this.saveWizard.bind(this)}
            isCompany={this.state.payeeType === 'company'}
            terms={Terms}
          />
        }

        {this.state.payoutMethod === 'check' &&
          <Check
            ref='check'
            {...this.props}
            save={this.save.bind(this)}
            saveWizard={this.saveWizard.bind(this)}
            isCompany={this.state.payeeType === 'company'}
            terms={Terms}
          />
        }

        {this.state.payoutMethod === 'paypal' &&
          <Paypal
            ref='paypal'
            {...this.props}
            save={this.save.bind(this)}
            saveWizard={this.saveWizard.bind(this)}
            isCompany={this.state.payeeType === 'company'}
            terms={Terms}
          />
        }
      </View>
    );
  }
}

export default connect(
  ({celebrity}) => (celebrity),
  actions, null, {withRef: true}
)(PayoutPreferences)




function TermsCheckbox({isChecked, handleChecked, celebrityTermsAgreed, pushTerms}) {
  if(celebrityTermsAgreed) {
    return <View />; //don't show the terms once accepted, which they will be if any of the above sets of payoutInfo is already saved
  }

  return (
    <View style={{marginBottom: 10}}>
      <View style={{flexDirection: 'row', alignItems: 'center', alignSelf: 'center', justifyContent: 'space-around', left: -8, marginBottom: 2}} >
        <CheckBox
          size={30}
          color='rgb(200,200,200)'
          checked={isChecked}
          onPress={handleChecked}
          iconStyle={{marginLeft: 0}}
          uncheckedIconName="power-settings-new"
          checkedIconName="check-circle"
        />

      <Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 16,  color: 'rgb(200,200,200)', marginLeft: -10}}>
          I agree to the celebvidy
        </Text>
      </View>

      <TouchableOpacity onPress={pushTerms}>
        <Text style={{fontFamily: 'ProximaNova-Thin', fontSize: 16, alignSelf: 'center', color: 'rgba(41, 56, 212, 1)', textDecorationLine: 'underline'}}>
          Celebrity Terms and Conditions
        </Text>
      </TouchableOpacity>
    </View>
  );
}


const ownStyles = StyleSheet.create({
  segmentedControl: {
    backgroundColor: 'black',
    flex: 0,
    height: 44 + 1*StyleSheet.hairlineWidth,
    //borderBottomWidth: 1*StyleSheet.hairlineWidth,
    //borderBottomColor:  'rgba(27,58,233,.2)' ,
  },
  titleView: {
    backgroundColor: "rgba(21,21,24,1)",
  },
  selectedTitleView: {
    backgroundColor:  "black" ,
    borderLeftColor:  "rgba(22,22,26,1)" ,
    borderLeftWidth: 1*StyleSheet.hairlineWidth,
    borderRightColor:  "rgba(22,22,26,1)" ,
    borderRightWidth: 1*StyleSheet.hairlineWidth,
  },
  title: {
    color: 'gray',
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    paddingHorizontal: 2,
    paddingVertical: 14,
  },
  selectedTitle: {
    color: 'white',
  },
});
