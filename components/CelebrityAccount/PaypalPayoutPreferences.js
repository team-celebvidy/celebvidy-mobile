import {View, Text, TouchableHighlight, TouchableOpacity,  StyleSheet, Dimensions, PixelRatio, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import validator from 'validator';
import ExNavigator from 'react-navigation';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import dismissKeyboard from 'react-native-dismiss-keyboard';

import styles from '../../styles/FormStyles';

import SubmitButton from '../../widgets/GiftedSubmitButton';
import TextInput from '../../widgets/GiftedTextInput';
import NumberInput from '../../widgets/GiftedNumberInput';
import Select from '../../widgets/GiftedSelect';


var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';


class PaypalPayoutPreferences extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      firstEmail: null,
    };
  }

  render() {
    let {isCompany} = this.props;

    return (
      <GiftedForm
        formName='paypalPayoutPreferences'
        style={[styles.giftedForm, {height: height - 192, flex: 0}]}
        scrollEnabled={true}
        scrollOnTap={true}
        validators={this.getValidators(isCompany)}
      >

        <TextInput
          name={isCompany ? 'company' : 'fullName'}
          value={isCompany ? this.props.company : this.props.fullName}
          title={isCompany ? 'Company' : 'Full Name'}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.name = input}
          onSubmitEditing={(value) => this.tin.focus()}
        />

        <NumberInput
          name='tin'
          value={this.props.tin}
          title={isCompany ? 'TIN' : 'SSN'}
          returnKeyType='next'
          placeholder={isCompany ? '##-#######' : '###-##-####'}
          maxLength={isCompany ? 10 : 11}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          format={isCompany ? 'ein' : 'ssn'}
          ref={(input) => this.tin = input}
          onSubmitEditing={(value) => this.email.focus()}
        />

        <TextInput
          name='email'
          value={this.props.email}
          title='Paypal Email'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='none'
          ref={(input) => this.email = input}
          onSubmitEditing={(value) => this.emailConfirm.focus()}
          returnKeyType='next'
        />

        {this.state.firstEmail &&
          <TextInput
            name='emailConfirm'
            value={this.props.emailConfirm}
            title='Confirm Email'
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            widgetStyles={styles.input}
            underlined={true}
            autoCapitalize='none'
            ref={(input) => this.emailConfirm = input}
            returnKeyType={this.props.isCelebrityAccount && 'done'}
            onSubmitEditing={() => this.props.isCelebrityAccount && this.refs.submit.submit()}
          />
      }

        <GiftedForm.SeparatorWidget />
        <GiftedForm.SeparatorWidget />

        {this.props.terms}

        <SubmitButton
          {...this.props}
          ref='submit'
          formName='paypalPayoutPreferences'
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            this.props.save(isValid, values, postSubmit);
          }}
        />

      </GiftedForm>
    );
  }

  getValidators(isCompany) {
    return {
      [isCompany ? 'company' : 'fullName']: {
        title: isCompany ? 'Company' : 'Full Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 120],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }, {
          validator: (name) => {
            if(isCompany && name && name.length > 1) return true;
            return name && name.split(' ').length >= 2 && name.split(' ')[1] !== '';
          },
          message: `Individual's names must be at least 2 words`
        }]
      },
      tin: {
        title: isCompany ? 'TIN' : 'Social Security Number',
        validate: [{
          validator: 'isLength',
          arguments: [9, 9],
          message: '{TITLE} must be 9 numbers'
        }]
      },
      email: {
        title: 'Email',
        validate: [{
          validator: (firstEmail) => {
            let isValid = validator.isEmail(firstEmail || '');

            console.log('EMAIL', isValid);

            if(isValid) {
              this.setState({firstEmail});
            }
            else {
              this.setState({firstEmail: null});
            }

            return isValid;
          },
          message: 'Invalid Email Address',
        }]
      },
      emailConfirm: {
        title: 'Confirm Email',
        validate: [{
          validator: (email) => {
            console.log("MATCHES", email === this.state.firstEmail, email, this.state.firstEmail)
            return email === this.state.firstEmail;
          },
          message: 'Email addresses must match'
        }]
      }
    };
  }
}

export default connect(
  ({celebrity}) => () => {
    let props = {
      ...celebrity.bankPayoutInfo,
      ...celebrity.checkPayoutInfo,
      ...celebrity.paypalPayoutInfo,
    };

    if(!props.email) {
      props.email = celebrity.email;
    }

    if(!props.fullName && celebrity.bankPayoutInfo) {
      props.fullName = celebrity.bankPayoutInfo.firstName && (celebrity.bankPayoutInfo.firstName + ' ' + celebrity.bankPayoutInfo.lastName);
    }

    if(!props.fullName) {
      props.fullName = celebrity.full_name;
    }

    return props;
  },
  actions
)(PaypalPayoutPreferences)
