import {View, Text, TouchableHighlight, TouchableOpacity,  StyleSheet, Dimensions, PixelRatio, Navigator} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import ExNavigator from 'react-navigation';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import NetworkInfo from 'react-native-network-info';

import styles from '../../styles/FormStyles';

import SubmitButton from '../../widgets/GiftedSubmitButton';
import TextInput from '../../widgets/GiftedTextInput';
import NumberInput from '../../widgets/GiftedNumberInput';
import Select from '../../widgets/GiftedSelect';

import {DAYS, ALL_MONTHS as MONTHS, ALL_YEARS as YEARS} from '../../misc/MonthsYears';

var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';


class BankPayoutPreferences extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      dobFocused: false,
      dobShownOnce: false,
      day: '',
      month: '',
      year: '',
    };
  }

  componentDidMount() {
    this.setState({ipAddress: '98.167.81.40'}); //REMOVE THIS WHEN NetworkInfo package is installed in Xcode
    //NetworkInfo.getIPAddress((ipAddress) => this.setState({ipAddress}));
  }
  render() {
    let {isCompany} = this.props;

    return (
      <GiftedForm
        formName='bankPayoutPreferences'
        style={[styles.giftedForm, {height: height - 192, flex: 0}]}
        validators={this.getValidators(isCompany)}
      >

      <NumberInput
        name='routingNumber'
        value={this.props.routingNumber}
        title='Routing #'
        returnKeyType='next'
        validIcon={'checkmark'}
        invalidIcon={'close-round'}
        widgetStyles={styles.input}
        underlined={true}
        ref={(input) => this.routingNumber = input}
        onSubmitEditing={(value) => this.accountNumber.focus()}
      />

      <NumberInput
        name='accountNumber'
        value={this.props.accountNumber}
        title='Account #'
        returnKeyType='next'
        validIcon={'checkmark'}
        invalidIcon={'close-round'}
        widgetStyles={styles.input}
        underlined={true}
        ref={(input) => this.accountNumber = input}
        onSubmitEditing={(value) => isCompany ? this.company.focus() : this.tin.focus()}
      />

        {isCompany &&
          <TextInput
            name='company'
            value={this.props.company}
            title='Company'
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            widgetStyles={styles.input}
            underlined={true}
            autoCapitalize='words'
            ref={(input) => this.company = input}
            onSubmitEditing={(value) => this.tin.focus()}
          />
        }

        <NumberInput
          name='tin'
          value={this.props.tin}
          title={isCompany ? 'TIN' : 'SSN'}
          returnKeyType='next'
          placeholder={isCompany ? '##-#######' : '###-##-####'}
          maxLength={9}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.tin = input}
          format={isCompany ? 'ein' : 'ssn'}
          onSubmitEditing={(value) => this.firstName.focus()}
        />

        <TextInput
          name='firstName'
          value={this.props.firstName}
          title='First Name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.firstName = input}
          onSubmitEditing={(value) => this.lastName.focus()}
        />

        <TextInput
          name='lastName'
          value={this.props.lastName}
          title='Last Name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          ref={(input) => this.lastName = input}
          onSubmitEditing={(value) => this.email.focus()}
        />



        <TextInput
          name='email'
          value={this.props.email}
          title='Email'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='none'
          ref={(input) => this.email = input}
          returnKeyType={this.props.isCelebrityAccount && 'done'}
          onSubmitEditing={() => this.props.isCelebrityAccount && this.refs.submit.submit()}
        />

        <GiftedForm.SeparatorWidget />
        <GiftedForm.SeparatorWidget />

        {this.props.terms}

        <SubmitButton
          {...this.props}
          ref='submit'
          formName='bankPayoutPreferences'
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            values.ipAddress = this.state.ipAddress;
            this.props.save(isValid, values, postSubmit);
          }}
        />

      </GiftedForm>
    );
  }

  getValidators(isCompany) {
    let validators = {
      accountNumber: {
        title: 'Account Number',
        validate: [{
          validator: 'isLength',
          arguments: [6, 20],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} numbers'
        }]
      },
      routingNumber: {
        title: 'Routing Number',
        validate: [{
          validator: 'isLength',
          arguments: [6, 20],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} numbers'
        }]
      },
      firstName: {
        title: 'First Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 50],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      lastName: {
        title: 'Last Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 50],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      tin: {
        title: isCompany ? 'TIN' : 'Social Security Number',
        validate: [{
          validator: 'isLength',
          arguments: [9, 9],
          message: '{TITLE} must be 9 numbers'
        }]
      },
      email: {
        title: 'Email',
        validate: [{
          validator: 'isLength',
          arguments: [6, 255],
          message: 'Invalid Email Address',
        },{
          validator: 'isEmail',
          message: 'Invalid Email Address',
        }]
      },
    };

    if(isCompany) {
      validators = {
        ...validators,
        company: {
          title: 'Company Name',
          validate: [{
            validator: 'isLength',
            arguments: [1, 50],
            message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
          }]
        },
      }
    }

    return validators;
  }
}


export default connect(
  ({celebrity}) => {
    let props = {
      ...celebrity.checkPayoutInfo,
      ...celebrity.paypalPayoutInfo,
      ...celebrity.bankPayoutInfo,
    };

    if(!props.email) {
      props.email = celebrity.email;
    }

    if(!props.firstName && celebrity.paypalPayoutInfo) {
      props.firstName = celebrity.paypalPayoutInfo.fullName && (celebrity.paypalPayoutInfo.fullName.split(' ')[0]);
      props.lastName = celebrity.paypalPayoutInfo.fullName && (celebrity.paypalPayoutInfo.fullName.split(' ')[1]);
    }

    if(!props.firstName && celebrity.checkPayoutInfo) {
      props.firstName = celebrity.checkPayoutInfo.fullName && (celebrity.checkPayoutInfo.fullName.split(' ')[0]);
      props.lastName = celebrity.checkPayoutInfo.fullName && (celebrity.checkPayoutInfo.fullName.split(' ')[1]);
    }

    if(!props.firstName) {
      props.firstName = celebrity.first_name;
      props.lastName = celebrity.last_name;
    }

    return props;
  },
  actions
)(BankPayoutPreferences)
