import {View, Text, TouchableOpacity,  StyleSheet, Dimensions, Navigator} from 'react-native';
import React, { Component } from 'react';
import Mixpanel from 'react-native-mixpanel';
import _ from 'underscore';
import {GiftedForm, GiftedFormManager} from 'react-native-gifted-form';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../styles/FormStyles';

import TextInput from '../../widgets/GiftedTextInput';
import Select from '../../widgets/GiftedSelect';
import SubmitButton from '../../widgets/GiftedSubmitButton';
import NumberInput from '../../widgets/GiftedNumberInput';

import {CELEB_INDUSTRIES} from '../../misc/IndustryOptions';

var {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import * as actions from '../../actions';


class CelebrityBasicInfo extends Component {
  constructor(props, context) {
    super(props, context);
    this.inputs = [];
    this.state = {};
  }

  saveWizard() {
    let values = GiftedFormManager.getValues('basicInfo');
    let isValid = GiftedFormManager.validate('basicInfo', true).isValid;

    return this.save(isValid, values);
  }
  save(isValid, values, postSubmit=null) {
    //minor hack to make sure selects receive a checkmark or x when the whole form is validated.
    //since they use their placeholder as default values and show no checkmark until you touch it, this is needed.
    this.inputs[4].setState({neverFocused: false});

    if(!isValid) {
      alert('Please fix the issues displayed in red');
      return !this.props.isCelebrityAccount && Promise.reject(new Error('Invalid form submission')); //only wizard deals with rejected promises
    }

    let updateFunc = this.props.isCelebrityAccount ? 'updateCelebrity' : 'storeCelebrity';

    Mixpanel.set({$name: values.display_name, $first_name: values.first_name, $last_name: values.last_name, $email: values.email});

    return this.props[updateFunc](values)
      .then(() => {
        if(this.props.isCelebrityAccount) {
          postSubmit && postSubmit();
          this.props.isCelebrityAccount && this.props.navigator.pop();
        }
      })
      .catch((error) => {
        postSubmit && postSubmit();
        alert(error.message); //rarely will be called because of gifted-form validation in red, but would be a human-readable message thrown from the server
      })
  }

  render() {
    let {first_name, last_name, display_name, industry, instagram, tag_line, email} = this.props;
    console.log('RENDER');

    return (
      <GiftedForm
        formName='basicInfo'
        ref='form'
        style={styles.giftedForm}
        validators={this.getValidators()}
      >
        <GiftedForm.SeparatorWidget />

          <TextInput
            name='email'
            title='Email'
            validIcon={'checkmark'}
            invalidIcon={'close-round'}
            keyboardType='email-address'
            widgetStyles={styles.input}
            autoCapitalize='none'
            underlined={true}
            ref={(input) => this.inputs.push(input)}
            onSubmitEditing={(value) => this.inputs[1].focus()}
            value={email}
          />

        <TextInput
          name='first_name'
          title='First Name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[2].focus()}
          value={first_name}
        />

        <TextInput
          name='last_name'
          title='Last Name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[3].focus()}
          value={last_name}
        />

        <TextInput
          name='display_name'
          title='Display Name'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          autoCapitalize='words'
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[4].focus()}
          value={display_name}
        />

        <Select
          name='industry'
          title='Industry'
          pickers={[CELEB_INDUSTRIES]}
          selections={industry ? [industry] : ['Athletes']}
          ref={(input) => this.inputs.push(input)}
          onSubmitEditing={(value) => this.inputs[5].focus()}
          placeholder='Athletes'
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          widgetStyles={styles.input}
          underlined={true}
          value={industry}
         />

         <TextInput
           name='instagram'
           title='Instagram'
           validIcon={'checkmark'}
           invalidIcon={'close-round'}
           widgetStyles={styles.input}
           underlined={true}
           autoCapitalize='none'
           placeholder='optional'
           ref={(input) => this.inputs.push(input)}
           onSubmitEditing={(value) => this.inputs[6].focus()}
           value={instagram}
         />

        <TextInput
          name='tag_line'
          title='Tag Line'
          autoFocus={false}
          validIcon={'checkmark'}
          invalidIcon={'close-round'}
          title='Tag Line'
          placeholder=''
          autoCapitalize='words'
          clearButtonMode='while-editing'
          placeholderTextColor="rgba(100,100,100,1)"
          widgetStyles={styles.input}
          underlined={true}
          keyboardAppearance='dark'
          autoCorrect={false}
          returnKeyType='next'
          scrollEnabled={false}
          ref={(input) => this.inputs.push(input)}
          value={tag_line}
          returnKeyType='done'
          onSubmitEditing={(value) => {
            if(this.props.isCelebrityAccount) {
              this.saveWizard();
            }
            else if(this.props.nextRoute) { //`nextRoute` should be passed from CelebrityAccountSetup wizard routes via the previous top right NEXT button that pushed this page
              this.saveWizard().then(() => {
                let {name, props} = this.props.nextRoute;
                this.props.navigator.pushWithProps(name, props);
              });
            }
          }}
        />


        <GiftedForm.SeparatorWidget />
        <GiftedForm.SeparatorWidget />

        <SubmitButton
          {...this.props}
          formName='basicInfo'
          onSubmit={(isValid, values, validationResults, postSubmit) => {
            this.save(isValid, values, postSubmit);
          }}
        />
      </GiftedForm>
    );
  }

  getValidators() {
    return {
      first_name: {
        title: 'First Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 48],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      last_name: {
        title: 'Last Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 48],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      display_name: {
        title: 'Display Name',
        validate: [{
          validator: 'isLength',
          arguments: [1, 64],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      tag_line: {
        title: 'Tag Line',
        validate: [{
          validator: 'isLength',
          arguments: [1, 80],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        }]
      },
      email: {
        title: 'Email address',
        validate: [{
          validator: 'isLength',
          arguments: [6, 255],
          message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
        },{
          validator: 'isEmail',
          message: 'Invalid Email Addresss'
        }]
      },
    };
  }
}



export default connect(
  ({celebrity, user}) => {
    let email = celebrity.email || (user.emails && user.emails[0].address);
    
    return {
      ...celebrity,
      email,
    };
  },
  actions, null, {withRef: true}
)(CelebrityBasicInfo)
