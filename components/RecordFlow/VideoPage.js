'use strict';
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  Navigator,
  PanResponder,
  LayoutAnimation,
  StatusBar,
} from 'react-native';
import React, { Component } from 'react';
import {Timer} from 'react-timer-mixin';
import Mixpanel from 'react-native-mixpanel';

const {width, height} = Dimensions.get('window');

import Video from 'react-native-video';
import YouTube from 'react-native-youtube';
import moment from 'moment';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../styles/record';

import ComboPlayer from './player_widgets/ComboPlayer';
import NavBar from './player_widgets/NavBar';
import PlayButton from './player_widgets/PlayButton';
import TimelineSlider from './player_widgets/TimelineSlider';
import VolumeOverlay from './player_widgets/VolumeOverlay';

import RNFS from 'react-native-fs';
import ModalProgress from '../../widgets/ModalProgress';
import CustomMessage from './CustomMessage';
import {youtubeUpload} from '../../misc/youtube';

import ddpClient from '../../ddp';
const ddp = ddpClient();
import * as actions from '../../actions';
import {dispatch} from '../../configureStore';
import {getDevice} from '../../components/App';


export default class VideoPage extends Timer(Component) {  //check later
  constructor(props) {
    super(props);
    this.state = {
      duration: 1.0, //timeline slider needs a non-zero duration or its width isn't correct. weird.
      currentTime: 0.0,
      paused: true,

      volumePercent: 70,
      endVolumePercent: 70,

      videoPlayedAtLeastOnce: false,

      uploadPercent: 0,
    };

    this.controlsOpacity = new Animated.Value(1);
    this.playButtonOpacity = new Animated.Value(0);
    this.volumeOpacity = new Animated.Value(0);
  }

  goBack() {
    this.setState({paused: !this.state.paused});
    //this.props.navigator.pop();
  }

  onProgress({currentTime, duration}) {
    if(!this.isSliding) this.setState({currentTime: currentTime, duration: duration || this.state.duration});
  }


  onSliderValueChange(value) {
    this.isSliding = true;
    if(parseInt(value) % 5 === 0) this.setState({currentTime: value}); //throttle it a bit so setState doesn make native slider lag
  }
  onSliderSlidingComplete(value) {
    if(value >= this.state.duration) this.seek(0);
    else this.seek(value);

    this.setTimeout(() => {
      this.isSliding = false;
      if(this.isYoutube()) {
        if(!this.state.paused && (new Date - this.firstPlayedTime) > 3000) { //don't allow slider to hide during initil 3 seconds if youtube video or logo will show
          this.hideControls(500);
        }
      }
      else if(!this.state.paused) {
        this.hideControls(500);
      }
    }, 1000);
  }
  seek(value) {
    this.player.seek(value);
  }


  volume() {
    return this.state.volumePercent/100;
  }

  onUploadProgress(uploadPercent) {
    uploadPercent = parseInt(uploadPercent); //ignore decimal percents

    if(this.state.uploadPercent !== uploadPercent) { //only setState if next integer reached
      this.setState({uploadPercent});
    }

    return true; //need to return true for promise chain to continue, see youtube upload code
  }
  onUploadComplete(youtubeId) {
    this.props.navigator.popToTop();
    Mixpanel.trackWithProperties('Upload Celebvidy Complete', {youtube_id: youtubeId});

    setTimeout(() => {
      this.props.dispatch({type: 'REFRESH_CELEBRITY_REQUESTS'});
    }, 800);
  }

  deliver() {
    this.modalProgress.show();

    Mixpanel.track('Upload Celebvidy');
    Mixpanel.timeEvent("Upload Celebvidy Complete");

    if(getDevice() === 'simulator') {
      this.setState({uploadPercent: 50});

      dispatch(actions.addYoutubeVideo({_id: this.props._id, youtube_id: 'ofnSojq-vqI', duration: 57 * 1000}));
      this.youtubeId = 'ofnSojq-vqI'; //set youtubeId so that onUploadComplete will be passed it from ModalProgress completion below

      setTimeout(() => {
        this.onUploadProgress(102); //102 percent is interpreted as complete -- onUploadComplete will be called when animation finishes
      }, 1000);
    }
    else {
      youtubeUpload(this.props.sourceUri, this.props.duration, this.props, this.onUploadProgress.bind(this), (youtubeId) => {
        this.youtubeId = youtubeId;
        this.props.recorder.removeAllSegments();
      });
    }
  }
  changeDeliveryMessage(delivery_message) {
    Mixpanel.trackWithProperties('Change Delivery Message', {delivery_message});
    ddp.callPromise('editRequest', [{_id: this.props._id, delivery_message}]);
  }

  isYoutube() {
    return !!this.props.youtube_id && !this.props.sourceUri;
  }

  render() {
    return (
      <View style={{marginTop: -64, width, height, backgroundColor: 'black'}}>
        <StatusBar hidden={true} animated={true} showHideTransition='slide' barStyle='light-content' />

        <View style={{width, height: height}} {...this._panResponder.panHandlers}>

          <ComboPlayer
            ref={(player) => this.player = player}
            sourceUri={this.props.sourceUri}
            //sourceUri={this.props.fakeSourceUri || this.props.sourceUri}
            //sourceUri='aoki_celebvidy'
            youtube_id={this.props.youtube_id}
            paused={this.state.paused}
            volume={this.volume()}

            onLoad={({duration}) => this.setState({duration})}
            onProgress={this.onProgress.bind(this)}
          />


          <View style={{position: 'absolute', top: 0, left: 0, width, height }} />

          <Animated.View style={[styles.controls, {
              height: height/2 + 45,
              opacity: this.controlsOpacity,
              bottom: 0,
            }]}>

            {!this.isYoutube() && <VolumeOverlay opacity={this.volumeOpacity} percent={this.state.volumePercent} />}

          </Animated.View>
        </View>
        <NavBar
          {...this.props}
          onPressBack={this.props.onPressPlayerBack}
          onSubmitVideo={() => {
            if(!this.state.paused) {
              this.togglePlayback();
            }

            if(this.props.onSubmitVideo) {
              this.props.onSubmitVideo(this.props.sourceUri, this.props.duration, () => this.props.recorder.removeAllSegments());
            }
            else {
              this.deliver();
            }
          }}
          opacity={this.controlsOpacity}
          rightButtonText={this.props.uploadText}
        />

        <Animated.View style={{position: 'absolute', left: width/2 - 40 - StyleSheet.hairlineWidth, top: height/2 - 40, opacity: this.controlsOpacity}} >
          <PlayButton
            opacity={this.playButtonOpacity}
            togglePlayback={this.togglePlayback.bind(this)}
            paused={this.state.paused}
            videoPlayedAtLeastOnce={this.state.videoPlayedAtLeastOnce}
            isYoutube={this.isYoutube()}
          />
        </Animated.View>

        <TimelineSlider
          opacity={this.controlsOpacity}
          duration={this.state.duration}
          currentTime={this.state.currentTime}
          onValueChange={(value) => this.onSliderValueChange(value) }
          onSlidingComplete={(value) => this.onSliderSlidingComplete(value) }
          isYoutube={this.isYoutube()}
        />

        <ModalProgress
          text='uploading'
          caption={`BOOYAH!`}
          ref={(modal) => this.modalProgress = modal}
          animateLogo={true}
          percent={Math.max(0, this.state.uploadPercent - 2)}
          //onPress={() => this.setState({restartCustomMessage: !this.state.restartCustomMessage})}
          hasAlert={typeof this.state.completedMessage === 'boolean' && !this.state.completedMessage}
          note='Please do not close the app until upload completes'
          onComplete={() => {
            this.onUploadComplete(this.youtubeId);
          }}
          onHideMessageBox={() => {
            LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
            this.setState({hideMessageBox: true});
          }}
        >
          {!!this.props.isCelebvidy && !!this.props.recipient_phone && !this.state.hideMessageBox && <CustomMessage
            //restart={this.state.restartCustomMessage}
            defaultMessage={this.props.celebrity.delivery_message}
            onStart={() => this.setState({completedMessage: false})}
            onNevermind={() => this.setState({completedMessage: true})}
            onSubmit={(message) => {
              this.setState({completedMessage: true});
              this.changeDeliveryMessage(message);
            }}
          />}
        </ModalProgress>
      </View>
    );
  }

  componentDidMount() {
    this.showControls(0, 'playButton');

    this.setTimeout(() => {
      if(this.props.modalProgress) this.props.modalProgress.hide();
    }, 500); //can't overlay show() call or will never complete. this happens if no video is recorded, since there is no delay between pages
  }
  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => {
        return !this.showPlayButton; //don't capture once the play button is showing so we can press it!
      },
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderGrant: (evt, gestureState) => {
        //usually something happens here, but we control the user experience in a more fine grained way
        //by determining whether to toggle volume controls if the user moves by a dy of 5
        //vs whether it's just a tap which is handled in the else statement of the release

        //we do track isTouching, so as not to automatically hide controls on a long press of the pause button until they release
        this.isTouching = true;
      },

      onPanResponderMove: (evt, {dy}) => {
        if(!this.showPlayButton && !this.isMoving && (dy > 5 || dy < -5)) {
          this.isMoving = true;
          this.showControls(0, 'volume');
        }

        //will start calculating volume percent going forward
        if(this.isMoving) {
          let percentChange = Math.round(-dy / 4);
          let tempPercent = this.state.endVolumePercent + percentChange;
          let percent = tempPercent > 0 ? Math.min(tempPercent, 100) : 0;

          this.setState({volumePercent: percent});
        }
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.isTouching = false;

        if(this.isMoving) { //release after moving
          this.isMoving = false;
          this.setState({endVolumePercent: this.state.volumePercent});

          //debounce volume visibility so that user's can repeatedly flick
          if(this.volumeTimer) clearTimeout(this.volumeTimer);

          this.volumeTimer = this.setTimeout(() => {
            if(!this.isSliding) this.hideControls(500); //hiding controls after moving volume (but not if the user decides to change currentTime)
          }, 2000);
        }
        else { //just a tap
          if(!this.showVolume && this.state.videoPlayedAtLeastOnce) this.toggleControls(); //show controls after tap (volume will not be shown), but also hides controls when visible
        }
      },
    });
  }

  toggleControls() {
    if(!this.controlsHidden && this.state.videoPlayedAtLeastOnce && !this.state.paused) this.hideControls();
    else this.showControls();
  }
  hideControls(duration=300, delay=0, callback) {
    //It's important to understand that the controls contains both the Volume and playButton
    //components and we toggle their visibility depending on taps vs moves.
    this.controlsHidden = true; //imperative toggling rather than using this.state for maximum performance
    this.showPlayButton = false;
    this.showVolume = false;


    setImmediate(() => {
      Animated.timing(this.controlsOpacity, {
        toValue: 0,
        duration: duration,
        delay: delay,
      }).start(() => {
        //we need to check that the user didn't quickly tap again and expects the given control to be visible
        if(!this.showPlayButton) this.playButtonOpacity.setValue(0);
        if(!this.showVolume) this.volumeOpacity.setValue(0);

        if(callback) callback();
      });
    });
  }
  showControls(duration=100, control='playButton') {
    this[control+'Opacity'].setValue(1);

    if(control === 'playButton') this.showPlayButton = true;
    if(control === 'volume') this.showVolume = true;

    setImmediate(() => {
      Animated.timing(this.controlsOpacity, {
        toValue: 1,
        duration: duration
      }).start(() => this.controlsHidden = false);
    });

    if(control === 'playButton') this.hideControlsAfterInactivity();
  }
  hideControlsAfterInactivity(duration=500, delay=2000, callback) {
    if(this.timer) clearTimeout(this.timer);

    this.timer = this.setTimeout(() => {
      if(this.isYoutube() && !this.state.videoPlayedAtLeastOnce && (this.state.currentTime < 3 || (new Date - this.firstPlayedTime) < 4000)) { //the youtube logo shows for 3 seconds after first played, even if you move the timeline slider past 3 seconds
        this.hideControlsAfterInactivity(duration, 1000, callback);
      }
      else if(this.isSliding || this.isTouching) {
        this.hideControlsAfterInactivity(duration, delay, callback);
      }
      else if(!this.state.paused && !this.isMoving) {
        this.hideControls(duration, 0, callback);
      }
    }, delay);
  }

  togglePlayback() {
    if(!this.showPlayButton) {
      this.showControls(); //show controls, and then they gotta press the play button again to actually toggle it
      return; //don't allow toggling playback when controls are hidden or you will see the youtube logo in the lower right
    }

    if(this.state.paused) {
      this.hideControlsAfterInactivity(1000, 500, () => {
        this.setState({videoPlayedAtLeastOnce: true}); //this delays the showing of the transparent pause button, so that the red Youtube play button isn't seen
      });

      this.hidePlayButton(1000);
      if(!this.firstPlayedTime) {
        this.firstPlayedTime = new Date;
      }
    }

    this.setState({paused: !this.state.paused});
  }

  hidePlayButton(delay) {
    Animated.timing(this.playButtonOpacity, {
      toValue: 0,
      duration: 300,
      delay: 2000,
    }).start(() => {
      this.showPlayButton = false;
    });
  }
}
