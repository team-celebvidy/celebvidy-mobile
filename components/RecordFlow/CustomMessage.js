import {View, Text, TouchableOpacity, StyleSheet, Dimensions, PixelRatio, Modal, Animated, Easing, PanResponder} from 'react-native';
import React, { Component } from 'react';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import {Timer} from 'react-timer-mixin';
import {GiftedFormManager} from 'react-native-gifted-form';

var {width, height} = Dimensions.get('window');

import _ from 'underscore';

import TextArea from '../../widgets/GiftedTextArea';
import VanishingView from '../../widgets/VanishingView';

import formStyles from '../../styles/FormStyles';


export default class CustomMessage extends Timer(Component) { //ckeck later
  constructor(props, context) {
    super(props, context);
    this.state = {
      containerVisible: true,
      introVisible: true,
      formVisible: false,
      thankyouVisible: false,
      complete: false,
      message: props.defaultMessage,
    };
  }

  componentWillUnmount() {
    delete GiftedFormManager.stores.deliveryMessage; //delete so GiftedForm doesn't cache the past message
  }

  onStart() {
    this.setState({introVisible: false});
    this.setState({formVisible: true});
    this.props.onStart && this.props.onStart();
  }
  onNevermind() {
    this.setState({containerVisible: false, formVisible: false});
    this.props.onNevermind && this.props.onNevermind();
  }
  onSubmit() {
    this.setState({formVisible: false, thankyouVisible: true, complete: true});

    this.setTimeout(() => {
      this.setState({containerVisible: false});
    }, 4000);

    this.setTimeout(() => {
      this.props.onSubmit && this.props.onSubmit(this.state.message);
    }, 2000);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.restart != this.props.restart) this.restart();
  }
  restart() {
    if(this.state.complete || this.state.formVisible) return;
    this.setState({containerVisible: true, introVisible: true, formVisible: false, thankyouVisible: false});
  }


  intro() {
    return (
      <VanishingView visible={this.state.introVisible}>
        <Text style={[formStyles.fieldTitle, {marginTop: 50, textAlign: 'center',}]}>
          Would you like to provide a custom message while you wait for the celebvidy to upload?
        </Text>

        <View style={{flexDirection: 'row', justifyContent: 'space-around', paddingHorizontal: 80}}>
          <TouchableOpacity onPress={() => this.onNevermind()}>
            <Text style={[formStyles.fieldTitle, {marginTop: 8, textAlign: 'center', fontSize: 20, fontFamily: 'Montserrat-Light'}]}>NO</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.onStart()}>
            <Text style={[formStyles.fieldTitle, {marginTop: 8, textAlign: 'center', fontSize: 20, fontFamily: 'Montserrat-Light'}]}>YES</Text>
          </TouchableOpacity>
        </View>

      </VanishingView>
    );
  }

  form() {
    return (
      <VanishingView visible={this.state.formVisible}>
        <Text style={[formStyles.fieldTitle, {marginBottom: 7}]}>Customize Message to Recipient (optional)</Text>

        <TextArea
          title='Message'
          formName='deliveryMessage'
          widgetStyles={formStyles.smallTextArea}
          keyboardAppearance='dark'
          autoCorrect={false}
          scrollEnabled={false}
          autoFocus={false}
          selectTextOnFocus={true}
          value={this.props.defaultMessage}
          onChangeText={(message) => this.setState({message})}
        />

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TouchableOpacity onPress={() => this.onNevermind()}>
            <Text style={[formStyles.fieldTitle, {marginTop: 7, textAlign: 'left', fontSize: 19}]}>nevermind</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.onSubmit()}>
            <Text style={[formStyles.fieldTitle, {marginTop: 7, textAlign: 'right', fontSize: 19}]}>save</Text>
          </TouchableOpacity>
        </View>
      </VanishingView>
    );
  }

  thankyou() {
    return (
      <VanishingView visible={this.state.thankyouVisible}>
        <Text style={[formStyles.fieldTitle, {marginTop: 50, textAlign: 'center'}]}>Thanks for the custom message. The recipient will appreciate it.</Text>
      </VanishingView>
    );
  }
  render() {
    return (
      <VanishingView visible={this.state.containerVisible} style={styles.container}>
        {this.state.formVisible && this.form()}
        {this.state.introVisible && this.intro()}
        {this.state.thankyouVisible && this.thankyou()}
      </VanishingView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 30,
    width: width,
    paddingHorizontal: 20,
  }
});
