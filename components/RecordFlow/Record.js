'use strict';
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  Navigator,
  NativeModules,
  Easing,
} from 'react-native';
import React, { Component } from 'react';
import {Timer} from 'react-timer-mixin';
import Mixpanel from 'react-native-mixpanel';

const {width, height} = Dimensions.get('window');

import Recorder from 'react-native-screcorder';
const { RNControlFlashlight } = NativeModules;

import VideoPage from './VideoPage';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../styles/record';
import commonStyles from '../../styles/common';

import ModalProgress from '../../widgets/ModalProgress';

import NavBar from './record_widgets/NavBar';
import Script from './record_widgets/Script';
import ScriptNote from './record_widgets/ScriptNote';
import ResetButton from './record_widgets/ResetButton';
import RecordButton from './record_widgets/RecordButton';
import PreviewButton from './record_widgets/PreviewButton';
import ComboPlayer from './player_widgets/ComboPlayer';


let fakeSourceUri;
//let fakeSourceUri = 'aoki_celebvidy';
//fakeSourceUri = 'aoki_interview_edited';
//fakeSourceUri = null;

export default class Record extends Timer(Component) {
  componentDidMount() {
    this.setTimeout(() => {
      Animated.timing(this.noteOpacity, {
        toValue: 1,
        duration: 500,
      }).start();
    }, 1000);

    this.setTimeout(() => {
      Animated.timing(this.noteOpacity, {
        toValue: 0,
        duration: 600,
      }).start();
    }, 4000);
  }

  constructor(props, context) {
    super(props, context);

    this.percent = new Animated.Value(0);

    this.percent.addListener(({value: durationPercent}) => {
      this.setState({durationPercent});
    });

    this.buttonOpacity = new Animated.Value(.9);
    this.noteOpacity = new Animated.Value(0);
    this.titleMarginTop = new Animated.Value(50);

    this.state = {
      side: 'front',
      recording: false,
      flash: false,
      nbSegments: 0,
      durationPercent: 0,
      currentDuration: 0,
      maxDuration: 60000,
      limitReached: false,
      config: {
        video: {
          enabled: true,
          format: 'MPEG4',
          bitrate: 4000000,
          timescale: 1,
          quality: "HighestQuality",
        },
        audio: {
          enabled: true,
          bitrate: 128000, // 128kbit/s
          channelsCount: 1, // Mono output
          format: "MPEG4AAC",
          quality: "HighestQuality" // HighestQuality || MediumQuality || LowQuality
        }
      }
    };
  }


  startBarAnimation() {
    this.animRunning = true;
    this.animBar = Animated.timing(
      this.percent, {
        toValue: 100,
        duration: this.state.maxDuration - this.state.currentDuration
      }
    );
    this.animBar.start(() => {
      if(this.animRunning) this.finish(); //The video duration limit has been reached
    });
  }
  resetBarAnimation() {
    Animated.spring(this.percent, {toValue: 0}).start();
  }
  stopBarAnimation() {
    this.animRunning = false;
    if(this.animBar) this.animBar.stop();
  }


  fadeOutButtons() {
    Animated.timing(this.buttonOpacity, {toValue: 0, duration: 300, easing: Easing.circ}).start(() => this.setState({buttonsHidden: true}));
  }
  fadeInButtons() {
    Animated.timing(this.buttonOpacity, {toValue: .9, duration: 300, easing: Easing.circ}).start(() => this.setState({buttonsHidden: false}));
  }

  record() {
    if (this.state.limitReached) return;
    this.refs.recorder.record();
    this.startBarAnimation();
    this.fadeOutButtons();
    this.setState({recording: true});
  }

  pause() {
    if(!this.state.recording) return;
    this.refs.recorder.pause();
    this.stopBarAnimation();
    this.fadeInButtons();
    this.setState({recording: false, nbSegments: ++this.state.nbSegments});
  }

  finish() {
    this.refs.recorder.pause();
    this.stopBarAnimation();
    this.fadeInButtons();
    this.setState({recording: false, limitReached: true, nbSegments: ++this.state.nbSegments});
  }

  reset() {
    this.resetBarAnimation();
    this.refs.recorder.removeAllSegments();
    this.setState({
      recording: false,
      nbSegments: 0,
      currentDuration: 0,
      limitReached: false
    });
  }

  _toggleSide() {
    var side = (this.state.side == "front") ? "back" : "front";
    this.setState({side: side});
  }
  toggleSide() {
    if(!this.state.flash) this._toggleSide();
    else this.toggleFlash(() => this._toggleSide());
  }
  toggleFlash(callback) {
    callback = callback || function() {};

    if(!this.state.flash) RNControlFlashlight.turnFlashlight('flashlightOn', () => null, callback);
    else RNControlFlashlight.turnFlashlight('flashlightOff', () => null, callback);

    this.setState({flash: !this.state.flash})
  }


  onRecordDone() {
    this.setState({nbSegments: 0});
  }

  onNewSegment(segment) {
    this.state.currentDuration += segment.duration * 1000;
  }

  preview() {
    if(this.state.nbSegments === 0) {
      return alert(`Sorry you have nothing to play. You haven't recorded anything yet.`);
    }

    this.modalProgress.show();

    if(this.state.flash) {
      this.toggleFlash();
    }

    this.refs.recorder.save((error, sourceUri) => {
      if(!this.props.isProfileMedia) Mixpanel.track('Preview Celebvidy');

      this.props.navigator.pushWithProps('VideoPage', {
        ...this.props,

        sourceUri,
        //fakeSourceUri: fakeSourceUri,

        showDeliver: true,
        modalProgress: this.modalProgress,
        recorder: this.refs.recorder, //used to remove all segments in onComplete
        duration: this.state.currentDuration,
      });
    });
  }

  render() {
    return (
      <View style={{position: 'absolute', top: 0, left: 0, width, height, marginTop: -64}}>
        <Recorder
          ref="recorder"
          config={this.state.config}
          device={this.state.side}
          flash={this.state.flash}
          onNewSegment={this.onNewSegment.bind(this)}
          style={{position: 'absolute', top: 0, left: 0, width, height}}
        />

        {fakeSourceUri && <ComboPlayer
          ref={(player) => this.player = player}
          sourceUri={fakeSourceUri}
          paused={!this.state.recording}
          onProgress={() => null}
        />}

        <View style={{position: 'absolute', top: 0, left: 0, width, height}}>
          <ModalProgress
            text='processing'
            ref={(modal) => this.modalProgress = modal}
            animateLogo={false}
          />


          <Script
            recipient={this.props.recipient}
            type={this.props.kind || 'Ask Me Anything'}
            script={this.props.script}
            titleMarginTop={this.titleMarginTop}
          />

          <ScriptNote noteOpacity={this.noteOpacity} />

          <NavBar
            navigator={this.props.navigator}
            titleMarginTop={this.titleMarginTop}
            toggleSide={this.toggleSide.bind(this)}
            toggleFlash={this.toggleFlash.bind(this)}
            flash={this.state.flash}
            side={this.state.side}
            recipient={this.props.recipient}
            onPressBack={() => {
              this.refs.recorder.removeAllSegments();
              this.props.navigator.pop();
              this.props.onPressBack && this.props.onPressBack();
            }}
           />

          <View style={styles.controls}>
            <ResetButton hidden={this.state.buttonsHidden} opacity={this.buttonOpacity} reset={this.reset.bind(this)} />

            <RecordButton
              record={this.record.bind(this)}
              pause={this.pause.bind(this)}
              recording={this.state.recording}
              limitReached={this.state.limitReached}
              durationPercent={this.state.durationPercent}
            />

            <PreviewButton hidden={this.state.buttonsHidden} opacity={this.buttonOpacity} preview={this.preview.bind(this)} />
          </View>

        </View>
      </View>
    );
  }
}
