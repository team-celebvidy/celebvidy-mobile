'use strict';

import {
  Linking,
  Platform,
  ActionSheetIOS,
  Dimensions,
  View,
  Text,
  Navigator,
  PixelRatio,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import React, { Component } from 'react';

import _ from 'underscore';
import {Timer} from 'react-timer-mixin';

const {height, width} = Dimensions.get('window');
const LOGO = require('../../images/logo.png');

import GiftedMessenger from 'react-native-gifted-messenger';
import Communications from 'react-native-communications';
import dismissKeyboard from 'react-native-dismiss-keyboard';

var STATUS_BAR_HEIGHT = Navigator.NavigationBar.Styles.General.StatusBarHeight;

import ddpClient from '../../ddp';
const ddp = ddpClient();

/** not using Android stuff yet
if (Platform.OS === 'android') {
  var ExtraDimensions = require('react-native-extra-dimensions-android');
  var STATUS_BAR_HEIGHT = ExtraDimensions.get('STATUS_BAR_HEIGHT');
}
**/

export default class CelebrityConcierge extends Timer(Component) { //check later

  constructor(props) {
    super(props);

    this._messages = [];
    this.state = {
      messages: this._messages,
      isLoadingEarlierMessages: false,
      typingMessage: '',
      allLoaded: false,
    };
  }

  getInitialMessage() {
    let text = !this.props.order.isCelebrityRejected ? 'What can we help you with?' : 'Hello '+this.props.celebrity.first_name+' what was wrong with the celebvidy request?';
    return this.createCelebvidyMessage(text);
  }
  createCelebvidyMessage(text) {
    return {
      text,
      name: 'celebvidy',
      image: require('../../images/logo.png'),
      position: 'left',
      date: new Date(),
      uniqueId: Math.round(Math.random() * 10000), // simulating server-side unique id generation
    };
  }

  setMessages(messages) {
    this._messages = messages;
    this.setState({messages});
  }
  appendMessage(message) {
    this.setMessages(this._messages.concat(message));
  }

  componentDidMount() {
    this.setTimeout(() => this.subscribe(), 500); //prevent glitchiness during push transition
  }
  subscribe() {
    this.setState({typingMessage: 'Celebvidy is loading messages...'});

    ddp.subscribePromise('messages', [this.props.order._id, 'celebrity'])
      .then((id) => {
        this.subscriptionId = id;

        if(_.size(ddp.collections.messages) === 0) {
          this.setState({typingMessage: 'Celebvidy is typing a message...'});
          this.setTimeout(() => this.setState({typingMessage: ''}), 3000);
          this.setTimeout(() => this.setMessages([this.getInitialMessage()]), 3300);
        }
        else {
          this.setState({typingMessage: ''});
          let messages = [this.getInitialMessage()];

          _.each(ddp.collections.messages, (message) => {
            messages.push({
              ...message,
              image: message.name === 'celebvidy' ? LOGO : null,
              uniqueId: message._id,
            });
          });

          messages[0].date = messages[1].date; //make sure first fake automated message has date equal to first message from user
          this.setMessages(messages);
        }

        this.observer = ddp.observe("messages");
        this.observer.added = this.handleReceive.bind(this);

        this.markAsRead(true);
      });
  }
  componentWillUnmount() {
    this.unsubscribe();
    this.markAsRead();
  }

  unsubscribe() {
    this.observer.stop();
    ddp.unsubscribe(this.subscriptionId);
    ddp.collections.messages = {};
  }
  markAsRead(celebrity_is_viewing_concierge=false) {
    ddp.call('markCelebrityConciergeMessageAsRead', [{transaction_id: this.props.order._id, celebrity_is_viewing_concierge}]); //is_viewing===true stops emails and push notifications from going out
    this.props.dispatch({type: 'EDIT_REQUEST', payload: {new_message_for_celebrity: 0}});
  }

  handleSend(message = {}) {
    message.uniqueId = Math.round(Math.random() * 10000);
    this.appendMessage(message);

    //variable used by the server to determine whether to create automated responses
    let automatedResponse = null;
    if(this.props.order.isCelebrityRejected){
      if(this._messages.length === 2) automatedResponse = 'more_info'; //message #3 will be generated on the server on behalf of celebvidy
      else if (this._messages.length === 4) automatedResponse = 'thanks'; //message #5 will be generated on the server on behalf of celebvidy
    }

    ddp.call('submitCelebrityConciergeMessage', [{
      transaction_id: this.props.order._id,
      message: message.text,
      automatedResponse
    }], (error) => {
      if(!error) this.setMessageStatus(message.uniqueId, 'delivered');
      //else this.setMessageStatus(message.uniqueId, 'ErrorButton'); //not implemented yet
    });
  }
  handleReceive(id) {
    let message = ddp.collections.messages[id];

    //only observe messages from celebvidy as messages from user will already be added in this.handleSend()
    if(message.name === 'celebvidy') {
      this.setState({typingMessage: 'Celebvidy is typing a message...'});
      this.setTimeout(() => this.setState({typingMessage: ''}), 1500);
      this.setTimeout(() => this.setMessages(this._messages.concat({...message, image: LOGO, uniqueId: message._id})), 1600);
    }
  }


  setMessageStatus(uniqueId, status) {
    let messages = [];
    let found = false;

    _.each(this._messages, (message) => {
      if (message.uniqueId === uniqueId) {
        messages.push({...message, status});
        found = true;
      } else {
        messages.push(message);
      }
    });

    if(found === true) {
      this.setMessages(messages);
    }
  }

  render() {
    return (
      <GiftedMessenger
        ref={(c) => this._GiftedMessenger = c}

        styles={getMessengerStyles()}


        autoFocus={false}
        messages={this.state.messages}
        handleSend={this.handleSend.bind(this)}
        maxHeight={Dimensions.get('window').height - Navigator.NavigationBar.Styles.General.NavBarHeight - STATUS_BAR_HEIGHT}

        senderName={this.props.order.celebrity_name}
        senderImage={null}
        onImagePress={this.onImagePress}
        displayNames={true}

        parseText={true} // enable handlePhonePress, handleUrlPress and handleEmailPress
        handlePhonePress={this.handlePhonePress}
        handleUrlPress={this.handleUrlPress}
        handleEmailPress={this.handleEmailPress}

        //isLoadingEarlierMessages={this.state.isLoadingEarlierMessages}

        typingMessage={this.state.typingMessage}


        //maxHeight={height - 64}
        useInitials={false}
        forceRenderImage={true}
        keyboardShouldPersistTaps={false}
        loadEarlierMessagesButton={false}
        submitOnReturn={false}
        placeholder='Tell us what you need...'
        placeholderTextColor='rgb(48, 48, 48)'
        textInputProps={{
          keyboardAppearance: 'dark',
          autoCorrect: false,
          minHeight: 32,
        }}
        multiline={true}

        leftControlBar={
          <TouchableOpacity onPress={() => {
              dismissKeyboard();
              setTimeout(() => this.props.navigator.pushWithProps('Faq', this.props), 300);
            }}
          >
            <View style={{width: 60, marginLeft: -10, backgroundColor: 'rgb(27, 27, 27)'}}>
              <Text style={{color: '#dcdcdc', fontSize: 17, fontWeight: 'bold', textAlign: 'center', padding: 10, paddingTop: 12, fontFamily: 'Helvetica'}}>
                FAQ
              </Text>
            </View>
          </TouchableOpacity>
        }
      />
    );
  }

  handleUrlPress(url) {
    Linking.openURL(url);
  }

  // TODO
  // make this compatible with Android
  handlePhonePress(phone) {
    if (Platform.OS !== 'android') {
      var BUTTONS = [
        'Text message',
        'Call',
        'Cancel',
      ];
      var CANCEL_INDEX = 2;

      ActionSheetIOS.showActionSheetWithOptions({
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX
      },
      (buttonIndex) => {
        switch (buttonIndex) {
          case 0:
            Communications.phonecall(phone, true);
            break;
          case 1:
            Communications.text(phone);
            break;
        }
      });
    }
  }

  handleEmailPress(email) {
    Communications.email(email, null, null, null, null);
  }
}


function getMessengerStyles() {
  return {
    container: {
      width,
      height: height - 64,
      //backgroundColor: '#181818',
      backgroundColor: 'transparent',
    },
    listView: {
      //backgroundColor: 'rgb(5,5,5)',
    },
    textInputContainer: {
      borderTopWidth: StyleSheet.hairlineWidth,
      borderTopColor: '#373737',
      flexDirection: 'row',
      paddingLeft: 10,
      paddingRight: 10,
      position: 'absolute',
      bottom: 0,
      width,
      backgroundColor: 'rgb(27, 27, 27)',
    },
    textInput: {
      margin: 0,
      marginVertical: 5,
      paddingTop: 3,
      paddingLeft: 7,

      height: 30,
      width: 100,
      backgroundColor: '#5e5e5e',
      flex: 1,
      fontSize: 15,
      color: 'rgb(20, 20, 20)',
      borderBottomWidth: StyleSheet.hairlineWidth,
      borderBottomColor: '#737070',
      borderTopWidth: 2*StyleSheet.hairlineWidth,
      borderTopColor: '#202020',
    },
    sendButton: {
      marginTop: 11,
      marginLeft: 10,
      color: 'rgb(0, 26, 255)',
    },
    sendButtonDisabled: {
      color: 'white',
    },

    name: {
      color: '#aaaaaa',
      fontSize: 14,
      fontFamily: 'Proxima Nova',
      marginLeft: 50,
      marginBottom: 5,
    },

    bubble: {
      borderRadius: 1,
      paddingLeft: 14,
      paddingRight: 14,
      paddingBottom: 10,
      paddingTop: 8,
    },
    text: {
      color: 'rgb(236, 236, 236)',
    },
    textLeft: {
    },
    textRight: {
      color: '#fff',
    },
    textCenter: {
      textAlign: 'center',
    },
    bubbleLeft: {
      marginRight: 70,
      backgroundColor: 'rgba(35, 35, 35, .9)',
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: 'rgb(45, 45, 45)',
      alignSelf: 'flex-start',
    },
    bubbleRight: {
      marginLeft: 70,
      backgroundColor: 'rgba(3, 28, 135, .9)',
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: 'rgb(11, 46, 163)',
      alignSelf: 'flex-end',
    },
    bubbleCenter: {
      backgroundColor: 'rgb(11, 46, 163)',
      alignSelf: 'center',
    },
    bubbleError: {
      backgroundColor: '#e01717',
    },

    date: {
      color: 'rgb(190, 190, 190)',
      fontFamily: 'ProximaNova-Thin',
      fontSize: 13,
      textAlign: 'center',
      marginBottom: 8,
    },
    link: {
      color: '#007aff',
      textDecorationLine: 'underline',
    },
  };
};
