'use strict';
import {
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';
import React, { Component } from 'react';
const {width, height} = Dimensions.get('window');

import Video from 'react-native-video';

export default class Preview extends Component {
  seek(value) {
    this.player.seek(value);
  }

  render() {
    let {paused, volume, sourceUri, onLoad, onProgress} = this.props;

    return (
      <Video
        ref={(player) => this.player = player}
        source={{uri: sourceUri}}
        paused={paused}
        volume={volume}
        style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}

        rate={1.0}
        muted={false}
        resizeMode="cover"
        repeat={true}

        onLoad={onLoad}
        onProgress={onProgress}

        onSeek={null}
        onEnd={null}
        onError={null}
        onLoadStart={null}
      />
    );
  }


}
