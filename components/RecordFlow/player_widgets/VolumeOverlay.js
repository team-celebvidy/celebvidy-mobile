import {StyleSheet, Text, Dimensions, Animated} from 'react-native';
import React, { Component } from 'react';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/Ionicons';

var {width, height} = Dimensions.get('window');

export default class VolumeOverlay extends Component {
  render() {
    return (
      <Animated.View style={[styles.container, {opacity: this.props.opacity}]} >
        <AnimatedCircularProgress
          size={224}
          width={12}
          fill={this.props.percent}
          tintColor="rgba(27,58,223,1)"
          backgroundColor="#212121">
          {
            (percent) => (
              <Text style={styles.percent}>
                { Math.round(percent) }%
              </Text>
            )
          }
        </AnimatedCircularProgress>

        <Icon name='volume-high' size={30} color='rgba(230, 230, 230, 0.85)' style={{top: -100}} />
        <Icon name='volume-low' size={30} color='rgba(230, 230, 230, 0.85)'  style={{top: -15}}/>
      </Animated.View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    top: -70,
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  percent: {
    color: 'rgba(255, 255, 255, .98)',
    fontSize: 45,
    fontWeight: "100",
    alignSelf: 'center',
    marginTop: -138,
    marginLeft: 12,
  },
});
