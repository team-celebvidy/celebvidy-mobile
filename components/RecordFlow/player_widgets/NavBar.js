import {StyleSheet, Text, View, Animated, TouchableOpacity, Dimensions} from 'react-native';
import React, { Component } from 'react';


import Icon from 'react-native-vector-icons/Ionicons';

import {BackButton} from '../../../widgets/NavButton';

import styles from '../../../styles/record';
import commonStyles from '../../../styles/common';

const {width, height} = Dimensions.get('window');


export default (props) => {
  return (
    <Animated.View
      style={{
        flexDirection: 'row', position: 'absolute', left: 0, top: 0, width, height: 64, backgroundColor: 'rgba(0,0,0,.6)',
        paddingTop: 13,
        justifyContent: 'space-between',
        overflow: 'hidden',
        opacity: props.opacity,
      }}
    >
      <View style={{marginTop: 5, flexDirection: 'row'}}>
        <BackButton
          navigator={props.navigator}
          backgroundColor='transparent'
          hitSlop={{top: 40, left: 40, bottom: 40, right: 40}}
          onPress={props.onPressBack}
        />
      </View>

      {props.showDeliver &&
        <TouchableOpacity
          activeOpacity={.7}
          onPress={props.onSubmitVideo}
          style={{marginRight: 10, marginTop: 10}}
          hitSlop={{top: 40, left: 40, bottom: 40, right: 40}}
        >
          <Text style={commonStyles.navbarText}>{props.rightButtonText || 'Deliver'}</Text>
        </TouchableOpacity>
      }
    </Animated.View>
  );
}
