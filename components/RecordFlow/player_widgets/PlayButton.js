import {StyleSheet, Text, View, Image, Animated, TouchableOpacity, TouchableHighlight, Dimensions} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Gradient from 'react-native-linear-gradient';

import styles from '../../../styles/record';
import commonStyles from '../../../styles/common';

const {width, height} = Dimensions.get('window');


export default (props) => {
  return (
    <Animated.View
      style={[{
        opacity: props.opacity,
        backgroundColor: 'transparent',
    }, props.style]}>

      <AnimatedCircularProgress
        size={90}
        width={4}
        fill={100}
        tintColor="rgba(9, 93, 255, 0.6)"
        backgroundColor="#212121"
        friction={5}
        tension={0}
        style={{position: 'absolute', top: -5 + StyleSheet.hairlineWidth, left: -5 + StyleSheet.hairlineWidth}}
      />

      <Gradient
        start={[0.0, 0.5]} end={[0.5, 1.0]}
        locations={[0, .25, .75, 1]}
        colors={['rgb(40, 40, 40)', 'rgb(35, 35, 35)', 'rgb(35, 35, 35)',  'rgb(40, 40, 40)']}
        style={{
          width: 80 + 2*StyleSheet.hairlineWidth,
          height: 80 + 2*StyleSheet.hairlineWidth,
          borderRadius: 40 + 1*StyleSheet.hairlineWidth,
          position: 'absolute',
          top: 0,
          left: 0,
          opacity: props.videoPlayedAtLeastOnce || !props.isYoutube ? .4 : 1,
      }}/>


      <View
        style={{
          width: 80 + 2*StyleSheet.hairlineWidth,
          height: 80 + 2*StyleSheet.hairlineWidth,
          borderRadius: 40 + 1*StyleSheet.hairlineWidth,
          backgroundColor: 'transparent',
          borderWidth: 1*StyleSheet.hairlineWidth,
          borderColor: 'rgba(70, 105, 254, 1)',
          position: 'absolute', top: 0, left: 0,
      }}/>

      <TouchableOpacity
        onPress={props.togglePlayback}
        onLongPress={props.togglePlayback}
        activeOpacity={.2}
        hitSlop={{top: 40, left: 40, bottom: 40, right: 40}}
      >

        {!props.paused
          ? <View style={{flexDirection: 'row', position: 'absolute', left: 24, top: 21}}>

              <Gradient
                start={[0.0, 0.5]} end={[0.5, 1.0]}
                locations={[0, .4,  .6, 1]}
                colors={['rgba(27,58,223,1)', 'rgba(25, 55, 213, 1)', 'rgba(25, 55, 213, 1)', 'rgba(27,58,223,1)']}
                style={{width: 12, height: 40,
                borderWidth: StyleSheet.hairlineWidth, borderColor: 'rgba(71, 98, 249, 1)'
              }} />

              <Gradient
                start={[0.0, 0.5]} end={[0.5, 1.0]}
                locations={[0, .4, .6, 1]}
                colors={['rgba(27,58,223,1)', 'rgba(25, 55, 213, 1)', 'rgba(25, 51, 213, 1)', 'rgba(27,58,223,1)']}
                style={{width: 12, height: 40, marginLeft: 9,
                  borderWidth: StyleSheet.hairlineWidth, borderColor: 'rgba(71, 98, 249, 1)'
              }} />

            </View>

          : <View>
              <Gradient
                start={[0.0, 0.5]} end={[0.5, 1.0]}
                locations={[0, .4, .5, .6, 1]}
                colors={['rgba(9, 93, 255, 0.6)', 'rgba(27,58,223,0.7)', 'rgba(25,56,221,0.8)', 'rgba(27,58,223,0.7)', 'rgba(9, 93, 255, 0.6)']}
                style={{
                  width: 80 + 2*StyleSheet.hairlineWidth,
                  height: 80 + 2*StyleSheet.hairlineWidth,
                  borderRadius: 40 + 1*StyleSheet.hairlineWidth,
                  backgroundColor: 'rgba(9, 93, 255, 0.9)',
                  position: 'absolute',
                  top: 0,
                  left: 0,
              }}/>

              <View style={{left: 24, top: 17}}>
                <Image source={require('../../../images/logo_play_button_black.png')}
                  style={{left: -8, top: -4, width: 55, height: 55, opacity: .85}}
                  />
              </View>
            </View>
          }
        </TouchableOpacity>
    </Animated.View>
  );
}
