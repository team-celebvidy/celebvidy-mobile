'use strict';
import {
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';
import React, { Component } from 'react';
const {width, height} = Dimensions.get('window');

import YoutubePlayer from './YoutubePlayer';
import StandardPlayer from './StandardPlayer';

export default class Preview extends Component {
  seek(value) {
    this.player.seek(value);
  }
  render() {
    let {paused, volume, youtube_id, sourceUri, onLoad, onProgress} = this.props;

    return (
      !!sourceUri
        ? <StandardPlayer
          ref={(player) => this.player = player}
          sourceUri={sourceUri}
          paused={paused}
          volume={volume}
          onLoad={onLoad}
          onProgress={onProgress}
        />
      : <YoutubePlayer
        ref={(player) => this.player = player}
        youtubeId={youtube_id}
        paused={paused}
        onProgress={onProgress}
      />
    );
  }


}
