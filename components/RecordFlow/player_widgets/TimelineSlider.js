import {StyleSheet, Text, View, Image, Animated, TouchableOpacity, Dimensions, SliderIOS} from 'react-native';
import React, { Component } from 'react';
import Slider from 'react-native-slider';
import Gradient from 'react-native-linear-gradient';

const {width, height} = Dimensions.get('window');

let img = require('../../../images/play_button.png');


export default class TimelineSlider extends Component{
  _secondsToDuration(seconds) {
    seconds = Number(seconds);

    let h = Math.floor(seconds / 3600); //our player likely doesnt have space to show hours--luckily we wont have videos that long
    let m = Math.floor(seconds % 3600 / 60);
    let s = Math.floor(seconds % 3600 % 60);

    return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : (m < 10 ? "0" : "")) + m + ":" + (s < 10 ? "0" : "") + s);
  }
  displayCurrentTime() {
    return this._secondsToDuration(this.props.duration === 1 ? 0 : this.props.currentTime); //make sure times show as 00:00 till ready
  }
  displayDuration() {
    return this._secondsToDuration(this.props.duration === 1 ? 0 : this.props.duration);
  }

  render() {
    let props = this.props;

    return (
      <Animated.View
        style={{
          position: 'absolute',
          left: 0,
          bottom: 0,
          width,
          opacity: props.opacity,
          height: 46,
          paddingTop: 12,
      }}>

        {this.props.isYoutube &&
          <Gradient
            start={[0.0, 0.0]} end={[1.0, 0.0]}
            locations={[0, .15, .35, .65, .79, 1]}
            colors={['rgba(16, 16, 25, .94)', 'rgba(16, 16, 25, .94)', 'rgba(16, 16, 16, .5)', 'rgba(16, 16, 16, .5)', 'rgba(16, 16, 25, .999)',  'rgba(16, 16, 25, .99)']}
            style={{
              width,
              height: 46,
              position: 'absolute',
              bottom: 0,
              left: 0,
          }}/>
        }

        <View style={{flexDirection: 'row', marginTop: 1}}>

          <Text style={{marginLeft: 10, marginTop: 2, width: 50, color: 'rgba(230, 230, 255, 0.85)', backgroundColor: 'transparent', fontFamily: 'Proxima Nova'}}>
            {this.displayCurrentTime()}
          </Text>

          <Slider
            ref={(slider) => this.slider = slider}
            style={[styles.container, {width: width - 120}]}

            trackStyle={styles.track}
            thumbStyle={styles.thumb}
            thumbTouchSize={{width: 50, height: 40}}

            maximumTrackTintColor= "rgba(63,84,134, 1)"
            minimumTrackTintColor= "rgba(32, 56, 215, 1)"
            minimumValue={0}
            maximumValue={props.duration || 60}
            //step={1}
            value={props.currentTime}
            onValueChange={props.onValueChange}
            onSlidingComplete={props.onSlidingComplete}
          />

          <Text style={{marginLeft: 10, marginTop: 2, width: 50, color: 'rgba(230, 230, 230, 0.85)', backgroundColor: 'transparent', fontFamily: 'Proxima Nova'}}>
            {this.displayDuration()}
          </Text>

        </View>
      </Animated.View>
    );
  }
}


var styles = StyleSheet.create({
  container: {
    height: 20,
  },
  track: {
    height: 1,
    backgroundColor: 'rgba(48, 48, 48, 1)',
  },
  thumb: {
    width: 10,
    height: 10,
    backgroundColor: '#3180db',
    borderRadius: 10 / 2,
    shadowColor: '#3180db',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 2,
    shadowOpacity: 1,
  }
});
