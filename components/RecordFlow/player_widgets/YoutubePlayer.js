'use strict'
import {
  StyleSheet,
  View,
  Dimensions,
  Animated,
} from 'react-native';
import React, { Component } from 'react';

const {width, height} = Dimensions.get('window');

import YouTube from 'react-native-youtube';


export default class Preview extends Component {
  constructor(props, context) {
    super(props, context);
    this.opacity = new Animated.Value(1);
  }

  seek(value) {
    this.player.seekTo(value);
  }

  onProgress({currentTime, duration}) {
    currentTime = Math.round(currentTime);
    duration = Math.round(duration);

    //loop back to the beginning since youtube `loop` param shows red play button for a second
    if(currentTime === duration - 1 && currentTime > 0) {
      setTimeout(() => {
        this.seek(0);
      }, 1000); //setTimeout because the end of the video is being cut off, and for some reason in the past i had to detect duration-1
    }
  }

  componentDidMount() {
    Animated.timing(this.opacity, {
      toValue: 0,
      delay: 4500,
      duration: 1500
    }).start();
  }

  render() {
    let {paused, youtubeId, onProgress} = this.props;

    return (
      <View style={{alignSelf: 'stretch', width, height: height, backgroundColor: 'black'}}>
        <YouTube
          ref={(player) => this.player = player}
          videoId={youtubeId}
          play={!paused}
          style={{alignSelf: 'stretch', width, height: height, backgroundColor: 'black'}}

          hidden={false}
          playsInline={true}
          controls={0}
          showinfo={false}
          rel={false}
          modestbranding={true}

          onProgress={(data) => {
            onProgress(data);
            this.onProgress(data);
          }}

          //onReady={(e)=>{this.setState({isReady: true})}}
          //onChangeState={(e)=>{this.setState({status: e.state})}}
          //onChangeQuality={(e)=>{this.setState({quality: e.quality})}}
          //onError={(e)=>{this.setState({error: e.error})}}
        />

        <Animated.View ref='cover' style={{position: 'absolute', width, height, top: 0, left: 0, backgroundColor: 'black', opacity: this.opacity}} />
      </View>
    );
  }


}
