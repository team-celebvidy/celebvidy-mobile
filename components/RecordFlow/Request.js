import {View, Text, Image, TouchableHighlight,  StyleSheet, Dimensions, Navigator, ScrollView, TouchableOpacity, InteractionManager, Alert, LayoutAnimation} from 'react-native';
import React, { Component } from 'react';
import Mixpanel from 'react-native-mixpanel';
import Icon from 'react-native-vector-icons/Ionicons';
import ImageProgress from 'react-native-image-progress';
import Progress from 'react-native-progress';

import styles from '../../styles/FormStyles';
import commonStyles from '../../styles/common';

import CelebvidyDetails from '../CelebvidyDetails';

import ActionButton from '../../widgets/ActionButton';
import ButtonPair from '../../widgets/ButtonPair';
import ActionSheet from '../../widgets/ActionSheet';
import LogoSvg from '../LogoSvg';

import {MONTHS, YEARS} from '../../misc/MonthsYears';
import {setImmediateAnimationFrame} from '../../misc/helpers';
import {isAdmin} from '../../misc/isAdmin';

const {width, height} = Dimensions.get('window');

import {connect} from 'react-redux';
import {rejectCelebvidy, editRequest, api} from '../../actions';

class Request extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showActionSheet: false,
    };
  }

  componentDidMount() {
    Mixpanel.trackWithProperties('View Request', {...this.props.order, celebrity_id: this.props.celebrity._id, celebrity: this.props.celebrity.display_name});

    if(this.props.order.new_message_for_celebrity ) {
      setTimeout(() => {
        if(this.props.order.status === 'video_rejected') {
          alert('Your celebvidy has been rejected. Please visit the concierge to correct it.');
        }
        else alert("There's a new message from the concierge for you.");
      }, 750);
    }
  }

  pushRecordPage() {
    let {order, celebrity, navigator} = this.props;
    Mixpanel.track('Record');
    navigator.pushWithProps('Record', {...order, celebrity, isCelebvidy: true, uploadText: 'Deliver'});
  }

  render() {
    let {order, celebrity, navigator} = this.props;
    let {status, phase, youtube_id} = order;

    return (
        <View style={{width, height: height - 64, backgroundColor: 'black'}}>
          <LogoSvg
            size={300}
            color='rgb(20, 20, 20)'
            strokeWidth={10}
            style={[styles.starBg, {marginTop: -64}]}
          />

          <CelebvidyDetails {...order} formName={'editRequest'} editable={false} viewedAfterCheckout={true} />

         {this.renderButtonPair()}

         {this.props.celebrity.email === 'jamesgillmore@gmail.com' && order.status !== 'order_approved' &&
          <View style={{top: 70}}>
            <TouchableOpacity onPress={() => this.moveBackToNew()}>
              <Text style={{color: 'white', fontFamily: 'ProximaNova-Thin', textAlign: 'center', fontSize: 14}}>ADMIN: MOVE BACK TO NEW</Text>
            </TouchableOpacity>
          </View>
         }

         {!/complete|canceled/.test(phase) && !/celeb_rejected/.test(status) && <ActionButton
           text={!!youtube_id ? 'RE-RECORD CELEBVIDY' : 'RECORD CELEBVIDY'}
           touchableStyle={{position: 'absolute', bottom: 0, width, height: 60}}
           style={{height: 60}}
           iconStyle={{fontSize: 18, marginRight: 7}}
           onPress={() => this.pushRecordPage()}
         />}

         {order.gift && <Text style={[commonStyles.note, {position: 'absolute', top: 108, left: 50, fontSize: 16, color: 'rgba(61, 91, 255, 1)'}]}>(gift)</Text>}

        <ActionSheet
          visible={this.state.showActionSheet}
          onCancel={() => this.hideActionSheet()}
        >
          <ActionSheet.Button onPress={() => this.selectRejectReason('too_personal')} >
            Too Personal
          </ActionSheet.Button>
          <ActionSheet.Button onPress={() => this.selectRejectReason('not_a_match')} >
            Not a Match for Me
          </ActionSheet.Button>
          <ActionSheet.Button onPress={() => this.selectRejectReason('not_comfortable')} >
            Don't feel comfortable
          </ActionSheet.Button>
          <ActionSheet.Button onPress={() => this.selectRejectReason('no_way')} >
            No Way!
          </ActionSheet.Button>
          <ActionSheet.Button onPress={() => this.selectRejectReason('other')} >
            Other
          </ActionSheet.Button>
        </ActionSheet>
      </View>
    );
  }

  renderButtonPair() {
    let {order, celebrity, navigator} = this.props;
    let {youtube_id, status, _id} = order;

    if(!!youtube_id && status === 'video_submitted' && isAdmin(celebrity)) {
      return (
        <ButtonPair
         style={{paddingLeft: 12, paddingRight: 12, marginTop: 25, marginBottom: 0}}

         button1text='WATCH'
         button1icon='social-youtube'
         button1style={{backgroundColor: 'rgb(4, 35, 170)', borderColor: 'rgb(15, 50, 230)'}}
         icon1style={{fontSize: 12, marginTop: 1,}}
         button1onPress={() => {
           Mixpanel.track('Celebrity Watch Celebvidy');
           navigator.pushWithProps('Celebvidy', {youtube_id: order.youtube_id[order.youtube_id.length-1]})
         }}

         button2text='EXCLUSIVE: DELIVER'
         button2icon='paper-airplane'
         button2style={{backgroundColor: 'rgb(25, 25, 25)', borderColor: 'rgb(40, 40, 40)'}}
         icon2style={{fontSize: 14, marginTop: 1}}
         button2onPress={() => this.approveVideo()}
        />
      );
    }
    else if(!!youtube_id) {
      return (
        <ButtonPair
         style={{paddingLeft: 12, paddingRight: 12, marginTop: 25, marginBottom: 0}}

         button1text='WATCH'
         button1icon='social-youtube'
         button1style={{backgroundColor: 'rgb(4, 35, 170)', borderColor: 'rgb(15, 50, 230)'}}
         icon1style={{fontSize: 12, marginTop: 1,}}
         button1onPress={() => {
           Mixpanel.track('Celebrity Watch Celebvidy');
           navigator.pushWithProps('Celebvidy', {youtube_id: order.youtube_id[order.youtube_id.length-1]})
         }}

         button2text='CONCIERGE'
         button2icon='chatboxes'
         notification2={this.props.order.new_message_for_celebrity}
         button2style={{backgroundColor: 'rgb(37, 37, 37)', borderColor: 'rgb(48, 48, 48)'}}
         icon2style={{fontSize: 14, marginTop: 1}}
         button2onPress={() => this.pushConcierge()}
        />
      );
    }
    else if(this.iRejected()) {
      return (
        <ButtonPair
          style={{paddingLeft: 12, paddingRight: 12, marginTop: 25, marginBottom: 0}}

          button1text='UN-REJECT'
          button1icon='checkmark'
          button1style={{backgroundColor: 'rgb(116, 2, 144)', borderColor: 'rgb(136, 2, 164)'}}
          icon1style={{fontSize: 12, marginTop: 1,}}
          button1onPress={() => this.unReject()}

          button2text='CONCIERGE'
          button2icon='chatboxes'
          notification2={this.props.order.new_message_for_celebrity}
          button2style={{backgroundColor: 'rgb(37, 37, 37)', borderColor: 'rgb(48, 48, 48)'}}
          icon2style={{fontSize: 14, marginTop: 1}}
          button2onPress={() => this.pushConcierge()}
         />
      );
    }
    else {
      return (
        <ButtonPair
          style={{paddingLeft: 12, paddingRight: 12, marginTop: 25, marginBottom: 0}}

          button1text='REJECT'
          button1icon='close'
          button1style={{backgroundColor: 'rgb(170, 0, 0)', borderColor: 'rgb(221, 12, 12)'}}
          icon1style={{fontSize: 12, marginTop: 1,}}
          button1onPress={() => this.setState({showActionSheet: true})}

          button2text='CONCIERGE'
          button2icon='chatboxes'
          notification2={this.props.order.new_message_for_celebrity}
          button2style={{backgroundColor: 'rgb(37, 37, 37)', borderColor: 'rgb(48, 48, 48)'}}
          icon2style={{fontSize: 14, marginTop: 1}}
          button2onPress={() => this.pushConcierge()}
         />
      );
    }
  }

  pushConcierge() {
    let {order, celebrity, navigator} = this.props;
    setTimeout(() => Mixpanel.track('Celebrity Concierge'), 1000);
    navigator.pushWithProps('CelebrityConcierge', {order, celebrity});
  }

  iRejected() {
    return this.props.order.status === 'celeb_rejected';
  }

  unReject() {
    LayoutAnimation.configureNext({...LayoutAnimation.Presets.spring, duration: 300});
    this.props.dispatch(editRequest({_id: this.props.order._id, status: 'order_approved'}, {optimistic: true}));
  }
  approveVideo() {
    LayoutAnimation.configureNext({...LayoutAnimation.Presets.spring, duration: 300});
    this.props.dispatch(editRequest({_id: this.props.order._id, status: 'video_approved'}, {optimistic: true}));
  }
  moveBackToNew() {
    LayoutAnimation.configureNext({...LayoutAnimation.Presets.spring, duration: 300});
    this.props.dispatch(editRequest({_id: this.props.order._id, status: 'order_approved', youtube_id: null}, {optimistic: true}));
  }

  selectRejectReason(reason) {
    Mixpanel.trackWithProperties('Reject Celebvidy', {reason});

    this.hideActionSheet(() => {
      LayoutAnimation.configureNext({...LayoutAnimation.Presets.spring, duration: 300});
      this.props.dispatch(rejectCelebvidy({status: 'celeb_rejected', reason, _id: this.props.order._id}, {optimistic: true}));

      setTimeout(() => {
        Alert.alert(
          'ADD EXPLANATION?',
          "Do you have any additional info to add? The more you can tell us, the more seamless we can make the system for you.",
          [{text: 'NO', onPress: () => null}, {text: 'YES', onPress: () => {
            this.props.navigator.pushWithProps('CelebrityConcierge', {order: this.props.order, celebrity: this.props.celebrity, isCelebrityRejected: true});
          }}]
        );
      }, 400);
    });
  }

  hideActionSheet(callback) {
    setImmediateAnimationFrame(() => {
      this.setState({showActionSheet: false}, () => {
        callback && setTimeout(callback, 300);
      });
    });
  }
}

export default connect(
  ({celebrity, currentRequest}, props) => ({celebrity, order: {...props.order, ...currentRequest}}),
  null, null, {withRef: true}
)(Request);

const AVATAR_SIZE = 120;

const ST = StyleSheet.create({
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  celebName: {
    fontSize: 26,
    fontFamily: 'Proxima Nova',
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: 'white',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  note: {
    fontSize: 13,
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    lineHeight: 20,
    color: 'white',
    backgroundColor: 'transparent',
    textShadowOffset: {
      width: 1*StyleSheet.hairlineWidth,
      height: -1*StyleSheet.hairlineWidth,
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
})
