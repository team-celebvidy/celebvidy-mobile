import {StyleSheet, Text, View, Animated, TouchableOpacity, Dimensions} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../../styles/record';
import commonStyles from '../../../styles/common';

const {width, height} = Dimensions.get('window');


export default (props) => {
  return (
    <Animated.View style={{
        position: 'absolute', top: height/2 + 40, left: 50, width: width - 100, height: 30, backgroundColor: 'rgba(0,0,0, .7)',
        borderRadius: 2,
        opacity: props.noteOpacity,
      }}>
      <Text style={{color: 'white', fontFamily: 'ProximaNova-Thin', lineHeight: 21, textAlign: 'center',}}>
        Swipe up/down to hide & show the script
      </Text>
    </Animated.View>
  );
}
