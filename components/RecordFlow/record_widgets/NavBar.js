import {StyleSheet, Text, View, Animated, TouchableOpacity, Dimensions} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import {BackButton} from '../../../widgets/NavButton';

import styles from '../../../styles/record';
import commonStyles from '../../../styles/common';

const {width, height} = Dimensions.get('window');


export default (props) => {
  let recipient = props.recipient || ''; //fix during development so when you go straight to VideoPage after reload it doesn't break without a recipient :(

  return (
    <View
      style={{
        flexDirection: 'row', position: 'absolute', left: 0, width, height: 64, backgroundColor: 'rgba(0,0,0,.6)',
        paddingTop: 10,
        justifyContent: 'space-between',
        overflow: 'hidden',
      }}
    >
      <View style={{marginTop: 10, flexDirection: 'row'}}>
        <BackButton navigator={props.navigator} backgroundColor='transparent' onPress={props.onPressBack} />
      </View>

      <Animated.View style={{flexDirection: 'row', position: 'absolute', top: 13, left: 75, marginTop: props.titleMarginTop,
        width: width - 150, justifyContent: 'center', alignItems: 'flex-start',
      }}>
        <Text style={commonStyles.pageTitle}>
          {recipient.substring(0, 23)}
          {recipient.length >= 23 && '...'}
        </Text>
      </Animated.View>

      <View style={{marginTop: 2, flexDirection: 'row'}}>
        {props.side === 'back' &&
          <TouchableOpacity
            activeOpacity={.7}
            onPress={() => props.toggleFlash()}
            hitSlop={{top: 20, left: 20, bottom: 20, right: 10}}
            >
            <Icon name={props.flashOn ? 'flash' : 'flash-off'} style={{fontSize: 27, color: 'rgba(27,58,223,1)', marginRight: 12, marginTop: 8}} />
          </TouchableOpacity>
        }

        <TouchableOpacity
          activeOpacity={.7}
          onPress={() => props.toggleSide()}
          hitSlop={{top: 20, left: 10, bottom: 20, right: 20}}
          >
          <Icon name='ios-reverse-camera-outline' style={{fontSize: 36, color: 'rgba(27,58,223,1)', marginRight: 12, marginTop: 3}} />
        </TouchableOpacity>
      </View>
    </View>
  );
}
