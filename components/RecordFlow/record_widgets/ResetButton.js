import {StyleSheet, Text, View, Animated, TouchableOpacity, Dimensions} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../../styles/record';
import commonStyles from '../../../styles/common';
import React, { Component } from 'react';

const {width, height} = Dimensions.get('window');


export default (props) => {
  return (
    <TouchableOpacity onPress={() => !props.hidden && props.reset()}  hitSlop={{top: 40, left: 40, bottom: 40, right: 40}}
      style={{
        position: 'absolute',
        left: 40,
        top: 13,
    }}>
      <Animated.View
        style={{
          width: 70,
          height: 70,
          borderRadius: 45,
          backgroundColor: 'rgba(22, 22, 22, .6)',
          position: 'absolute',
          top: -2,
          left: 0,
          opacity: props.opacity,
      }}>
        <Icon name='android-refresh' size={50} color='rgba(215, 215, 215, 0.8)'
          style={{
            top: 11,
            left: -18,
            transform: [{rotateY: '-180deg'}]
          }}
        />
      </Animated.View>
    </TouchableOpacity>
  );
}
