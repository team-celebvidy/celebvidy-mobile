import {StyleSheet, Text, View, Animated, TouchableOpacity, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import React, { Component } from 'react';
import styles from '../../../styles/record';
import commonStyles from '../../../styles/common';

const {width, height} = Dimensions.get('window');


export default (props) => {
  return (
    <TouchableOpacity
      onPress={() => !props.hidden && props.preview()}
      hitSlop={{top: 40, left: 40, bottom: 40, right: 40}}
      style={{
        position: 'absolute',
        right: 40,
        top: 13,
    }}>
      <Animated.View
        style={{
          width: 70,
          height: 70,
          borderRadius: 45,
          backgroundColor: 'rgba(22, 22, 22, .6)',
          position: 'absolute',
          top: -2,
          right: 0,
          opacity: props.opacity,
      }}>
        <Icon name='ios-play' size={58} color='rgba(215, 215, 215, 0.8)'
          style={{
            top: 7,
            left: 25,
          }}
        />
      </Animated.View>
    </TouchableOpacity>
  );
}
