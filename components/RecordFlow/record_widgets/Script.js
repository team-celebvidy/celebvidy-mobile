import {StyleSheet, Text, View, Animated, TouchableOpacity, Dimensions, PanResponder} from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../../../styles/record';
import commonStyles from '../../../styles/common';

const {width, height} = Dimensions.get('window');


export default class Script extends Component  {
  constructor(props, context) {
    super(props, context);
    this.dy = new Animated.Value(0);
    this.endY = 0;
  }
  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderTerminationRequest: (evt, gestureState) => true,

      onPanResponderMove: (evt, {dy}) => {
        dy = dy/(1.1 + dy/500); //as you draw downward, the script box decelerates so it becomes harder (impossible) to fully drag it down
        this.dy.setValue(dy+ this.endY); //set native native state on Animated.Value
      },

      onPanResponderRelease: (evt, {dy, vy}) => {
        dy = dy/(1.1 + dy/500);
        vy = vy > 0 ? vy : -1 * vy;
        let duration = 50;

        if(this.endY >= 0) {  //script is visible to start (i.e. it's ending with a positive endY)
          if(dy > 100) this.endY = -236;                //pulling downward so far, it deserves to fling back up above the nav bar!
          else if(dy >= 0 && dy < 100) this.endY = 0;   //pulling downward but not far enough to earn a fling
          else if(dy < -35) {                           //pushing upward with at a least change of 35
            this.endY = -236;
            duration /= 40; //we want pushing upward to not feel stuck and slow
          }
          else if(dy < 0) this.endY = 0;                //not pushing upward hard enough to deserve to be hidden above the nav bar
        }
        else { //script is hidden above the nav bar
          if(dy >= 40) this.endY = 0;                   //pulling downward hard enough to become visible again
          else if(dy < 40) this.endY = -236;            //not pulling hard enough buddy!
        }

        //animate to the endY determined above (factoring in vy velocity, excellent!)
        Animated.spring(this.dy, {toValue: this.endY, duration: duration*vy}).start();

        //animate the nav bar title to be visible when the script is hidden. cool!
        if(this.endY === -236) Animated.timing(this.props.titleMarginTop, {toValue: 0, duration: Math.min(duration*vy*100, 300), delay: 100}).start();
        else if(this.endY === 0) Animated.timing(this.props.titleMarginTop, {toValue: 50, duration: Math.min(duration*vy*5, 200), delay: 0}).start()
      },
    });
  }

  render() {
    let props = this.props;

    return (
      <View  {...this._panResponder.panHandlers}
        style={{position: 'absolute', top: 80,  left: 0, width, height: height - 200, paddingLeft: 15, paddingRight: 15}}>
        <Animated.View
          style={{marginTop: this.dy, backgroundColor: 'rgba(0, 0, 0, 0.4)', padding: 10, borderWidth: StyleSheet.hairlineWidth, borderColor: 'rgba(28, 44, 80, 0.4)'}}>

          <Text style={[commonStyles.textInputTitle, {fontWeight: 'bold', textAlign: 'center'}]}>{props.recipient}</Text>
          <Text style={[commonStyles.textInputTitle, {fontWeight: 'bold', marginTop: 4, textAlign: 'center', color: 'rgba(41, 96, 218, 0.99)'}]}>{props.type}</Text>
          <Text style={[commonStyles.note, {textAlign: 'justify'}]}>
            {props.script}
          </Text>

        </Animated.View>
      </View>
    );
  }
}
