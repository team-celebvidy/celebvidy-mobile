import {AppRegistry, StyleSheet, View, Dimensions, Animated, Image, Easing} from 'react-native';
import React, { Component } from 'react';
import Art from 'ReactNativeART';
import TimerMixin, {Timer} from 'react-timer-mixin';
import {connect} from 'react-redux';

const {width, height} = Dimensions.get('window');
const {Surface, Group, Shape, Path} = Art;

import LogoSvg, {LogoText} from './LogoSvg';
import {setImmediateAnimationFrame} from '../misc/helpers';

const AnimatedShape = Animated.createAnimatedComponent(Shape);

const FILL_COLORS = [
  'rgb(13, 16, 57)',
  'rgb(13, 16, 57)',
  'rgb(14, 61, 201)',
  'rgb(27, 92, 223)',
  'rgb(27, 92, 223)',
  'rgba(0,0,0,0)'
];

const PARTICLE_COLORS = [
  'rgb(13, 16, 57)',
  'rgb(13, 16, 57)',
  'rgb(9, 34, 203)',
  'rgb(13, 16, 57)',
  'rgb(12, 38, 214)',
  'rgb(13, 16, 57)'
]


class LogoAnimationWrapper extends Timer(Component) {  //chech later
  constructor(props, context) {
    super(props, context);

    this.state = {
      visible: this.props.visible || false,
    };
  }

  //allow for declarative style control
  componentWillReceiveProps(nextProps) {
    if(nextProps.visible != this.props.visible) {
      if(nextProps.visible) this.show(nextProps);
      if(!nextProps.visible) this.hide();
      return;
    }

    //...checking just for inequality would mean when it goes back to 0 when you log out it shows. therefore we only animate on increments
    if(nextProps.showGratifyAnimation.count > this.props.showGratifyAnimation.count) {
      this.show(nextProps.showGratifyAnimation);
    }
  }

  show({callback, caption, darkenBackground}) {
    this.callback = callback;
    this.caption = caption; //it's a bit annoying that we do this. the reason is that sometimes basic props pass in captions, and other times it's from redux and the caption isn't deleted from the `showGratifyAnimation` reducer; the api should likely be unified solely around redux, which then requires changing the uploaders to call `actions.gratify()` instead of setting the `visible` property on this component
    this.darkenBackground = darkenBackground;
    this.setState({visible: true, animationComplete: false});
  }
  hide() {
    this.setState({visible: false, animationComplete: true});
  }


  onComplete() {
    this.hide();
    this.props.onComplete && this.props.onComplete();
    this.callback && callback();
    this.callback = null;
    this.caption = null;
    this.darkenBackground = null;
  }

  render() {
    if(this.state.visible || (this.state.animationComplete && this.props.hold)) {
      return (
        <View style={{position: 'absolute', top: 0, left: 0, backgroundColor: 'transparent', width, height}}>
          <LogoAnimation
            {...this.props}
            caption={this.caption}
            darkenBackground={this.darkenBackground}
            onComplete={this.onComplete.bind(this)}
          />
        </View>
      );
    }
    else return null;
  }
}

export default connect( ({showGratifyAnimation}) => ({showGratifyAnimation}) )(LogoAnimationWrapper)


const LogoAnimation = React.createClass({
  mixins: [TimerMixin],

  getInitialState: function() {
    return {
      animation: new Animated.Value(0),
      containerOpacity: new Animated.Value(1),
    };
  },

  componentDidMount() {
    setImmediateAnimationFrame(() => this.explode());
  },
  explode() {
    setImmediate(() => {
      requestAnimationFrame(() => {
        Animated.parallel([
          Animated.timing(this.state.animation, {
            duration: 6000 + (this.props.addonDuration || 0),
            toValue: 28,
            delay: 300,
          }),
          Animated.timing(this.state.containerOpacity, {
            duration: 500,
            toValue: 0,
            delay: 5500 + (this.props.addonDuration || 0),
            easing: Easing.easeInOut,
          })
        ]).start(this.props.onComplete);
      });
    });
  },


  render: function() {
    let logo_scale = this.state.animation.interpolate({
      inputRange: [0, .01, 6, 10, 12, 24, 28],
      outputRange: [0.0001, 0.0001, .1, 1, 1.2, .9, .01],
      extrapolate: 'clamp'
    });

    let logo_rotate = this.state.animation.interpolate({
      inputRange: [0, 4, 7, 14.5, 18, 22],
      outputRange: ['0deg', '450deg', '1500deg', '3240deg', '3240deg', '3240deg'],
    });

    let containerScale = this.state.containerOpacity.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });

    let backgroundOpacity;

    if(this.props.darkenBackground) {
      backgroundOpacity = this.state.animation.interpolate({
        inputRange: [0, 5, 27, 28],
        outputRange: [0, .9, .9, 0],
        extrapolate: 'clamp'
      });
    }

    let captionOpacity;

    if(this.props.caption) {
      captionOpacity = this.state.animation.interpolate({
        inputRange: [0, 12, 14, 28],
        outputRange: [0, 0, 1, 1],
        extrapolate: 'clamp',
      });
    }

    return (
      <Animated.View style={[styles.container, {opacity: this.state.containerOpacity}]}>
        {this.props.darkenBackground && <Animated.View style={{backgroundColor: 'black', opacity: backgroundOpacity, position: 'absolute', top: 0, left: 0, width, height}} />}

        <Animated.View style={{backgroundColor: 'transparent', top: -15, transform:[{scale: containerScale}]}}>

          <Surface width={width} height={height} style={{backgroundColor: 'transparent'}}>
            <Group x={width/2 - 75} y={height/2 - 75} style={{backgroundColor: 'transparent'}}>
              {this.getSmallExplosions(75, {x:89, y:75})}
            </Group>
          </Surface>

          <LogoSvg
            size={300} color='rgb(23, 46, 167)' strokeWidth={10}
            hideCircle={this.props.hideCircle}
            animation={this.state.animation}
            style={{
              position: 'absolute',
              left: width/2 - 150,
              top: height/2 - 150,
              transform: [
                {rotateZ: logo_rotate},
                {scale: logo_scale},
              ]
            }}
          />

          {this.props.caption &&
            <Animated.Text style={{
                opacity: captionOpacity,
                fontFamily: 'Code Light',
                fontSize: 42,
                textAlign: 'center',
                top: -130,
                color: 'rgb(23, 46, 167)',
                textShadowColor: 'rgb(138, 164, 254)',
                textShadowRadius: StyleSheet.hairlineWidth,
                textShadowOffset: {width: StyleSheet.hairlineWidth, height: -StyleSheet.hairlineWidth},
              }}>
              {this.props.caption}
            </Animated.Text>
          }

        </Animated.View>
      </Animated.View>
    );
  },

  getSmallExplosions: function(radius, offset) {
    return [0,1,2,3,4,5,6].map((v, i, t) => {

      var scaleOut = this.state.animation.interpolate({
        inputRange: [0, 5.99, 6, 13.99, 14, 21],
        outputRange: [0, 0, 1, 1, 1, 0],
        extrapolate: 'clamp'
      });

      var moveUp = this.state.animation.interpolate({
        inputRange: [0, 5.99, 14],
        outputRange: [0, 0, -15],
        extrapolate: 'clamp'
      });

      var moveDown = this.state.animation.interpolate({
        inputRange: [0, 5.99, 14],
        outputRange: [0, 0, 15],
        extrapolate: 'clamp'
      });

      var color_top_particle = this.state.animation.interpolate({
        inputRange: [6, 8, 10, 12, 17, 21],
        outputRange: shuffleArray(PARTICLE_COLORS)
      })

      var color_bottom_particle = this.state.animation.interpolate({
        inputRange: [6, 8, 10, 12, 17, 21],
        outputRange: shuffleArray(PARTICLE_COLORS)
      })

      var position = getXYParticle(7, i, radius)

      return (
        <Group
          key={i}
          x={position.x + offset.x }
          y={position.y + offset.y}
          rotation={getRandomInt(0, 40) * i}
        >
          <AnimatedCircle
            x={moveUp}
            y={moveUp}
            radius={15}
            scale={scaleOut}
            fill={color_top_particle}
          />
          <AnimatedCircle
            x={moveDown}
            y={moveDown}
            radius={8}
            scale={scaleOut}
            fill={color_bottom_particle}
          />
        </Group>
      )
    }, this);
  }
});


const AnimatedCircle = React.createClass({displayName: "Circle",
  render: function() {
    var radius = this.props.radius;
    var path = Path().moveTo(0, -radius)
        .arc(0, radius * 2, radius)
        .arc(0, radius * -2, radius)
        .close();

    return React.createElement(AnimatedShape, React.__spread({},  this.props, {d: path}));
  }
});

function getXYParticle(total, i, radius) {
  var angle = ( (2*Math.PI) / total ) * i;

  var x = Math.round((radius*2) * Math.cos(angle - (Math.PI/2)));
  var y = Math.round((radius*2) * Math.sin(angle - (Math.PI/2)));

  return {x, y};
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    return array;
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  }
});
