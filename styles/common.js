import {StyleSheet, Dimensions, PixelRatio} from 'react-native';

export default StyleSheet.create({
  coloredBox: {
    backgroundColor: "rgba(27,29,223,0.78)",
    borderWidth: 1/PixelRatio.get(),
    borderColor: "rgba(37,39,233,0.79)",
    opacity: .9,
    paddingLeft: 6,
    paddingRight: 6,
    height: 18,
    position: 'absolute',
    top: 20,
    right: 20,
    padding: 2,
    paddingLeft: 4,
    alignItems: 'center',
    flexDirection: 'row'
  },
  purpleBox: {
    backgroundColor: 'rgba(184, 27, 223, 1)',
    borderWidth: 1/PixelRatio.get(),
    borderColor: 'rgba(210, 56, 238, 1)',
  },
  redBox: {
    backgroundColor: 'rgba(223, 27, 27, 1)',
    borderWidth: 1/PixelRatio.get(),
    borderColor: 'rgba(244, 54, 44, 1)',
  },
  subtext: {
    color: 'white',
    fontFamily: 'Proxima Nova',
    fontSize: 12,
    textShadowColor: 'blackrgba(0, 0, 0, 0.3)',
    textShadowRadius: 1/PixelRatio.get(),
    textShadowOffset: {width: 1/PixelRatio.get(), height: 1/PixelRatio.get()},
    marginLeft: 3,
  },
  underline: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: 'rgba(193,193,193,0.32)',
  },
  textInputTitle: {
    color:  'rgb(238, 238, 238)',
    fontFamily: 'ProximaNova-Thin',
    fontSize: 16,
  },
  textInput: {
    fontSize: 16,
    fontFamily: 'Avenir',
    color: 'white',
  },
  note: {
    fontSize: 13,
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    lineHeight: 20,
    color: 'white',
    backgroundColor: 'transparent',
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -1/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
  pageTitle: {
    fontSize: 18,
    fontFamily: 'ProximaNova-Thin',
    lineHeight: 19,
    color: 'white',
    marginTop: 14,
    marginLeft: 10,
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -2/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor:  "rgba(13,50,246,0.6)" ,
  },
  navbarText: {
    color: 'rgba(27,58,223,1)',
    fontSize: 16,
    fontFamily: 'Montserrat-Light',
  }
});
