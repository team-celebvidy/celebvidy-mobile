import React, {StyleSheet, Dimensions} from 'react-native';

import {width, height, MARGIN_RIGHT, SLIDE_WIDTH, TOP_SLIDE_HEIGHT, BOTTOM_SLIDE_HEIGHT} from '../components/LoginFlow/TourFlow/constants';



export default StyleSheet.create({
 starBg: {
   position: 'absolute',
   left: width/2 - 300/2,
   top: height/2  - 300/2,
   width: 300,
 },
 mainContainer: {
   flexDirection: 'row',
   height, width,
   backgroundColor: 'black',
   marginTop: -64,
 },
 container: {
   width,
   flexDirection: 'row',
 },
 content: {
   flexDirection: 'row',
   alignSelf: 'stretch',
 },
 slide: {
   width,
   marginRight: MARGIN_RIGHT,
   alignItems: 'center',
   justifyContent: 'center',
 },
 image: {
   flex: 1,
   position: 'absolute',
   top: TOP_SLIDE_HEIGHT,
   left: 0,
   width,
   height,
   borderRadius: 6,
 },
 topOverlay: {
   position: 'absolute',
   top: 0,
   right: 0,
   height: TOP_SLIDE_HEIGHT,
 },
 inner: {
   flexDirection: 'row',
 },
 overlaySlide: {
   width,
   height: TOP_SLIDE_HEIGHT,
   marginRight: MARGIN_RIGHT,
   alignItems: 'center',
   paddingTop: TOP_SLIDE_HEIGHT/3 - 20,
 },
 pageTitle: {
   fontSize: 28,
   fontFamily: 'Code Light',
   lineHeight: 32,
   color: 'rgb(25, 25, 25)',
   color: 'white',
   //textShadowColor: 'rgb(0, 1, 49)',
   //textShadowColor: 'rgb(40, 40, 40)',
   //color: 'rgb(20, 2, 120)',
   //textShadowColor: 'rgb(47, 47, 47)',
   textShadowColor: 'rgb(0, 0, 125)',
   textShadowRadius: StyleSheet.hairlineWidth,
   textShadowOffset: {width: StyleSheet.hairlineWidth, height: -StyleSheet.hairlineWidth},
   backgroundColor: 'transparent',
 },
 pageCaption: {
   fontSize: 20,
   fontFamily: 'ProximaNova-Thin',
   lineHeight: 25,
   color: 'whitergb(177, 177, 177)',
   backgroundColor: 'transparent',
   textAlign: 'center',

   //backgroundColor: 'rgba(40, 40, 40, .5)',
   //borderColor: 'rgba(40, 40, 40, .7)',
   //borderWidth: StyleSheet.hairlineWidth,
   //borderWidth: 1,
   borderColor: 'white',
   marginHorizontal: 20,
   padding: 10,
 },
 captionBlur: {
   position: 'absolute',
   width: width - 40,
   top: 294,
   left: 20,
   height: 84,
   opacity: .9,
 }
});
