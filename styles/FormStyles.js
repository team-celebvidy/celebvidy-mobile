import {StyleSheet, Dimensions, PixelRatio} from 'react-native';
const {width, height} = Dimensions.get('window');
import _ from 'underscore';


const styles = StyleSheet.create({
  giftedForm: {
    backgroundColor: 'transparent',
    paddingHorizontal: 15,
    alignSelf: 'stretch',
    flex: 1,
  },
  modelPage: {
    backgroundColor: 'transparent',
  },
  fieldTitle: {
    fontFamily: 'ProximaNova-Thin',
    fontSize: 16,
    marginBottom: 2,
    color: 'white',
  },
  fieldSubtext: {
    fontFamily: 'Avenir',
    fontSize: 13,
    color: 'rgb(224, 224, 224)',
    marginBottom: 15,
  },
  note: {
    fontSize: 13,
    fontFamily: 'ProximaNova-Thin',
    textAlign: 'center',
    lineHeight: 20,
    color: 'white',
    backgroundColor: 'transparent',
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -1/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor: 'rgba(4, 10, 40, .6)',
  },
});


let input = {
  rowContainer: {
    backgroundColor:  "rgba(255,255,0,0)",
    //borderColor: "rgba(193,193,193,0.32)",
    //borderBottomWidth: 1/PixelRatio.get()
    borderBottomWidth: 0,
  },
  textInputTitleInline: {
    color:  'rgb(238, 238, 238)',
    fontFamily: 'ProximaNova-Thin',
    marginLeft: -10,
    fontSize: 16,
  },
  textInputInline: {
    fontSize: 16,
    fontFamily: 'Avenir',
    color: 'white',
    width: 210,
    marginLeft: 5,
    flex: 0,
  },
  modalValue: {
    fontSize: 16,
    fontFamily: 'Avenir',
    color: 'white',
    width: 210,
    marginLeft: 5,
    flex: 0,
    textAlign: 'right',
    marginRight: 7,
  },
  rowIcon: {
    fontSize: 17,
    color: 'rgba(193,193,193,0.32)',
    marginRight: 5
  },
  rowIconInvalid: {
    color: 'rgba(187, 5, 5, 0.98)',
  },
  rowIconValid: {
    color: 'rgba(27,92,223,1)',
  },
  underline: {
    borderBottomWidth: 1/PixelRatio.get(),
  },
  underlineIdle: {
    borderColor: 'rgba(193,193,193,0.32)',
  },
  underlineFocused: {
    borderColor: 'rgba(27,92,223,1)',
  },
  underlineError: {
    borderColor: 'rgba(187, 5, 5, 0.98)',
  },
  row: {

  },
  validationErrorRow: {

  },
  validationError: {
    color: 'rgba(187, 5, 5, 0.98)',
    marginLeft: -10,
  },
  switch: {
    backgroundColor: "rgba(20,20,21,1)",
    borderRadius: 16,
  },
  textAreaRow: {
    backgroundColor: "rgba(0,0,0,.75)",
    borderColor: "rgba(193,193,193,0.32)",
    borderWidth: 1/PixelRatio.get(),
  },
  textArea: {
    borderWidth: 0,
    color: "rgba(239,236,236,1)",
    fontFamily: 'AvenirNext-Regular',
    fontSize: 14,
  },
  disclosure: {
    width: 12,
    height: 12,
    marginRight: 15,
  }
};


input.switchTitle = input.modalTitle = input.textInputTitleInline;


export default {
  giftedForm: styles.giftedForm,
  modalPage: styles.modalPage,
  fieldTitle: styles.fieldTitle,
  fieldSubtext: styles.fieldSubtext,
  note: styles.note,
  input,
  bioInput: {
    ...input,
    rowContainer: _.extend({}, input.rowContainer, {height: 70})
  },
  largeTextArea: {
    ...input,
    textAreaRow: _.extend({}, input.textAreaRow, {height: 230}),
  },
  smallTextArea: {
    ...input,
    textAreaRow: _.extend({}, input.textAreaRow, {height: 111}),
  },
  widget: {
    ...input,
    rowContainer: _.extend({}, input.rowContainer, {borderBottomWidth: 0})
  },
  login: {
    ...input,
    textInputTitleInline: _.extend({}, input.textInputTitleInline, {})
  },
  modalWidget: {
    ...input,
    rowImage: _.extend({}, input.rowImage, {marginLeft: 0, marginRight: 4, marginTop: -1, width: 12}),
  },
  pageTitle: {
    fontSize: 20,
    fontFamily: 'ProximaNova-Thin',
    lineHeight: 20,
    color: 'white',
    marginTop: 6,
    marginLeft: 10,
    textShadowOffset: {
      width: 1/PixelRatio.get(),
      height: -2/PixelRatio.get(),
    },
    textShadowRadius: 1,
    textShadowColor:  "rgba(13,50,246,0.6)" ,
  },
  checkmark: {
    color: '#2038D8',
    fontSize: 15,
    marginBottom: 1,
    marginRight: 6,
    marginLeft: 2,
    textShadowRadius: 1/PixelRatio.get(),
    textShadowColor:  "rgb(80, 110, 184)" ,
    textShadowOffset: {height: 1/PixelRatio.get()}
  },
  inventoryOverride: {
    backgroundColor: 'orange',
  },
  characterCount: {
    height: 30,
    color: 'white',
    textAlign: 'right',
    marginTop: 7,
    fontFamily: 'ProximaNova-Thin',
    backgroundColor: 'transparent'
  },
  starBg: {
    position: 'absolute',
    left: width/2 - 300/2,
    top: height/2 - 300/2,
    width: 300,
  },
};
