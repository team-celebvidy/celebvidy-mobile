let date = new Date;
let currentMonth = date.getMonth() + 1;
let currentDay = date.getDate();
let currentYear = date.getUTCFullYear();

export const DAYS = [
  {name: '1'},
  {name: '2'},
  {name: '3'},
  {name: '4'},
  {name: '5'},
  {name: '6'},
  {name: '7'},
  {name: '8'},
  {name: '9'},
  {name: '10'},
  {name: '11'},
  {name: '12'},
  {name: '13'},
  {name: '14'},
  {name: '15'},
  {name: '16'},
  {name: '17'},
  {name: '18'},
  {name: '19'},
  {name: '20'},
  {name: '21'},
  {name: '22'},
  {name: '23'},
  {name: '24'},
  {name: '25'},
  {name: '26'},
  {name: '27'},
  {name: '28'},
  {name: '29'},
  {name: '30'},
  {name: '31'},
];

export const ALL_MONTHS = [
  {name: '1'},
  {name: '2'},
  {name: '3'},
  {name: '4'},
  {name: '5'},
  {name: '6'},
  {name: '7'},
  {name: '8'},
  {name: '9'},
  {name: '10'},
  {name: '11'},
  {name: '12'},
];

export const MONTHS = ALL_MONTHS.filter(month => {
  if(currentMonth === 12) {
    if(parseInt(month.name) === 12) return true;
    if(parseInt(month.name) === 1) return true;
    if(parseInt(month.name) === 2) return true;
    if(parseInt(month.name) === 3) return true;
  }
  else if(currentMonth === 11) {
    if(parseInt(month.name) === 11) return true;
    if(parseInt(month.name) === 12) return true;
    if(parseInt(month.name) === 1) return true;
    if(parseInt(month.name) === 2) return true;
  }
  else if(currentMonth === 10) {
    if(parseInt(month.name) === 10) return true;
    if(parseInt(month.name) === 11) return true;
    if(parseInt(month.name) === 12) return true;
    if(parseInt(month.name) === 1) return true;
  }
  else return parseInt(month.name) >= currentMonth
});

export const YEARS = [
  {name: '2016'},
  {name: '2017'},
  {name: '2018'},
  {name: '2019'},
  {name: '2020'},
  {name: '2021'},
  {name: '2022'},
  {name: '2024'},
  {name: '2025'},
  {name: '2026'},
  {name: '2027'},
  {name: '2028'},
].filter(year => parseInt(year.name) >= currentYear);


function generateYears(startYear) {
  let currentYear = new Date().getFullYear()
  let years = [];

  startYear = startYear || 1980;

  while(startYear <= currentYear) {
    years.push({name: (startYear++)+''});
  }

  return years;
}

export const ALL_YEARS = generateYears(1900);
