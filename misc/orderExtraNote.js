export default function orderExtraNote({gift, showcase, recipient_phone, formName}) {
  let isGift = !!gift;
  let canShowcase = !!showcase;
  let hasTxt = !!recipient_phone;

  recipient_phone = recipient_phone+'';
  if(recipient_phone.length === 10) {
    recipient_phone = spliceSlice(recipient_phone, 3, 0, '-');
    recipient_phone = spliceSlice(recipient_phone, 7, 0, '-');
  }
  else if(recipient_phone.length === 11) {
    recipient_phone = spliceSlice(recipient_phone, 1, 0, '-');
    recipient_phone = spliceSlice(recipient_phone, 4, 0, '-');
    recipient_phone = spliceSlice(recipient_phone, 8, 0, '-');
  }

  let phoneNum = formName === 'editRequest' ? '' : `(${recipient_phone}) `; //dont show customer phone # to celebrity;

  if(hasTxt && !isGift && !canShowcase) {
    return `this celebvidy has the optional 99 cent txt to the recipient ${phoneNum}from the celebrity`;
  }
  else {
    if(isGift && canShowcase) {
      return 'this celebvidy is a gift, approved for showcasing' + (hasTxt ? `, with the optional 99 cent txt to the recipient ${phoneNum}from the celebrity` : '');
    }
    else if(isGift) {
      return 'this celebvidy is a gift' + (hasTxt ? `, with the optional 99 cent txt to the recipient ${phoneNum}from the celebrity` : '');
    }
    else if(canShowcase) {
      return 'this celebvidy is approved for showcasing' + (hasTxt ? `, with the optional 99 cent txt to the recipient ${phoneNum}from the celebrity` : '');
    }
  }
}


function spliceSlice(str, index, count, add) {
  return str.slice(0, index) + (add || "") + str.slice(index + count);
}
