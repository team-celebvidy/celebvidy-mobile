import cloudinary from './cloudinary';
import {getIndustryImageSource} from './IndustryOptions';


//we need to have the app request an application config from the server on opening
//where we can provide new default avatars (among many other things) and if there is a 
//new version for the avatar not on the phone, grab it from cloudinary; otherwise
//grab it from the phone

export const DEFAULT_AVATAR = require('../images/default_avatar.jpg');
// export const DEFAULT_AVATAR = cloudinary.companySource('brand/default-avatar.jpg'); // it's best if the default avatar is stored on the phone -- there's also a bug with `<Img defaultSource={} ...` where it seems to require the image to be locally available

export const DEFAULT_BACKGROUND = getIndustryImageSource({name: 'All Celebrities'});
