export function setImmediateAnimationFrame(func, delay, context) {
  if(delay) {
    return setTimeout.call(context, () => {
      setImmediate(() => {
        requestAnimationFrame(func);
      });
    }, delay);
  }

  return setImmediate(() => {
    requestAnimationFrame(func);
  });
}
