import cloudinary from './cloudinary';


export function avatarSource({_id, avatar_photo, twitter_avatar}, options) {
  let uri;

  if(avatar_photo) {
    if(!isNaN(avatar_photo.charAt(0))) {
      uri = cloudinary.url(_id+'/'+avatar_photo, {prefix: 'celebrities_old', ...options}); //DELETE this after stale test data is removed
    }
    else {
      uri = cloudinary.url(avatar_photo, {...options});
    }  
  }
  else if(twitter_avatar) {
    //use twitter name in URL so we can find in cloudflare (note: special twitter url does not exist for background)
    uri = cloudinary.url(twitter_avatar.replace('http://', ''), {prefix: 'twitter', ...options});
  }
  else {
    uri = cloudinary.url('brand/default-avatar.jpg', {prefix: 'company', ...options});
  }

  return {uri};
}

export function backgroundSource({_id, background_photo, twitter_background}, options) {
  let uri;

  if(background_photo) {
    if(!isNaN(background_photo.charAt(0))) {
      uri = cloudinary.url(_id+'/'+background_photo, {prefix: 'celebrities_old', ...options});  //DELETE this after stale test data is removed
    }
    else {
      uri = cloudinary.url(background_photo, {...options});
    }  
  }
  else if(twitter_background) {
    uri = cloudinary.url(twitter_background.replace('http://', ''), {prefix: 'twitter', ...options});
  }
  else {
    uri = cloudinary.url('brand/default-background.jpg', {prefix: 'company', quality: 50, ...options});
  }

  return {uri};
}