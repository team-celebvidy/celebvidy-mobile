import {dispatch, getState} from '../configureStore';
import * as actions from '../actions';



export function goToCelebrityRequests(_id) {
  let {showDrawer} = getState();

  if(showDrawer) {
    dispatch({type: 'ONLY_HIDE_DRAWER'});
    setTimeout(() => {
      dispatch(actions.goToPage(1));
    }, 1000);
  }
  else {
    dispatch(actions.goToPage(1));
  }

  setTimeout(() => {
    dispatch({type: 'PUSH_NOTIFICATION_OPEN_REQUEST', payload:  {_id, date: new Date}});
  }, showDrawer ? 1200 : 300);
}

export function goToCustomerOrders(_id) {
  if(!getState().isCelebrity) {
    dispatch({type: 'ONLY_HIDE_DRAWER'});
    dispatch(actions.goToPage(1));

    setTimeout(() => {
      dispatch({type: 'PUSH_NOTIFICATION_OPEN_ORDER', payload: {_id, date: new Date}});
    }, 300);
  }
  else {
    let {showDrawer} = getState();

    if(!showDrawer) {
      dispatch(actions.goToPage(0));
      setTimeout(() => dispatch({type: 'SHOW_DRAWER'}), 500)
    }

    setTimeout(() => {
      dispatch({type: 'PUSH_NOTIFICATION_OPEN_ORDER', payload:  {_id, date: new Date}});
    }, !showDrawer ? 1800 : 1300);
  }
}

export function goToCelebrities() {
  dispatch({type: 'ONLY_HIDE_DRAWER'});
  dispatch(actions.goToPage(3));
}
