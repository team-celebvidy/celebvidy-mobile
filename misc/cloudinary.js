import {BASE_IMAGE_URL} from '../config';

//mimic the url method from the cloudinary API: 
//http://cloudinary.com/documentation/image_transformation_reference & https://github.com/cloudinary/cloudinary_npm
//NOTE: it's a dumb slightly customized clone of their method, which is all we need
const cloudinary = {
  url(fileName, {prefix='celebrities', version='v1', ...options}) {
    options = {
      crop: 'fill', 
      ...options
    };

    let paramString = Object.keys(options).reduce((paramString, key) => {
      return paramString += key.charAt(0) + '_' + options[key] + ',';
    }, '');

    paramString = paramString.substring(0, paramString.length -1);

    if(fileName.charAt(0) === '/') {
      fileName = fileName.substr(1);
    }

    return `${BASE_IMAGE_URL}/${paramString}/${version}/${prefix}/${fileName}`;
  },
  companyUrl(filename, options) {
    return this.url(filename, {prefix: 'company', ...options});
  },
  source(...args) {
    return {uri: this.url(...args)};
  },
  companySource(...args) {
    return {uri: this.companyUrl(...args)};
  }
}

export default cloudinary;