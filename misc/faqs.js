export const CUSTOMER_FAQS = [
  {question: 'My celebrity is not on Celebvidy yet, what do I do?', answer: 'Don’t worry, we’re on it. Everyday our team is reaching out to hundreds of celebrities, from musicians to actors, media personalities to sports stars. Our goal: “if they’re famous, they’re here.” In the meantime, invite your favorite celeb to join! Make your voice heard, Tweet out an invite!'},
  {question: 'How does my celebrity make my video?', answer: 'All celebrity members have access to the exclusive celebrity section of the celebvidy™ iPhone app which notifies them of incoming video requests and helps them to record your custom message. If your video-request is reviewed, approved, and then recorded, your credit card is charged for the initially agreed upon amount.'},
  {question: 'What happens if my video request is rejected?', answer: 'You will receive an email with your celebrity’s reason for rejection. You may re-submit another request if you so choose. Celebvidy encourages positive, light-hearted messages. Celebrities reserve the right to choose what they will record.'},
  {question: 'How long does it take?', answer: 'Depends. Some celebrities will respond immediately, others may take longer. Typically, a request should be returned within 2 weeks.'},
  {question: 'How do I know I’ll get my video?', answer: 'We deliver every video securely to you via the mobile application. We will notify you via email and push notification. If you have any questions you may call our 24 hour customer service hotline at 1-844-304-VIDY.'},
  {question: 'Are my celebvidy’s public?', answer: 'If you would like them to be, you can select yours to be shared on the official celebvidy™ YouTube channel. Or, you can decide to keep yours private and just between you and your celebrity. Toggle the "Showcase" option at checkout.'},
];


export const CELEBRITY_FAQS = CUSTOMER_FAQS;
