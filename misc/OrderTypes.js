export default [
  {name: 'All Celebvidys'},
  {name: 'Happy Birthday'},
  {name: 'Ask Me Anything'},
  {name: 'Happy Anniversary'},
  {name: 'Surprise Gift'},
  {name: 'Biggest Fan'},
  {name: 'Congrats'},
  {name: 'Other'},
];

export const ORDER_TYPES = [
  {name: 'Happy Birthday'},
  {name: 'Ask Me Anything'},
  {name: 'Happy Anniversary'},
  {name: 'Surprise Gift'},
  {name: 'Biggest Fan'},
  {name: 'Congrats'},
  {name: 'Other'},
];

export const ORDER_TYPE_DICT = {
  birthday: {
    name: 'Birthday',
    color: 'rgba(37,18,223,1)'
  },
  question: {
    color: 'question',
    color: 'rgba(27,58,223,1)'
  },
  anniversary: {
    color: 'Anniversary',
    color: 'rgba(32,38,223,1)'
  },
  surprise: {
    color: 'Surprise',
    color: 'rgba(39, 67, 180, 1)'
  },
  biggest_fan: {
    color: 'Biggest Fan',
    color: 'rgba(171, 20, 209, 1)'
  },
  congrats: {
    color: 'Congrats',
    color: 'rgba(39, 100, 180, 1)'
  },
  other: {
    color: 'Other',
    color: 'rgba(52, 82, 238, 1)'
  }
};
