import React, { Component } from 'react';

import {celebs, requests, orders} from './Fixtures';


export default function testState(isCelebrity, topNavigatorComponent, routeComponent) {
  let selectedComponent = topNavigatorComponent || 'Celebrities';
  let selectedRoute = routeComponent || selectedComponent;

  let state = {};

  let baseState = {
    //appReady: true,
    selectedComponent,
    selectedRoute,
    isCelebrity,

    showTour: false,
    tourReady: false,

    //these are now assumed to be whatever you might have set in the dev menu STATE ActionSheet
    //and merged in within the setDeveloperState() action:
    //showTabBar: true,
    //showStatusBar: true,
    //showDrawer: false,
    //tourAgain: false,
    //finishLater: false,
    //showIntroAnimation: false,
  };


  if(selectedComponent === 'AppIntroTour') {
    state = {
      showTour: true,
      //showDrawer: isCelebrity ? true : false, //for celebs, have drawer open for when they close the tour
      showStatusBar: false,
    };
  }
  else if(selectedComponent === 'CelebrityOwnProfile') {
    state = {
      showDrawer: false,
      page: 0,
    };
  }
  else if(selectedComponent === 'CelebrityAccount') {
    state = {
      //page: 0, //if page was set, it would trigger imperative calls in ScrollableTabView that trigger the GO_TO_PAGE action which changes components/routes
      showDrawer: true,
      showStatusBar: false,
    };
  }
  else if(selectedComponent === 'CustomerAccount') {
    state = {
      page: 0,
      showDrawer: false,
    };
  }
  else if(selectedComponent === 'CelebrityRequests') {
    state = {
      page: 1,
      showDrawer: false,
    };
  }
  else if(selectedComponent === 'CustomerOrders') {
    state = {
      page: 1,
      showDrawer: false,
    };
  }
  else if(selectedComponent === 'Industries') {
    state = {
      page: 2,
      showDrawer: false,
    };
  }
  else if(selectedComponent === 'Celebrities') {
    state = {
      page: 3,
      showDrawer: false,
    };
  }
  else if(selectedComponent === 'Share') {
    state = {
      page: 4,
      showDrawer: false,
    };
  }

  return Object.assign(baseState, state);
}


export function navigatorTestState(Component, route) {
  let routeStack = [];
  let stateProps = {
    celeb: celebs[0],
    order: orders[0],
    request: requests[0],
  };

  if(Component === 'AppIntroTour') {
    if(route === 'LoginSignup') {
      routeStack = ['AppIntroTour', 'LoginSignup'];
    }
    else if(route === 'EmailConfirmationCode') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode'];
    }
    else if(route === 'Password') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password'];
    }

    else if(route === 'priceRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute'];
    }
    else if(route === 'inventoryRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute'];
    }
    else if(route === 'charityRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute'];
    }
    else if(route === 'monthlyResetRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute', 'monthlyResetRoute'];
    }
    else if(route === 'celebrityBasicInfoRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute', 'monthlyResetRoute', 'celebrityBasicInfoRoute'];
    }
    else if(route === 'profileMediaRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute', 'monthlyResetRoute', 'celebrityBasicInfoRoute', 'profileMediaRoute'];
    }
    else if(route === 'bioRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute', 'monthlyResetRoute', 'celebrityBasicInfoRoute', 'profileMediaRoute', 'bioRoute'];
    }
    else if(route === 'notificationsRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute', 'monthlyResetRoute', 'celebrityBasicInfoRoute', 'profileMediaRoute', 'bioRoute', 'notificationsRoute'];
    }
    else if(route === 'payoutPreferencesRoute') {
      routeStack = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute', 'monthlyResetRoute', 'celebrityBasicInfoRoute', 'profileMediaRoute', 'bioRoute', 'notificationsRoute', 'payoutPreferencesRoute'];
    }
    else {//if(route === 'AppIntroTour')
      routeStack = ['AppIntroTour'];
    }
  }

  else if(Component === 'CustomerAccount') {
    if(route === 'CustomerBasicInfo') {
      routeStack = ['CustomerAccount', 'CustomerBasicInfo'];
    }
    else if(route === 'PaymentInfo') {
      routeStack = ['CustomerAccount', 'PaymentInfo'];
    }
    else if(route === 'ChangePassword') {
      routeStack = ['CustomerAccount', 'ChangePassword'];
    }
    else if(route === 'Faq') {
      routeStack = ['CustomerAccount', 'Faq'];
    }
    else if(route === 'FaqAnswer') {
      routeStack = ['CustomerAccount', 'Faq', 'FaqAnswer'];
    }
    else {//if(route === 'CustomerAccount')
      routeStack = ['CustomerAccount'];
    }
  }

  else if(Component === 'CelebrityAccount') {
    if(route === 'CelebrityBasicInfo') {
      routeStack = ['CelebrityAccount', 'CelebrityBasicInfo'];
    }
    else if(route === 'CelebrityInventory') {
      routeStack = ['CelebrityAccount', 'CelebrityInventory'];
    }
    else if(route === 'ProfileMedia') {
      routeStack = ['CelebrityAccount', 'ProfileMedia'];
    }
    else if(route === 'Bio') {
      routeStack = ['CelebrityAccount', 'Bio'];
    }
    else if(route === 'Notifications') {
      routeStack = ['CelebrityAccount', 'Notifications'];
    }
    else if(route === 'PayoutPreferences') {
      routeStack = ['CelebrityAccount', 'PayoutPreferences'];
    }
    else if(route === 'CelebrityCustomerOrders') {
      routeStack = ['CelebrityAccount', 'CustomerOrders'];
    }
    else if(route === 'Order') {
      routeStack = ['CelebrityAccount', 'CustomerOrders', 'Order'];
    }
    else if(route === 'Faq') {
      routeStack = ['CelebrityAccount', 'Faq'];
    }
    else if(route === 'FaqAnswer') {
      routeStack = ['CelebrityAccount', 'Faq', 'FaqAnswer'];
    }
    else if(route === 'EditOrder') {
      routeStack = ['CelebrityAccount', 'CustomerOrders', 'Order', 'EditOrder'];
    }
    else if(route === 'CelebrityPaymentInfo') {
      routeStack = ['CelebrityAccount', 'CelebrityPaymentInfo'];
    }
    else if(route === 'CelebrityChangePassword') {
      routeStack = ['CelebrityAccount', 'CelebrityChangePassword'];
    }
    else { //if(route === 'CelebrityAccount')
      routeStack = ['CelebrityAccount'];
    }
  }

  else if(Component === 'CelebrityOwnProfile') {
    //no navigator
  }

  else if(Component === 'CelebrityRequests') {
    if(route === 'Request') {
      routeStack = ['CelebrityRequests', 'Request'];
    }
    else if(route === 'Record') {
      routeStack = ['CelebrityRequests', 'Request', 'Record'];
    }
    else if(route === 'CelebrityConcierge') {
      routeStack = ['CelebrityRequests', 'Request', 'CelebrityConcierge'];
    }
    else if(route === 'VideoPage') {
      routeStack = ['CelebrityRequests', 'Request', 'Record', 'VideoPage'];
      //stateProps.sourceUri = 'celebvidy_team_rep_tour_2';
    }
    else {//if(route === 'CelebrityRequests')
      routeStack = ['CelebrityRequests'];
    }
  }

  else if(Component === 'CustomerOrders') {
    if(route === 'Order') {
      routeStack = ['CustomerOrders', 'Order'];
    }
    else if(route === 'Celebvidy') {
      routeStack = ['CustomerOrders', 'Order', 'Celebvidy'];
      stateProps.youtubeId = 'yW1-9DrmCi4';
    }
    else if(route === 'CustomerConcierge') {
      routeStack = ['CustomerOrders', 'Order', 'CustomerConcierge'];
    }
    else if(route === 'EditOrder') {
      routeStack = ['CustomerOrders', 'Order', 'EditOrder'];
    }
    else {//if(route === 'CustomerOrders')
      routeStack = ['CustomerOrders'];
    }
  }

  else if(Component === 'Industries') {
    //no navigator
  }

  else if(Component === 'Celebrities') {
    if(route === 'Celebrity') {
      routeStack = ['Celebrities', 'Celebrity'];
    }
    else if(route === 'CreateOrder') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder'];
    }
    else if(route === 'PaymentInfo') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder', 'PaymentInfo'];
    }
    else if(route === 'OptionalTxt') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder', 'PaymentInfo', 'OptionalTxt'];
    }
    else if(route === 'Confirm') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder', 'PaymentInfo', 'OptionalTxt', 'Confirm'];
    }
    else if(route === 'ThankYou') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder', 'PaymentInfo', 'OptionalTxt', 'Confirm', 'ThankYou'];
    }
    else if(route === 'CustomerLoginSignup') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder', 'PaymentInfo', 'OptionalTxt', 'CustomerLoginSignup'];
    }
    else if(route === 'EmailConfirmationCode') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder', 'PaymentInfo', 'OptionalTxt', 'CustomerLoginSignup', 'EmailConfirmationCode'];
    }
    else if(route === 'Password') {
      routeStack = ['Celebrities', 'Celebrity', 'CreateOrder', 'PaymentInfo', 'OptionalTxt', 'CustomerLoginSignup', 'EmailConfirmationCode', 'Password'];
    }
    else {//if(route === 'Celebrities')
      routeStack = ['Celebrities'];
    }
  }

  else if(Component === 'Share') {
    //no navigator
  }

  return {
    routeStack,
    stateProps,
  };
}



export function allRoutesInNavigator(topNavigatorComponent, isCelebrity) {
  let routes = [];

  if(topNavigatorComponent === 'AppIntroTour') {
    if(isCelebrity) {
      routes = ['AppIntroTour', 'LoginSignup', 'EmailConfirmationCode', 'Password', 'priceRoute', 'inventoryRoute', 'charityRoute', 'monthlyResetRoute', 'celebrityBasicInfoRoute', 'profileMediaRoute', 'bioRoute', 'notificationsRoute', 'payoutPreferencesRoute'];
    }
    else {
      routes = ['AppIntroTour'];
    }
  }

  else if(topNavigatorComponent === 'CustomerAccount') {
    routes = ['CustomerAccount', 'CustomerBasicInfo', 'PaymentInfo', 'ChangePassword', 'Faq', 'FaqAnswer'];
  }

  else if(topNavigatorComponent === 'CelebrityAccount') {
    routes = ['CelebrityAccount', 'CelebrityBasicInfo', 'CelebrityInventory', 'ProfileMedia', 'Bio', 'Notifications', 'PayoutPreferences', 'CelebrityCustomerOrders', 'Order', 'EditOrder', 'CelebrityPaymentInfo', 'CelebrityChangePassword', 'Faq', 'FaqAnswer'];
  }

  else if(topNavigatorComponent === 'CelebrityRequests') {
    routes = ['CelebrityRequests', 'Request', 'Record', 'CelebrityConcierge', 'VideoPage'];
  }

  else if(topNavigatorComponent === 'CustomerOrders') {
    routes = ['CustomerOrders', 'Order', 'EditOrder', 'CustomerConcierge', 'Celebvidy'];
  }

  else if(topNavigatorComponent === 'Celebrities') {
    routes = ['Celebrities', 'Celebrity', 'CelebvidyDetails', 'PaymentInfo', 'OptionalTxt', 'Confirm', 'ThankYou', 'CustomerLoginSignup', 'EmailConfirmationCode', 'Password'];
  }

  return routes;
}
