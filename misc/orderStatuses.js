const customerStatuses = {
  order_submitted: 				{color: 'rgba(62, 146, 246, 0.8)',  icon: 'clipboard', 			      status: 'pending'},
  order_rejected: 				{color: 'red',                      icon: 'close', 				        status: 'rejected'},
  order_approved: 				{color: 'rgba(8, 83, 251, 0.8)',    icon: 'checkmark', 				    status: 'approved'},

  video_submitted: 				{color: 'rgba(8, 83, 251, 0.8)',    icon: 'checkmark', 			      status: 'approved'},
  video_rejected: 				{color: 'rgba(8, 83, 251, 0.8)',    icon: 'checkmark', 				    status: 'approved'},
  video_approved: 				{color: 'rgba(25, 157, 78, .8)',    icon: 'paper-airplane', 		  status: 'delivered'},

  celeb_rejected: 				{color: 'rgba(8, 83, 251, 0.8)',    icon: 'checkmark', 				    status: 'approved'},

  refunded: 							{color: 'orange',                   icon: 'close', 							  status:  'refunded'},
  credited: 							{color: 'purple',                   icon: 'close', 					      status:  'credited'},

  shared: 							  {color: 'orange',                   icon: 'share', 					      status:  'shared'},
};

export function customerOrderStatus(status) {
  return customerStatuses[status] || {color: 'rgba(62, 146, 246, 0.8)',  icon: 'clipboard', 			      status: 'pending'};
}

export function isOrderEditable(status) {
  status = customerStatuses[status] ? customerStatuses[status].status : 'pending';
  return /pending|approved|rejected/.test(status);
}
