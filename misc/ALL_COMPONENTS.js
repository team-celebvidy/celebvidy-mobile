const CelebrityAccountSetup = require('../components/LoginFlow/CelebrityAccountSetup');
const CircularTouchSliderPages = require('../components/LoginFlow/CircularTouchSliderPages');

const ALL_COMPONENTS = {
  CelebrityAccount: require('../components/CelebrityAccount/CelebrityAccount').default,
  CelebrityBasicInfo: require('../components/CelebrityAccount/CelebrityBasicInfo').default,
  CelebrityInventory: require('../components/CelebrityAccount/CelebrityInventory').default,
  ProfileMedia: require('../components/CelebrityAccount/ProfileMedia').default,
  Bio: require('../components/CelebrityAccount/Bio').default,
  Notifications: require('../components/CelebrityAccount/Notifications').default,
  PayoutPreferences: require('../components/CelebrityAccount/PayoutPreferences').default,
  CelebrityCustomerOrders: require('../components/CelebrityAccount/CelebrityCustomerOrders').default,
  CelebrityPaymentInfo: require('../components/CelebrityAccount/CelebrityPaymentInfo').default,
  CelebrityChangePassword: require('../components/CelebrityAccount/CelebrityChangePassword').default,
  CelebrityTerms: require('../components/CelebrityAccount/CelebrityTerms').default,

  CelebrityRequests: require('../components/CelebrityRequests').default,
  Request: require('../components/RecordFlow/Request').default,
  Record: require('../components/RecordFlow/Record').default,
  VideoPage: require('../components/RecordFlow/VideoPage').default,
  CelebrityConcierge: require('../components/RecordFlow/CelebrityConcierge').default,

  CustomerAccount: require('../components/CustomerAccount/CustomerAccount').default,
  CustomerBasicInfo: require('../components/CustomerAccount/CustomerBasicInfo').default,
  CustomerOrders: require('../components/CustomerOrders').default,
  ChangePassword: require('../components/ChangePassword').default,

  Faq: require('../components/Faq').default,
  FaqAnswer: require('../components/Faq').FaqAnswer,

  Order: require('../components/CustomerOrderFlow/Order').default,
  EditOrder: require('../components/CustomerOrderFlow/EditOrder').default,
  Celebvidy: require('../components/CustomerOrderFlow/Celebvidy').default,
  CustomerConcierge: require('../components/CustomerOrderFlow/CustomerConcierge').default,

  Celebrities: require('../components/Celebrities').default,
  Celebrity: require('../components/Celebrity').default,
  CreateOrder: require('../components/CheckoutFlow/CreateOrder').default,
  OptionalTxt: require('../components/CheckoutFlow/OptionalTxt').default,
  PaymentInfo: require('../components/CheckoutFlow/PaymentInfo').default,
  Confirm: require('../components/CheckoutFlow/Confirm').default,
  ThankYou: require('../components/CheckoutFlow/ThankYou').default,

  CelebvidyDetails: require('../components/CelebvidyDetails').default,

  CelebrityOwnProfile: require('../components/CelebrityOwnProfile').default,
  Industries: require('../components/Industries').default,
  Share: require('../components/Share').default,

  AppIntroTour: require('../components/LoginFlow/TourFlow/AppIntroTour').default,
  CelebrityPin: require('../components/LoginFlow/TourFlow/CelebrityPin').default,
  LoginSignup: require('../components/LoginFlow/LoginSignup').default,
  CustomerLoginSignup: require('../components/CustomerAccount/CustomerLoginSignup').default,
  EmailConfirmationCode: require('../components/LoginFlow/EmailConfirmationCode').default,
  Password: require('../components/LoginFlow/Password').default,

  Price: CircularTouchSliderPages.price,
  Inventory: CircularTouchSliderPages.Inventory,
  Charity: CircularTouchSliderPages.Charity,
  MonthlyReset: CircularTouchSliderPages.MonthlyReset, //touch slider pages

  priceRoute: CelebrityAccountSetup.priceRoute,
  inventoryRoute: CelebrityAccountSetup.inventoryRoute,
  charityRoute: CelebrityAccountSetup.charityRoute,
  monthlyResetRoute: CelebrityAccountSetup.monthlyResetRoute,
  celebrityBasicInfoRoute: CelebrityAccountSetup.celebrityBasicInfoRoute,
  profileMediaRoute: CelebrityAccountSetup.profileMediaRoute,
  bioRoute: CelebrityAccountSetup.bioRoute,
  notificationsRoute: CelebrityAccountSetup.notificationsRoute,
  payoutPreferencesRoute: CelebrityAccountSetup.payoutPreferencesRoute,
};

export default ALL_COMPONENTS;

window.ALL_COMPONENTS = ALL_COMPONENTS;
