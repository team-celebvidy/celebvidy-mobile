export function phone(numString='') {
  numString = numString.replace(/\D/g,'');

  let num = '';
  let one = '';

  if(numString[0] === '1')  {
    one = '1-';
    if(numString.length === 1) {
      return '1';
    }
    else {
      numString = numString.slice(1);
    }
  }

  if(numString.length <= 3) {
    return one+numString;
  }

  if(numString.length > 3) {
    num = numString.slice(0, 3) + '-' + numString.slice(3, 6);
  }

  if(numString.length > 6) {
    num += '-' + numString.slice(6);
  }

  return (one+num).substr(0, one ? 14 : 12);
}

export function ssn(numString='') {
  numString = numString.replace(/-/g, '');

  let num = '';

  if(numString.length <= 3) {
    return numString;
  }

  if(numString.length > 3) {
    num = numString.slice(0, 3) + '-' + numString.slice(3, 5);
  }

  if(numString.length > 5) {
    num += '-' + numString.slice(5);
  }

  return num;
}

export function ein(numString='') {
  numString = numString.replace(/-/g, '');

  if(numString.length <= 2) {
    return numString;
  }
  else {
    return numString.slice(0, 2) + '-' + numString.slice(2);
  }
  
}
