import { Linking } from 'react-native';
import _ from 'underscore';

import { dispatch, getState } from '../configureStore';
import * as actions from '../actions';
import { goToCustomerOrders, goToCelebrityRequests, goToCelebrities } from './goTo.js';
import ddpClient from '../ddp';
const ddp = ddpClient();

let lastInBackground = new Date;

export function listenToIncomingLinks(notification, isFirstOpen=false) {
  Linking.addEventListener('url', ({url}) => handleIncomingLink(url));
  Linking.getInitialURL().then(handleIncomingLink);
}

function handleIncomingLink(url) {
  if(!url) return; //may be no initialUrl

  let path = url.substr(url.indexOf('.com') + 5);
  let [type, _id] = path.split('/');

  if(type === 'video') { //eg: celebvidy.com/video/98usdfhiu
    let orderIds = getState().customerOrders.map(order => order._id);

    if(!_.contains(orderIds, _id)) { //if the user doesn't have this order ID it's cuz it was SHARED with them, in which case we present the row on the Customer Orders page, but only link to the video (not the order details)
      actions.apiFetch('find-celebvidy', [{_id}])
        .then((payload) => dispatch({type: 'RECEIVED_CELEBVIDY', payload})); //payload is celebvidy transaction object
    }

    goToCustomerOrders(_id);
  }
  else if(type === 'order') { //eg: celebvidy.com/order/98usdfhiu
    goToCustomerOrders(_id);
  }
  else if(type === 'request') { //eg: celebvidy.com/request/98usdfhiu
    goToCelebrityRequests(_id);
  }
  else if(type === 'customer-concierge') { //eg: celebvidy.com/order/98usdfhiu
    goToCustomerOrders(_id);
  }
  else if(type === 'celebrity-concierge') { //eg: celebvidy.com/order/98usdfhiu
    goToCelebrityRequests(_id);
  }
  else if(type && !_id) { //eg: celebvidy.com/steve-aoki
    let slug = type; //type is in fact a slug corresponding to a celebrity name

    actions.apiFetch('find-celebrity', [{slug}])
      .then((payload) => dispatch({type: 'CELEBRITY_FROM_URL', payload})); //payload is celebrity object

    goToCelebrities();
  }
}
