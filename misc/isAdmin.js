export function isAdmin(customer, celebrity) {
  return (customer && isAdminEmail(customer.email)) || (celebrity && isAdminEmail(celebrity.email));
}

export function isAdminEmail(email) {
  return /@celebvidy\.com|@faceyspacey\.com|jamesgillmore@gmail\.com|zhurst@gmail\.com|austinhurst@gmail\.com|austin@hurstcap\.com|dan@victorypoker\.net|davidmaisel@ymail\.com/.test(email);
}

export function hasAutoApproveRequest(customer, celebrity) {
  return (customer && customer.automatically_approve_request) || (celebrity && celebrity.automatically_approve_request);
}

export function hasAutoApproveVideo(customer, celebrity) {
  return (customer && customer.automatically_approve_video) || (celebrity && celebrity.automatically_approve_video);
}
