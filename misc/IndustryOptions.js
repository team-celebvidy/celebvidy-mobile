import { Dimensions } from 'react-native';

import cloudinary from './cloudinary';


const INDUSTRIES = [
  {color: 'rgb(22, 30, 220)', name: 'All Celebrities', singularName: 'Celebrity', count: 149, min: 20, max: 1000},
  {color: 'rgb(44, 52, 82)', name: 'Athletes', singularName: 'Athlete', count: 20, min: 20, max: 1000},
  {color: 'rgb(86, 22, 255)', name: 'Musicians', singularName: 'Musician', count: 55, min: 20, max: 1000},
  {color: 'rgb(180, 28, 14)', name: 'Actors', singularName: 'Actor', count: 34, min: 20, max: 1000},
  {color: 'rgb(152, 48, 189)', name: 'Comedians', singularName: 'Comedian', count: 5, min: 20, max: 1000},
  {color: 'rgb(201, 134, 19)', name: 'Influencers', singularName: 'Influencer', count: 7, min: 20, max: 1000},
  {color: 'rgb(46, 40, 3)', name: 'Culinary Experts', singularName: 'Culinary Expert', count: 23, min: 20, max: 1000},
  {color: 'rgb(203, 21, 103)', name: 'Dancers', singularName: 'Dancer', count: 5, min: 20, max: 1000},
  {color: 'rgb(143, 164, 148)', name: 'Leaders', singularName: 'Leader', count: 0, min: 20, max: 1000},
  {color: 'rgb(22, 64, 172)', name: 'TV Personalities', singularName: 'TV Personality', count: 10, min: 20, max: 1000},
  {color: 'rgb(0, 130, 195)', name: 'Fashion Icons', singularName: 'Fashion Icon', count: 7, min: 20, max: 1000},
  {color: 'rgb(208, 190, 11)', name: 'Other Trendsetters', singularName: 'Trendsetter', count: 33, min: 20, max: 1000},
];

export default INDUSTRIES;


export const INDUSTRY_SOURCES = {
  'all-celebrities': require('../images/industries/all-celebrities.jpg'),
  'athletes': require('../images/industries/athletes.jpg'),
  'musicians': require('../images/industries/musicians.jpg'),
  'actors': require('../images/industries/actors.jpg'),
  'comedians': require('../images/industries/comedians.jpg'),
  'influencers': require('../images/industries/influencers.jpg'),
  'culinary-experts': require('../images/industries/culinary-experts.jpg'),
  'dancers': require('../images/industries/dancers.jpg'),
  'leaders': require('../images/industries/leaders.jpg'),
  'tv-personalities': require('../images/industries/tv-personalities.jpg'),
  'fashion-icons': require('../images/industries/fashion-icons.jpg'),
  'other-trendsetters': require('../images/industries/other-trendsetters.jpg'),
};


const width = Dimensions.get('window').width;
const height = width / 2;
const prefix = 'company/industries';
const options = {prefix, width, height};

export function getIndustryImageSource(industry) {
  let name = industry.name.toLowerCase().replace(/ /g, '-');

  if(INDUSTRY_SOURCES[name] && !industry.forceUpdate) {
    return INDUSTRY_SOURCES[name];
  }
  else {
    return cloudinary.source(`${name}.jpg`, options);
  }
}


export const CELEB_INDUSTRIES = INDUSTRIES.slice(1);

export function colorForIndustry(name) {
  return INDUSTRIES.reduce((acc, industry) => {
    return industry.name === name && industry.color || acc;
  }, 0)
}

export function singularIndustryName(name) {
  if(!name) return 'Celebrity';
  
  return INDUSTRIES.reduce((acc, industry) => {
    return industry.name === name && industry.singularName || acc;
  }, 0)
}
