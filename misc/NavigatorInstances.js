const NavigatorInstances = {};
export default NavigatorInstances;
//this global exists here rather than in /widgets/navigator.js so DeveloperMenu can import it without
//being re-patched during hot reloading when the Navigator is re-patched, which is often since it imports every Component in the app
