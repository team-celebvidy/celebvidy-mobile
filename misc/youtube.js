import { DeviceEventEmitter } from 'react-native';
import React, { Component } from 'react';
var RNUploader = require('NativeModules').RNUploader;
var RNFS = require('react-native-fs');

import {ENVIRONMENT} from '../config';
import ddpClient from '../ddp';
const ddp = ddpClient();

import * as actions from '../actions';
import {dispatch} from '../configureStore';


export function youtubeUpload(sourceUri, duration, order, onProgress, onComplete) {
  youtubeUploadOnProgress(onProgress);

  sourceUri = sourceUri.replace('file:///private/', '');
  let permanentFilePath = RNFS.DocumentDirectoryPath + '/celebvidies/'+order._id+'.mp4';
  let youtubeId;

  return RNFS.mkdir(RNFS.DocumentDirectoryPath + '/celebvidies', true)
    .then(() => RNFS.unlink(permanentFilePath))
    .then(() => RNFS.moveFile(sourceUri, permanentFilePath))
    .catch(() => RNFS.moveFile(sourceUri, permanentFilePath)) //an error is thrown if no old video file to remove, but in both cases we do the same thing
    .then(() => {
      return Promise.all([
        ddp.callPromise('youtubeAccessToken', [null, {ENVIRONMENT}]),
        RNFS.stat(permanentFilePath),
      ]);
    })
    .then(([accessToken, {size}]) => uploadVideo(accessToken, permanentFilePath, size))
    .then(({youtube_id, accessToken}) => setMetaData(accessToken, youtube_id, order))
    .then((youtube_id) => {
      onProgress(101);
      dispatch(actions.addYoutubeVideo({_id: order._id, youtube_id, duration}));
      youtubeId = youtube_id;
    })
    .then(() => onProgress(102)) //102 is interpreted as 100 by ModalProgress, which closes once it reaches 100 (see `percent` prop in VideoPage -- -2 is subtracted from percent to facilitate not completing until we signal it here)
    .then(() => {
      onComplete(youtubeId);
      return permanentFilePath;
    }); //return file path so calling code can use it to display video from local source until youtube is ready
}

function uploadVideo(accessToken, filePath, fileSize) {
  let options = {
    url: 'https://www.googleapis.com/upload/youtube/v3/videos?part=snippet,status',
    files: [
      {
        name: 'file[]',
        filename: filePath.replace(/^.*[\\\/]/g, ''),
        filepath: filePath,
        filetype: 'video/mp4',
      }
    ],
    method: 'POST',
    params: {},
    headers: {
      'ContentType': 'application/json',
      'Authorization': 'Bearer ' + accessToken,
      'x-upload-content-length': fileSize,
      'x-upload-content-type': 'video/mp4',
    },
    //chunkedMode: false,
  };

  return new Promise((resolve, reject) => {
    RNUploader.upload(options, (error, res) => {
      if(error) {
        console.log('UPLOAD ERROR', error);
        return reject(error);
      }

      console.log('RESPONSE', res, JSON.parse(res.data));

      let youtube_id = JSON.parse(res.data).id;
      resolve({youtube_id, accessToken});
    });
  });
}



export function youtubeUploadOnProgress(onProgress) {
  DeviceEventEmitter.addListener('RNUploaderProgress', (data)=>{
      let bytesWritten = data.totalBytesWritten;
      let bytesTotal = data.totalBytesExpectedToWrite;
      let progress = data.progress;

      onProgress(parseInt(progress));

      if(progress >= 100) {
        DeviceEventEmitter.removeAllListeners('RNUploaderProgress');
      }
    });
}


function setMetaData(accessToken, youtube_id, {recipient, celebrity_name, kind, showcase}) {
  let metaData = {
    id: youtube_id,
    kind: "youtube#video",
    snippet: {
      title: `Celebvidy for ${recipient} (${kind || 'AMA'} from ${celebrity_name})`,
      description: `ON-DEMAND Shoutout for ${recipient} (${kind || 'AMA'} from ${celebrity_name}) via Celebvidy.com`,
      tags: [recipient, kind, celebrity_name, 'celebvidy', 'on-demand', 'shoutout'],
      categoryId: 24,
    },
    status: {
      embeddable: true,
      publicStatsViewable: true,
      privacyStatus: showcase ? 'public' : 'unlisted',
    }
  };

  return fetch("https://www.googleapis.com/youtube/v3/videos?part=snippet,status", {
    method: 'PUT',
    headers: {
      'Authorization': 'Bearer ' + accessToken,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(metaData)
  })
    .then(() => youtube_id);
}
