import { DeviceEventEmitter } from 'react-native';
import React, { Component } from 'react';
var RNUploader = require('NativeModules').RNUploader;
var RNFS = require('react-native-fs');

import ddpClient from '../ddp';
const ddp = ddpClient();

import * as actions from '../actions';
import {dispatch} from '../configureStore';


export function youtubeUpload(sourceUri, duration, celebrity, onProgress, onComplete) {
  youtubeUploadOnProgress(onProgress);

  sourceUri = sourceUri.replace('file:///private/', '');
  let filePath = RNFS.DocumentDirectoryPath + '/videos/profile-video.mp4';

  return RNFS.mkdir(RNFS.DocumentDirectoryPath + '/videos', true)
    .then(() => RNFS.unlink(filePath))
    .then(() => RNFS.moveFile(sourceUri, filePath))
    .catch(() => RNFS.moveFile(sourceUri, filePath)) //an error is thrown if no old video file to remove, but in both cases we do the same thing
    .then(() => {
      return Promise.all([
        ddp.callPromise('youtubeAccessToken'),
        RNFS.stat(filePath),
      ]);
    })
    .then(([accessToken, {size}]) => uploadVideo(accessToken, filePath, size))
    .then(({youtube_id, accessToken}) => setMetaData(accessToken, youtube_id, celebrity.display_name))
    .then((youtube_id) => onProgress(101) && dispatch(actions.updateCelebrity({youtube_id, duration})))
    .then(() => onProgress(102)) //102 is interpreted as 100 by ModalProgress, which closes once it reaches 100 (see `percent` prop in VideoPage -- -2 is subtracted from percent to facilitate not completing until we signal it here)
    .then(() => {
      onComplete();
      return filePath;
    }); //return file path so calling code can use it to display video from local source until youtube is ready
}

function uploadVideo(accessToken, filePath, fileSize) {
  let options = {
    url: 'https://www.googleapis.com/upload/youtube/v3/videos?part=snippet,status',
    files: [
      {
        name: 'file[]',
        filename: filePath.replace(/^.*[\\\/]/g, ''),
        filepath: filePath,
        filetype: 'video/mp4',
      }
    ],
    method: 'POST',
    params: {},
    headers: {
      'ContentType': 'application/json',
      'Authorization': 'Bearer ' + accessToken,
      'x-upload-content-length': fileSize,
      'x-upload-content-type': 'video/mp4',
    },
    //chunkedMode: false,
  };

  return new Promise((resolve, reject) => {
    RNUploader.upload(options, (error, res) => {
      if(error) {
        console.log('UPLOAD ERROR', error);
        return reject(error);
      }

      console.log('RESPONSE', res, JSON.parse(res.data));

      let youtube_id = JSON.parse(res.data).id;
      resolve({youtube_id, accessToken});
    });
  });
}



export function youtubeUploadOnProgress(onProgress) {
  DeviceEventEmitter.addListener('RNUploaderProgress', (data)=>{
      let bytesWritten = data.totalBytesWritten;
      let bytesTotal = data.totalBytesExpectedToWrite;
      let progress = data.progress;

      onProgress(parseInt(progress));

      if(progress >= 100) {
        DeviceEventEmitter.removeAllListeners('RNUploaderProgress');
      }
    });
}


function setMetaData(accessToken, youtube_id, celebrityName) {
  let metaData = {
    id: youtube_id,
    kind: "youtube#video",
    snippet: {
      title: `${celebrityName} on Celebvidy`,
      description: `ON-DEMAND Shoutouts from ${celebrityName} via Celebvidy.com`,
      tags: [celebrityName, 'celebvidy', 'on-demand', 'shoutout'],
      categoryId: 24,
    },
    status: {
      embeddable: true,
      publicStatsViewable: true,
      privacyStatus: 'public', //'unlisted',
    }
  };

  return fetch("https://www.googleapis.com/youtube/v3/videos?part=snippet,status", {
    method: 'PUT',
    headers: {
      'Authorization': 'Bearer ' + accessToken,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(metaData)
  })
    .then(() => youtube_id);
}
