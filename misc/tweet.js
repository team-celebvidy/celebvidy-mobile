var KDSocialShare = require('NativeModules').KDSocialShare;

export default function tweet(text, image, link, onSuccess, onFail) {
  KDSocialShare.tweet({
      text: text || 'Chek out my profile on Celebvidy.com:',
      link: link || 'https://www.celebvidy.com/',
      //imagelink: imageLink || 'https://artboost.com/apple-touch-icon-144x144.png',
      image: image || 'splash',
    }, (results) => {
      console.log(results);

      let success = true;

      if (success) onSuccess && onSuccess();
      else onFail && onFail();
    }
  );
}
