import { PushNotificationIOS, Alert, Vibration, AppState } from 'react-native';

import {dispatch, getState} from '../configureStore';
import * as actions from '../actions';
import {goToCelebrityRequests, goToCustomerOrders} from './goTo.js';
import {ENVIRONMENT} from '../config';
import ddpClient from '../ddp';
const ddp = ddpClient();

let lastInBackground = new Date;

function handleNotification(notification, isFirstOpen=false) {
  let {type, _id} = notification.getData();
  let message = notification.getAlert();
  PushNotificationIOS.setApplicationIconBadgeNumber(0); //remove badge count on receive notification and on first load of app (ideally in the future it's only removed only when the associated order/request is viewed)
  ddp.call('setBadgeCountToZero');

  if(!isFirstOpen) {
    Vibration.vibrate();
  }

  let route = getState().selectedRoute;
  if(/Record|Concierge/.test(route)) {
    return;
  }

  console.log('HANDLE NOTIFICATION', AppState.currentState, isFirstOpen, notification);

  let go = () => {
    if(getState().isCelebrity) {
      if(type === 'request') {
        goToCelebrityRequests(_id); //push the corresponding row in the navigator by _id
      }
      else if(type === 'order') {
        goToCustomerOrders(_id);
      }
    }
    else {
      if(type === 'order') {
        goToCustomerOrders(_id);
      }
    }
  };

  if(isFirstOpen || AppState.currentState === 'background' || lastInBackground > new Date - 2000) { //background state sometimes occurs when you re-open the app while it was just in the background. Other times, you can detect if it just changed states within the last 2 seconds.
    go();
  }
  else {
    Alert.alert('Notification', message + ' - CHECK THIS OUT?', [
        {text: 'LATER', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'YES', onPress: () => go()},
      ]
    );
  }
}


export function listenToPushNotifications(userType) {
  PushNotificationIOS.setApplicationIconBadgeNumber(0);
  PushNotificationIOS.addEventListener('notification', handleNotification);

  let notification = PushNotificationIOS.getInitialNotification ? PushNotificationIOS.getInitialNotification() : PushNotificationIOS.popInitialNotification();

  if(notification) {
    console.log('ON OPEN NOTIFICATION', getState().selectedRoute)
    handleNotification(notification, true);
  }

  if(isNonGuest()) {
    ddp.call('userActive', [true]);
  }

  AppState.addEventListener('change', (state) => {

    if(state === 'active') {
      lastInBackground = new Date;

      if(isNonGuest()) {
        ddp.call('userActive', [true]);
      }
    }
    else if(state === 'background') {
      if(isNonGuest()) {
        ddp.call('userActive', [false]);
      }
    }
  });
}


function isNonGuest() {
  let {celebrity, customer} = getState();
  return !!(celebrity || customer);
}

export function registerPushNotifications(callback) {
  let handler = (device_token) => {
    callback && callback();
    ddp.call('registerPushNotifications', [{device_token}, {ENVIRONMENT}]);
    PushNotificationIOS.removeEventListener('register', handler);
  };

  PushNotificationIOS.addEventListener('register', handler);
  PushNotificationIOS.requestPermissions();

  let removeListener = () => PushNotificationIOS.removeEventListener('register', handler);
  return removeListener;
}

window.registerPushNotifications = registerPushNotifications;
