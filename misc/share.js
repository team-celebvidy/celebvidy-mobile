import { ActionSheetIOS } from 'react-native';
import React, { Component } from 'react';

export default function showShareActionSheet(message, url, onSuccess, onFail) {
  ActionSheetIOS.showShareActionSheetWithOptions({
    url: url || 'http://www.celebvidy.com',
    message: message || 'Checkout Celebvidy.com',
    excludedActivityTypes: [
      //'com.apple.UIKit.activity.PostToTwitter'
    ]
  },
  (error) =>  onFail && onFail(),
  (success, method) => {
    if (success) onSuccess && onSuccess();
    else onFail && onFail();
  });
}
