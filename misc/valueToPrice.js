import formatMoney from 'format-money';

export default function valueToPrice(value, format=true) {
  let price;

  if(value <= 50) { // $20 - $500
    price = parseInt(value) * 10;
    if(price <= 20) price = 20;
  }
  else if (value < 75) { // $550 - $950
    if		 (value < 53) price = 550;
    else if(value < 56) price = 600;
    else if(value < 59) price = 650;
    else if(value < 62) price = 700;
    else if(value < 65) price = 750;
    else if(value < 67) price = 800;
    else if(value < 69) price = 850;
    else if(value < 71) price = 900;
    else if(value < 75) price = 950;

  }
  else if (value <= 100) { // $1,000 - $10,000
    if     (value < 78) price = 1000;
    else if(value < 82) price = 2000;
    else if(value < 85) price = 3000;
    else if(value < 87) price = 4000;
    else if(value < 89) price = 5000;
    else if(value < 91) price = 6000;
    else if(value < 93) price = 7000;
    else if(value < 95) price = 8000;
    else if(value < 97) price = 9000;
    else if(value <= 100) price = 10000;
  }

  return format ? formatMoney(price, false) : price;
}




export function priceToValue(price, defaultValue) {
  if(!price) return defaultValue;

  let value;
  price = parseInt((price+'').replace('$', '').replace(',', ''));

  if(price <= 500) { // $20 - $500
    value = price / 10;
    if(value <= 2) value = 2;
  }
  else if (price < 1000) { // $550 - $950
    if		 (price === 600) value = 52;
    else if(price === 600) value = 55;
    else if(price === 650) value = 58;
    else if(price === 700) value = 61;
    else if(price === 750) value = 64;
    else if(price === 800) value = 66;
    else if(price === 850) value = 68;
    else if(price === 900) value = 70;
    else if(price === 950) value = 74;
  }

  else if (price <= 20000) { // // $1,000 - $10,000
    if     (price === 1000) value = 77;
    else if(price === 2000) value = 81;
    else if(price === 3000) value = 84;
    else if(price === 4000) value = 86;
    else if(price === 5000) value = 88;
    else if(price === 6000) value = 90;
    else if(price === 7000) value = 92;
    else if(price === 8000) value = 94;
    else if(price === 9000) value = 96;
    else if(price === 10000) value = 100;
  }

  return value;
}
