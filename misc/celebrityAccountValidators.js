export default {
  name: {
    title: 'Name',
    validate: [{
      validator: 'isLength',
      arguments: [1, 48],
      message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
    }]
  },
  email: {
    title: 'Email address',
    validate: [{
      validator: 'isLength',
      arguments: [6, 255],
    },{
      validator: 'isEmail',
    }]
  },
  tagline: {
    title: 'Tag Line',
    validate: [{
      validator: 'isLength',
      arguments: [1, 96],
      message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
    }]
  },
  bio: {
    title: 'Bio',
    validate: [{
      validator: 'isLength',
      arguments: [0, 5000],
      message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
    }]
  },


  password: {
    title: 'Password',
    validate: [{
      validator: 'isLength',
      arguments: [6, 16],
      message: '{TITLE} must be between {ARGS[0]} and {ARGS[1]} characters'
    }]
  },
};
