import * as XCODE_CONFIG from './generatedBuildConfig';

export const IP = XCODE_CONFIG.IP || 'localhost';
export const BUNDLE_ID = __DEV__ ? 'com.celebvidy.CelebvidyDev' : XCODE_CONFIG.BUNDLE_ID;
export const SENTRY_RELEASE = __DEV__ ? 'development_2' : 'production_2';


let environment;

switch(BUNDLE_ID) {
  case 'com.celebvidy.CelebvidyDev':
    environment = 'development';
    break;
  case 'com.celebvidy.CelebvidyStaging':
    environment = 'staging';
    break;
  case 'com.celebvidy.Celebvidy':
    environment = 'production';
    break;
}

export const ENVIRONMENT = environment;


let cloudName = environment === 'production' ? 'celebvidy' : 'celebvidy-staging';
export const BASE_IMAGE_URL = `http://res.cloudinary.com/${cloudName}/image/upload`;

