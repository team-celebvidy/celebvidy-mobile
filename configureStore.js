import { Platform } from 'react-native';
import React, { Component } from 'react';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';

import thunk from 'redux-thunk';
import * as storage from 'redux-storage'
import filter from 'redux-storage-decorator-filter'
import createEngine from 'redux-storage-engine-reactnativeasyncstorage';
import devTools from 'remote-redux-devtools';
import { LOAD, SAVE } from 'redux-storage';
import {HOST} from './config';

import * as reducers from './reducers';
//dont import actions or actionTypes -- it breaks hot module replacement of redux/reducers


let store;
let engine;
let reducer;
let enhancer;

export default function configureStore() {
  engine = createEngine('appstate');
  engine = filter(engine, null, ['appStateLoaded', 'appReady', 'tourReady', 'tourPage', 'tooltipStep', 'connected', 'showOrders']);
  const cacheState = storage.createMiddleware(engine); //OLD PARAMETER:  , [LOAD, 'SET_TOUR_READY', 'SET_TOUR_PAGE']  ... replaced by reducer key filtering above instead

  if(__DEV__) {
    enhancer = compose(
      applyMiddleware(thunk, cacheState),
      /**
      devTools({
        name: Platform.OS,
        hostname: HOST, //'10.0.1.201',
        port: 5678,
        filters: {blacklist: ['REDUX_STORAGE_SAVE']}
      })
      **/
    );
  }
  else {
    enhancer = compose(applyMiddleware(thunk, cacheState));
  }

  //reset reducer state on LOGOUT
  const appReducer = combineReducers(reducers);
  const rootReducer = (state, action) => {
    if (action.type === 'LOGOUT') {
      let {celebrities, celebrityPin, receivedCelebvidys} = state; //preserve these fields even on log out
      state = undefined;
      state = appReducer(state, action);

      state.celebrities = celebrities;
      state.celebrityPin = celebrityPin;
      state.receivedCelebvidys = receivedCelebvidys;

      return state;
    }

    return appReducer(state, action);
  }

  reducer = storage.reducer(rootReducer);


  store = createStore(reducer, enhancer);

  //retreive previously cached state and inject into Redux store!
  const load = storage.createLoader(engine);
  load(store)
      .then((newState) => console.log('Loaded state:', newState))
      .catch(() => console.log('Failed to load previous state'));


  if(__DEV__ && module.hot) {
    module.hot.accept(() => {
      let nextRootReducer = storage.reducer(rootReducer);
      store.replaceReducer(nextRootReducer);
      load(store).then((newState) => console.log('Loaded state:', newState))
    });
  }

  return store;
}

//These functions unfortunately don't work any more after a reducer is replaced during HMR :(
//Try not to use them!
export function getStore() {
  return store || createStore(reducer, enhancer); //after hot module replacement of redux files (reducers.js, actions.js, etc) store becomes undefined
}

export function dispatch(...args) {
  return getStore().dispatch(...args);
}

export function getState() {
  return getStore().getState();
}

export function clearStorage() {
  engine.save({});
}

window.clearStorage = clearStorage
