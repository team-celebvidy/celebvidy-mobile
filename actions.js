import * as types from './actionTypes';
const API_REDUCERS_BY_ACTION_TYPE = types.API_REDUCERS_BY_ACTION_TYPE;
import ddpClient from './ddp';
import _ from 'underscore';
import {GiftedFormManager} from 'react-native-gifted-form';
import {HOST, ENVIRONMENT} from './config';
import Mixpanel from 'react-native-mixpanel';

const ddp = ddpClient();

export function setDeveloperState(developerTestState) {
  return (dispatch, getState) => {
    let state = Object.assign({}, getState(), developerTestState); //make sure any state toggled in dev menu persists
    state.scrollEnabled = state.showTabBar && !state.showDrawer;

    dispatch({
      type:  types.SET_DEVELOPER_STATE,
      payload: state,
    });
  };
}


export function selectComponent(component) {
  return {
    type: types.SELECT_COMPONENT,
    payload: component
  };
}

export function selectRoute(routeComponent) {
  return {
    type: types.SELECT_ROUTE,
    payload: routeComponent
  };
}

export function logout() {
  return (dispatch, getState) => {
     dispatch({type: types.SHOW_TOUR});
     setTimeout(() => {
       dispatch({type: types.LOGOUT});
       ddp.logoutPromise();
     }, 3050);
  };
}


export function goToPage(page) {
  return (dispatch, getState) => {
    dispatch({
      type: types.GO_TO_PAGE,
      payload: {page: page, userType: getState().isCelebrity ? 'celebrity' : 'customer'} //selectedComponent reducer selects component based on page and user type
    });
  };
}


export function setUserType(userType, tourReady) {
  return {
    type: userType === 'customer' ? types.SET_USER_TYPE_TO_CUSTOMER : types.SET_USER_TYPE_TO_CELEBRITY,
    payload: tourReady
  };
}

export function toggleTabBar(show) {
  if(typeof show !== 'undefined') {
    return {
      type: show ? types.SHOW_TAB_BAR : types.HIDE_TAB_BAR,
    };
  }

  return (dispatch, getState) => {
    dispatch({ type: getState().showTabBar ? types.HIDE_TAB_BAR : types.SHOW_TAB_BAR });
  };
}

export function toggleStatusBar(show) {
  if(typeof show !== 'undefined') {
    return {
      type: show ? types.SHOW_STATUS_BAR : types.HIDE_STATUS_BAR,
    };
  }

  return (dispatch, getState) => {
    dispatch({ type: getState().showStatusBar ? types.HIDE_STATUS_BAR : types.SHOW_STATUS_BAR });
  };
}

//toggle drawer's reducer uses current state to automatically determine whether to open or cloase
//since hamburger menu has no idea of the state, but we preserve typical show/hide interface as well for toggling after celebrity tour
export function toggleDrawer(show) {
  if(typeof show !== 'undefined') {
    return {
      type: show ? types.SHOW_DRAWER : types.HIDE_DRAWER,
    };
  }

  return (dispatch, getState) => {
    dispatch({ type: getState().showDrawer ? types.HIDE_DRAWER : types.SHOW_DRAWER });
  };
}

export function toggleIntroAnimation(show) {
  if(typeof show !== 'undefined') {
    return {
      type: show ? types.SHOW_INTRO_ANIMATION : types.HIDE_INTRO_ANIMATION,
    };
  }

  return (dispatch, getState) => {
    dispatch({ type: getState().showIntroAnimation ? types.HIDE_INTRO_ANIMATION : types.SHOW_INTRO_ANIMATION });
  };
}


export function toggleTourReady(show) {
  return (dispatch, getState) => {
    dispatch({ type: getState().tourReady ? types.SET_TOUR_NOT_READY : types.SET_TOUR_READY });
  };
}
export function toggleTourAgain(show) {
  return (dispatch, getState) => {
    dispatch({ type: getState().tourAgain ? types.SET_TOUR_AGAIN_FALSE : types.SET_TOUR_AGAIN_TRUE });
  };
}
export function toggleFinishLater(show) {
  return (dispatch, getState) => {
    dispatch({ type: getState().finishLater ? types.SET_FINISH_LATER_FALSE : types.SET_FINISH_LATER_TRUE });
  };
}


export function toggleTour(show, type) {
  return {
    type: show ? types.SHOW_TOUR : types.HIDE_TOUR,
    payload: type,
  };
}

export function setTourReady() {
  return {
    type: types.SET_TOUR_READY,
  };
}
export function setTourPage(page) {
  return {
    type: types.SET_TOUR_PAGE,
    payload: page
  };
}
export function unclipTour() {
  return {
    type: types.UNCLIP_TOUR,
  };
}
export function clipTour() {
  return {
    type: types.CLIP_TOUR,
  };
}

export function unclipPanel() {
  return {
    type: types.UNCLIP_PANEL,
  };
}
export function clipPanel() {
  return {
    type: types.CLIP_PANEL,
  };
}


export function selectCelebritiesTab(index) {
  return {
    type: types.SELECT_CELEBRITIES_TAB,
    payload: index,
  };
}

export function selectRequestsTab(index) {
  return {
    type: types.SELECT_REQUESTS_TAB,
    payload: index,
  };
}

export function selectPayoutTypeTab(index) {
  return {
    type: types.SELECT_PAYOUT_TYPE_TAB,
    payload: index,
  };
}


export function gratify(payload={}) {
  return (dispatch, getState) => {
    dispatch({type: 'GRATIFY_ANIMATION', payload});
  };
}



function requestResponse(actionType, response) {
  return {
    type: actionType,
    payload: response,
  };
}


function requestError(actionType, error) {
  return (dispatch, getState) => {
    dispatch({
      type: actionType+'_FETCHING',
      payload: false,
    });

    ///potential to dispatch caught error object, but currently they are being imperatively caught in components
  }
}


export function apiFetch(apiMethod, args) {
  let params = args[0];
  let options = args[1]; //not currently used, but the idea is it will pass global info about the user, e.g. user agent, bundle_id, etc, the ddp methods already use it to pass the environment, so that one server can send push notifications to both production and staging

  let baseUrl = 'http://' + HOST + '/';
  let url = baseUrl + apiMethod + '?' + serialize(params);

  return fetch(url).then((response) => response.json());
}

function apiDDP(apiMethod, args) {
  console.log("API DDP", apiMethod, args)
  return ddp.callPromise(apiMethod, args);
}

function serialize(obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}


export function api(actionType, params, options) {
  let {makeRequest, mergeState, optimistic, publicRequest} = {
    makeRequest: true,
    mergeState: false,
    optimistic: false,
    publicRequest: false,
    ...options,
  };


  return (dispatch, getState) => {
    let reducerApis = API_REDUCERS_BY_ACTION_TYPE[actionType];
    let reducerName;
    let apiMethod;

    if(_.isObject(reducerApis)) {
      reducerName = reducerApis.reducer;
      apiMethod = reducerApis.api;
      actionType = reducerApis.action || actionType; //reducerApis even overwrite the action type
    }
    else {
      reducerName = apiMethod = reducerApis; //reducer and api method share the same name and reducerApis is a string
    }

    let state = getState();

    if(_.isObject(params)) delete params['']; //gifted form can give you an empty key for modal widgets without names
    //if(state[reducerName+'Fetching']) return; //don't double fetch

    if(mergeState) { //send current reducer state merged with new params in API request
      params = {...state[reducerName], ...params}
    }


    if(!makeRequest) { //wizards (checkout, celeb signup) store data along the way before making the final API request
      let action = requestResponse(actionType, params);
      dispatch(action);
      return Promise.resolve(); //return a promise so that `.then/catch` can be used as normal
    }
    else { //dispatch API request and set fetching flags
      if(optimistic) { //some api actions such as `submitCustomer/CelebrityConciergeMessage` need to display the data immediately
        let action = requestResponse(actionType, params);
        dispatch(action);
      }

      dispatch({type: actionType+'_FETCHING', payload: {apiMethod, ...params}});

      let fetchMethod = publicRequest ? apiFetch : apiDDP;

      return fetchMethod(apiMethod, [params, {ENVIRONMENT}])
        .then((res) => {
          let action = requestResponse(actionType, res);
          dispatch(action);
          return res; //return for use in `this.props.someAction().then((res) => ...)`
        })
        .catch((error) => {
          let action = requestError(actionType, error);
          dispatch(action);

          //throw an error object as our client would expect it, which is just the plain old message , rather than a Meteor.Error object
          let message = error.reason || error.message || error.error; //error.reason is a string that can be displayed to the user and error.error is a hyphenated error name. They are generated on the server in /server/api.js Meteor.methods by: `throw new Meteor.Error('foo-bar', 'Bad move buddy')`
          message = message.replace(/-/g, ' ').replace(/\[|\]/g, ''); //remove hyphens and brackets (this is only for some errors thrown by Meteor; not our throws, and even not some of Meteor's better throws)
          message = message.charAt(0).toUpperCase() + message.slice(1); //capitalize first letter

          throw new Error(message);  //`throw` returns values so they can be used in components via `this.props.someAction().catch()`
        });
    }
  };
}






/** API TO BE USED AS REDUX ACTIONS IN COMPONENTS, EG: `this.props.updateCelebrity(params)` **/

export function getCelebrities(params) {
  return api(types.CELEBRITIES, params, {publicRequest: true});
}
export function getCelebrityRequests(params) {
  return api(types.CELEBRITY_REQUESTS, params);
}
export function getCustomerOrders(params) {
  return api(types.CUSTOMER_ORDERS, params);
}


export function getCelebrity() {
  return api(types.CELEBRITY);
}
export function getCustomer() {
  return api(types.CUSTOMER);
}
export function getUser() {
  return (dispatch, getState) => {
    let isCelebrity = getState().isCelebrity;
    let action;

    if(isCelebrity) {
      dispatch({type: 'CLEAR_CELEBRITY'}); //this solves an obscure bug where if a user logs out of a celeb account that has is_also_customer === true, and then logs into a celeb account without that flag, the client will try to load the customer object from the server and fail. This is because the system merges the new celebrity reducer state with the old one. We could change that, but we need the merge state for any celeb updates, so we do this to continue keeping that simple
      action = getCelebrity();
    }
    else {
      dispatch({type: 'CLEAR_CUSTOMER'});
      action = getCustomer();
    }

    return dispatch(action);
  }
}


export function storeCelebrity(params) {
  return api(types.CELEBRITY, params, {makeRequest: false, mergeState: true});
}
export function updateCelebrity(params) {
  Mixpanel.registerSuperProperties(params);

  return (dispatch, getState) => {
    return dispatch(api(types.CELEBRITY, {...getState().celebrity, ...params}));
  };
}
export function updateCelebrityWithAllState() {
  return api(types.CELEBRITY, null, {mergeState: true});
}

export function updateCustomer(params) {
  Mixpanel.registerSuperProperties(params);
  return api(types.CUSTOMER, params);
}

export function storeCelebvidy(params) {
  return api(types.PURCHASE_CELEBVIDY, params, {makeRequest: false, mergeState: true});
}
export function purchaseCelebvidy() {
  return (dispatch, getState) => {
    storeCelebvidy({card: undefined, expiration: undefined, security_code: undefined});

    return dispatch(api(types.PURCHASE_CELEBVIDY, null, {mergeState: true}))
      .then(() => {
        let state = getState();

        if(state.celebrity && !state.customer && !state.customer._id) { //first order made by a celebrity requires us to load up their customer reducer store
          return dispatch(getCustomer());
        }
      });
  };
}
export function updateCelebvidy() {
  return api(types.UPDATE_CELEBVIDY, null, {mergeState: true});
}
export function addYoutubeVideo(params) {
  return api(types.ADD_YOUTUBE_VIDEO, params);
}
export function editOrder(params, options) {
  return api(types.EDIT_ORDER, params);
}
export function editRequest(params, options) {
  return api(types.EDIT_REQUEST, params, options);
}
export function deliverCelebvidy(params) {
  return api(types.DELIVER_CELEBVIDY, params);
}
export function rejectCelebvidy(params, options) {
  return api(types.REJECT_CELEBVIDY, params, options);
}



export function submitCelebrityConciergeMessage(params) {
  return apiPostOptimistic(types.SUBMIT_CELEBRITY_CONCIERGE_MESSAGE, params, {optimistic: true});
}
export function submitCustomerConciergeMessage(params) {
  return apiPostOptimistic(types.SUBMIT_CUSTOMER_CONCIERGE_MESSAGE, params, {optimistic: true});
}


export function getCelebrityConciergeMessages(params) {
  return api(types.CELEBRITY_CONCIERGE_MESSAGES, params);
}
export function getCustomerConciergeMessages(params) {
  return api(types.CUSTOMER_CONCIERGE_MESSAGES, params);
}






export function verifyCelebrityPin(params) {
  return api(types.CELEBRITY_PIN_VERIFICATION, params);
}

export function sendConfirmationEmailCode(email) {
  return api(types.CONFIRM_EMAIL_CODE, {email});
}
export function sendConfirmationEmailCodeForgotPassword(email) {
  return api(types.CONFIRM_EMAIL_CODE_FORGOT_PASSWORD, {email});
}
export function findForgotPasswordLoginTokenFromCode(email, code) {
  return api(types.FORGOT_PASSWORD_LOGIN_TOKEN, {email, code});
}
export function resetPassword(password) {
  return api(types.RESET_USER_PASSWORD, {password});
}


/** CUSTOM API REQUESTS/ACTIONS **/
export function loginWithToken() {
  return (dispatch, getState) => {
    let {user: {_id, token}} = getState();

    //the goal here is not to have millions of window-shoppers connecting to DDP websockets,
    //but rather only those that have signed up or are in the process of signing up.
    if(!token) {
      dispatch({type: types.LOGIN_FAILED, payload: 'noToken'}); //this is fine
      console.log('no token')
      return Promise.resolve(); //just resolve so our guest can continue
    }
    else {
      dispatch({type: types.LOGIN_FETCHING});

      return ddp.loginWithTokenPromise(token)
        .then((res) => completeLogin(dispatch, getState, res.token, {tokenLogin: true}))
        .then(() => {
          console.log('login successful');
          Mixpanel.identify(_id);
        })
        .catch((error) => handleLoginError(dispatch, error));
    }
  }
}


export function loginWithEmail(email, password) {
  return (dispatch, getState) => {
    dispatch({type: types.LOGIN_FETCHING});

    return ddp.loginWithEmailPromise(email, password)
      .then((res) => completeLogin(dispatch, getState, res.token))
      .catch((error) => handleLoginError(dispatch, error));
  }
}

export function loginWithTwitter(authData) {
  return (dispatch, getState) => {
    dispatch({type: types.LOGIN_FETCHING});

    //get the celebrity pin (known as agent_code on the server) from reducer state
    let params = {
      roles: ['celebrity'],
      agent_code: getState().celebrityPin
    };

    return ddp.loginWithTwitterPromise(authData, params)
      .then((res) => completeLogin(dispatch, getState, res.token))
      .catch((error) => handleLoginError(dispatch, error));
  }
}

//dont export this. in component code used the specialized versions below: `this.props.celebrity/customerSignupWithEmail()`
function signUpWithEmail(email, password, params) {
  return (dispatch, getState) => {
    dispatch({type: types.LOGIN_FETCHING});

    //get the celebrity pin (known as agent_code on the server) from reducer state
    if(params.roles[0] === 'celebrity') {
      params.agent_code = parseInt(getState().celebrityPin);
    }

    console.log('ACTION', email, password, params)

    return ddp.signUpWithEmailPromise(email, password, params)
      .then((res) => completeLogin(dispatch, getState, res.token))
      .catch((error) => handleLoginError(dispatch, error));
  }
}


export function changePassword(newPassword) {
  return (dispatch, getState) => {
    dispatch({type: types.LOGIN_FETCHING});

    let oldPassword = getState().forgotPasswordLoginTokenFromCode; //the naming is wrong, but basically we generate a temporary password rather than try to log the person with old login tokens (...cuz they wont work)

    return ddp.changePasswordPromise(oldPassword, newPassword)
      .then(() => dispatch({type: types.LOGIN_COMPLETE}))
      .catch((error) => handleLoginError(dispatch, error));
  }
}



function completeLogin(dispatch, getState, token, options) {
  console.log('completeLogin')

  return ddp.user()
    .then((user) => storeUserLoginInfo(dispatch, {...user, token})) //store user details in user reducer
    .then(() => storeCustomerOrCelebrity(dispatch))                 //retreive and store customer or celebrity details in customer or celebrity reducer (based on userTYpe discovered in storeUserLoginInfo)
    .then(() => {
      let state = getState();

      if(!(options && options.tokenLogin)) { //automatically refresh My Celebvidys if user automatically logged in NOT via token (so that ListViews are not empty) (i.e. for signup/login/forgotPassword -- the reason is because in these cases the ListView's already missed their opportunity on first load to make an ajax request for the data, cuz they wouldn't be able to get the data anyway until the user logs in)
        setTimeout(() => {
          if(state.isCelebrity) dispatch({type: 'REFRESH_CELEBRITY_REQUESTS'});
          else dispatch({type: 'REFRESH_CUSTOMER_ORDERS'});
        }, 3000);
      }

      GiftedFormManager.stores = {};

      if(state.isCelebrity && state.celebrity.is_also_customer) {
        return dispatch(getCustomer());
      }
      else return Promise.resolve();
    })
    .then(() => dispatch({type: types.LOGIN_COMPLETE}))             //turn off any loading indicators
    .then(() => getState);                                          //return getState function for used by component code if needed
}

function handleLoginError(dispatch, error) {
  dispatch({type: types.LOGIN_FAILED, payload: error, error: true});

  console.log('REQUEST ERROR', error);

  //throw an error object as our client would expect it, which is just the plain old message , rather than a Meteor.Error object
  let message = error.reason || error.message || error.error; //error.reason is a string that can be displayed to the user and error.error is a hyphenated error name. They are generated on the server in /server/api.js Meteor.methods by: `throw new Meteor.Error('foo-bar', 'Bad move buddy')`
  message = message.replace(/-/g, ' ').replace(/\[|\]/g, '');
  throw new Error(message);  //`throw` returns values so they can be used in components via `this.props.someAction().catch()`
}


function storeUserLoginInfo(dispatch, user) {
  return dispatch({type: types.USER_LOGIN_INFO, payload: user});
}

function storeCustomerOrCelebrity(dispatch) {
  return dispatch(getUser());
}


export function celebritySignupWithEmail(email, password) {
  return signUpWithEmail(email, password, {roles: ['celebrity']});
}
export function customerSignupWithEmail(email, password) {
  return signUpWithEmail(email, password, {roles: ['customer']});
}
