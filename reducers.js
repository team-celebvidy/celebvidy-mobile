import { combineReducers } from 'redux';
import * as types from './actionTypes';
import { LOAD, SAVE } from 'redux-storage';
import {allRoutesInNavigator} from './misc/testState';

import _ from 'underscore';

const COMPONENTS_BY_USER_TYPE_AND_PAGE = {
  celebrity: ['CelebrityOwnProfile', 'CelebrityRequests', 'Industries', 'Celebrities', 'Share'],
  customer: ['CustomerAccount', 'CustomerOrders', 'Industries', 'Celebrities', 'Share']
};

/**
celebrityUnlocked
**/



export function appUpToDate(state=true, action={}) {
  switch (action.type) {
    case 'UPDATE_INSTALLED':
      return false; //return false so on next open of the app, the app knows to not hide the splash screen (see MainContainer.js)
    case 'UP_TO_DATE':
      return true; //will be called on next open of the app, and codePush will dispatch this once ready
    default:
      return state;
  }
}



export function appStateLoaded(state=false, action={}) {
  switch (action.type) {
    case LOAD:
    case types.LOGOUT:
      return true;
    default:
      return state;
  }
}



export function page(state=0, action={}) {
  switch (action.type) {
    case types.GO_TO_PAGE:
      return action.payload.page;
    case types.HIDE_TOUR:
      return action.payload === 'customer' ? 2 : 0;
    case types.SET_DEVELOPER_STATE:
      return action.payload.page;
    default:
      return state;
  }
}



export function selectedComponent(state='AppIntroTour', action={}) {
  switch (action.type) {
    case LOAD:
      return state;
    case types.SELECT_COMPONENT:
      return action.payload;

    case types.GO_TO_PAGE:
      return COMPONENTS_BY_USER_TYPE_AND_PAGE[action.payload.userType][action.payload.page];
    case types.SHOW_DRAWER:
      return 'CelebrityAccount';
    case types.HIDE_DRAWER:
      return 'CelebrityOwnProfile';
    case types.SHOW_TOUR:
      return 'AppIntroTour';
    case types.HIDE_TOUR:
      return action.payload === 'customer' ? 'Industries' : 'CelebrityAccount';

    case types.SET_DEVELOPER_STATE:
      return action.payload.selectedComponent;
    default:
      return state;
  }
}



export function selectedRoute(state='AppIntroTour', action={}) {
  switch (action.type) {
    case LOAD:
      return state;
    case types.SELECT_ROUTE:
      return action.payload;

    case types.GO_TO_PAGE:
      return COMPONENTS_BY_USER_TYPE_AND_PAGE[action.payload.userType][action.payload.page];
    case types.SHOW_DRAWER:
      return 'CelebrityAccount';
    case types.HIDE_DRAWER:
      return 'CelebrityOwnProfile';
    case types.SHOW_TOUR:
      return 'AppIntroTour';
    case types.HIDE_TOUR:
      return action.payload === 'customer' ? 'Industries' : 'CelebrityAccount';

    case types.SET_DEVELOPER_STATE:
      return action.payload.selectedRoute || state;
    default:
      return state;
  }
}


export function isCelebrity(state=false, action={}) {
  switch (action.type) {
    case types.USER_LOGIN_INFO:
      return _.contains(action.payload.profile.roles, 'celebrity');
    case types.LOGOUT:
      return false;
    case types.SET_USER_TYPE_TO_CELEBRITY:
      return true;
    case types.SET_USER_TYPE_TO_CUSTOMER:
      return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.isCelebrity;
    default:
      return state;
  }
}


export function showTabBar(state=true, action={}) {
  switch (action.type) {
    case types.SHOW_TAB_BAR:
    case types.HIDE_TOUR: //insure tab bar is always up when you return from the tour
    case types.CLIP_TOUR:
    case types.SHOW_DRAWER:
    case types.HIDE_DRAWER:
    case 'PUSH_NOTIFICATION_OPEN_REQUEST':
      return true;
    case types.HIDE_TAB_BAR:
    case 'PUSH_NOTIFICATION_OPEN_ORDER':
      return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.showTabBar;
    default:
      return state;
  }
}

export function scrollEnabled(state=true, action={}) {
  switch (action.type) {
    case types.SHOW_TAB_BAR:
      return true;
    case types.HIDE_TAB_BAR:
      return false;
    case types.SHOW_DRAWER:
      return false;
    case types.HIDE_DRAWER:
      return true;
    case types.SET_DEVELOPER_STATE:
      return action.payload.scrollEnabled;
    default:
      return state;
  }
}


export function showStatusBar(state=true, action={}) {
  switch (action.type) {
    case types.SHOW_STATUS_BAR:
      return true;
    case types.HIDE_STATUS_BAR:
      return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.showStatusBar;
    default:
      return state;
  }
}


export function showDrawer(state=false, action={}) {
  switch (action.type) {
    case types.SHOW_DRAWER:
      return true;
    case types.HIDE_DRAWER:
    case 'ONLY_HIDE_DRAWER': //triggers no other reducers that would otherwise also respond to HIDE_DRAWER
      return false;
    case types.HIDE_TOUR:
      return action.payload === 'customer' ? false : state;
    case types.SET_USER_TYPE_TO_CUSTOMER:
      return false;
    case types.SET_USER_TYPE_TO_CELEBRITY:
      return true;
    case types.SET_DEVELOPER_STATE:
      return action.payload.showDrawer;
    default:
      return state;
  }
}



export function showOrders(state=false, action={}) {
  switch (action.type) {
    case 'SHOW_ORDERS':
      return true;
    case 'PUSH_NOTIFICATION_OPEN_ORDER':
      return action.payload.date;
    case 'HIDE_DRAWER':
    case 'ONLY_HIDE_DRAWER':
      return false;
    default:
      return state;
  }
}

export function showTour(state=true, action={}) {
  switch (action.type) {
    case types.LOGOUT:
      return true;
    case types.SHOW_TOUR:
      return true;
    case types.HIDE_TOUR:
      return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.showTour;
    default:
      return state;
  }
}

export function tourPage(state=0, action={}) {
  switch (action.type) {
    case types.SHOW_TOUR:
      return 0;
    case types.SET_TOUR_PAGE:
      return action.payload;
    default:
      return state;
  }
}

export function tourReady(state=false, action={}) {
  switch (action.type) {
    case types.SET_TOUR_READY:
      return true;
    case types.SET_TOUR_NOT_READY:
      return false;
    case types.LOGOUT:
      return false;
    case types.HIDE_TOUR:
      return false;
    case types.SHOW_TOUR:
      return false;
    case types.SET_USER_TYPE_TO_CUSTOMER:
    case types.SET_USER_TYPE_TO_CELEBRITY:
      return action.payload === 'tourReady' || state;
    case types.SET_DEVELOPER_STATE:
      return action.payload.tourReady;
    default:
      return state;
  }
}

export function tourAgain(state=false, action={}) {
  switch (action.type) {
    case types.LOGOUT:
      return false;
    case types.SHOW_TOUR:
      return action.payload === 'again';
    //case types.HIDE_TOUR:
      //return false;
    case types.SET_TOUR_AGAIN_TRUE:
      return true;
    case types.SET_TOUR_AGAIN_FALSE:
      return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.tourAgain;
    default:
      return state;
  }
}

export function finishLater(state=false, action={}) {
  switch (action.type) {
    case types.LOGOUT:
      return false;
    case types.HIDE_TOUR:
      return action.payload === 'finishLater';
    case types.SET_FINISH_LATER_TRUE:
      return true;
    case types.SET_FINISH_LATER_FALSE:
      return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.finishLater;
    default:
      return state;
  }
}

export function tourClipped(state=false, action={}) {
  switch (action.type) {
    case types.UNCLIP_TOUR:
      return false;
    case types.CLIP_TOUR:
      return true;
    case types.SET_DEVELOPER_STATE:
      return action.payload.tourClipped;
    default:
      return state;
  }
}

export function panelClipped(state=true, action={}) {
  switch (action.type) {
    case types.UNCLIP_PANEL:
      return false;
    case types.CLIP_PANEL:
      return true;
    //case types.LOGOUT:
      //return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.panelClipped;
    default:
      return state;
  }
}


let showIntroDefault = __DEV__ ? false : true;

export function showIntroAnimation(state=showIntroDefault, action={}) {
  switch (action.type) {
    case types.SHOW_INTRO_ANIMATION:
      return true;
    case types.HIDE_INTRO_ANIMATION:
      return false;
    case types.SET_DEVELOPER_STATE:
      return action.payload.showIntroAnimation;
    default:
      return state;
  }
}


export function tooltipStep(state=false, action={}) {
  switch (action.type) {
    case types.TOOLTIP_STEP:
      return action.payload;
    case types.TOOLTIP_STEPS_COMPLETE:
      return false;
    default:
      return state;
  }
}

/** API REDUCERS **/

export function user(state={loggedIn: false}, action={}) {
  switch (action.type) {
    case types.USER_LOGIN_INFO:
      return {
        ...action.payload, //login token will be in the payload
        loggedIn: true,
      };
    case types.LOGOUT:
      return {
        loggedIn: false,
      };
    case types.LOGIN_FAILED:
      if(action.payload) { //true login failure when token exists
        return {
          error: true,
          loggedIn: false,
          payload: action.payload,
          token: state.token,
        };
      }
      else { //login failure likely because the user hasn't signed up yet
        return {
          loggedIn: false,
          token: state.token, //but if it's some other type of error, let's insure the token is remembered in redux storage
        };
      }
    default:
      return state;
  }
}




export function celebrity(state={}, action={}) {
  switch (action.type) {
    case 'CLEAR_CELEBRITY':
      return {};
    case types.CELEBRITY:
      state = action.error ? {error: true} : state;

      return {
        ...state,
        ...action.payload,
      };
    case types.PURCHASE_CELEBVIDY:
      if(action.payload.celebrity_id === state._id && action.payload.completedOrder === true) {
        return {
          ...state,
          inventory: --state.inventory,
        };
      }
      else return state;
    default:
      return state;
  }
}
export function celebrityFetching(state=false, action={}) {
  switch (action.type) {
    case types.CELEBRITY_FETCHING:
      return action.payload === false ? false : true;
    case types.CELEBRITY:
      return false;
    default:
      return state;
  }
}

export function customer(state={}, action={}) {
  switch (action.type) {
    case 'CLEAR_CUSTOMER':
      return {};
    case types.CUSTOMER:
      state = action.error ? {error: true} : state;

      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}
export function customerFetching(state=false, action={}) {
  switch (action.type) {
    case types.CUSTOMER_FETCHING:
      return action.payload === false ? false : true;
    case types.CUSTOMER:
      return false;
    default:
      return state;
  }
}

export function currentCheckoutOrder(state={}, action={}) {
  switch (action.type) {
    case types.PURCHASE_CELEBVIDY:
      if(action.payload.completedOrder === true) {
        return {};
      }
      else return action.payload;
    default:
      return state;
  }
}
export function currentCheckoutOrderFetching(state=false, action={}) {
  switch (action.type) {
    case types.PURCHASE_CELEBVIDY_FETCHING:
      return action.payload === false ? false : true;
    case types.PURCHASE_CELEBVIDY:
      return false;
    default:
      return state;
  }
}

export function currentOrder(state={}, action={}) {
  switch (action.type) {
    case types.EDIT_ORDER:
      return action.payload;
    case 'CLEAR_CURRENT_ORDER':
      return {};
    default:
      return state;
  }
}
export function currentOrderFetching(state=false, action={}) {
  switch (action.type) {
    case types.EDIT_ORDER_FETCHING:
      return action.payload === false ? false : true;
    case types.EDIT_ORDER:
      return false;
    default:
      return state;
  }
}

export function currentRequest(state={}, action={}) {
  switch (action.type) {
    case 'SET_CURRENT_REQUEST':
      return action.payload;
    case types.EDIT_REQUEST:
      return {
        ...state,
        ...action.payload,
      };
    case 'CLEAR_CURRENT_REQUEST':
      return {};
    case types.REJECT_CELEBVIDY:
      return {
        ...state,
        ...action.payload,
        status: action.payload.status,
        reason: action.payload.reason,
      }
    case types.DELIVER_CELEBVIDY:
      return {
        ...state,
        sourceUri: action.payload.sourceUri,
        youtubeId: action.payload.youtubeId,
      };
    default:
      return state;
  }
}
export function currentRequestFetching(state=false, action={}) {
  switch (action.type) {
    case types.EDIT_REQUEST_FETCHING:
      return action.payload === false ? false : true;
    case types.EDIT_REQUEST:
      return false;
    default:
      return state;
  }
}

export function celebrityConciergeMessages(state=[], action={}) {
  switch (action.type) {
    case types.CELEBRITY_CONCIERGE_MESSAGES:
      return action.payload;
    case types.SUBMIT_CELEBRITY_CONCIERGE_MESSAGE:
      return state ? state.concat([action.payload]) : [action.payload];
    default:
      return state;
  }
}
export function celebrityConciergeMessagesFetching(state=false, action={}) {
  switch (action.type) {
    case types.CELEBRITY_CONCIERGE_MESSAGES_FETCHING:
      return action.payload === false ? false : true;
    case types.CELEBRITY_CONCIERGE_MESSAGES:
      return false;
    default:
      return state;
  }
}

export function customerConciergeMessages(state=[], action={}) {
  switch (action.type) {
    case types.CUSTOMER_CONCIERGE_MESSAGES:
      return action.payload;
    case types.SUBMIT_CUSTOMER_CONCIERGE_MESSAGE:
      return state ? state.concat([action.payload]) : [action.payload];
    default:
      return state;
  }
}
export function customerConciergeMessagesFetching(state=false, action={}) {
  switch (action.type) {
    case types.CUSTOMER_CONCIERGE_MESSAGES_FETCHING:
      return action.payload === false ? false : true;
    case types.CUSTOMER_CONCIERGE_MESSAGES:
      return false;
    default:
      return state;
  }
}


export function celebrities(state=[], action={}) {
  switch (action.type) {
    case types.CELEBRITIES:
      return action.payload.page === 1 ? action.payload.rows : state.concat(action.payload.rows);

    case types.CELEBRITY:
      return spliceArrayWithUpdate(state, action.payload);
    case types.PURCHASE_CELEBVIDY:
      if(action.payload.completedOrder === true) {
        return decrementPurchasedCelebrityIfinArray(state, action.payload);
      }
      else return state;

    default:
      return state;
  }
}
export function celebritiesFetching(state=false, action={}) {
  switch (action.type) {
    case types.CELEBRITIES_FETCHING:
      return action.payload === false ? false : true;
    case types.CELEBRITIES:
      return false;
    default:
      return state;
  }
}

export function celebrityRequests(state=[], action={}) {
  switch (action.type) {
    case types.CELEBRITY_REQUESTS:
      return action.payload.page === 1 ? action.payload.rows : state.concat(action.payload.rows);
    case types.EDIT_REQUEST:
    case types.REJECT_CELEBVIDY:
    case types.DELIVER_CELEBVIDY:
      return spliceArrayWithUpdate(state, action.payload);
    default:
      return state;
  }
}
export function celebrityRequestsFetching(state=false, action={}) {
  switch (action.type) {
    case types.CELEBRITY_REQUESTS_FETCHING:
      return action.payload === false ? false : true;
    case types.CELEBRITY_REQUESTS:
      return false;
    default:
      return state;
  }
}

export function customerOrders(state=[], action={}) {
  switch (action.type) {
    case types.CUSTOMER_ORDERS:
      return action.payload.page === 1 ? action.payload.rows : state.concat(action.payload.rows);
    case types.EDIT_ORDER:
    case types.REJECT_CELEBVIDY:
    case types.DELIVER_CELEBVIDY:
      return spliceArrayWithUpdate(state, action.payload);
    case types.PURCHASE_CELEBVIDY:
      return spliceArrayWithUpdateAndPossiblyPrepend(state, action.payload);
    default:
      return state;
  }
}
export function customerOrdersFetching(state=false, action={}) {
  switch (action.type) {
    case types.CUSTOMER_ORDERS_FETCHING:
      return action.payload === false ? false : true;
    case types.CUSTOMER_ORDERS:
      return false;
    default:
      return state;
  }
}


export function receivedCelebvidys(state=[], action={}) {
  switch (action.type) {
    case 'RECEIVED_CELEBVIDY':
      return [action.payload].concat(state);
    default:
      return state;
  }
}


export function deliveredCelebvidy(state={}, action={}) {
  switch (action.type) {
    case types.DELIVER_CELEBVIDY:
      return action.payload;
    default:
      return state;
  }
}
export function rejectedCelebvidy(state={}, action={}) {
  switch (action.type) {
    case types.REJECT_CELEBVIDY:
      return action.payload;
    default:
      return state;
  }
}
export function submittedCustomerConciergeMessage(state={}, action={}) {
  switch (action.type) {
    case types.SUBMIT_CUSTOMER_CONCIERGE_MESSAGE:
      return action.payload;
    default:
      return state;
  }
}
export function submittedCelebrityConciergeMessage(state={}, action={}) {
  switch (action.type) {
    case types.SUBMIT_CELEBRITY_CONCIERGE_MESSAGE:
      return action.payload;
    default:
      return state;
  }
}

export function celebrityPin(state=null, action={}) {
  switch (action.type) {
    case types.CELEBRITY_PIN_VERIFICATION:
      return action.payload;
    default:
      return state;
  }
}
export function celebrityPinFetching(state=false, action={}) {
  switch (action.type) {
    case types.CELEBRITY_PIN_VERIFICATION_FETCHING:
      return action.payload === false ? false : true;
    case types.CELEBRITY_PIN_VERIFICATION:
      return false;
    default:
      return state;
  }
}

export function loginFetching(state=false, action={}) {
  switch (action.type) {
    case types.LOGIN_FETCHING:
      return action.payload === false ? false : true;
    case types.LOGIN_COMPLETE:
    case types.LOGIN_FAILED:
      return false;
      return false;
    default:
      return state;
  }
}


export function appReady(state=false, action={}) {
  switch (action.type) {
    case types.LOGIN_COMPLETE:
      return 'loggedInUser'; //appRedy is used everywhere for its truthiness, so it won't break if we give it some extra info for places that would like to know if the app is ready because we have a signed up user
    case types.LOGIN_FAILED:
      return action.payload === 'noToken' ? 'noToken' : 'loginFailed';
    case types.LOGOUT:
      return 'logout';
    default:
      return state;
  }
}


export function lastLogin(state=null, action={}) {
  switch (action.type) {
    case 'LAST_LOGIN':
      return action.payload;
    default:
      return state;
  }
}


export function connected(state=false, action={}) {
  switch (action.type) {
    case types.CONNECT:
      return action.payload;
    default:
      return state;
  }
}


export function celebrityFromUrl(state={}, action={}) {
  switch(action.type) {
    case 'CELEBRITY_FROM_URL':
      return {
        ...action.payload,
      };
    default:
      return state;
  }
}

export function celebritiesFetchOptions(state={
  sort: 'popular',
  industry: 'All Celebrities',
  keywords: '',
}, action={}) {
  switch (action.type) {
    case types.SELECT_CELEBRITIES_TAB:
      return {
        ...state,
        sort: action.payload,
        keywords: '',
      };
    case types.SELECT_INDUSTRY:
      return {
        ...state,
        industry: action.payload,
        keywords: '',
      };
    case types.SEARCH_CELEBRITY:
      return {
        ...state,
        keywords: action.payload,
      };
    case 'REFRESH_CELEBRITIES':
      return {
        ...state, //for now used only to refresh the list by virtue of immutable object reference comparison
      };
    default:
      return state;
  }
}

export function customerOrdersFetchOptions(state={tab: 'orders'}, action={}) {
  switch (action.type) {
    case 'REFRESH_CUSTOMER_ORDERS':
      return {
        ...state, //for now used only to refresh the list by virtue of immutable object reference comparison
      };
    case 'PUSH_NOTIFICATION_OPEN_ORDER':
    if(new Date - appOpenedDate < 2000) return state; //HACK: it solves the problem of 2 spinners (refresh spinner, initial spinner) appearing at the top of Requests listview on first app open from a push notification

      return {
        ...state,
        tab: 'orders',
      }
    default:
      return state;
  }
}

export function pushedRequestId(state=null, action={}) {
    switch(action.type) {
      case 'PUSH_NOTIFICATION_OPEN_REQUEST':
        return action.payload._id || null;
      case 'OPENED_PUSH_NOTIFICATION_REQUEST':
        return null;
      default:
        return state;
    }
}

export function pushedOrderId(state=null, action={}) {
    switch(action.type) {
      case 'PUSH_NOTIFICATION_OPEN_ORDER':
        return action.payload._id || null;
      case 'OPENED_PUSH_NOTIFICATION_ORDER':
        return null;
      default:
        return state;
    }
}

const appOpenedDate = new Date;
export function requestsFetchOptions(state={
  status: 'new',
  kind: 'All Celebvidys',
}, action={}) {
  switch (action.type) {
    case types.SELECT_REQUESTS_TAB:
      return {
        ...state,
        status: action.payload,
      };
    case types.SELECT_KIND:
      return {
        ...state,
        kind: action.payload,
      };
    case 'REFRESH_CELEBRITY_REQUESTS':
      return {
        ...state
      };
    case 'PUSH_NOTIFICATION_OPEN_REQUEST':
      if(new Date - appOpenedDate < 2000) return state; //HACK: it solves the problem of 2 spinners (refresh spinner, initial spinner) appearing at the top of Requests listview on first app open from a push notification

      return {
        status: 'new',
        kind: 'All Celebvidys',
      };
    default:
      return state;
  }
}

export function selectedPayoutTypeTab(state=0, action={}) {
  switch (action.type) {
    case types.SELECT_PAYOUT_TYPE_TAB:
      return action.payload;
    default:
      return state;
  }
}


export function confirmEmailCode(state='', action={}) {
  switch (action.type) {
    case types.CONFIRM_EMAIL_CODE:
      return action.payload;
    default:
      return state;
  }
}
export function confirmEmailCodeFetching(state=false, action={}) {
  switch (action.type) {
    case types.CONFIRM_EMAIL_CODE_FETCHING:
      return action.payload === false ? false : true;
    case types.CONFIRM_EMAIL_CODE:
      return false;
    default:
      return state;
  }
}


export function showGratifyAnimation(state={
  count: 0,
  callback: null,
  caption: null,
  darkenBackground: false
}, action={}) {
  switch(action.type) {
    case 'GRATIFY_ANIMATION':
      return {
        count: state.count + 1,
        ...action.payload,
        caption: (action.payload && action.payload.caption) || null, //make sure to remove previous caption if one isn't provided so it's not displayed in animation
      }
    default:
      return state;
  }
}


export function confirmEmailCodeForgotPassword(state=null, action={}) {
  switch (action.type) {
    case types.CONFIRM_EMAIL_CODE_FORGOT_PASSWORD:
      return action.payload;
    default:
      return state;
  }
}
export function confirmEmailCodeForgotPasswordFetching(state=false, action={}) {
  switch (action.type) {
    case types.CONFIRM_EMAIL_CODE_FORGOT_PASSWORD_FETCHING:
      return action.payload === false ? false : true;
    case types.CONFIRM_EMAIL_CODE_FORGOT_PASSWORD:
      return false;
    default:
      return state;
  }
}

export function forgotPasswordLoginTokenFromCode(state='', action={}) {
  switch (action.type) {
    case types.FORGOT_PASSWORD_LOGIN_TOKEN:
      return action.payload;
    default:
      return state;
  }
}
export function forgotPasswordLoginTokenFromCodeFetching(state=false, action={}) {
  switch (action.type) {
    case types.FORGOT_PASSWORD_LOGIN_TOKEN_FETCHING:
      return action.payload === false ? false : true;
    case types.FORGOT_PASSWORD_LOGIN_TOKEN:
      return false;
    default:
      return state;
  }
}



function spliceArrayWithUpdate(models, newModel) {
  if(!newModel || !newModel._id) return models;

  return models.map((model) => {
    if(model._id === newModel._id) return {...model, ...newModel};
    else return model;
  });
}


function spliceArrayWithUpdateAndPossiblyPrepend(models, newModel) {
  if(!newModel || !newModel._id) return models;

  let found = false;

  models = models.map((model) => {
    if(model._id === newModel._id) {
      found = true;
      return {...model, ...newModel};
    }
    else return model;
  });

  if(!found) {
    return [newModel].concat(models);
  }
  else return models;
}


function decrementPurchasedCelebrityIfinArray(models, order) {
  return models.map((model) => {
    if(model._id === order.celebrity_id) {
      return {...model, inventory: --model.inventory};
    }
    else return model;
  });
}
